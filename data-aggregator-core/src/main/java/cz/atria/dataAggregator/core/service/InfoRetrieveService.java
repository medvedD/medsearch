package cz.atria.dataAggregator.core.service;

import java.net.URI;
import java.util.Collection;
import java.util.Map;
import java.util.SortedSet;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.Term;

/**
 * todo: добавить поиск по произвольному запросу (т.е. AND\OR\NOT, разные уровни вложенности, и т.д.).
 * todo: Использовать сразу фомарт lucene, или формировать свою структуру (граф?), которурую потмо конвертировать в lucene?
 *
 * @author rnuriev
 * @since 24.07.2015.
 */
public interface InfoRetrieveService {
    /**простой поиск: подразумевает AND условия для всех указанных term*/
    SortedSet<SearchResultEntry> retrieve(Collection<Source> sources, Collection<Term> terms);
    SortedSet<SearchResultEntry> retrieve(Collection<Source> sources, Object query);
    SortedSet<SearchResultEntry> retrieve(Collection<Source> sources, Map<String, Object> queries);
    /**простой поиск: подразумевает AND условия для всех указанных term*/
    SortedSet<SearchResultEntry> retrieve(Collection<Term> terms);
    SortedSet<SearchResultEntry> retrieve(Object query);
    SortedSet<SearchResultEntry> retrieve(Map<String, Object> queries);
    String highlight(Document document, String query);
    /**highlight by already instantiated query*/
    String highlight(Document document, Object query);
    String highlight(Integer documentId, Object query);
    String highlight(URI uri, Object query);
    Object createQuery(String queryAsString);
    Object createQuery(Collection<Term> terms);
}
