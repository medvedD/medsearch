package cz.atria.dataAggregator.core.service;

/**
 * @author rnuriev
 * @since 28.08.2015.
 */
public interface SmtpService {
    String EMAIL_FROM = "cz.atria.dataAggregator.core.email.from";
    String EMAIL_PSW = "cz.atria.dataAggregator.core.email.psw";
    String EMAIL_SMTP_AUTH = "cz.atria.dataAggregator.core.email.smtp.auth";
    String EMAIL_SMTP_STARTTLS_ENABLE = "cz.atria.dataAggregator.core.email.smtp.starttls.enable";
    String EMAIL_SMTP_HOST = "cz.atria.dataAggregator.core.email.smtp.host";
    String EMAIL_SMTP_PORT = "cz.atria.dataAggregator.core.email.smtp.port";
    void send(String to, String msg);
}
