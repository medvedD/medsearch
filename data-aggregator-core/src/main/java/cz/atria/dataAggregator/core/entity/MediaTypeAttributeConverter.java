package cz.atria.dataAggregator.core.entity;

import javax.persistence.AttributeConverter;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.MediaType;
/**
 * @author rnuriev
 * @since 30.07.2015.
 */
public class MediaTypeAttributeConverter implements AttributeConverter<MediaType, String> {
    @Override
    public String convertToDatabaseColumn(MediaType attribute) {
        if(attribute == null) {
            return null;
        } else {
            return attribute.toString();
        }
    }

    @Override
    public MediaType convertToEntityAttribute(String dbData) {
        if(StringUtils.isEmpty(dbData)) {
            return null;
        } else {
            return MediaType.parseMediaType(dbData);
        }
    }
}
