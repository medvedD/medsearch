package cz.atria.dataAggregator.core.convert;

import java.io.InputStream;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

/**
 * @author rnuriev
 * @since 14.09.2015.
 */
@Service
public class ConvertHTMLServiceImpl implements ConvertService {
    @Override
    public <T> InputStream convert(T content) {
        if (content instanceof InputStream) {
            return (InputStream) content;
        } else {
            // todo
            return null;
        }
    }

    @Override
    public MediaType getSupportedMediaType() {
        return MediaType.TEXT_HTML;
    }
}
