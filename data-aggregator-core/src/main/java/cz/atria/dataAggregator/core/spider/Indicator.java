package cz.atria.dataAggregator.core.spider;

/**
 * @author rnuriev
 * @since 05.11.2015.
 */
public interface Indicator {
    boolean isShuttingDown();
    boolean isFinished();
}
