package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
@Table
@Entity
@Deprecated
public class Term implements Serializable {
    @Id
    @GeneratedValue
    Integer id;

    /**actually term value, which used for search*/
    String name;

    @ManyToOne
    @JoinColumn (name = "vocabulary_id")
    Vocabulary vocabulary;

    @ManyToOne
    @JoinColumn (name = "parent_id")
    Term parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    public Term getParent() {
        return parent;
    }

    public void setParent(Term parent) {
        this.parent = parent;
    }

    @Transient
    public String getParentName() {
        return getParent() != null ? getParent().getName() : null;
    }
    @Transient
    public String getVocabularyName() {
        return getVocabulary() != null ? getVocabulary().getName() : null;
    }
}
