package cz.atria.dataAggregator.core.entity;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author rnuriev
 * @since 03.08.2015.
 */
public enum SourceType {
    URL, RSS, FILE, GOOGLE_ALERT;
    static Collection<String> valuesAsString = new HashSet<>();
    public static Collection<String> valuesAsString() {
        if(valuesAsString.isEmpty()) {
            for (SourceType sourceType : values()) {
                valuesAsString.add(sourceType.name());
            }
        }
        return valuesAsString;
    }
}
