package cz.atria.dataAggregator.core;

/**
 * @author rnuriev
 * @since 16.10.2015.
 */
public interface DependencyAccessorA {
    <T> void set(T t);

    <T> T get(Class<T> c);
}
