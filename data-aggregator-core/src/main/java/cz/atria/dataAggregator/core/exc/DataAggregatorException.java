package cz.atria.dataAggregator.core.exc;

/**
 * @author rnuriev
 * @since 03.09.2015.
 */
public class DataAggregatorException extends RuntimeException {
    Reason reason;

    public DataAggregatorException(Reason reason) {
        this.reason = reason;
    }
    public DataAggregatorException(Reason reason, String message) {
        super(message);
        this.reason = reason;
    }

    public DataAggregatorException(String message, Throwable cause, Reason reason) {
        super(message, cause);
        this.reason = reason;
    }

    public Reason getReason() {
        return reason;
    }

}
