package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author rnuriev
 * @since 12.08.2015.
 */
@Entity
@Table(name = "term_element_synonym")
public class TermElementSynonym implements Serializable {
    @Id
    @GeneratedValue
    Integer id;

    String name;

    @ManyToOne
    @JoinColumn (nullable = false)
    TermElement termElement;

    public TermElementSynonym() {
    }

    public TermElementSynonym(String name, TermElement termElement) {
        this.name = name;
        this.termElement = termElement;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TermElement getTermElement() {
        return termElement;
    }

    public void setTermElement(TermElement termElement) {
        this.termElement = termElement;
    }
}
