package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author rnuriev
 * @since 01.09.2015.
 */
@Entity
@Table (uniqueConstraints = @UniqueConstraint(name = Query.USER_NAME_UK_NAME, columnNames = {"user_id", "name"}))
public class Query implements Serializable {
    public static final String USER_NAME_UK_NAME = "query_name_user_uk";
    @Id
    @GeneratedValue
    Integer id;

    @Column (nullable = false)
    String name;

    @ManyToOne
    @JoinColumn (name = "user_id")
    User user;

    @ManyToMany
    @JoinTable (name = "query_source", joinColumns = @JoinColumn (name = "query_id"), inverseJoinColumns = @JoinColumn (name = "source_id"))
    List<Source> sources;

    Integer slop;

    public Query() {
    }

    public Integer getSlop() {
        return slop;
    }

    public void setSlop(Integer slop) {
        this.slop = slop;
    }

    public Query(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
