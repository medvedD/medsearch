package cz.atria.dataAggregator.core.spider.crawler4j;

import java.util.regex.Pattern;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author rnuriev
 * @since 29.07.2015.
 */
public class BasicCrawler extends WebCrawler {
    private static final Pattern EXCLUDE_EXTENSIONS = Pattern.compile(".*\\.(bmp|gif|jpg|png)$");


    public interface PageCallback {
        void process(String url, HtmlParseData htmlParseData);
    }


    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        if (EXCLUDE_EXTENSIONS.matcher(href).matches()) {
            return false;
        }
        // todo: m.b. check by domain?
        if (getMyController().getCustomData() != null && getMyController().getCustomData() instanceof CustomData) {
            logger.debug("shouldVisit:href={},baseUrl={}", href, ((CustomData)( getMyController().getCustomData())).getBaseUrl());
            return href.startsWith(((CustomData)( getMyController().getCustomData())).getBaseUrl());
        } else {
            return false;
        }
    }

    @Override
    public void visit(Page page) {
        final WebURL webURL = page.getWebURL();
        int docid = webURL.getDocid();
        String url = webURL.getURL();
        logger.debug("visit:url={}, Docid: {}, domain={}, ParentDocid={}, ParentUrl={}", url, docid, webURL.getDomain(), webURL.getParentDocid(), webURL.getParentUrl());
        logger.info("URL: {}", url);
        if (page.getParseData() instanceof HtmlParseData) {
            if (getMyController().getCustomData() != null && getMyController().getCustomData() instanceof CustomData) {
                CustomData customData = (CustomData)getMyController().getCustomData();
                logger.debug("visit:url={},baseUrl={}", url, customData.baseUrl);
                customData.getPageCallback().process(url, (HtmlParseData) page.getParseData());
            }
        }
    }
}
