package cz.atria.dataAggregator.core.gooalert;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author rnuriev
 * @since 06.08.2015.
 */
public interface GoogleAlertService<T> {
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    void synchronizeAlertList();
    List<T> getAlerts();
    List<T> getAlerts(String query, String delivery);
    T createAlert(String query, String language, String region, String deliveryTo);
    T getAlert(String id);
    void deleteAlert(String id);
}
