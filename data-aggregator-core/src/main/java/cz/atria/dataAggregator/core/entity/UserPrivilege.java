package cz.atria.dataAggregator.core.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author rnuriev
 * @since 07.08.2015.
 */
@Entity
@Table (name = "user_privilege")
public class UserPrivilege {
    /**
     * todo: m.b. it is not necessary
     */
    @Id
    @GeneratedValue
    Integer id;

    @ManyToOne(cascade = CascadeType.REMOVE)
    User user;
    @ManyToOne(cascade = CascadeType.REMOVE)
    Privilege privilege;
    @Column (name = "is_set")
    Boolean isSet;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsSet() {
        return isSet == null ? false : isSet;
    }

    public void setIsSet(Boolean isSet) {
        this.isSet = isSet;
    }

    public UserPrivilege() {
    }

    public UserPrivilege(User user, Privilege privilege) {
        this.user = user;
        this.privilege = privilege;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privilege privilege) {
        this.privilege = privilege;
    }

    @Transient
    public String getPrivilegeName() {
        return getPrivilege() != null ? getPrivilege().getName() : null;
    }
}
