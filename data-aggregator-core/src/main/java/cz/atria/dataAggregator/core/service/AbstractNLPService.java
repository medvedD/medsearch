package cz.atria.dataAggregator.core.service;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

import org.apache.commons.lang.StringUtils;

/**
 * @author rnuriev
 * @since 04.08.2015.
 */
public abstract class AbstractNLPService implements NLPService {
    public static boolean isItPhrase(String s) {
        if (StringUtils.isEmpty(s)) {
            return false;
        } else {
            return s.trim().contains(" ");  // todo: use regexp
        }
    }

    @Override
    public Map<Integer, Float> convertToTopMatched(Map<Integer, SortedMap<String, Float>> matchesAll) {
        Map<Integer, Float> matches = new HashMap<>();
        for (Map.Entry<Integer, SortedMap<String, Float>> matchEntry : matchesAll.entrySet()) {
            for (Map.Entry<String, Float> scoreEntry : matchEntry.getValue().entrySet()) {
                matches.put(matchEntry.getKey(), scoreEntry.getValue());    // assumed, that first record have best score
                break;
            }
        }
        return matches;
    }
}
