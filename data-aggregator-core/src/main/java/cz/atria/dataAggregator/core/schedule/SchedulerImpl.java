package cz.atria.dataAggregator.core.schedule;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.print.Doc;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.DateUtils;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.ContentInputStreamImpl;
import cz.atria.dataAggregator.core.DependencyAccessor;
import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.core.Settings;
import cz.atria.dataAggregator.core.charset.CharsetService;
import cz.atria.dataAggregator.core.db.DocumentSqlUpdate;
import cz.atria.dataAggregator.core.db.DocumentTitleSqlUpdate;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.rss.FeedCallback;
import cz.atria.dataAggregator.core.rss.RssService;
import cz.atria.dataAggregator.core.service.DocumentService;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.MediaTypeService;
import cz.atria.dataAggregator.core.service.NLPService;
import cz.atria.dataAggregator.core.service.SourceService;
import cz.atria.dataAggregator.core.spider.Indicator;
import cz.atria.dataAggregator.core.spider.SpiderService;

/**
 * @author rnuriev
 * @since 01.08.2015.
 */
public class SchedulerImpl implements Scheduler, DependencyAccessor {
    final Logger log = LoggerFactory.getLogger(getClass());
    @PersistenceContext
    EntityManager em;

    @Autowired
    NLPService nlpService;

    @Autowired
    DocumentService documentService;

    @Autowired
    EntityService entityService;

    @Override
    public void indexDocuments() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        final String tab = Common.getTabName(Document.class);
        try {
            List<Document> documents = em.createQuery("from Document where coalesce(isIndexed, false) = false", Document.class).getResultList();
            statData.setAll(documents.size());
            log.info("indexDocuments:document to index count:" + statData.getAll());
            for (Document document : documents) {
                try {
                    statData.current();
                    nlpService.indexThis(document.getId(), documentService.getContent(document.getCacheUri() != null ? document.getCacheUri() : document.getUri(), String.class));
                    document.setIsIndexed(true);
                    entityService.update(tab, "isIndexed", document.getId(), true);
                    statData.ok();
                } catch (Exception e) {
                    log.error("documentId=" + document.getId(), e);
                }
            }
        } finally {
            log.info("indexDocuments:complete; {}", statData);
            statDataMap.remove(Thread.currentThread());
        }
    }

    @Autowired
    MediaTypeService mediaTypeService;

    @Override
    public void cacheDocuments() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        try {
            List<Document> documents = em.createQuery("from Document where cacheUri is null", Document.class).getResultList();
            statData.setAll(documents.size());
            for (Document document : documents) {
                try {
                    statData.current();
                    if (document.getCacheUri() == null) {
                        String contentNormalized = null;
                        MediaType mediaType = document.getMediaType();
                        // todo: m.b. redefine mediaType explicitly
                        if (mediaType == null) {
                            mediaType = mediaTypeService.detectMediaType(document.getUri(), MediaType.class);
                        }
                        Charset charset = document.getCharset();
                        // todo: m.b. redefine charset explicitly
                        if (charset == null) {
                            charset = charsetService.detect(document.getUri());
                        }
                        if (mediaType != null) {
                            ContentInputStreamImpl contentObject = documentService.getContent(document.getUri(), ContentInputStreamImpl.class, charset);
                            InputStream contentAsStream = documentService.getContentExtracted(contentObject.getContent(), mediaType, charset);
                            contentNormalized = IOUtils.toString(contentAsStream);
                            URI cachedUri = documentService.cacheDocument(contentNormalized, charset);
                            document.setCacheUri(cachedUri);
                            document.setLastModified(contentObject.getLastModified());
                            document.setEntityTag(contentObject.getEntityTag());
                            documentSqlUpdate.execute(document);
                            log.debug("cached:uri={} to cacheUri={}", document.getUri(), cachedUri);
                        } else {
                            log.warn("can't determine mediaType for document: {}", document.getUri());
                        }
                    } else {
                        // todo: m.b. add file existence
                    }
                    statData.ok();
                } catch (Exception e) {
                    log.error("doc=" + Common.reflectionToString(document, "doc.id=" + document.getId()), e);
                }
            }
        } finally {
            log.info("cacheDocuments:complete; {}", statData);
            statDataMap.remove(Thread.currentThread());
        }
    }

    @Autowired
    SourceService sourceService;

    @Autowired
    RssService rssService;

    @Autowired
    CharsetService charsetService;

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public void readRss() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        try {
            List<Source> sourcesByMediaType = sourceService.getSourcesByMediaType(MediaType.APPLICATION_ATOM_XML, cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_RSS_XML);
            statData.setAll(sourcesByMediaType.size());
            for (final Source source : sourcesByMediaType) {
                try {
                    statData.current();
                    rssService.process(source, new FeedCallback() {
                        @Override
                        public void process(String link) {
                            try {
                                Document document = documentService.getDocument(link);
                                URI cacheUri = null;
                                URI uri = URI.create(link);
                                Charset charset = charsetService.detect(uri);
                                MediaType mediaType = document != null && document.getMediaType() != null ? document.getMediaType() : MediaType.TEXT_HTML;
                                if (document == null || document.getCacheUri() == null) {
                                    if (charset != null) {
                                        String content = documentService.getContentExtracted(uri, String.class, mediaType, charset);
                                        cacheUri = documentService.cacheDocument(content, charset);
                                    } else {
                                        String content = documentService.getContentExtracted(uri, String.class, mediaType);
                                        cacheUri = documentService.cacheDocument(content);
                                    }
                                }
                                if (document == null) {
                                    documentService.createDocument(source, URI.create(link), cacheUri, null, charset);  // todo: add title
                                } else {
                                    documentService.updateDocument(document, source, cacheUri);
                                }
                            } catch (Exception e) {
                                log.error("link=" + link, e);
                            }
                        }
                    }, true);
                    statData.ok();
                } catch (Exception e) {
                    log.warn("source=" + ToStringBuilder.reflectionToString(source), e);
                }
            }
        } finally {
            log.info("readRss:complete; {}", statData);
            statDataMap.remove(Thread.currentThread());
        }
    }

    @Override
    public void clearIndex() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        final String tab = Common.getTabName(Doc.class);
        try {
            List<Document> documents = entityService.get(Document.class);
            statData.setAll(documents.size());
            for (Document document : documents) {
                statData.current();
                nlpService.removeIndexForDocument(document.getId());
                document.setIsIndexed(false);
                entityService.update(tab, "isIndexed", document.getId(), false);
                statData.ok();
            }
        } finally {
            log.info("clearIndex:complete");
            statDataMap.remove(Thread.currentThread());
        }
    }

    @Override
    public void removeOrphanUploadedFiles() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        try {
            String uploadDirName = SettingService.getSetting(Settings.UPLOAD_DIR_PROP, String.class, Settings.DEFAULT_UPLOAD_DIR);
            File uploadDir = new File(uploadDirName);
            if (uploadDir.exists()) {
                if (uploadDir.isDirectory()) {
                    File[] files = uploadDir.listFiles();
                    statData.setAll(files.length);
                    for (File uploadedFile : files) {
                        statData.current();
                        try {
                            // we use source searching by explicitly defining pre- and sufix, due to URI representations issue (multiple slashes or single slash)
                            // todo: use URI equality for Source search, if possible
                            List<Source> sources = sourceService.getSourcesByUriPrefixAndSuffix("file", uploadedFile.toURI().getPath());
                            if (sources.isEmpty()) {
                                uploadedFile.delete();
                                log.debug("uploaded file deleted:{}", uploadedFile.getPath());
                            }
                            statData.ok();
                        } catch (Exception e) {
                            log.error("file=" + uploadedFile.getPath(), e);
                        }
                    }
                } else {
                    log.warn("directory expected:{}", uploadDirName);
                }
            } else {
                log.warn("uploaded dir not exists:{}", uploadDirName);
            }
        } catch (Exception e) {
            log.error("", e);
        } finally {
            log.info("removeOrphanUploadedFiles:complete");
            statDataMap.remove(Thread.currentThread());
        }
    }

    @Autowired
    DocumentTitleSqlUpdate documentTitleSqlUpdate;

    @Override
    public void setDocumentsTitle() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        try {
            // todo: set titles explicitly, if possible. For example, for rss.
            List<Document> documentsWithEmptyTitle = entityService.getDocumentsWithEmptyTitle();
            statData.setAll(documentsWithEmptyTitle.size());
            for (Document document : documentsWithEmptyTitle) {
                try {
                    statData.current();
                    String content = documentService.getContent(document.getUri(), String.class);
                    String title = Jsoup.parse(content).title();
                    if (StringUtils.isNotEmpty(title)) {
                        documentTitleSqlUpdate.execute(title, document.getId());
                    }
                    statData.ok();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        } catch (Exception e) {
            log.error("", e);
        } finally {
            log.info("setDocumentsTitle:complete; {}", statData);
            statDataMap.remove(Thread.currentThread());
        }
    }

    public void setMediaTypeService(MediaTypeService mediaTypeService) {
        this.mediaTypeService = mediaTypeService;
    }

    public MediaTypeService getMediaTypeService() {
        return mediaTypeService;
    }

    @Override
    public void detectMediaType() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        final String tab = Common.getTabName(Document.class);
        try {
            List<Document> documentsWithUndefinedMediaType = entityService.getDocumentsWithUndefinedMediaType();
            statData.setAll(documentsWithUndefinedMediaType.size());
            for (Document document : documentsWithUndefinedMediaType) {
                try {
                    statData.current();
                    MediaType mediaType = mediaTypeService.detectMediaType(document.getUri(), MediaType.class);
                    if (mediaType != null) {
                        document.setMediaType(mediaType);
                        entityService.update(tab, "mediaType", document.getId(), mediaType.toString());
                        statData.ok();
                    }
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        } finally {
            log.info("detectMediaType:complete; {}", statData);
            statDataMap.remove(Thread.currentThread());
        }
    }

    /**
     * todo: add support for HTTP cache facilities (like Expired, no-cache, max-age etc headers)
     */
    @Override
    public void updateDocuments() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        int all = 0, modified = 0, failed = 0;
        try {
            List<Document> docs = documentService.getDocumentsToUpdateCandidates();
            all = docs.size();
            statData.setAll(all);
            for (Document doc : docs) {
                log.trace("{}:doc.id={},doc.uri={}", methName, doc.getId(), doc.getUri());
                statData.setCurrent(statData.getCurrent() + 1);
                try {
                    if (updateDocument(doc)) {
                        modified++;
                        try {
                            Thread.sleep(SettingService.getSetting(Settings.POLITENESS_DELAY, Integer.class, 3000));    // todo: m.b. use delay determined by robots.txt
                        } catch (InterruptedException e) {
                            log.warn("", e);
                        }
                    }
                    statData.ok();
                } catch (Exception e) {
                    failed++;
                    log.error("", e);
                }
            }
        } finally {
            log.info("updateDocuments:complete:all={} processed (modified)={} failed={}", all, modified, failed);
            statDataMap.remove(Thread.currentThread());
        }
    }

    @Autowired
    SpiderService spiderService;

    /**
     * todo: add support for HTTP cache facilities (like Expired, no-cache, max-age etc headers)
     * todo: m.b. use thread pool
     */
    @Override
    public Collection<Indicator> updateSources() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        int all = 0;
        try {
            Collection<Indicator> indicators = new ArrayList<>();
            List<Source> sources = sourceService.getHtmlSourcesToUpdateCandidates();
            all = sources.size();
            statData.setAll(all);
            for (final Source source : sources) {
                statData.current();
                Indicator indicator = updateSource(source);
                if (indicator != null) {
                    indicators.add(indicator);
                    statData.ok();
                }
            }
            return indicators;
        } finally {
            log.info("updateSources:complete:all={}", all);
            statDataMap.remove(Thread.currentThread());
        }
    }

    @Override
    public Indicator updateSource(Integer sourceId) {
        Source source = entityService.get(Source.class, sourceId);
        return updateSource(source);
    }

    protected boolean updateDocument(Document doc) throws IOException {
        HttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(doc.getUri());
        request.setHeader("User-Agent", "Mozilla/5.0");
        request.setHeader("Accept", "text/html,application/xhtml+xml");
        if (StringUtils.isNotEmpty(doc.getEntityTag())) {
            request.setHeader(HttpHeaders.IF_NONE_MATCH, doc.getEntityTag());
        }
        if (doc.getLastModified() != null) {
            request.setHeader(HttpHeaders.IF_MODIFIED_SINCE, org.apache.http.client.utils.DateUtils.formatDate(doc.getLastModified()));
        }
        HttpResponse response = client.execute(request);
        log.trace("updateDocument:doc.uri={},status={}", doc.getUri(), response.getStatusLine().getStatusCode());
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_MODIFIED) {
            return false;
        } else {
            // todo: how about moving (30x responses)
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            Charset charset = null;
            MediaType mediaType = null;
            if (entity.getContentType() != null && StringUtils.isNotEmpty(entity.getContentType().getValue())) {
                charset = Common.extractCharset(entity.getContentType().getValue());
                if (charset == null) {
                    charset = doc.getCharset();
                }
                mediaType = Common.extractMediaType(entity.getContentType().getValue());
            } else {
                log.warn("cant detect charset and mediaType. Previous used. Doc=" + Common.reflectionToString(doc, "doc.uri=" + doc.getUri()));
                charset = doc.getCharset();
                mediaType = doc.getMediaType();
            }
            URI cacheUri = null;
            InputStream contentExtracted;
            if (charset != null) {
                contentExtracted = documentService.getContentExtracted(content, mediaType, charset);
                cacheUri = documentService.cacheDocument(contentExtracted, charset);
            } else {
                contentExtracted = documentService.getContentExtracted(content, mediaType);
                cacheUri = documentService.cacheDocument(contentExtracted);
            }
            nlpService.removeIndexForDocument(doc.getId());
            final Header eTag = response.getFirstHeader(HttpHeaders.ETAG);
            if (eTag != null && StringUtils.isNotEmpty(eTag.getValue())) {
                doc.setEntityTag(eTag.getValue());
            } else {
                doc.setEntityTag(null);
            }
            final Header lastModifiedHeader = response.getFirstHeader(HttpHeaders.LAST_MODIFIED);
            if (lastModifiedHeader != null && StringUtils.isNotEmpty(lastModifiedHeader.getValue())) {
                doc.setLastModified(DateUtils.parseDate(lastModifiedHeader.getValue()));
            } else {
                doc.setLastModified(null);
            }
            doc.setMediaType(mediaType);
            doc.setCharset(charset);
            final URI previousCacheUri = doc.getCacheUri();
            doc.setCacheUri(cacheUri);
            documentSqlUpdate.execute(doc);
            nlpService.indexThis(doc.getId());
            if (previousCacheUri != null) {
                try {
                    if (previousCacheUri.getScheme().equals("file")) {
                        File file = new File(previousCacheUri);
                        file.delete();
                    }
                } catch (Exception e) {
                    log.warn("previousCacheUri=" + previousCacheUri, e);
                }
            }
            return true;
        }
    }

    @Autowired
    DocumentSqlUpdate documentSqlUpdate;

    @Override
    public void updateDocument(Integer documentId) throws IOException {
        Document doc = entityService.getA(Document.class, documentId);
        updateDocument(doc);
    }

    protected Indicator updateSource(Source source) {
        Indicator indicator = null;
        log.trace("{}:source.uri={}", Common.getMethodName(), source.getUri());
        try {
            indicator = spiderService.visit(source);
            while (!indicator.isFinished() && !indicator.isShuttingDown()) {
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            log.error("source=" + Common.reflectionToString(source), e);
        }
        return indicator;
    }

    public class StatData {
        String name;
        int all;
        int current;
        int ok;
        final Date created = new Date();
        public int getOk() {
            return ok;
        }

        public void ok() {
            ok++;
        }

        public void current() {
            current++;
        }

        public void setOk(int ok) {
            this.ok = ok;
        }

        public StatData(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAll() {
            return all;
        }

        public void setAll(int all) {
            this.all = all;
        }

        public int getCurrent() {
            return current;
        }

        public void setCurrent(int current) {
            this.current = current;
        }

        @Override
        public String toString() {
            return "{" +
                    "name='" + name + '\'' +
                    ", created=" + created +
                    ", all=" + all +
                    ", current=" + current +
                    ", ok=" + ok +
                    '}';
        }
    }

    static Map<Thread, StatData> statDataMap = new HashMap<>();

    @Override
    public String stat() {
        String res = "";
        for (Map.Entry<Thread, StatData> threadStatDataEntry : statDataMap.entrySet()) {
            res += threadStatDataEntry.getKey() + ":" + threadStatDataEntry.toString() + "\n";
        }
        log.info(res);
        return res;
    }

    @Autowired
    DataSource dataSource;

    @Override
    public void removeOrphanedCaches() {
        final String methName = Common.getMethodName();
        log.info(methName + ":begin");
        final StatData statData = new StatData(methName);
        statDataMap.put(Thread.currentThread(), statData);
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement("select cacheUri from document where cacheUri like '%/'||?");
            File dir = new File(SettingService.getSetting(Settings.CACHE_DIR, String.class, Settings.CACHE_DIR_DEFAULT));
            File[] files = dir.listFiles();
            for (File file : files) {
                try {
                    statData.current();
                    final String name = file.getName();
                    ps.setString(1, name);
                    ResultSet rs = ps.executeQuery();
                    if (!rs.next()) {
                        file.delete();
                        statData.ok();
                    }
                    rs.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        } catch (SQLException e) {
            log.error("", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    log.warn("", e);
                }
            }
            log.info("{}:complete; {}", methName, statData);
            statDataMap.remove(Thread.currentThread());
        }
    }
}
