package cz.atria.dataAggregator.core.spider;

import cz.atria.dataAggregator.core.entity.Source;

/**
 * @author rnuriev
 * @since 29.07.2015.
 */
public interface SpiderService {
    Indicator visit(Source source);
}
