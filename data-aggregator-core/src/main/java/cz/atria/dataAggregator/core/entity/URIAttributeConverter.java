package cz.atria.dataAggregator.core.entity;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rnuriev
 * @since 27.07.2015.
 */
public class URIAttributeConverter implements AttributeConverter<URI, String> {
    public static String defaultCharset = "UTF-8";
    static Logger log = LoggerFactory.getLogger(URIAttributeConverter.class);
    @Override
    public String convertToDatabaseColumn(URI attribute) {
        if(attribute == null) {
            return null;
        } else {
            try {
                return URLDecoder.decode(attribute.toString(), defaultCharset);
            } catch (UnsupportedEncodingException e) {
                log.warn("", e);
                return attribute.toString();
            }
        }
    }

    @Override
    public URI convertToEntityAttribute(String dbData) {
        if(dbData == null || dbData.isEmpty()) {
            return null;
        } else {
            try {
                return URI.create(dbData.replaceAll(" ", "%20"));
            } catch (Exception e) {
                log.warn("", e);
                return null;
            }
        }
    }
}
