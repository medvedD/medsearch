package cz.atria.dataAggregator.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author rnuriev
 * @since 14.10.2015.
 */
public class SpringDBHelper {
    final static ApplicationContext contextDb = new ClassPathXmlApplicationContext("cz/atria/dataAggregator/core/data-aggregator-db-context.xml");
    public static <T> T getBean(Class<T> clazz) {
        return contextDb.getBean(clazz);
    }
}
