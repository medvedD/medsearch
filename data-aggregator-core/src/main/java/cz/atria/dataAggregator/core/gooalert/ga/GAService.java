package cz.atria.dataAggregator.core.gooalert.ga;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
//import org.nnh.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rnuriev
 * @since 06.08.2015.
 */
public class GAService {
    Logger log = LoggerFactory.getLogger(getClass());
    private String _email = "";
    private String _pwd = "";
    private String _alert_user_id = "";
    private Map<String, String> mapAlertId = new HashMap();
    private String cookies = "";
    private HttpClient client = new DefaultHttpClient();
    private static final String HEADER_USER_AGENT = "Mozilla/5.0";
    private static final String HEADER_ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
    private static final String HEADER_ACCEPT_LANGUAGE = "en-US,en;q=0.5";
    private static final String HEADER_CONTENT_TYPE = "application/x-www-form-urlencoded";

    public GAService(String email, String pwd) throws Exception {
//        if (StringUtils.isValidEmailAddress(email) && !StringUtils.isEmpty(pwd)) {
//            this._email = email;
//            this._pwd = pwd;
//        } else {
//            throw new Exception("Please use email and password correctly!");
//        }
    }

    public boolean doLogin() {
        boolean ret = false;

//        try {
//            String loginPage = this.getPageContent("https://accounts.google.com/ServiceLoginAuth");
//            List postParams = this.getLoginFormParams(loginPage, this._email, this._pwd);
//            String html = this.postData("https://accounts.google.com/ServiceLoginAuth", postParams);
//            Document doc = Jsoup.parse(html);
//            Elements els = doc.select("span[id^=errormsg]");
//            if (els != null && els.size() > 0) {
//                return false;
//            }
//
//            html = this.postData("https://www.google.com/alerts", (List) null);
//            doc = Jsoup.parse(html);
//            Elements scripts = doc.select("script");
//            Iterator var9 = scripts.iterator();
//
//            while (var9.hasNext()) {
//                Element script = (Element) var9.next();
//                if (script.html().startsWith("window.STATE")) {
//                    String data = script.html();
//                    data = data.substring(data.indexOf(" = ") + 3, data.length() - 1);
//                    JSONParser parser = new JSONParser();
//                    JSONArray json = (JSONArray) parser.parse(data);
//                    if (json.get(1) != null) {
//                        JSONArray array = (JSONArray) ((JSONArray) json.get(1)).get(1);
//                        if (array != null && array.size() > 0) {
//                            Iterator var15 = array.iterator();
//
//                            while (var15.hasNext()) {
//                                Object obj = var15.next();
//                                JSONArray anArray = (JSONArray) obj;
//                                this.mapAlertId.put((String) anArray.get(1), (String) ((JSONArray) ((JSONArray) ((JSONArray) anArray.get(2)).get(6)).get(0)).get(11));
//                            }
//                        }
//                    }
//
//                    this._alert_user_id = (String) json.get(3);
//                    ret = true;
//                    break;
//                }
//            }
//        } catch (Exception var17) {
//            log.error("", var17);
//        }

        return ret;
    }

    public String createAlert(Alert alert) {
        String id = "";
        if (alert != null) {
            try {
                ArrayList ex = new ArrayList();
                ex.add(new BasicNameValuePair("params", this.buildParamValue(alert, false)));
                id = this.parseData(this.postData("https://www.google.com/alerts/create?", ex));
            } catch (Exception var4) {
                id = var4.getMessage();
            }
        }

        return id;
    }

    public String updateAlert(Alert alert) {
        String id = "";
//        if (alert != null && !StringUtils.isEmpty(alert.getId())) {
//            try {
//                ArrayList ex = new ArrayList();
//                ex.add(new BasicNameValuePair("params", this.buildParamValue(alert, true)));
//                id = this.parseData(this.postData("https://www.google.com/alerts/modify?", ex));
//            } catch (Exception var4) {
//                id = var4.getMessage();
//            }
//        }

        return id;
    }

    public List<Alert> getAlerts() {
        return this.getAlerts("", "", "");
    }

    public List<Alert> getAlerts(String searchQuery) {
        return this.getAlerts(searchQuery, "", "");
    }

    public Alert getAlertById(String alertId) {
        List lst = this.getAlerts("", alertId, "");
        return lst != null && lst.size() > 0 ? (Alert) lst.get(0) : null;
    }

    public List<Alert> getAlertByDelivery(String deliveryBy) {
        return this.getAlerts("", "", deliveryBy);
    }

    private List<Alert> getAlerts(String searchQuery, String alertId, String deliveryBy) {
        ArrayList lst = new ArrayList();
//
//        try {
//            String html = this.postData("https://www.google.com/alerts", (List) null);
//            Document doc = Jsoup.parse(html);
//            Elements scripts = doc.select("script");
//            Iterator var9 = scripts.iterator();
//
//            while (var9.hasNext()) {
//                Element script = (Element) var9.next();
//                if (script.html().startsWith("window.STATE")) {
//                    String data = script.html();
//                    data = data.substring(data.indexOf(" = ") + 3, data.length() - 1);
//                    JSONParser parser = new JSONParser();
//                    JSONArray json = (JSONArray) parser.parse(data);
//                    if (json.get(1) != null) {
//                        JSONArray array = (JSONArray) ((JSONArray) json.get(1)).get(1);
//                        if (array != null && array.size() > 0) {
//                            Iterator var15 = array.iterator();
//
//                            while (true) {
//                                Alert alert;
//                                JSONArray part;
//                                String var24;
//                                int i1 = 0;
//                                do {
//                                    JSONArray anArray;
//                                    String feedUrl;
//                                    do {
//                                        do {
//                                            if (!var15.hasNext()) {
//                                                return lst;
//                                            }
//
//                                            Object obj = var15.next();
//                                            alert = new Alert();
//                                            anArray = (JSONArray) obj;
//                                            if (i1 == 1) {
//                                                try {
//                                                    alert.setEditKey((((JSONArray) ((JSONArray) ((JSONArray) (anArray).get(2)).get(6)).get(0)).get(14)).toString());
//                                                } catch (Exception e) {
//                                                    log.warn("array=" + array.toString(), e);
//                                                }
//                                            }
//                                            i1++;
//                                        }
//                                        while (!StringUtils.isEmpty(alertId) && !alertId.equalsIgnoreCase((String) anArray.get(1)));
//
//                                        feedUrl = "https://www.google.com/alerts/feeds/" + anArray.get(3);
//                                        alert.setId((String) anArray.get(1));
//                                        anArray = (JSONArray) anArray.get(2);
//                                        alert.setHowMany(((Long) anArray.get(5)).toString());
//                                        part = (JSONArray) anArray.get(3);
//                                    }
//                                    while (!StringUtils.isEmpty(searchQuery) && ((String) part.get(1)).indexOf(searchQuery) < 0);
//
//                                    alert.setSearchQuery(StringEscapeUtils.unescapeHtml3((String) part.get(1)));
//                                    // this is empirically finding. Language marker doesn't actually reflect language setting
//                                    boolean isAnyLanguage = part.size() == 9;
//                                    part = (JSONArray) part.get(3);
//                                    if (isAnyLanguage) {
//                                        alert.setLanguage("");
//                                    } else {
//                                        alert.setLanguage((String) part.get(1));
//                                    }
//                                    alert.setRegion((String) part.get(2));
//                                    String[] sources = new String[]{""};
//                                    if (anArray.get(4) != null) {
//                                        JSONArray feedKey = (JSONArray) anArray.get(4);
//                                        sources = new String[feedKey.size()];
//
//                                        for (int i = 0; i < feedKey.size(); ++i) {
//                                            sources[i] = feedKey.get(i).toString();
//                                        }
//                                    }
//
//                                    alert.setSources(sources);
//                                    part = (JSONArray) ((JSONArray) anArray.get(6)).get(0);
//                                    var24 = ((Long) part.get(1)).toString();
//                                    feedUrl = feedUrl + "/" + part.get(11);
//                                    if ("1".equalsIgnoreCase(var24)) {
//                                        alert.setDeliveryTo((String) part.get(2));
//                                        var24 = "EMAIL";
//                                    } else {
//                                        alert.setDeliveryTo(feedUrl);
//                                        var24 = "feed";
//                                    }
//                                } while (!StringUtils.isEmpty(deliveryBy) && !deliveryBy.equalsIgnoreCase(var24));
//
//                                alert.setHowOften(((Long) part.get(4)).toString());
//                                lst.add(alert);
//                            }
//                        }
//                    }
//                    break;
//                }
//            }
//        } catch (Exception var23) {
//            log.error("", var23);
//        }

        return lst;
    }

    public boolean deleteAlert(String alertId) {
//        if (!StringUtils.isEmpty(alertId)) {
//            try {
//                ArrayList e = new ArrayList();
//                e.add(new BasicNameValuePair("params", "[null,\"" + alertId + "\"]"));
//                this.postData("https://www.google.com/alerts/delete?", e);
//                return true;
//            } catch (Exception var3) {
//                return false;
//            }
//        } else {
//            return false;
//        }
        return false;
    }

    public boolean deleteAlert(List<String> lstAlertId) {
        if (lstAlertId != null && !lstAlertId.isEmpty()) {
            Iterator var3 = lstAlertId.iterator();

            while (var3.hasNext()) {
                String alertId = (String) var3.next();
                this.deleteAlert(alertId);
            }

            return true;
        } else {
            return false;
        }
    }

    private String buildParamValue(Alert alert, boolean isEdit) {
//        String domainExt = "com";
//        String feedKey = "1";
//        String email = this._email;
//        if ("feed".equalsIgnoreCase(alert.getDeliveryTo())) {
//            feedKey = "2";
//            alert.setHowOften("1");
//            email = "";
//        }
//
//        if (!StringUtils.isEmpty(email)) {
//            domainExt = email.substring(email.lastIndexOf(".") + 1);
//        }
//
//        String sources = Arrays.toString(alert.getsources());
//        if ("[]".equalsIgnoreCase(sources)) {
//            sources = "null";
//        }
//
//        String howOften = "";
//        if ("1".equalsIgnoreCase(alert.getHowOften())) {
//            howOften = "[]," + alert.getHowOften();
//        } else if ("2".equalsIgnoreCase(alert.getHowOften())) {
//            howOften = "[null,null,3]," + alert.getHowOften();
//        } else if ("3".equalsIgnoreCase(alert.getHowOften())) {
//            howOften = "[null,null,3,3]," + alert.getHowOften();
//        }
//
//        String update = "";
//        if (isEdit) {
//            update = "\"" + alert.getId() + "\",";
//        }
//
//        String flagLanguage = "1";
//        if (alert.getLanguage() == "") {
//            alert.setLanguage("en");
//            flagLanguage = "0";
//        }
//
//        String flagRegion = "1";
//        if ("".equalsIgnoreCase(alert.getRegion())) {
//            alert.setRegion("US");
//            flagRegion = "0";
//        }
//
//        String searchQuery = alert.getSearchQuery();
//        if (alert.getSearchQuery().contains("\"")) {
//            searchQuery = searchQuery.replace("\"", "\\\"");
//        }
//
//        return "[null," + update + "[null,null,null,[null,\"" + searchQuery + "\",\"" + domainExt + "\",[null,\"" + alert.getLanguage() + "\",\"" + alert.getRegion() + "\"],null,null,null," + flagRegion + "," + flagLanguage + "]," + sources + "," + alert.getHowMany() + ",[[null," + feedKey + ",\"" + email + "\"," + howOften + ",\"en-US\"," + (isEdit ? "1" : "null") + ",null,null,null,null,\"" + (isEdit ? (String) this.mapAlertId.get(alert.getId()) : "0") + "\"]]]]";
            return "";
    }

    private String postData(String url, List<NameValuePair> postParams) throws ClientProtocolException, IOException {
        if (postParams != null && postParams.size() == 1 && "params".equalsIgnoreCase(((NameValuePair) postParams.get(0)).getName())) {
            url = url + "x=" + this._alert_user_id;
        }

        HttpPost post = new HttpPost(url);
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        if (postParams != null && postParams.size() > 0) {
            post.setEntity(new UrlEncodedFormEntity(postParams, "UTF-8"));
        }

        HttpResponse response = this.client.execute(post);
        HttpEntity entity = response.getEntity();
        return EntityUtils.toString(entity, "UTF-8");
    }

    private String getPageContent(String url) throws Exception {
        HttpGet request = new HttpGet(url);
        request.setHeader("User-Agent", "Mozilla/5.0");
        request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        request.setHeader("Accept-Language", "en-US,en;q=0.5");
        HttpResponse response = this.client.execute(request);
        HttpEntity entity = response.getEntity();
        this.setCookies(response.getFirstHeader("Set-Cookie") == null ? "" : response.getFirstHeader("Set-Cookie").toString());
        return EntityUtils.toString(entity, "UTF-8");
    }

    private List<NameValuePair> getLoginFormParams(String html, String username, String password) throws UnsupportedEncodingException {
        Document doc = Jsoup.parse(html);
        Element loginform = doc.getElementById("gaia_loginform");
        Elements inputElements = loginform.getElementsByTag("input");
        ArrayList paramList = new ArrayList();

        String key;
        String value;
        for (Iterator var9 = inputElements.iterator(); var9.hasNext(); paramList.add(new BasicNameValuePair(key, value))) {
            Element inputElement = (Element) var9.next();
            key = inputElement.attr("name");
            value = inputElement.attr("value");
            if (key.equals("Email")) {
                value = username;
            } else if (key.equals("Passwd")) {
                value = password;
            }
        }

        return paramList;
    }

    private String parseData(String data) {
        String id = "";

        try {
            JSONParser parser = new JSONParser();
            JSONArray json = (JSONArray) parser.parse(data);
            json = (JSONArray) ((JSONArray) json.get(4)).get(0);
            id = (String) json.get(1);
            this.mapAlertId.put(id, (String) ((JSONArray) ((JSONArray) ((JSONArray) json.get(3)).get(6)).get(0)).get(11));
        } catch (Exception var5) {
            ;
        }

        return id;
    }

    private String getCookies() {
        return this.cookies;
    }

    private void setCookies(String cookies) {
        this.cookies = cookies;
    }

    private static class GOOGLE {
        private static final String GOOGLE_URL = "https://www.google.com";
        private static final String LOGIN_URL = "https://accounts.google.com/ServiceLoginAuth";
        private static final String ALERT_URL = "https://www.google.com/alerts";
        private static final String CREATE_ALERT_URL = "https://www.google.com/alerts/create?";
        private static final String EDIT_ALERT_URL = "https://www.google.com/alerts/modify?";
        private static final String DELETE_ALERT_URL = "https://www.google.com/alerts/delete?";

        private GOOGLE() {
        }

        private static class LOGIN_FORM {
            private static final String FORM_ID = "gaia_loginform";
            private static final String EMAIL = "Email";
            private static final String PASSWORD = "Passwd";

            private LOGIN_FORM() {
            }
        }
    }
}
