package cz.atria.dataAggregator.core;

import org.springframework.http.MediaType;

/**
 * @author rnuriev
 * @since 14.09.2015.
 */
public enum Format {
    PDF(cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_PDF, "pdf"),
    DOC(cz.atria.dataAggregator.core.entity.MediaType.OOXML_DOCUMENT, "docx"),
    HTML(cz.atria.dataAggregator.core.entity.MediaType.TEXT_HTML, "html");
    MediaType mediaType;
    String extension;

    Format(MediaType mediaType, String extension) {
        this.mediaType = mediaType;
        this.extension = extension;
    }

    Format(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getExtension() {
        return extension;
    }

    public MediaType getMediaType() {
        return mediaType;
    }
}
