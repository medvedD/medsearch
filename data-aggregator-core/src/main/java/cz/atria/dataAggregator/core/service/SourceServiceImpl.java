package cz.atria.dataAggregator.core.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.SourceDetail;
import cz.atria.dataAggregator.core.entity.SourceType;

/**
 * @author rnuriev
 * @since 06.08.2015.
 */
@Service
public class SourceServiceImpl implements SourceService {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    @PersistenceContext
    EntityManager em;

    @Override
    @Transactional
    public void createSource(String name, URI uri, SourceType sourceType, MediaType mediaType) {
        Source source = new Source(name, uri, mediaType, sourceType);
        em.persist(source);
    }

    @Override
    @Transactional
    public void createSource(String name, URI uri, SourceType sourceType) {
        createSource(name, uri, sourceType, null);
    }

    @Override
    @Transactional
    public void createSourceWithDetail(String name, URI uri, SourceType sourceType, URI resourceUri, MediaType resourceMediaType) {
        createSourceWithDetail(name, uri, sourceType, resourceUri, resourceMediaType, null);
    }

    @Override
    public void createSourceWithDetail(String name, URI uri, SourceType sourceType, URI resourceUri, MediaType resourceMediaType, String externalId) {
        SourceDetail sourceDetail = new SourceDetail(name, uri, null, sourceType, resourceUri, resourceMediaType, externalId);
        em.persist(sourceDetail);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public List<SourceDetail> getSourceDetailsByType(SourceType sourceType) {
        return em.createQuery("from SourceDetail where type = :type", SourceDetail.class).setParameter("type", sourceType).getResultList();
    }

    @Autowired
    EntityService entityService;

    @Override
    @Transactional
    public void removeSource(Source source) {
        entityService.remove(source);
    }

    @Override
    public List<Source> getSourcesByMediaType(MediaType... mediaTypes) {
        List<SourceDetail> sourceDetails = em.createQuery("from SourceDetail where resourceMediaType in (:mediaTypes)", SourceDetail.class).setParameter("mediaTypes", Arrays.asList(mediaTypes)).getResultList();
        List<Integer> sourceDetailIds = new ArrayList<>();
        for (SourceDetail sourceDetail : sourceDetails) {
            sourceDetailIds.add(sourceDetail.getId());
        }
        TypedQuery<Source> sourceQuery = em.createQuery(String.format("from Source where mediaType in (:mediaTypes)%1$s", sourceDetailIds.isEmpty() ? "" : " and id not in (:ids)"), Source.class)
                .setParameter("mediaTypes", Arrays.asList(mediaTypes));
        if (!sourceDetailIds.isEmpty()) {
            sourceQuery = sourceQuery.setParameter("ids", sourceDetailIds);
        }
        List<Source> sources = sourceQuery.getResultList();
        List<Source> res = new ArrayList<>(sources);
        for (SourceDetail sourceDetail : sourceDetails) {
            res.add(sourceDetail);
        }
        return res;
    }

    @Override
    public Source getSource(URI uri) {
        try {
            return em.createQuery("from Source where uri = :uri", Source.class).setParameter("uri", uri).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Source> getSourcesByUriPrefixAndSuffix(String prefix, String suffix) {
        return em.createQuery("from Source where uri like '%'||:suffix and uri like :prefix||'%'", Source.class)
                .setParameter("prefix", prefix)
                .setParameter("suffix", suffix)
                .getResultList();
    }

    @Override
    public List<Source> getHtmlSourcesToUpdateCandidates() {
        return em.createQuery(
                "from Source where uri like 'http%' and (mediaType is null or mediaType in (:mediaTypes))" +
                " and (type is null or type in (:types))", Source.class)
                .setParameter("mediaTypes", Arrays.asList(MediaType.TEXT_HTML, MediaType.APPLICATION_XHTML_XML))
                .setParameter("types", Arrays.asList(SourceType.URL))
                .getResultList();
    }
}
