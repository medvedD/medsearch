package cz.atria.dataAggregator.core.spider.crawler4j;

import java.io.File;
import java.net.URI;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.atria.dataAggregator.core.charset.CharsetService;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.service.DocumentService;
import cz.atria.dataAggregator.core.spider.Indicator;
import cz.atria.dataAggregator.core.spider.SpiderService;
import edu.uci.ics.crawler4j.parser.HtmlParseData;

/**
 * Crawler4j specific impl
 *
 * @author rnuriev
 * @since 29.07.2015.
 */
@Service
public class SpiderServiceCrawler4jImpl implements SpiderService {
    final Logger log = LoggerFactory.getLogger(getClass());
    /**
     * todo: customize
     */
    String folder = "data";

    public SpiderServiceCrawler4jImpl() {
        try {
            File dir = new File(folder);
            if (!dir.exists()) {
                dir.mkdirs();
            }
        } catch (Exception e) {
            log.warn("", e);
        }
    }

    @Autowired
    DocumentService documentService;

    @Autowired
    CrawlController crawlController;

    @Autowired
    CharsetService charsetService;

    @Override
    public Indicator visit(final Source source) {
        final edu.uci.ics.crawler4j.crawler.CrawlController crawlController0 = crawlController.process(folder, source.getUri(), new BasicCrawler.PageCallback() {
            @Override
            public void process(String url, HtmlParseData htmlParseData) {
                try {
                    log.trace("crawler:source.url={},doc.url={}", source.getUri(), url);
                    URI docUri = URI.create(url);
                    Document document = documentService.getDocumentA(docUri);
                    // todo: use meta tags for charset and mediaType detection
                    Charset charset = null;
                    URI cacheUri = null;
                    if (document != null) {
                        charset = document.getCharset();
                        cacheUri = document.getCacheUri();
                    }
                    if (charset == null) {
                        charset = charsetService.detect(docUri);
                    }
                    if (cacheUri == null) {
                        cacheUri = documentService.cacheDocument(htmlParseData.getText());
                    }
                    if (document == null) {
                        documentService.createDocumentA(source, docUri, cacheUri, htmlParseData.getTitle(), charset);
                    } else {
                        // todo: m.b. simple update is necessary
                        documentService.createDocumentA(source, document, cacheUri, htmlParseData.getTitle(), charset);
                    }
                } catch (Exception e1) {
                    log.error("", e1);
                }
            }
        });
        return new Indicator() {
            @Override
            public boolean isShuttingDown() {
                return crawlController0.isShuttingDown();
            }

            @Override
            public boolean isFinished() {
                return crawlController0.isFinished();
            }
        };
    }
}
