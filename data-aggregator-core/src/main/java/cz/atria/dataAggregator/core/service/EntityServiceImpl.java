package cz.atria.dataAggregator.core.service;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Privilege;
import cz.atria.dataAggregator.core.entity.Query;
import cz.atria.dataAggregator.core.entity.QueryTermEntry;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.Synonym;
import cz.atria.dataAggregator.core.entity.Term;
import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.entity.TermElementSynonym;
import cz.atria.dataAggregator.core.entity.User;
import cz.atria.dataAggregator.core.entity.UserPrivilege;

/**
 * @author rnuriev
 * @since 27.07.2015.
 */
@Service
public class EntityServiceImpl implements EntityService {
    @PersistenceContext
    EntityManager em;

    @Override
    public <T> T get(Class<T> entityClass, Object id) {
        return em.find(entityClass, id);
    }

    @Override
    @Transactional (readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public <T> List<T> get(Class<T> entityClass, Collection ids) {
        return em.createQuery(String.format("from %1$s where id in (:ids)", Common.getEntityName(entityClass)), entityClass).setParameter("ids", ids).getResultList();
    }

    @Override
    @Transactional (readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public <T> T getA(Class<T> entityClass, Object id) {
        return get(entityClass, id);
    }

    @Override
    public <T> List<T> get(Class<T> entityClass) {
        return em.createQuery("from " + entityClass.getSimpleName(), entityClass).getResultList();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public List<Document> getDocuments(Source source) {
        if (em.contains(source)) {
            return source.getDocuments();
        } else {
            return getDocuments(source.getId());
        }
    }

    @Override
    public List<Document> getDocuments(Integer sourceId) {
        Object result = em.createQuery("select s.documents from Source s where s.id = :sourceId").setParameter("sourceId", sourceId).getResultList();
        return (List<Document>) result;
    }

    @Override
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public List<Source> getSources(Integer documentId) {
        List resultList = em.createQuery("select d.sources from Document d where d.id = :documentId").setParameter("documentId", documentId).getResultList();
        return resultList;
    }

    @Override
    public List<Synonym> getSynonyms(Term term) {
        return em.createQuery("from Synonym where term.id = :termId", Synonym.class).setParameter("termId", term.getId()).getResultList();
    }

    @Override
    public List<Term> getChildren(Term term) {
        return em.createQuery("from Term where parent.id = :parentId", Term.class).setParameter("parentId", term.getId()).getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void save(Object o) {
        em.merge(o);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void persist(Object o) {
        em.persist(o);
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRED)
    public void persistExtTx(Object o) {
        em.persist(o);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void remove(Object o) {
        if (!em.contains(o)) {
            o = em.merge(o);    // todo: em.find seems more appropriate ?
        }
        em.remove(o);
    }

    @Override
    public UserPrivilege getUserPrivilege(User user, Privilege privilege) {
        try {
            return em.createQuery("from UserPrivilege where user.id = :userId and privilege.id = :privilegeId", UserPrivilege.class).setParameter("userId", user.getId()).setParameter("privilegeId", privilege.getId()).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<UserPrivilege> getUserPrivilegesOrdered(User user) {
        return em.createQuery("from UserPrivilege where user.id = :userId order by privilege.id asc", UserPrivilege.class).setParameter("userId", user.getId()).getResultList();
    }

    @Override
    public List<Privilege> getPrivilegesOrdered() {
        return em.createQuery("from Privilege order by id asc", Privilege.class).getResultList();
    }

    @Override
    public List<TermElement> getChildren(TermElement termElement) {
        return em.createQuery("from TermElement where parent.id = :parentId", TermElement.class).setParameter("parentId", termElement.getId()).getResultList();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public User getUser(String login) {
        try {
            return em.createQuery("from User where lower(login) = lower(:login)", User.class).setParameter("login", login).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public List<Query> getQueries(String login) {
        if (StringUtils.isEmpty(login)) {
            return em.createQuery("from Query where user is null order by name", Query.class).getResultList();
        } else {
            User user = getUser(login);
            return em.createQuery("from Query where user is not null and user.id = :userId order by name", Query.class)
                    .setParameter("userId", user.getId())
                    .getResultList();
        }
    }

    @Override
    public Query getQueryByName(User user, String name) {
        try {
            if (user == null) {
                return em.createQuery("from Query where user is null and lower(name) = lower(:name)", Query.class).setParameter("name", name).getSingleResult();
            } else {
                return em.createQuery("from Query where user is not null and user.id = :userId and lower(name) = lower(:name)", Query.class)
                        .setParameter("name", name)
                        .setParameter("userId", user.getId())
                        .getSingleResult();
            }
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public List<Query> getQueriesByName(String name) {
        return em.createQuery("from Query where user is null and lower(name) = lower(:name)", Query.class).setParameter("name", name).getResultList();
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void saveQueryConditions(Query query, List<Source> sources, List<QueryTermEntry> queryTermEntries) {
        if (!em.contains(query)) {
            query = em.find(Query.class, query.getId());
            for (QueryTermEntry queryTermEntry : queryTermEntries) {
                queryTermEntry.setQuery(query);
            }
        }
        query.setSources(sources);
        for (QueryTermEntry queryTermEntry : getQueryTermEntriesOrdered(query)) {
            em.remove(queryTermEntry);
        }
        for (QueryTermEntry queryTermEntry : queryTermEntries) {
            em.persist(queryTermEntry);
        }
    }

    @Override
    public List<Source> getSources(Query query) {
        return em.createQuery("select q.sources from Query q where q.id = :queryId").setParameter("queryId", query.getId()).getResultList();
    }

    @Override
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public List<QueryTermEntry> getQueryTermEntriesOrdered(Query query) {
        return em.createQuery("from QueryTermEntry where query.id = :queryId order by pos asc", QueryTermEntry.class).setParameter("queryId", query.getId()).getResultList();
    }

    @Override
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    public List<TermElementSynonym> getTermElementSynonyms(TermElement termElement) {
        return em.createQuery("from TermElementSynonym where termElement.id = :termElementId", TermElementSynonym.class).setParameter("termElementId", termElement.getId()).getResultList();
    }

    @Override
    public List<Document> getDocumentsWithEmptyTitle() {
        return em.createQuery("from Document where title is null or length(title) = 0", Document.class).getResultList();
    }

    @Override
    public List<Document> getDocumentsWithUndefinedMediaType() {
        return em.createQuery("from Document where mediaType is null or length(mediaType) = 0", Document.class).getResultList();
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void update(String tab, String col, Integer id, Object val) {
        jdbcTemplate.update(String.format("update %1$s set %2$s = ? where id = ?", tab, col), val, id);
    }

}
