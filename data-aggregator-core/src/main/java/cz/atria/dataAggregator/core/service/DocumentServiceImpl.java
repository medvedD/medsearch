package cz.atria.dataAggregator.core.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.ToTextContentHandler;
import org.apache.tika.sax.ToXMLContentHandler;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.AbstractContent;
import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.DetectHtml;
import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.core.Settings;
import cz.atria.dataAggregator.core.charset.CharsetService;
import cz.atria.dataAggregator.core.db.DocumentJdbcInsert;
import cz.atria.dataAggregator.core.db.DocumentSqlUpdate;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.SourceType;
import cz.atria.dataAggregator.core.lng.Language;
import cz.atria.dataAggregator.core.lng.LanguageService;
import cz.atria.dataAggregator.core.rss.FeedCallback;
import cz.atria.dataAggregator.core.rss.RssService;
import cz.atria.dataAggregator.core.spider.SpiderService;

/**
 * @author rnuriev
 * @since 27.07.2015.
 */
@Service
public class DocumentServiceImpl implements DocumentService {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    @PersistenceContext
    EntityManager em;
    @Autowired
    CharsetService charsetService;

    @Override
    public <T> T getContent(URI uri, Class<T> asClass) {
        Charset charset = charsetService.detect(uri);   // todo: it's ineffective => so, open resource once
        return getContent(uri, asClass, charset);
    }

    /**
     * todo: process 403 responses, for, e.g. http://ru.stackoverflow.com/questions/439986/spring-restful-service
     */
    @Override
    public <T> T getContent(URI uri, Class<T> asClass, Charset charset) {
        T res = null;
        try {
            uri = Common.normilize(uri);
            URL url = uri.toURL();
            if (InputStream.class.isAssignableFrom(asClass)) {
                res = (T) url.openStream();
            } else if (String.class.isAssignableFrom(asClass)) {
                if (charset != null) {
                    res = (T) IOUtils.toString(Common.get(uri), charset);
                } else {
                    res = (T) IOUtils.toString(Common.get(uri), "utf-8");   // todo: m.b. customize default charset
                }
            } else if (AbstractContent.class.isAssignableFrom(asClass)) {
                res = Common.get(uri, asClass);
            }
        } catch (Exception e) {
            log.error("", e);
        }
        return res;
    }

    @Override
    public <T> T getContentExtracted(URI uri, Class<T> asClass, MediaType mediaType, Charset charset) {
        T contentAsIs = getContent(uri, asClass, charset);
        return getContentExtracted(contentAsIs, mediaType, charset);
    }

    @Override
    public <T> T getContentExtracted(T contentAsIs, MediaType mediaType, Charset charset) {
        if (charset == null) {
            charset = Charset.forName("UTF-8"); // todo: customize
        }
        Class asClass = contentAsIs.getClass();
        T contentExtracted = contentAsIs;
        if (mediaType.equals(MediaType.TEXT_HTML)) {
            org.jsoup.nodes.Document document = null;
            if (String.class.isAssignableFrom(asClass)) {
                document = Jsoup.parse((String) contentAsIs);
            } else if (InputStream.class.isAssignableFrom(asClass)) {
                try {
                    document = Jsoup.parse((InputStream) contentAsIs, charset.name(), "");
                } catch (IOException e) {
                    log.error("", e);
                }
            } else {
                log.warn("type not supported yet:{}" + asClass.getCanonicalName());
            }
            if (document != null) {
                String contentExtractedAsString = document.body().text();
                if (String.class.isAssignableFrom(asClass)) {
                    contentExtracted = (T) contentExtractedAsString;
                } else if (InputStream.class.isAssignableFrom(asClass)) {
                    contentExtracted = (T) IOUtils.toInputStream(contentExtractedAsString, charset);
                } else {
                    log.warn("not yet support type:" + asClass);
                }
            } else {
                log.warn("can't obtain document object. Use content as is.");
            }
        } else {
            ToTextContentHandler handler = new ToTextContentHandler();
            try {
//                InputStream stream = Common.normilize(uri).toURL().openStream();
                AutoDetectParser parser = new AutoDetectParser();
                Metadata metadata = new Metadata();
                try {
                    if (contentAsIs instanceof InputStream) {
                        parser.parse((InputStream) contentAsIs, handler, metadata);
                    } else if (contentAsIs instanceof String) {
                        parser.parse(IOUtils.toInputStream((String) contentAsIs), handler, metadata);
                    }
                    if (String.class.isAssignableFrom(asClass)) {
                        contentExtracted = (T) handler.toString();
                    } else if (InputStream.class.isAssignableFrom(asClass)) {
                        contentExtracted = (T) IOUtils.toInputStream(handler.toString());
                    } else {
                        log.warn("not yet support type:" + asClass);
                    }
                } catch (Exception e) {
                    log.error("", e);
                }
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return contentExtracted;
    }

    @Override
    public <T> T getContentExtracted(T content, MediaType mediaType) {
        T contentCopy = content;
        Charset charset = null;
        if (content instanceof InputStream) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                IOUtils.copy((InputStream) content, bos);
                contentCopy = (T) new ByteArrayInputStream(bos.toByteArray());
                InputStream contentForCharsetDetection = new ByteArrayInputStream(bos.toByteArray());
                charset = charsetService.detect(contentForCharsetDetection);
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return getContentExtracted(contentCopy, mediaType, charset);
    }

    @Override
    public <T> T getContentExtracted(URI uri, Class<T> asClass, MediaType mediaType) {
        Charset charset = charsetService.detect(uri);   // todo: it's ineffective => so, open resource once
        return getContentExtracted(uri, asClass, mediaType, charset);
    }

    @Autowired
    EntityService entityService;

    @Autowired
    NLPService nlpService;

    @Override
    public <T> T getContent(Document document, Class<T> asClass) {
        URI uri = document.getCacheUri() != null ? document.getCacheUri() : document.getUri();
        T content = null;
        if (document.getCharset() != null) {
            content = getContent(uri, asClass, document.getCharset());
        } else {
            content = getContent(uri, asClass);
        }
        return content;
    }

    final Analyzer defaultAnalyzer = new EnglishAnalyzer();

    @Override
    public Analyzer getAnalyzer(Document document) {
        return defaultAnalyzer; // todo: get analyzer by language
    }

    @Autowired
    SpiderService spiderService;

    @Autowired
    MediaTypeService mediaTypeService;

    @Autowired
    RssService rssService;

    /**
     * todo: m.b. exclude document creation here, due to schedulers already do it. Or delegate task to schedulers immediately, from here
     */
    @Override
    @Transactional
    public void createDocumentBySource(final Source source) {
        MediaType mediaType = source.getMediaType();
        if (mediaType == null) {
            mediaType = mediaTypeService.detectMediaType(source.getUri(), MediaType.class);
        }
        if (mediaType != null) {
            if (mediaType.equals(cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_RSS_XML) || mediaType.equals(MediaType.APPLICATION_ATOM_XML)) {
                rssService.process(source, new FeedCallback() {
                    @Override
                    public void process(String link) {
                        Document document = getDocument(link);
                        URI cacheUri = null;
                        final MediaType docMediaType = document != null && document.getMediaType() != null ? document.getMediaType() : MediaType.TEXT_HTML;
                        if (document == null || document.getCacheUri() == null) {
                            URI uri = URI.create(link);
                            Charset charset = charsetService.detect(uri);
                            if (charset != null) {
                                String content = getContentExtracted(uri, String.class, docMediaType, charset);
                                cacheUri = cacheDocument(content, charset);
                            } else {
                                String content = getContentExtracted(uri, String.class, docMediaType);
                                cacheUri = cacheDocument(content);
                            }
                        }
                        if (document == null) {
                            createDocument(source, URI.create(link), cacheUri);
                        } else {
                            updateDocument(document, source, cacheUri);
                        }
                    }
                });
            } else if (mediaType.equals(MediaType.TEXT_HTML)) {
                // todo: м.б. добавить признак - искать вложенные документы
                Charset charset = charsetService.detect(source.getUri());
                String content = null;
                URI cacheUri = null;
                org.jsoup.nodes.Document document;
                if (charset != null) {
                    content = getContent(source.getUri(), String.class, charset);
                    document = Jsoup.parse(content);
                    cacheUri = cacheDocument(document.body().text(), charset);
                    createDocument(source, source.getUri(), cacheUri, document.title(), charset);
                } else {
                    content = getContent(source.getUri(), String.class);
                    document = Jsoup.parse(content);
                    cacheUri = cacheDocument(document.body().text());
                    createDocument(source, source.getUri(), cacheUri, document.title());
                }
                spiderService.visit(source);
            } else {
                ToXMLContentHandler handler = new ToXMLContentHandler();
                try {
                    InputStream stream = Common.normilize(source.getUri()).toURL().openStream();
                    AutoDetectParser parser = new AutoDetectParser();
                    Metadata metadata = new Metadata();
                    try {
                        parser.parse(stream, handler, metadata);
                        String title = metadata.get("title");
                        if (StringUtils.isEmpty(title)) {
                            String uriAsString = source.getUri().toString();
                            title = Common.decodeSpace(uriAsString.substring(uriAsString.lastIndexOf('/') + 1));
                        }
                        String charsetName = metadata.get("Content-Encoding");
                        if (StringUtils.isEmpty(charsetName)) {
                            String contentType = metadata.get("Content-Type");
                            if (StringUtils.isNotEmpty(contentType)) {
                                String charsetParamEntry = "charset=";
                                if (contentType.contains(charsetParamEntry)) {
                                    charsetName = contentType.substring(contentType.indexOf(charsetParamEntry) + charsetParamEntry.length());
                                }
                            }
                        }
                        Charset charset = null;
                        if (StringUtils.isNotEmpty(charsetName)) {
                            charset = Charset.forName(charsetName);
                        }
                        URI cacheDocumentUri = null;

                        String contentWhole = handler.toString();
                        String contentActual = Jsoup.parse(contentWhole).body().text();
                        if (charset == null) {
                            charset = charsetService.detect(contentActual);
                        }
                        if (charset != null) {
                            cacheDocumentUri = cacheDocument(contentActual, charset);
                        } else {
                            cacheDocumentUri = cacheDocument(contentActual);
                        }
                        createDocument(source, source.getUri(), cacheDocumentUri, title, charset);
                    } catch (Exception e) {
                        log.error("", e);
                    } finally {
                        try {
                            stream.close();
                        } catch (Exception e) {
                            log.warn("source.uri=" + source.getUri(), e);
                        }
                    }
                } catch (IOException e) {
                    log.error("", e);
                }
            }
        }
    }

    @Override
    public void createDocumentBySourceA(Source source) {
        createDocumentBySource(source);
    }

    @Override
    public void createDocumentBySource(Integer sourceId) {
        Source source = em.find(Source.class, sourceId);
        createDocumentBySource(source);
    }

    @Override
    @Transactional
    public void createDocument(Source source, URI uri, URI cacheUri) {
        createDocument(source, uri, cacheUri, null);
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void createDocumentA(Source source, URI uri, URI cacheUri) {
        createDocument(source, uri, cacheUri);
    }

    @Override
    @Transactional
    public void updateDocument(Document document, Source source, URI cacheUri) {
        if (document.addSource(source)) {
            String content = getContent(cacheUri != null ? cacheUri : document.getUri(), String.class);
            nlpService.updateIndex(source.getId(), document.getId(), content);
        }
        if (document.getCacheUri() == null) {
            document.setCacheUri(cacheUri);
        }
    }

    @Override
    @Transactional
    public void createDocument(Source source, URI uri, URI cacheUri, String title) {
        Charset charset = charsetService.detect(uri);   // todo: exclude duplicate reading of resource
        createDocument(source, uri, cacheUri, title, charset);
    }

    @Override
    @Transactional
    public void createDocument(Source source, URI uri, URI cacheUri, String title, Charset charset) {
        Document document = getDocument(uri);
        createDocument0(source, document, uri, cacheUri, title, charset);
    }

    // todo: не получается Autowired
    DocumentJdbcInsert documentJdbcInsert;

    @Autowired
    DataSource dataSource;

    @PostConstruct
    public void init() {
        documentJdbcInsert = new DocumentJdbcInsert(dataSource);
    }

    @Autowired
    DocumentSqlUpdate documentSqlUpdate;

    protected void createDocument0(Source source, Document document, URI uri, URI cacheUri, String title, Charset charset) {
        boolean isNewSource = false;
        boolean isNewDocument = document == null;
        if (document == null) {
            document = new Document(source, uri);
            document.setCacheUri(cacheUri);
            document.setTitle(title);
        } else {
            // todo: refactor this (done due to LazyInitializationException)
            if (!em.contains(document)) {
                document = em.find(Document.class, document.getId());
            }
            isNewSource = document.addSource(source);
        }
        if (document.getMediaType() == null) {
            document.setMediaType(mediaTypeService.detectMediaType(uri, MediaType.class));
        }
        if ((document.getCharset() == null && charset != null) ||
                (document.getCharset() != null && charset == null) ||
                (document.getCharset() != null && charset != null && !document.getCharset().equals(charset))) {
            document.setCharset(charset);
        }
        if (isNewDocument) {
            // so - we create document explicitly by jdbc due to unresolved problem with connection leaks with JPA
            documentJdbcInsert.execute(document, source.getId());
        }
        String content = null;
        if (!document.getIsIndexed()) {
            if (content == null) {
                content = getContent(cacheUri != null ? cacheUri : uri, String.class);
            }
            nlpService.indexThis(source.getId(), document.getId(), content);
            document.setIsIndexed(true);
        } else if (isNewSource) {
            if (content == null) {
                content = getContent(cacheUri != null ? cacheUri : uri, String.class);
            }
            nlpService.updateIndex(source.getId(), document.getId(), content);
        }
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void createDocumentA(Source source, URI uri, URI cacheUri, String title, Charset charset) {
        createDocument(source, uri, cacheUri, title, charset);
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void createDocumentA(Source source, Document document, URI cacheUri, String title, Charset charset) {
        createDocument0(source, document, document.getUri(), cacheUri, title, charset);
    }

    public DocumentServiceImpl() {
        try {
            File dir = new File(SettingService.getSetting(Settings.CACHE_DIR, String.class, Settings.CACHE_DIR_DEFAULT));
            if (!dir.exists()) {
                dir.mkdirs();
            }
        } catch (Exception e) {
            log.warn("", e);
        }
    }

    @Override
    public URI cacheDocument(Object content) {
        Object contentCopy1 = content;
        Object contentCopy2 = content;
        if (content instanceof InputStream) {
            InputStream[] copy = new InputStream[2];
            Common.clone((InputStream) content, copy);
            contentCopy1 = copy[0];
            contentCopy2 = copy[1];
        }
        Charset charset = charsetService.detect(contentCopy1);
        if (charset == null) {
            charset = Charset.defaultCharset();
        }
        return cacheDocument(contentCopy2, charset);
    }

    @Override
    public URI cacheDocument(Object content, Charset charset) {
        if (content instanceof InputStream) {
            InputStream[] copy = new InputStream[2];
            Common.clone((InputStream) content, copy);
            final boolean isHtml = DetectHtml.isHtml(copy[0]);
            if (isHtml) {
                content = Common.extractBody(copy[1], charset);
            } else {
                content = copy[1];
            }
        } else if (content instanceof String) {
            final boolean isHtml = DetectHtml.isHtml(content);
            if (isHtml) {
                content = Common.extractBody(content, charset);
            }
        }
        String baseDirActual = SettingService.getSetting(Settings.CACHE_DIR, String.class, Settings.CACHE_DIR_DEFAULT);
        File file = new File(baseDirActual + "/" + System.currentTimeMillis());    // todo: precise file name
        URI uri = null;
        try {
            FileOutputStream fos = new FileOutputStream(file);  // todo: check for file existence
            if (content instanceof String) {
                if (charset != null) {
                    fos.write(((String) content).getBytes(charset));
                } else {
                    fos.write(((String) content).getBytes());
                }
            } else if (content instanceof InputStream) {
                IOUtils.copy((InputStream) content, fos);
            } else {
                log.warn("todo:not supported yet (267):" + content.getClass());
            }
            fos.flush();
            fos.close();
            uri = file.toURI();
        } catch (Exception e1) {
            log.error("", e1);
        }
        return uri;
    }

    @Override
    public boolean isDocumentRegistered(String uri) {
        return isDocumentRegistered(URI.create(uri));
    }

    @Override
    public boolean isDocumentRegistered(URI uri) {
        try {
            em.createQuery("from Document where uri = :uri", Document.class).setParameter("uri", uri).getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public Document getDocument(URI uri) {
        try {
            return em.createQuery("from Document where uri = :uri", Document.class).setParameter("uri", uri).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Document getDocumentA(URI uri) {
        return getDocument(uri);
    }

    @Override
    public Document getDocument(String uri) {
        return getDocument(URI.create(uri));
    }

    @Override
    @Transactional
    public void removeDocumentBySource(Source source) {
        removeDocumentBySource(source.getId());
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void removeDocumentBySourceA(Source source) {
        removeDocumentBySource(source);
    }

    @Override
    @Transactional
    public void removeDocumentBySource(Integer sourceId) {
        Source source = entityService.get(Source.class, sourceId);
        List<Document> documents = entityService.getDocuments(sourceId);
        for (Document document : documents) {
            List<Source> sources = entityService.getSources(document.getId());
            if (sources.size() > 1) {
                source.removeDocument(document);
                continue;
            }
            final URI uri = document.getUri();
            if (uri.getScheme().equals("file")) {
                File file = new File(uri);
                file.delete();
            }
            URI cachedUri = document.getCacheUri();
            if (cachedUri != null && cachedUri.getScheme().equals("file")) {
                File file = new File(cachedUri);
                file.delete();
            }
            if (!em.contains(document)) {
                document = em.find(Document.class, document.getId());
            }
            em.remove(document);
            nlpService.removeIndexForDocument(document.getId());
        }
    }

    @Autowired
    LanguageService languageService;

    @Override
    public List<Language> detectLanguages(Integer documentId) {
        Document document = entityService.get(Document.class, documentId);
        String content = getContent(document, String.class);
        return languageService.detect(content);
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public List<Document> getDocumentsToUpdateCandidates() {
        return em.createQuery("from Document where uri like 'http%'").getResultList();
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void addAllFromDirectory(String directoryName, String namePrefix) {
        log.info("addAllFromDirectory:begin");
        int all = 0, added = 0;
        try {
            String uploadDirName = SettingService.getSetting(Settings.UPLOAD_DIR_PROP, String.class, Settings.DEFAULT_UPLOAD_DIR);
            File uploadDir = new File(uploadDirName);
            if (!uploadDir.exists()) {
                uploadDir.mkdirs();
            }
            File dir = new File(directoryName);
            for (File file : dir.listFiles()) {
                all++;
                log.debug("trying to add source from:{}", file.getName());
                try {
                    Path from = file.toPath();
                    File uploadedFile = new File(uploadDir, file.getName());
                    Path uploadedPath = uploadedFile.toPath();
                    Files.copy(from, uploadedPath);
                    MediaType mediaType = mediaTypeService.detectMediaType(uploadedFile.toURI(), MediaType.class);
                    Source source = new Source(StringUtils.isEmpty(namePrefix) ? file.getName() : namePrefix + all, uploadedFile.toURI(), mediaType, SourceType.FILE);
                    entityService.persist(source);
                    createDocumentBySource(source);
                    log.debug("source added:{}", file.getName());
                    added++;
                } catch (Exception e) {
                    log.error("failed to add source:" + file.getName(), e);
                }
            }
        } finally {
            log.info("addAllFromDirectory:complete:all={} added={}", all, added);
        }
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public List<Source> getSourcesForDocument(Integer documentId) {
        return em.createQuery("select sources from Document d where d.id = :id").setParameter("id", documentId).getResultList();
    }

    @Override
    public List<Integer> getSourceIdsForDocument(Integer documentId) {
        return jdbcTemplate.queryForList("select source_id from document_source where document_id = ?", new Object[]{documentId}, Integer.class);
    }

    @Override
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void addAllFromDirectory(String directoryName) {
        addAllFromDirectory(directoryName, null);
    }
}
