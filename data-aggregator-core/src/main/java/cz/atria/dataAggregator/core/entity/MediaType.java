package cz.atria.dataAggregator.core.entity;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
public class MediaType extends org.springframework.http.MediaType {
    public MediaType(String type) {
        super(type);
    }
    /**seems this type registration not finished*/
    public final static String APPLICATION_RSS_XML_VALUE = "application/rss+xml";
    public final static org.springframework.http.MediaType APPLICATION_RSS_XML = valueOf(APPLICATION_RSS_XML_VALUE);

    public final static String APPLICATION_PDF_VALUE = "application/pdf";
    public final static org.springframework.http.MediaType APPLICATION_PDF = valueOf(APPLICATION_PDF_VALUE);

    public final static String APPLICATION_MSWORD_VALUE = "application/msword";
    public final static org.springframework.http.MediaType APPLICATION_MSWORD = valueOf(APPLICATION_MSWORD_VALUE);
    public final static String OOXML_DOCUMENT_VALUE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public final static org.springframework.http.MediaType OOXML_DOCUMENT = valueOf(OOXML_DOCUMENT_VALUE);

    public final static String APPLICATION_RTF_VALUE = "application/rtf";
    public final static org.springframework.http.MediaType APPLICATION_RTF = valueOf(APPLICATION_RTF_VALUE);
}
