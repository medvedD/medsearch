package cz.atria.dataAggregator.core;

/**
 * @author rnuriev
 * @since 15.10.2015.
 */
public class Settings {
    public static final String UPLOAD_DIR_PROP = "cz.atria.dataAggregator.uploadDir";
    public static final String DEFAULT_UPLOAD_DIR = "uploaded";
    public static final String HITS_COUNT = "cz.atria.dataAggregator.hits.count";
    public static final String POLITENESS_DELAY = "cz.atria.dataAggregator.politeness.delay";
    public static final String CACHE_DIR = "cz.atria.dataAggregator.cache.dir";
    public static final String CACHE_DIR_DEFAULT = "data";
    public static final String WAIT_FOR_BACKGROUND_JAVASCRIPT_STARTING_BEFORE = "cz.atria.dataAggregator.waitForBackgroundJavaScriptStartingBefore";
    public static final long WAIT_FOR_BACKGROUND_JAVASCRIPT_STARTING_BEFORE_DEFAULT = 30000L;
}
