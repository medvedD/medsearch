package cz.atria.dataAggregator.core.entity;

import java.nio.charset.Charset;

import javax.persistence.AttributeConverter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rnuriev
 * @since 30.09.2015.
 */
public class CharsetConverter implements AttributeConverter<Charset, String> {
    static Logger log = LoggerFactory.getLogger(CharsetConverter.class);
    @Override
    public String convertToDatabaseColumn(Charset attribute) {
        if (attribute == null) {
            return null;
        } else {
            return attribute.name();
        }
    }

    @Override
    public Charset convertToEntityAttribute(String dbData) {
        if (StringUtils.isEmpty(dbData)) {
            return null;
        } else {
            try {
                return Charset.forName(dbData);
            } catch (Exception e) {
                log.error("charset=" + dbData, e);
                return null;
            }
        }
    }
}
