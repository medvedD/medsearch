package cz.atria.dataAggregator.core;

import cz.atria.dataAggregator.core.service.MediaTypeService;

/**
 * @author rnuriev
 * @since 16.10.2015.
 */
public interface DependencyAccessor {
    void setMediaTypeService(MediaTypeService mediaTypeService);
    MediaTypeService getMediaTypeService();
}
