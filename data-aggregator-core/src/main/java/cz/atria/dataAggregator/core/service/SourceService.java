package cz.atria.dataAggregator.core.service;

import java.net.URI;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.SourceDetail;
import cz.atria.dataAggregator.core.entity.SourceType;

/**
 * @author rnuriev
 * @since 06.08.2015.
 */
public interface SourceService {
    @Transactional
    void createSource(String name, URI uri, SourceType sourceType, MediaType mediaType);

    @Transactional
    void createSource(String name, URI uri, SourceType sourceType);

    @Transactional
    void createSourceWithDetail(String name, URI uri, SourceType sourceType, URI resourceUri, MediaType resourceMediaType);

    @Transactional
    void createSourceWithDetail(String name, URI uri, SourceType sourceType, URI resourceUri, MediaType resourceMediaType, String externalId);

    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<SourceDetail> getSourceDetailsByType(SourceType sourceType);

    @Transactional
    void removeSource(Source source);

    @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<Source> getSourcesByMediaType(MediaType ... mediaTypes);
    Source getSource(URI uri);
    List<Source> getSourcesByUriPrefixAndSuffix(String prefix, String suffix);
    List<Source> getHtmlSourcesToUpdateCandidates();
}
