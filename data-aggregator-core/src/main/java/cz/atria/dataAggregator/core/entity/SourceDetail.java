package cz.atria.dataAggregator.core.entity;

import java.net.URI;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.http.MediaType;

/**this entity used for internal purposes, for represent some additional info of sources.
 * @author rnuriev
 * @since 06.08.2015.
 */
@Entity
@Table
public class SourceDetail extends Source {

    public SourceDetail() {
    }

    public SourceDetail(String name, URI uri, MediaType mediaType, SourceType type, URI resourceUri, MediaType resourceMediaType, String externalId) {
        super(name, uri, mediaType, type);
        this.resourceUri = resourceUri;
        this.resourceMediaType = resourceMediaType;
        this.externalId = externalId;
    }

    public SourceDetail(String name, URI uri, MediaType mediaType, SourceType type, URI resourceUri, MediaType resourceMediaType) {
        this(name, uri, null, type, resourceUri, resourceMediaType, null);
    }

    public SourceDetail(String name, URI uri, SourceType type, URI resourceUri, MediaType resourceMediaType) {
        this(name, uri, null, type, resourceUri, resourceMediaType);
    }

    /**this is actual URI which used to process source. It could be different from source.uri, cause later used mainly for end-user presentation*/
    @Convert (converter = URIAttributeConverter.class)
    URI resourceUri;

    @Convert (converter = MediaTypeAttributeConverter.class)
    org.springframework.http.MediaType resourceMediaType;

    /**actual meaning of this attribute relates to source type. E.g. for google alerts it's alert id.*/
    String externalId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public URI getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(URI resourceUri) {
        this.resourceUri = resourceUri;
    }

    public MediaType getResourceMediaType() {
        return resourceMediaType;
    }

    public void setResourceMediaType(MediaType resourceMediaType) {
        this.resourceMediaType = resourceMediaType;
    }
}
