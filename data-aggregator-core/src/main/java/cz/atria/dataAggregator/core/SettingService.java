package cz.atria.dataAggregator.core;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author rnuriev
 * @since 17.08.2015.
 */
public class SettingService {
    final static Logger log = LoggerFactory.getLogger(SettingService.class);
    public static <T> T getSetting(String name, Class<T> asClass, T defaultValue) {
        String propertyValue = System.getProperty(name);
        if (StringUtils.isNotEmpty(propertyValue)) {
            try {
                if (List.class.isAssignableFrom(asClass)) {
                    String[] values = StringUtils.split(propertyValue, ',');
                    return (T) Arrays.asList(values);
                } else {
                    return (T) asClass.getConstructor(String.class).newInstance(propertyValue);
                }
            } catch (Exception e) {
                log.warn("", e);
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }
    public static <T> void setSetting(String name, T value) {
        if (value != null) {
            System.setProperty(name, value.toString());
        } else {
            System.clearProperty(name);
        }
    }
}
