package cz.atria.dataAggregator.core.convert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.w3c.tidy.Tidy;

/**
 * @author rnuriev
 * @since 14.09.2015.
 */
@Service (ConvertXHTMLServiceImpl.NAME)
public class ConvertXHTMLServiceImpl implements ConvertService {
    public static final String NAME = "convertXHTMLService";
    @Override
    public <T> InputStream convert(T content) {
        InputStream in = null;
        if (content instanceof InputStream) {
            in = (InputStream) content;
        } else {
            // todo
        }
        ByteArrayOutputStream xhtmlContent = new ByteArrayOutputStream();
        try {
            Tidy tidy = new Tidy();
            tidy.setShowWarnings(true);
            tidy.setInputEncoding("UTF-8");
            tidy.setOutputEncoding("UTF-8");
            tidy.setXHTML(true);
            tidy.setMakeClean(true);
            tidy.parseDOM(in, xhtmlContent);
            return new ByteArrayInputStream(xhtmlContent.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public MediaType getSupportedMediaType() {
        return MediaType.APPLICATION_XHTML_XML;
    }
}
