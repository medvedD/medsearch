package cz.atria.dataAggregator.core;

import java.util.Date;

/**
 * @author rnuriev
 * @since 09.11.2015.
 */
public interface Content<T> {
    T getContent();

    void setContent(T content);

    Date getLastModified();

    void setLastModified(Date lastModified);

    String getEntityTag();

    void setEntityTag(String entityTag);
    Class<T> getType();
}
