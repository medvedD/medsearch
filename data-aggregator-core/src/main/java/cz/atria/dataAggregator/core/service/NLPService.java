package cz.atria.dataAggregator.core.service;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.store.Directory;

import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.entity.Document;

/**It's common Natural Language Processing service. Concrete implementation use some of NLP tool, like lucene, OpenNLP, etc
 * @author rnuriev
 * @since 24.07.2015.
 */
public interface NLPService {
    /**@return список предложений. Порядок элементов совпадает с порядком предложений в тексте.
     * */
    String[] sentenceSplitter(Object content);

    /**executes search query on given content
     * @return score value for given query
     * @deprecated
     * */
    @Deprecated
    double matches(Object content, Object query);
    /**@deprecated previously indexing assumed*/
    @Deprecated
    double matches(Object content, Object query, Analyzer analyzer);

    Map<Integer, Float> matches(Object query, Analyzer analyzer);
    Map<Integer, Float> matches(Object query, Directory analyzer);
    /**@param queries map: language to query list
     * @return map: key:documentId, value: scores of current document by language, sorted by score value descending
     * */
    Map<Integer, SortedMap<String, Float>> matchesAll(Map<String, Object> queries);
    Map<Integer, Float> matches(Map<String, Object> queries);
    /**get top matches (top scores) only
     * */
    Map<Integer, Float> convertToTopMatched(Map<Integer, SortedMap<String, Float>> matchesAll);
    /**todo: default analyzer used. Need to determine explicitly*/
    Map<Integer, Float> matches(Object query);
    Object createConjuctionQuery(String[] terms);
    Object createConjuctionQuery(Map<String, List<String>> termWithSynonyms);
    Object createQuery(String query, Analyzer analyzer);
    Object createQuery(String query);
    <T> T highlight(Document document, String queryString, Class<T> asClass);
    <T> T highlight(Document document, Object query, Class<T> asClass);
    <T> T highlight(Document document, Object query, Class<T> asClass, String lang);
    <T> T highlightFragment(Document document, Object query, Class<T> asClass, String lang);
    <T> T highlightFragment(T content, Object query, Class<T> asClass, String lang);
    <T> T highlight(URI uri, Object query);
    <T> T highlight(URI uri, Object query, String lang);
    <T> T highlight(URI uri, Object query, String lang, Charset charset);
    <T> T highlight(Integer documentId, Object query);
    <T> T highlight(T content, Object query);
    <T> void indexThis(Integer documentId, T text, Analyzer analyzer);
    <T> void indexThis(Integer sourceId, Integer documentId, T text, Analyzer analyzer);
    /**todo: need to determine analyzer explicitly*/
    <T> void indexThis(Integer documentId, T text);
    <T> void indexThis(Integer sourceId, Integer documentId, T text);
    /**add new sourceId for existing indexed document*/
    <T> void updateIndex(Integer sourceId, Integer documentId, T content);
    <T> void indexThis(Integer documentId);
    Object createQuery(List<SearchTermEntry> query, Integer slop);
    /**@return map: language to query*/
    Map<String, Object> createQueries(List<SearchTermEntry> query, Integer slop);
    Map<String, Object> createQueries(String queryText);
    void removeIndexForDocument(Integer documentId);
}
