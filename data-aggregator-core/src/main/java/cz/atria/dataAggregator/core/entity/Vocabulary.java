package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
@Table
@Entity
@Deprecated
public class Vocabulary implements Serializable {
    @Id
    @GeneratedValue
    Integer id;
    String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
