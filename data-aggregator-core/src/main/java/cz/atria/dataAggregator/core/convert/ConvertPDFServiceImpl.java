package cz.atria.dataAggregator.core.convert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.pdf.BaseFont;

import cz.atria.dataAggregator.core.charset.CharsetService;
import cz.atria.dataAggregator.core.entity.MediaType;

/**
 * @author rnuriev
 * @since 14.09.2015.
 */
@Service
public class ConvertPDFServiceImpl implements ConvertService {
    public static final String DEFAULT_FONT_NAME = "Lucida Sans Unicode";
    final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    @Qualifier(ConvertXHTMLServiceImpl.NAME)
    ConvertService convertXhtmlService;
    @Autowired
    CharsetService charsetService;

    @Override
    public <T> InputStream convert(T content) {
        try {
            ITextRenderer renderer = new ITextRenderer();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Charset charset = null;
            if (content instanceof InputStream) {
                IOUtils.copy((InputStream) content, bos);
                ByteArrayInputStream contentForCharset = null;
                contentForCharset = new ByteArrayInputStream(bos.toByteArray());
                content = (T) new ByteArrayInputStream(bos.toByteArray());
                charset = charsetService.detect(contentForCharset);
            } else if (content instanceof String) {
                charset = charsetService.detect(content);
            }

            String contentAsXhtml = null;
            if (charset != null) {
                contentAsXhtml = IOUtils.toString(convertXhtmlService.convert(content), charset);
            } else {
                log.warn("can't detect charset. default used");
                contentAsXhtml = IOUtils.toString(convertXhtmlService.convert(content), "UTF-8");
            }
            try {
                // we explicitly add font, due to incorrect display for some symbols, e.g. cyrillic.
                // todo: m.b. it's is not necessary for all languages, esp, for english
                ITextFontResolver fontResolver = renderer.getFontResolver();
                String fontPath = getClass().getClassLoader().getResource("fonts/l_10646.ttf").getPath();

                fontResolver.addFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Document documentParsed = Jsoup.parse(contentAsXhtml);
                documentParsed.head().append(String.format("<style>*{font-family: \"%1$s\";}</style>", DEFAULT_FONT_NAME));
                List<Node> nodes = new ArrayList<Node>(documentParsed.head().childNodes());
                for (Node node : nodes) {
                    if (node.nodeName().equals("meta")) {
                        node.remove();
                    }
                }
                contentAsXhtml = documentParsed.toString();
            } catch (Exception e) {
                log.warn("add font error.", e);
            }

            renderer.setDocumentFromString(contentAsXhtml);
            renderer.layout();
            ByteArrayOutputStream pdfContent = new ByteArrayOutputStream();
            renderer.createPDF(pdfContent);
            pdfContent.close();
            return new ByteArrayInputStream(pdfContent.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public org.springframework.http.MediaType getSupportedMediaType() {
        return MediaType.APPLICATION_PDF;
    }
}
