package cz.atria.dataAggregator.core.liquibase;

import java.net.URLDecoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

/**
 * @author rnuriev
 * @since 14.10.2015.
 */
public class DecodeURL implements CustomTaskChange {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    String tableName;
    String columnName;

    public DecodeURL() {
    }

    public DecodeURL(String tableName, String columnName) {
        this.tableName = tableName;
        this.columnName = columnName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }


    @Override
    public void execute(Database database) throws CustomChangeException {
        execute((JdbcConnection) database.getConnection());
    }

    public void execute(JdbcConnection connectionLb) throws CustomChangeException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        PreparedStatement psUpd = null;

        try {
            ps = connectionLb.prepareStatement(String.format("select %1$s from %2$s where %1$s like ", columnName, tableName) + "'%\\%%'");
            psUpd = connectionLb.prepareStatement(String.format("update %1$s set %2$s = ? where %2$s = ?", tableName, columnName));
            rs = ps.executeQuery();
            while (rs.next()) {
                String value = rs.getString(1);
                String valueDecoded = URLDecoder.decode(value, "UTF-8");
                if (valueDecoded.contains("%20")) {
                    valueDecoded = valueDecoded.replaceAll("%20", " ");
                }
                psUpd.setString(1, valueDecoded);
                psUpd.setString(2, value);
                psUpd.executeUpdate();
                connectionLb.commit();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    log.warn("", e);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.warn("", e);
                }
            }
        }
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {

    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {

    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }
}
