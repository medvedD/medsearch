package cz.atria.dataAggregator.core.gooalert.ga;

//import org.nnh.bean.DeliveryTo;

/**
 * @author rnuriev
 * @since 06.08.2015.
 */
public class Alert { //extends org.nnh.bean.Alert {
    String editKey;

    public Alert() {
    }

    public Alert(String query, String lang) {
        this(query, lang, "");
    }

    public Alert(String query, String lang, String region) {
      //  this(query, lang, "", DeliveryTo.EMAIL);
    }

    public Alert(String query, String lang, String region, String deliveryTo) {
//        setSearchQuery(query);
//        setLanguage(lang);
//        setRegion(region);
//        setDeliveryTo(deliveryTo);
    }

    public String getEditKey() {
        return editKey;
    }

    public void setEditKey(String editKey) {
        this.editKey = editKey;
    }
}
