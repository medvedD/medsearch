package cz.atria.dataAggregator.core.rss;

import cz.atria.dataAggregator.core.entity.Source;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
public interface RssService {
    /**non-block processing*/
    void process(Source source, FeedCallback feedCallback);
    void process(Source source, FeedCallback feedCallback, boolean isBlock);
}
