package cz.atria.dataAggregator.core.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLConnection;

import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import cz.atria.dataAggregator.core.Common;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
@Service
public class MediaTypeServiceImpl implements MediaTypeService {
    final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public <T> T detectMediaType(URI uri, Class<T> asClass) {
        T res = null;
        // todo: m.b. optimize further, without opening connection
        String scheme = uri.getScheme();
        InputStream is = null;
        try {
            if (scheme != null) {
                if (scheme.equals("http") || scheme.equals("https")) {
                    URLConnection urlConnection = null;
                    try {
                        urlConnection = uri.toURL().openConnection();
                        is = urlConnection.getInputStream();
                        String contentType = urlConnection.getContentType();
                        String mimeType = null;
                        if (StringUtils.isNotEmpty(contentType)) {
                            if (contentType.lastIndexOf(';') > 0) {
                                mimeType = contentType.substring(0, contentType.lastIndexOf(';'));
                            } else {
                                mimeType = contentType;
                            }
                        }
                        if (mimeType != null) {
                            res = convert(mimeType, asClass);
                        } else {
                            // todo: determine by other method
                        }
                    } catch (Exception e) {
                        log.error("", e);
                    }
                }
                if (res == null || res.toString().equals("content/unknown")) {
                    try {
                        String mediaType = null;
                        try {
                            mediaType = tika.detect(uri.toURL());
                        } catch (IOException e) {
                            log.warn("", e);
                            mediaType = tika.detect(Common.get(uri, InputStream.class));
                        }
                        res = convert(mediaType, asClass);
                    } catch (Exception e) {
                        log.error("", e);
                    }
                }
                // check for RSS (#1043)
                if (res != null && res.equals(MediaType.TEXT_XML)) {
                    try {
                        if (is != null) {
                            String mediaType = tika.detect(is);
                            res = convert(mediaType, asClass);
                        }
                    } catch (Exception e) {
                        log.error("", e);
                    }
                }

            }
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    log.warn("", e);
                }
            }
        }
        return res;
    }

    protected <T> T convert(String val, Class<T> asClass) {
        T res = null;
        if (String.class.isAssignableFrom(asClass)) {
            res = (T) val;
        } else if (MediaType.class.isAssignableFrom(asClass)) {
            res = (T) MediaType.parseMediaType(val);
        }
        return res;
    }

    final Tika tika = new Tika();
}
