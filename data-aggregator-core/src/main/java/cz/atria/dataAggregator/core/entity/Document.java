package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.springframework.http.MediaType;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
@Entity
@Table
public class Document implements Serializable {
    @Id
    @GeneratedValue
    Integer id;

    @ManyToMany (targetEntity = Source.class)
    @JoinTable (
            name = "document_source",
            uniqueConstraints = @UniqueConstraint(name = "document_source_uk", columnNames = {"document_id", "source_id"}),
            joinColumns = @JoinColumn (name = "document_id", nullable = false,
                    foreignKey = @ForeignKey(name = "document_source_doc_fk", foreignKeyDefinition = "foreign key(document_id) references document on delete cascade")),
            inverseJoinColumns = @JoinColumn (name = "source_id", nullable = false,
                    foreignKey = @ForeignKey(name = "document_source_source_fk", foreignKeyDefinition = "foreign key(source_id) references source on delete cascade")
            ))
    List<Source> sources;

    protected boolean isEqual(Source s1, Source s2) {
        if (s1.getId() != null && s2.getId() != null) {
            return s1.getId().equals(s2.getId());
        } else if (s1.getUri() != null && s2.getUri() != null) {
            return s1.getUri().equals(s2.getUri());
        }
        return false;
    }

    /**@return true - if source actually added, false - otherwise
     * */
    public boolean addSource(Source source) {
        if (sources == null) {
            sources = new ArrayList<>();
        } else {
            for (Source sourceExists : sources) {
                if (isEqual(sourceExists, source)) {
                    return false;
                }
            }
        }
        sources.add(source);
//        source.addDocument(this);
        return true;
    }

    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }

    @Convert (converter = URIAttributeConverter.class)
    @Column (name = "uri", unique = true)
    URI uri;

    /**this is cached (local) uri.*/
    @Convert (converter = URIAttributeConverter.class)
    URI cacheUri;

    @Convert (converter = MediaTypeAttributeConverter.class)
    MediaType mediaType;

    /**this is copy of entire page with prerequisites (for page display purposes, as-is), internally or externally accessible.
     * Copy made by some type of tool (like httrack, wget)*/
    @Convert (converter = URIAttributeConverter.class)
    URI copyUri;

    @Convert (converter = DocumentTitleTruncateConverter.class)
    String title;

    Boolean isIndexed;

    @Convert (converter = CharsetConverter.class)
    Charset charset;

    /**related to HTTP Last-Modified header*/
    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "last_modified")
    Date lastModified;

    /**related to HTTP ETag header*/
    @Column (name = "e_tag")
    String entityTag;

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getEntityTag() {
        return entityTag;
    }

    public void setEntityTag(String entityTag) {
        this.entityTag = entityTag;
    }

    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public URI getCopyUri() {
        return copyUri;
    }

    public void setCopyUri(URI copyUri) {
        this.copyUri = copyUri;
    }

    public Boolean getIsIndexed() {
        return isIndexed == null ? false : isIndexed;
    }

    public void setIsIndexed(Boolean isIndexed) {
        this.isIndexed = isIndexed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public URI getCacheUri() {
        return cacheUri;
    }

    public void setCacheUri(URI cacheUri) {
        this.cacheUri = cacheUri;
    }

    public Document(Source source, URI uri) {
        addSource(source);
        this.uri = uri;
    }

    public Document() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @Deprecated
    public Source getSource() {
        return sources != null && !sources.isEmpty() ? sources.get(0) : null;
    }
    @Deprecated
    public void setSource(Source source) {
        if (source.getId() != null && sources != null && !sources.isEmpty()) {
            for (Source source1 : sources) {
                if (source1.getId().equals(source.getId())) {
                    sources.remove(source1);
                    break;
                }
            }
        }
        addSource(source);
    }

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public void removeSource(Source source) {
        if (sources != null) {
            Source sourceFound = null;
            for (Source s : sources) {
                if (isEqual(s, source)) {
                    sourceFound = s;
                    break;
                }
            }
            if (sourceFound != null) {
                sources.remove(sourceFound);
                sourceFound.removeDocument(this);
            }
        }
    }
}
