package cz.atria.dataAggregator.core.convert;

import java.io.InputStream;

/**
 * @author rnuriev
 * @since 14.09.2015.
 */
public interface ConvertService {
    /**@param content content to convert. HTML format assumed.
     *
     * */
    <T> InputStream convert(T content);
    org.springframework.http.MediaType getSupportedMediaType();
}
