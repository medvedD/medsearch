package cz.atria.dataAggregator.core.service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * httrack tool related service.
 *
 * @author rnuriev
 * @since 27.11.2015.
 */
@Service
public class HttrackService {
    public String getPath(String savePath) throws IOException {
        final String pathname = savePath + "/hts-cache/new.txt";
        File file;
        if (pathname.startsWith("file")) {
            file = new File(URI.create(pathname));
        } else {
            file = new File(pathname);
        }
        Reader in = new FileReader(file);
        final CSVParser parser = new CSVParser(in, CSVFormat.EXCEL.withHeader().withDelimiter('\t'));
        String pageFileLocalPath = null;
        for (CSVRecord record : parser) {
            String statuscode = record.get("statuscode");
            // todo: m.b. this is not correct, if records doesn't preserve order for given resource
            if (StringUtils.isEmpty(statuscode) || statuscode.equals("" + HttpStatus.SC_MOVED_PERMANENTLY) || statuscode.equals("" + HttpStatus.SC_MOVED_TEMPORARILY)) {
                // ignore redirects
                continue;
            }
            String s = record.get("localfile");
            if (StringUtils.isNotEmpty(s)) {
                pageFileLocalPath = s;
                break;
            }
        }
        return pageFileLocalPath;
    }
}
