package cz.atria.dataAggregator.core.entity;

import javax.persistence.AttributeConverter;

/**
 * @author rnuriev
 * @since 14.10.2015.
 */
public class DocumentTitleTruncateConverter extends TruncateConverter implements AttributeConverter<String, String> {

    @Override
    protected String getTableName() {
        return "DOCUMENT";
    }

    @Override
    protected String getColumnName() {
        return "TITLE";
    }
}
