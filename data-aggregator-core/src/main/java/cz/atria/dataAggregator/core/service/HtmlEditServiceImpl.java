package cz.atria.dataAggregator.core.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;

import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author rnuriev
 * @since 17.09.2015.
 */
@Service
public class HtmlEditServiceImpl implements HtmlEditService {
    final Logger log = LoggerFactory.getLogger(getClass());
    @Override
    public <T, R> void insertIntoHead(T content, String insertValue, R res) {
        Reader reader = null;
        if (content instanceof InputStream) {
            reader = new InputStreamReader((InputStream) content);
        } else if (content instanceof String) {
            reader = new StringReader((String) content);
        }
        HTMLEditorKit htmlKit = new HTMLEditorKit();
        HTMLDocument htmlDoc = (HTMLDocument) htmlKit.createDefaultDocument();
        HTMLEditorKit.Parser parser = new ParserDelegator();
        try {
            parser.parse(reader, htmlDoc.getReader(0), true);
            Element headElement = null;
            for (Element element : htmlDoc.getRootElements()) {
                if (element.getName().equals("html")) {
                    for (int i = 0; i < element.getElementCount(); i++) {
                        Element element1 = element.getElement(i);
                        if (element1.getName().equals("head")) {
                            headElement = element1;
                            break;
                        }
                    }
                }
            }
            htmlDoc.insertBeforeEnd(headElement, insertValue);
            if (OutputStream.class.isAssignableFrom(res.getClass())){
                htmlKit.write((OutputStream)res, htmlDoc, 0, htmlDoc.getLength());
            } else if (Writer.class.isAssignableFrom(res.getClass())) {
                htmlKit.write((Writer)res, htmlDoc, 0, htmlDoc.getLength());
            } else {
                // todo: add if need
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }
}
