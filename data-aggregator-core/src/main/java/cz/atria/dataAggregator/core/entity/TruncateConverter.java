package cz.atria.dataAggregator.core.entity;

import java.sql.ResultSet;

import javax.persistence.AttributeConverter;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.atria.dataAggregator.core.SpringDBHelper;

/**
 * @author rnuriev
 * @since 14.10.2015.
 */
public abstract class TruncateConverter implements AttributeConverter<String, String> {
    static Logger log = LoggerFactory.getLogger(TruncateConverter.class);
    private int length;
    protected int getLength() {
        if (length < 1) {
            try {
                DataSource dataSource = SpringDBHelper.getBean(DataSource.class);
                ResultSet columns = dataSource.getConnection().getMetaData().getColumns(null, null, getTableName(), getColumnName());
                while (columns.next()) {
                    length = columns.getInt("COLUMN_SIZE");
                    break;
                }
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return length;
    }

    protected abstract String getTableName();
    protected abstract String getColumnName();

    @Override
    public String convertToDatabaseColumn(String attribute) {
        if (StringUtils.isEmpty(attribute)) {
            return attribute;
        } else {
            int length = getLength();
            return length > 0 && attribute.length() > length ? attribute.substring(0, length) : attribute;
        }
    }

    @Override
    public String convertToEntityAttribute(String dbData) {
        return dbData;
    }
}
