package cz.atria.dataAggregator.core.service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import cz.atria.dataAggregator.core.SettingService;

/**
 * @author rnuriev
 * @since 28.08.2015.
 */
@Service
public class SmtpServiceImpl implements SmtpService {
    @Override
    public void send(String to, String msg) {
        final String username = SettingService.getSetting(EMAIL_FROM, String.class, "dr.dataAggregator@gmail.com");
        final String password = SettingService.getSetting(EMAIL_PSW, String.class, "qw12er34");

        Properties props = new Properties();
        props.put("mail.smtp.auth", SettingService.getSetting(EMAIL_SMTP_AUTH, String.class, "true"));
        props.put("mail.smtp.starttls.enable", SettingService.getSetting(EMAIL_SMTP_STARTTLS_ENABLE, String.class, "true"));
        props.put("mail.smtp.host", SettingService.getSetting(EMAIL_SMTP_HOST, String.class, "smtp.gmail.com"));
        props.put("mail.smtp.port", SettingService.getSetting(EMAIL_SMTP_HOST, String.class, "587"));
        props.put("mail.debug", "true");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            session.setDebug(true);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("Recover password");
            message.setText(msg);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
