package cz.atria.dataAggregator.core.rss.rome;

import java.net.URI;
import java.net.URL;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.stereotype.Service;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.rss.FeedCallback;
import cz.atria.dataAggregator.core.rss.RssService;
import cz.atria.dataAggregator.core.rss.RssServiceBase;

/** ROME implementation
 * @author rnuriev
 * @since 30.07.2015.
 */
@Service
public class RssServiceRomeImpl extends RssServiceBase implements RssService {
    @Override
    protected void process0(Source source, URI uri, FeedCallback feedCallback) {
        try {
            URL feedUrl = uri.toURL();
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new XmlReader(feedUrl));
            for (SyndEntry syndEntry : feed.getEntries()) {
                try {
                    String link = syndEntry.getLink();
                    URI entryUri = URI.create(link);
//                    if (source.getType() != null && source.getType().equals(SourceType.GOOGLE_ALERT) && entryUri.getHost().contains(".google.")) {
//                        // google alerts rss entries resolved to actual links
//                        List<NameValuePair> parse = URLEncodedUtils.parse(entryUri, "UTF-8");
//                        for (NameValuePair nameValuePair : parse) {
//                            if (nameValuePair.getName().equals("url")) {
//                                link = nameValuePair.getValue();
//                                break;
//                            }
//                        }
//                    }
                    processBase(source, link);
                } catch (Exception e) {
                    log.warn("feedEntry=" + ToStringBuilder.reflectionToString(syndEntry), e);
                }
            }
        } catch (Exception e) {
            log.error("uri=" + uri, e);
        }
    }
}
