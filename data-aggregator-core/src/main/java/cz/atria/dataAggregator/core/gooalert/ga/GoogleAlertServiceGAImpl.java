package cz.atria.dataAggregator.core.gooalert.ga;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.core.gooalert.GoogleAlertService;
import cz.atria.dataAggregator.core.gooalert.PropertyAccessor;
import cz.atria.dataAggregator.core.service.SmtpService;
import cz.atria.dataAggregator.core.service.SourceService;

/**
 * Impl by: <a href = "https://github.com/nnhiti/google-alert-api">google-alert-api</a>
 *
 * @author rnuriev
 * @since 06.08.2015.
 */
public class GoogleAlertServiceGAImpl implements GoogleAlertService<Alert>, PropertyAccessor {
    Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    SourceService sourceService;


    @Value("${dataAggregator.source.google.alert.login:dr.dataAggregator@gmail.com}")
    String gooAccount;
    @Value("${dataAggregator.source.google.alert.password:qw12er34}")
    String password;

    @PostConstruct
    public void init() {
        String eMail = SettingService.getSetting(SmtpService.EMAIL_FROM, String.class, null);
        if (eMail == null || eMail.equalsIgnoreCase(gooAccount)) {
            // todo: use same password source, or set password of google account by SettingService
            String password2 = SettingService.getSetting(SmtpService.EMAIL_PSW, String.class, null);
            if (password2 != null) {
                this.password = password2;
                log.info("googleAlert account password will used from settings, not properties.");
            }
        }
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void synchronizeAlertList() {
//        log.info("synchronizeAlertList:begin");
//        try {
//            GAService service = new GAService(gooAccount, password);
//            service.doLogin();
//            final List<Alert> emailAlerts = service.getAlertByDelivery(DeliveryTo.EMAIL);
//            final List<Alert> rssAlerts = service.getAlertByDelivery(DeliveryTo.FEED);
//            // 1.
//            for (Alert alert : emailAlerts) {
//                try {
//                    Alert rssAlertAnalog = null;
//                    for (Alert rssAlert : rssAlerts) {
//                        if (isEqual(alert, rssAlert)) {
//                            rssAlertAnalog = rssAlert;
//                            break;
//                        }
//                    }
//                    if (rssAlertAnalog == null) {
//                        rssAlertAnalog = new Alert();
//                        alert.setId(null);
//                        alert.setDeliveryTo(DeliveryTo.FEED);
//                        BeanUtils.copyProperties(alert, rssAlertAnalog);
//                        String rssAlertId = service.createAlert(rssAlertAnalog);
//                        rssAlertAnalog = service.getAlertById(rssAlertId);  // this is just for RRS URL retrieval
//                        log.debug("created:" + rssAlertAnalog.getSearchQuery());
//                        sourceService.createSourceWithDetail(
//                                alert.getSearchQuery(),
//                                URI.create(String.format("https://www.google.ru/alerts/edit?s=%1$s&email=%2$s", alert.getEditKey(), gooAccount)),
//                                SourceType.GOOGLE_ALERT,
//                                URI.create(rssAlertAnalog.getDeliveryTo()),
//                                MediaType.APPLICATION_RSS_XML,
//                                alert.getId());
//                    } else {
//                        boolean isFound = false;
//                        for (SourceDetail source : sourceService.getSourceDetailsByType(SourceType.GOOGLE_ALERT)) {
//                            if (isFound = source.getExternalId().equals(alert.getId())) {
//                                break;
//                            }
//                        }
//                        if (!isFound) {
//                            sourceService.createSourceWithDetail(
//                                    alert.getSearchQuery(),
//                                    URI.create(String.format("https://www.google.ru/alerts/edit?s=%1$s&email=%2$s", alert.getEditKey(), gooAccount)),
//                                    SourceType.GOOGLE_ALERT,
//                                    URI.create(rssAlertAnalog.getDeliveryTo()),
//                                    MediaType.APPLICATION_RSS_XML,
//                                    alert.getId());
//                        }
//                    }
//                } catch (Exception e) {
//                    log.error("", e);
//                }
//            }
//            // 2. delete sources, if they don't exists
//            for (SourceDetail source : sourceService.getSourceDetailsByType(SourceType.GOOGLE_ALERT)) {
//                try {
//                    boolean isFound = false;
//                    for (Alert alert : emailAlerts) {
//                        if (isFound = alert.getId().equals(source.getExternalId())) {
//                            break;
//                        }
//                    }
//                    if (!isFound) {
//                        sourceService.removeSource(source);
//                    }
//                } catch (Exception e) {
//                    log.error("", e);
//                }
//            }
//        } catch (Exception e) {
//            log.error("", e);
//        } finally {
//            log.info("synchronizeAlertList:complete");
//        }
    }

    @Override
    public List<Alert> getAlerts() {
        GAService service = null;
        try {
            service = new GAService(gooAccount, password);
            service.doLogin();
            return service.getAlerts();
        } catch (Exception e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Alert> getAlerts(String query, String delivery) {
        GAService service = null;
        try {
            service = new GAService(gooAccount, password);
            service.doLogin();
            List<Alert> alertByDelivery = service.getAlertByDelivery(delivery);
            List<Alert> alerts = new ArrayList<>();
            for (Alert alert : alertByDelivery) {
//                if (alert.getSearchQuery().equals(query)) {
//                    alerts.add(alert);
//                }
            }
            return alerts;
        } catch (Exception e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Alert createAlert(String query, String language, String region, String deliveryTo) {
        try {
            GAService service = new GAService(gooAccount, password);
            service.doLogin();
            Alert alert = new Alert(query, language, region, deliveryTo);
            String rssAlertId = service.createAlert(alert);
            return service.getAlertById(rssAlertId);
        } catch (Exception e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Alert getAlert(String id) {
        try {
            GAService service = new GAService(gooAccount, password);
            service.doLogin();
            return service.getAlertById(id);
        } catch (Exception e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteAlert(String id) {
        try {
            GAService service = new GAService(gooAccount, password);
            service.doLogin();
            service.deleteAlert(id);
        } catch (Exception e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    public String getGooAccount() {
        return gooAccount;
    }

    public void setGooAccount(String gooAccount) {
        this.gooAccount = gooAccount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    protected boolean isEqual(Alert emailAlert, Alert rssAlert) {
//        return emailAlert.getSearchQuery().equals(rssAlert.getSearchQuery())
//                && Arrays.equals(emailAlert.getsources(), rssAlert.getsources())
//                && emailAlert.getLanguage().equals(rssAlert.getLanguage())
//                && emailAlert.getRegion().equals(rssAlert.getRegion())
//                ;
        return false;
    }
}
