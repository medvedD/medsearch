package cz.atria.dataAggregator.core.service;

import java.net.URI;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
public interface MediaTypeService {
    <T> T detectMediaType(URI uri, Class<T> asClass);
}
