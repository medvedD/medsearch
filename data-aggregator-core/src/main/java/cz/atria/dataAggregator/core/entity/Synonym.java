package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
@Table
@Entity
@Deprecated
public class Synonym implements Serializable {
    @Id
    @GeneratedValue
    Integer id;

    String name;

    @OneToOne
    @JoinColumn (name = "term_id")
    Term term;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }
}
