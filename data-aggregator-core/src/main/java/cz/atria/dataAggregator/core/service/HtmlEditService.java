package cz.atria.dataAggregator.core.service;

/**
 * @author rnuriev
 * @since 17.09.2015.
 */
public interface HtmlEditService {
    <T, R> void insertIntoHead(T content, String insertValue, R res);
}
