package cz.atria.dataAggregator.core.service;

import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Privilege;
import cz.atria.dataAggregator.core.entity.Query;
import cz.atria.dataAggregator.core.entity.QueryTermEntry;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.Synonym;
import cz.atria.dataAggregator.core.entity.Term;
import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.entity.TermElementSynonym;
import cz.atria.dataAggregator.core.entity.User;
import cz.atria.dataAggregator.core.entity.UserPrivilege;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
public interface EntityService {
    <T> T get(Class<T> entityClass, Object id);
    @Transactional (readOnly = true, propagation = Propagation.REQUIRES_NEW)
    <T> List<T> get(Class<T> entityClass, Collection ids);
    @Transactional (readOnly = true, propagation = Propagation.REQUIRES_NEW)
    <T> T getA(Class<T> entityClass, Object id);
    <T> List<T> get(Class<T> entityClass);
    /**it's just wrapper for property of entity for entity not-managed cases*/
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<Document> getDocuments(Source source);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<Document> getDocuments(Integer sourceId);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<Source> getSources(Integer documentId);
    List<Synonym> getSynonyms(Term term);
    List<Term> getChildren(Term term);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void save(Object o);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void persist(Object o);
    /**persist with external transaction context*/
    @Transactional (propagation = Propagation.REQUIRED)
    void persistExtTx(Object o);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void remove(Object o);
    UserPrivilege getUserPrivilege(User user, Privilege privilege);
    List<UserPrivilege> getUserPrivilegesOrdered(User user);
    List<Privilege> getPrivilegesOrdered();
    List<TermElement> getChildren(TermElement termElement);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    User getUser(String login);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<Query> getQueries(String login);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    Query getQueryByName(User user, String name);
    /**get queries by null user*/
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<Query> getQueriesByName(String name);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void saveQueryConditions(Query query, List<Source> sources, List<QueryTermEntry> queryTermEntries);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<Source> getSources(Query query);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<QueryTermEntry> getQueryTermEntriesOrdered(Query query);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    List<TermElementSynonym> getTermElementSynonyms(TermElement termElement);
    List<Document> getDocumentsWithEmptyTitle();
    List<Document> getDocumentsWithUndefinedMediaType();
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void update(String tab, String col, Integer id, Object val);
}
