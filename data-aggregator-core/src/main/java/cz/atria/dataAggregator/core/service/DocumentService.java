package cz.atria.dataAggregator.core.service;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.lng.Language;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
public interface DocumentService {
    <T> T getContent(Document document, Class<T> asClass);
    <T> T getContent(URI uri, Class<T> asClass);
    <T> T getContent(URI uri, Class<T> asClass, Charset charset);
    /**get actual content, without markups, tags, meta-data, etc*/
    <T> T getContentExtracted(URI uri, Class<T> asClass, MediaType mediaType, Charset charset);
    <T> T getContentExtracted(URI uri, Class<T> asClass, MediaType mediaType);
    <T> T getContentExtracted(T content, MediaType mediaType, Charset charset);
    <T> T getContentExtracted(T content, MediaType mediaType);
    /**todo: это специфично для lucene. Возможно стоит перенести*/
    Analyzer getAnalyzer(Document document);
    /**создание документов по заданному источнику*/
    @Transactional
    void createDocumentBySource(Source source);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void createDocumentBySourceA(Source source);
    @Transactional
    void createDocumentBySource(Integer sourceId);
    @Transactional
    void createDocument(Source source, URI uri, URI cacheUri);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void createDocumentA(Source source, URI uri, URI cacheUri);
    @Transactional
    void updateDocument(Document document, Source source, URI cacheUri);
    @Transactional
    void createDocument(Source source, URI uri, URI cacheUri, String title);
    @Transactional
    void createDocument(Source source, URI uri, URI cacheUri, String title, Charset charset);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void createDocumentA(Source source, URI uri, URI cacheUri, String title, Charset charset);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void createDocumentA(Source source, Document document, URI cacheUri, String title, Charset charset);
    URI cacheDocument(Object content);
    URI cacheDocument(Object content, Charset charset);
    @Transactional (readOnly = true, propagation = Propagation.NOT_SUPPORTED)
    boolean isDocumentRegistered(String uri);
    boolean isDocumentRegistered(URI uri);
    Document getDocument(URI uri);
    @Transactional (propagation = Propagation.REQUIRES_NEW, readOnly = true)
    Document getDocumentA(URI uri);
    Document getDocument(String uri);
    @Transactional
    void removeDocumentBySource(Source source);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void removeDocumentBySourceA(Source source);
    @Transactional
    void removeDocumentBySource(Integer sourceId);
    List<Language> detectLanguages(Integer documentId);
    @Transactional (propagation = Propagation.REQUIRES_NEW, readOnly = true)
    List<Document> getDocumentsToUpdateCandidates();
    /**batch add sources from directory.*/
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void addAllFromDirectory(String directoryName);
    @Transactional (propagation = Propagation.REQUIRES_NEW)
    void addAllFromDirectory(String directoryName, String namePrefix);
    @Transactional (propagation = Propagation.REQUIRES_NEW, readOnly = true)
    List<Source> getSourcesForDocument(Integer documentId);
    List<Integer> getSourceIdsForDocument(Integer documentId);
}
