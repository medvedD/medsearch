package cz.atria.dataAggregator.core.gooalert.ga;

import javax.annotation.PostConstruct;

//import org.nnh.bean.DeliveryTo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.gooalert.GoogleAlertService;

/** this version use RSS only alerts.
 * @author rnuriev
 * @since 07.10.2015.
 */
@Service("googleAlertService")
public class GoogleAlertServiceRSSOnlyGAImpl extends GoogleAlertServiceGAImpl  implements GoogleAlertService<Alert> {

    @PostConstruct
    public void init() {
        super.init();
    }

    @Override
    @Transactional (propagation = Propagation.NOT_SUPPORTED)
    public void synchronizeAlertList() {
//        log.info("synchronizeAlertList:begin");
//        try {
//            GAService service = new GAService(gooAccount, password);
//            final boolean doLoginRet = service.doLogin();
//            log.debug("doLoginRet={}", doLoginRet);
//            final List<Alert> rssAlerts = service.getAlertByDelivery(DeliveryTo.FEED);
//            log.debug("rssAlerts.size={}", rssAlerts.size());
//            for (Alert alert : rssAlerts) {
//                try {
//                    boolean isFound = false;
//                    for (SourceDetail source : sourceService.getSourceDetailsByType(SourceType.GOOGLE_ALERT)) {
//                        if (isFound = (source.getExternalId() != null && source.getExternalId().equals(alert.getId()))) {
//                            break;
//                        }
//                    }
//                    if (!isFound) {
//                        sourceService.createSourceWithDetail(
//                                alert.getSearchQuery(),
//                                URI.create(String.format("https://www.google.ru/alerts/edit?s=%1$s&email=%2$s", alert.getEditKey(), gooAccount)),
//                                SourceType.GOOGLE_ALERT,
//                                URI.create(alert.getDeliveryTo()),
//                                MediaType.APPLICATION_RSS_XML,
//                                alert.getId());
//                    }
//                } catch (Exception e) {
//                    log.error("alert=" + ToStringBuilder.reflectionToString(alert), e);
//                }
//            }
//
//            // 2. delete sources, if they don't exists
//            for (SourceDetail source : sourceService.getSourceDetailsByType(SourceType.GOOGLE_ALERT)) {
//                try {
//                    boolean isFound = false;
//                    for (Alert alert : rssAlerts) {
//                        if (isFound = (source.getExternalId() != null && source.getExternalId().equals(alert.getId()))) {
//                            break;
//                        }
//                    }
//                    if (!isFound) {
//                        sourceService.removeSource(source);
//                    }
//                } catch (Exception e) {
//                    log.error("source=" + ToStringBuilder.reflectionToString(source), e);
//                }
//            }
//        } catch (Exception e) {
//            log.error("", e);
//        } finally {
//            log.info("synchronizeAlertList:complete");
//        }
    }
}
