package cz.atria.dataAggregator.core.spider.crawler4j;

/**
 * @author rnuriev
 * @since 29.07.2015.
 */
public class CustomData {
    String baseUrl;
    BasicCrawler.PageCallback pageCallback;

    public CustomData(String baseUrl, BasicCrawler.PageCallback pageCallback) {
        this.baseUrl = baseUrl;
        this.pageCallback = pageCallback;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public BasicCrawler.PageCallback getPageCallback() {
        return pageCallback;
    }
}
