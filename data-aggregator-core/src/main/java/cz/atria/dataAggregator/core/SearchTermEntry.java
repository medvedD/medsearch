package cz.atria.dataAggregator.core;

import org.apache.commons.lang.StringUtils;

import cz.atria.dataAggregator.core.entity.Operator;
import cz.atria.dataAggregator.core.entity.QueryTermEntry;
import cz.atria.dataAggregator.core.entity.TermElement;

/**
 * @author rnuriev
 * @since 14.08.2015.
 */
public class SearchTermEntry {
    Operator operator;
    /**explicitly set text or text, constracted from TermElement with all conditions */
    protected String searchPhrase;
    /** this is internal entry. Not visualized actually*/
    TermElement termElement;
    Boolean isSynonym;
    Boolean isChildren;

    public SearchTermEntry() {
    }

    public SearchTermEntry(String searchPhrase) {
        this(null, searchPhrase, null, null, null);
    }

    public SearchTermEntry(TermElement termElement) {
        this.termElement = termElement;
    }

    public SearchTermEntry(TermElement termElement, Boolean isSynonym, Boolean isChildren) {
        this(null, null, termElement, isSynonym, isChildren);
    }
    public SearchTermEntry(Operator operator, String searchPhrase) {
        this(operator, searchPhrase, null, null, null);
    }
    public SearchTermEntry(Operator operator, String searchPhrase, TermElement termElement, Boolean isSynonym, Boolean isChildren) {
        this.operator = operator;
        this.searchPhrase = searchPhrase;
        this.termElement = termElement;
        this.isSynonym = isSynonym;
        this.isChildren = isChildren;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**return single base term, without synonyms and  children*/
    public String getSearchPhraseBase() {
        return StringUtils.isNotEmpty(searchPhrase) ? searchPhrase : (termElement != null ? termElement.getName() : null);
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }

    public TermElement getTermElement() {
        return termElement;
    }

    public void setTermElement(TermElement termElement) {
        this.termElement = termElement;
    }

    public Boolean getIsSynonym() {
        return isSynonym == null ? false : isSynonym;
    }

    public void setIsSynonym(Boolean isSynonym) {
        this.isSynonym = isSynonym;
    }

    public Boolean getIsChildren() {
        return isChildren == null ? false : isChildren;
    }

    public void setIsChildren(Boolean isChildren) {
        this.isChildren = isChildren;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;

        SearchTermEntry that = (SearchTermEntry) o;

        if (operator != that.operator) return false;
        if (termElement != null && that.termElement != null) {
            if (!termElement.getId().equals(that.termElement.getId())) {
                return false;
            }
        } else if ((termElement == null && that.termElement != null) || (termElement != null && that.termElement == null)) {
            return false;
        } else if (termElement == null && that.termElement == null) {
            if (searchPhrase != null ? !searchPhrase.equals(that.searchPhrase) : that.searchPhrase != null)
                return false;
        }
        if (isSynonym != null ? !isSynonym.equals(that.isSynonym) : that.isSynonym != null) return false;
        return !(isChildren != null ? !isChildren.equals(that.isChildren) : that.isChildren != null);

    }

    @Override
    public int hashCode() {
        int result = operator != null ? operator.hashCode() : 0;
        result = 31 * result + (searchPhrase != null ? searchPhrase.hashCode() : 0);
        result = 31 * result + (termElement != null ? termElement.hashCode() : 0);
        result = 31 * result + (isSynonym != null ? isSynonym.hashCode() : 0);
        result = 31 * result + (isChildren != null ? isChildren.hashCode() : 0);
        return result;
    }

    public QueryTermEntry convert() {
        QueryTermEntry queryTermEntry = new QueryTermEntry(getTermElement(), isSynonym, isChildren, getSearchPhraseBase(), null, null, getOperator());
        return queryTermEntry;
    }

    public static SearchTermEntry convert(QueryTermEntry queryTermEntry) {
        SearchTermEntry searchTermEntry = new SearchTermEntry(queryTermEntry.getOperator(), queryTermEntry.getSearchPhrase(), queryTermEntry.getTermElement(), queryTermEntry.getIsSynonym(), queryTermEntry.getIsChildren());
        return searchTermEntry;
    }
}
