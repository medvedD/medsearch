package cz.atria.dataAggregator.core.db;

import java.sql.Types;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.entity.Document;

/**
 * @author rnuriev
 * @since 04.12.2015.
 */
@Component
public class DocumentSqlUpdate extends SqlUpdate {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    public DocumentSqlUpdate(DataSource ds) {
        super(ds, "update document set e_tag = ?, last_modified = ?, cacheUri = ?, mediaType = ?, charset = ? where id = ?");
        declareParameter(new SqlParameter("e_tag", Types.VARCHAR));
        declareParameter(new SqlParameter("last_modified", Types.TIMESTAMP));
        declareParameter(new SqlParameter("cacheUri", Types.VARCHAR));
        declareParameter(new SqlParameter("mediaType", Types.VARCHAR));
        declareParameter(new SqlParameter("charset", Types.VARCHAR));
        declareParameter(new SqlParameter("id", Types.INTEGER));
    }

    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void execute(Document doc) {
        final int updateRes = update(doc.getEntityTag(), doc.getLastModified(), doc.getCacheUri(), doc.getMediaType(), doc.getCharset(), doc.getId());
        if (updateRes == 0) {
            log.warn("document not updated:id={}", doc.getId());
        }
    }
}
