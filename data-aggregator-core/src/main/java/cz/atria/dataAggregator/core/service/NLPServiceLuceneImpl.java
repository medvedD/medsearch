package cz.atria.dataAggregator.core.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.nl.DutchAnalyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexNotFoundException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.DefaultEncoder;
import org.apache.lucene.search.highlight.Encoder;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.NullFragmenter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleFragmenter;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.apache.lucene.search.spans.SpanQuery;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.DependencyAccessorA;
import cz.atria.dataAggregator.core.DetectHtml;
import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.core.Settings;
import cz.atria.dataAggregator.core.entity.Operator;
import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.entity.TermElementSynonym;
import cz.atria.dataAggregator.core.lng.Language;
import cz.atria.dataAggregator.core.lng.LanguageService;

/**
 * lucene implementation
 *
 * @author rnuriev
 * @since 27.07.2015.
 */
@Service
public class NLPServiceLuceneImpl extends AbstractNLPService implements
        NLPService,
        DependencyAccessorA {
    final Logger log = LoggerFactory.getLogger(getClass());

    final String indexPath = "index";

    public NLPServiceLuceneImpl() {
        File indexDir = new File(indexPath);
        if (!indexDir.exists()) {
            indexDir.mkdirs();
        }
        try {
            directory = new SimpleFSDirectory(indexDir.toPath());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    Class<? extends Directory> direcoryClass;

    public void setDirecoryClass(Class<? extends Directory> direcoryClass) {
        this.direcoryClass = direcoryClass;
    }

    @Override
    public String[] sentenceSplitter(Object content) {
        return new String[0];
    }

    @Override
    public double matches(Object content, Object query) {
        return 0;
    }

    /**
     * per language directory used
     */
    @Deprecated
    Directory directory;
    final Map<Analyzer, IndexWriter> analyzerIndexWriterMap = new HashMap<>();

    @Deprecated
    public void setDirectory(Directory directory) {
        this.directory = directory;
    }

    public void addDirectory(String name, Directory directory) {
        LANG_DIRECTORY_MAP.put(name, directory);
    }

    protected IndexWriter getIndexWriter(Analyzer analyzer) {
        if (!analyzerIndexWriterMap.containsKey(analyzer)) {
            try {
                analyzerIndexWriterMap.put(analyzer, new IndexWriter(directory, new IndexWriterConfig(analyzer)));
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return analyzerIndexWriterMap.get(analyzer);
    }

    // todo: m.b. store content itself (so - exclude explicit store)
    final Field.Store storeDefault = Field.Store.NO;

    public <T> void indexThis(Integer documentId, T text, Analyzer analyzer) {
        cz.atria.dataAggregator.core.entity.Document document = entityService.get(cz.atria.dataAggregator.core.entity.Document.class, documentId);
        indexThis(document.getSource().getId(), documentId, text, getIndexWriter(analyzer));
    }

    @Override
    public <T> void indexThis(Integer sourceId, Integer documentId, T text, Analyzer analyzer) {
        indexThis(documentId, documentId, text, getIndexWriter(analyzer));
    }

    private <T> void updateIndex(Integer sourceId, Integer documentId, String language, T content) {
        IndexReader indexReader = null;
        try {
            indexReader = DirectoryReader.open(getDirectoryByLanguageISOName(language));
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);
            final NumericRangeQuery<Integer> query = NumericRangeQuery.newIntRange(fieldNameDocumentId, documentId, documentId, true, true);
            TopDocs search = indexSearcher.search(query, 1);
            List<Integer> sourceIds = new ArrayList<>();
            sourceIds.add(sourceId);
            // todo: m.b. this is not effective in compare of using IndexWriter.updateDocument, but it's is only working solution for now.
            if (search.totalHits > 0) {
                Document document = indexSearcher.doc(search.scoreDocs[0].doc);
                for (IndexableField field : document.getFields(fieldNameSourceId)) {
                    sourceIds.add(Integer.valueOf(field.stringValue()));
                }
                removeIndexForDocument(documentId);
            } else {
                //todo: this case m.b. result from unused language => so not error actual. //    log.warn("document not found:documentId={}", documentId);
            }
            indexThis(sourceIds, documentId, content, getIndexWriterByLanguageISOName(language));
        } catch (IOException e) {
            log.error("", e);
        } finally {
            if (indexReader != null) {
                try {
                    indexReader.close();
                } catch (Exception e) {
                    log.warn("", e);
                }
            }
        }
    }

    private <T> void indexThis(Integer sourceId, Integer documentId, T text, IndexWriter indexWriter) {
        indexThis(Arrays.asList(sourceId), documentId, text, indexWriter);
    }
    /**
     * todo: вынести индексацию в отдельный процесс
     */
    private <T> void indexThis(List<Integer> sourceIds, Integer documentId, T text, IndexWriter indexWriter) {
        try {
            if (text == null) {
                log.warn("text empty:documentId={}", documentId);
                return;
            }
            final Document document = new Document();
            if (text instanceof String) {
                document.add(new TextField(fieldNameContent, (String) text, storeDefault));
            } else if (text instanceof Reader) {
                document.add(new TextField(fieldNameContent, (Reader) text));
            } else if (text instanceof InputStream) {
                String textAsString = null;
                try {
                    textAsString = IOUtils.toString((InputStream) text);
                    document.add(new TextField(fieldNameContent, textAsString, storeDefault));
                } catch (IOException e) {
                    log.error("", e);   // todo
                }
            } else {
                log.warn("indexer: unsupported text class:" + text.getClass().getCanonicalName());
            }
            document.add(new IntField(fieldNameDocumentId, documentId, Field.Store.YES));
            for (Integer sourceId : sourceIds) {
                document.add(new IntField(fieldNameSourceId, sourceId, Field.Store.YES));
            }
            try {
                indexWriter.addDocument(document);
            } catch (IOException e) {
                log.error("", e);
            } finally {
                if (indexWriter != null) {
                    try {
                        indexWriter.commit();
                    } catch (Exception e) {
                        log.error("", e);
                    }
                }
            }
        } finally {
            if (indexWriter != null) {
                try {
                    indexWriter.close();
                } catch (Exception e) {
                    log.warn("", e);
                }
            }
        }
    }

    /**
     * ISO 639 language name as keys used
     */
    private final static Map<String, Analyzer> languageToAnalyzerMap = new HashMap<>();

    protected Analyzer getAnalyzerByLanguageISOName(String name) {
        if (!languageToAnalyzerMap.containsKey(name)) {
            Analyzer analyzer = defaultAnalyzer;
            if (name.equals("en")) {
                analyzer = new EnglishAnalyzer();
            } else if ("de".equals(name)) {
                analyzer = new DutchAnalyzer();
            } else if ("ru".equals(name)) {
                analyzer = new RussianAnalyzer();
            }
            languageToAnalyzerMap.put(name, analyzer);
        }
        return languageToAnalyzerMap.get(name);
    }

    //    private static final Map<String, IndexWriter> LANG_INDEX_WRITER_MAP = new HashMap<>();
    private static final Map<String, Directory> LANG_DIRECTORY_MAP = new HashMap<>();

    private Directory getDirectoryByLanguageISOName(String name) {
        if (!LANG_DIRECTORY_MAP.containsKey(name)) {
            if (direcoryClass == null || SimpleFSDirectory.class.isAssignableFrom(direcoryClass)) {
                File indexDir = new File(indexPath + "/" + name);
                if (!indexDir.exists()) {
                    indexDir.mkdirs();
                }
                try {
                    LANG_DIRECTORY_MAP.put(name, new SimpleFSDirectory(indexDir.toPath()));
                } catch (IOException e) {
                    log.error("", e);
                }
            } else {
                try {
                    LANG_DIRECTORY_MAP.put(name, direcoryClass.newInstance());
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        }
        return LANG_DIRECTORY_MAP.get(name);
    }

    protected IndexWriter getIndexWriterByLanguageISOName(String name) {
        Directory directoryLang = getDirectoryByLanguageISOName(name);
        Analyzer analyzerLang = getAnalyzerByLanguageISOName(name);
        try {
            return new IndexWriter(directoryLang, new IndexWriterConfig(analyzerLang));
        } catch (IOException e) {
            log.error("", e);
            return null;
        }
//        if (!LANG_INDEX_WRITER_MAP.containsKey(name)) {
//            Directory directoryLang = getDirectoryByLanguageISOName(name);
//            Analyzer analyzerLang = getAnalyzerByLanguageISOName(name);
//            try {
//                LANG_INDEX_WRITER_MAP.put(name, new IndexWriter(directoryLang, new IndexWriterConfig(analyzerLang)));
//            } catch (IOException e) {
//                log.error("", e);
//            }
//        }
//        return LANG_INDEX_WRITER_MAP.get(name);
    }

    @Override
    public <T> void indexThis(Integer documentId, T text) {
        List<Language> languages = languageService.detect(text);
        for (Language language : languages) {
            List<Integer> sourceIds = documentService.getSourceIdsForDocument(documentId);
            indexThis(sourceIds, documentId, text, getIndexWriterByLanguageISOName(language.getName()));
        }
    }

    @Override
    public <T> void indexThis(Integer sourceId, Integer documentId, T text) {
        List<Language> languages = languageService.detect(text);
        for (Language language : languages) {
            indexThis(sourceId, documentId, text, getIndexWriterByLanguageISOName(language.getName()));
        }
    }

    @Override
    public <T> void updateIndex(Integer sourceId, Integer documentId, T content) {
        List<String> languages = languageService.getAvailableLanguages();
        for (String language : languages) {
            updateIndex(sourceId, documentId, language, content);
        }
    }

    @Override
    public <T> void indexThis(Integer documentId) {
        cz.atria.dataAggregator.core.entity.Document document = entityService.getA(cz.atria.dataAggregator.core.entity.Document.class, documentId);
        String content = documentService.getContent(document.getCacheUri() != null ? document.getCacheUri() : document.getUri(), String.class);
        indexThis(documentId, content);
    }

    @Autowired
    LanguageService languageService;

    /**
     * see also: 201511031117
     */
    @Override
    public Map<String, Object> createQueries(List<SearchTermEntry> query, Integer slop) {
        Map<String, Object> res = new HashMap<>();
        final List<String> languages = languageService.getAvailableLanguages();
        /*Альтернативный алгоритм
        1. Собирается строка, как есть, и парсится одной.
        2. Если slop > 0, то добавляется AND SpanNearQuery, с расстоняиями между основными терминами
        * */
        StringBuilder termEntriesAllStr = new StringBuilder();
        boolean isFirst = true;
        if (query.size() > 2) {
            termEntriesAllStr.append(StringUtils.repeat("(", query.size() - 2));
        }
        int i = 0;
        for (SearchTermEntry searchTermEntry : query) {
            i++;
            if (!isFirst) {
                termEntriesAllStr.append(" " + searchTermEntry.getOperator().nameLucene() + " ");
            }
            if (searchTermEntry.getIsChildren() || searchTermEntry.getIsSynonym()) {
                termEntriesAllStr.append("(");
            }
            String searchPhraseBase = searchTermEntry.getSearchPhraseBase();
            if (StringUtils.isNotEmpty(searchPhraseBase)) {
                searchPhraseBase = searchPhraseBase.trim();
                boolean isExactPhrase = searchPhraseBase.startsWith("\"") && searchPhraseBase.endsWith("\"");
                boolean isCloseNeed = false;
                if (isItPhrase(searchPhraseBase) && !isExactPhrase) {
                    termEntriesAllStr.append("(");
                    isCloseNeed = true;
                }
                termEntriesAllStr.append(searchPhraseBase);
                if (isCloseNeed) {
                    termEntriesAllStr.append(")");
                }

                // synonyms
                if (searchTermEntry.getTermElement() != null && searchTermEntry.getIsSynonym()) {
                    List<TermElementSynonym> termElementSynonyms = entityService.getTermElementSynonyms(searchTermEntry.getTermElement());
                    if (!termElementSynonyms.isEmpty()) {
                        for (TermElementSynonym termElementSynonym : termElementSynonyms) {
                            termEntriesAllStr.append(" OR ");
                            String termElementSynonymName = termElementSynonym.getName();
                            if (isItPhrase(termElementSynonymName)) {
                                termEntriesAllStr.append("\"" + termElementSynonymName + "\"");
                            } else {
                                termEntriesAllStr.append(termElementSynonymName);
                            }
                        }
                    }
                }

                // children
                if (searchTermEntry.getTermElement() != null && searchTermEntry.getIsChildren()) {
                    List<TermElement> children = entityService.getChildren(searchTermEntry.getTermElement());
                    if (!children.isEmpty()) {
                        for (TermElement termElement : children) {
                            termEntriesAllStr.append(" OR ");
                            String termElementName = termElement.getName();
                            if (isItPhrase(termElementName)) {
                                termEntriesAllStr.append("\"" + termElementName + "\"");
                            } else {
                                termEntriesAllStr.append(termElementName);
                            }
                        }
                    }
                }


            }
            if (searchTermEntry.getIsChildren() || searchTermEntry.getIsSynonym()) {
                termEntriesAllStr.append(")");
            }
            if (query.size() > 2 && i > 1 && i < query.size()) {
                termEntriesAllStr.append(")");
            }
            if (isFirst) {
                isFirst = false;
            }
        }

        boolean isUseSlop = true;
        if (query.size() > 1) {
            for (SearchTermEntry searchTermEntry : query) {
                if (searchTermEntry.getOperator() != null && searchTermEntry.getOperator() != Operator.AND) {
                    isUseSlop = false;
                    break;
                }
            }
        } else {
            isUseSlop = false;
        }

        for (String language : languages) {
            Object queryParsed = createQuery(termEntriesAllStr.toString(), getAnalyzerByLanguageISOName(language));
            if (isUseSlop && query.size() > 1 && (slop != null && slop > 0)) {
                List<SpanQuery> spanQueriesList = new ArrayList<>();
                for (int j = 0; j < query.size(); j++) {
                    SpanQuery spanQuery = null;
                    SearchTermEntry searchTermEntry = query.get(j);
                    String queryStr = searchTermEntry.getSearchPhraseBase();
                    if (StringUtils.isNotEmpty(queryStr)) {
                        queryStr = queryStr.trim();
                        Object queryForEntry = createQuery(queryStr, getAnalyzerByLanguageISOName(language));
                        if (queryForEntry instanceof TermQuery) {
                            TermQuery termQuery = (TermQuery) queryForEntry;
                            spanQuery = new SpanTermQuery(termQuery.getTerm());
                        } else if (queryForEntry instanceof PhraseQuery) {
                            PhraseQuery phraseQuery = (PhraseQuery) queryForEntry;
                            SpanQuery[] spanQueriesForPhrase = new SpanQuery[phraseQuery.getTerms().length];
                            int termIndex = 0;
                            for (Term term : phraseQuery.getTerms()) {
                                spanQueriesForPhrase[termIndex++] = new SpanTermQuery(term);
                            }
                            spanQuery = new SpanNearQuery(spanQueriesForPhrase, 0, true);
                        } else {
                            log.error("unsupported query type:" + queryForEntry);
                        }
                    }
                    if (spanQuery != null) {
                        spanQueriesList.add(spanQuery);
                    }
                }
                SpanQuery[] spanQueries = new SpanQuery[spanQueriesList.size()];
                for (int k = 0; k < spanQueriesList.size(); k++) {
                    spanQueries[k] = spanQueriesList.get(k);
                }
                SpanNearQuery spanNearQuery = new SpanNearQuery(spanQueries, slop, true);
                BooleanQuery booleanQuery = new BooleanQuery();
                booleanQuery.add((Query) queryParsed, BooleanClause.Occur.MUST);
                booleanQuery.add(spanNearQuery, BooleanClause.Occur.MUST);
                res.put(language, booleanQuery);
            } else {
                res.put(language, queryParsed);
            }
        }
        return res;
    }

    public Map<String, Object> createQueries1(List<SearchTermEntry> query, Integer slop) {
        StringBuilder builder = new StringBuilder();
        StringBuilder termList = new StringBuilder();   // for language determination
        // todo: temporary. Decide how to implement spans?
        if (slop != null && slop > 0 && !query.isEmpty() && query.size() == 2 && query.get(1) != null && query.get(1).getOperator() != null && query.get(1).getOperator().equals(Operator.AND)) {
            builder.append("\"");
            for (SearchTermEntry searchTermEntry : query) {
                builder.append(searchTermEntry.getSearchPhraseBase());
                builder.append(' ');
            }
            builder.append(String.format("\"~%1$d", slop));
        } else {
            builder.append('(');
            boolean isFirst = true;
            for (SearchTermEntry searchTermEntry : query) {
                termList.append(searchTermEntry.getSearchPhraseBase());
                termList.append(' ');
                if (!isFirst) {
                    builder.append(' ');
                    builder.append(searchTermEntry.getOperator().nameLucene());
                    builder.append(' ');
                }
                if (searchTermEntry.getIsSynonym() || searchTermEntry.getIsChildren()) {
                    builder.append('(');
                }
                // todo: add children, if any
                if (isItPhrase(searchTermEntry.getSearchPhraseBase())) {
                    builder.append('\"');
                    builder.append(searchTermEntry.getSearchPhraseBase());
                    builder.append('\"');
                    if (slop != null && slop > 0) {
                        builder.append("~" + slop);
                    }
                } else {
                    builder.append(searchTermEntry.getSearchPhraseBase());
                }

                if (searchTermEntry.getIsSynonym()) {
                    if (searchTermEntry.getTermElement() != null) {
                        List<TermElementSynonym> termElementSynonyms = entityService.getTermElementSynonyms(searchTermEntry.getTermElement());
                        if (termElementSynonyms != null && !termElementSynonyms.isEmpty()) {
                            for (TermElementSynonym termElementSynonym : termElementSynonyms) {
                                builder.append(' ');
                                builder.append(Operator.OR.nameLucene());
                                builder.append(' ');
                                if (isItPhrase(termElementSynonym.getName())) {
                                    builder.append('\"');
                                    builder.append(termElementSynonym.getName());
                                    builder.append('\"');
                                    if (slop != null && slop > 0) {
                                        builder.append("~" + slop);
                                    }
                                } else {
                                    builder.append(termElementSynonym.getName());
                                }
                            }
                        }
                    }
                }

                if (searchTermEntry.getIsChildren()) {
                    if (searchTermEntry.getTermElement() != null) {
                        List<TermElement> children = entityService.getChildren(searchTermEntry.getTermElement());
                        if (children != null && !children.isEmpty()) {
                            int cnt = 0;
                            for (TermElement termElement : children) {
                                builder.append(' ');
                                builder.append(Operator.OR.nameLucene());
                                builder.append(' ');
                                if (isItPhrase(termElement.getName())) {
                                    builder.append('\"');
                                    builder.append(termElement.getName());
                                    builder.append('\"');
                                    if (slop != null && slop > 0) {
                                        builder.append("~" + slop);
                                    }
                                } else {
                                    builder.append(termElement.getName());
                                }
                                // todo: we explicitly limit children entries
                                if (cnt++ > 10) {
                                    break;
                                }
                            }
                        }
                    }
                }


                if (searchTermEntry.getIsSynonym() || searchTermEntry.getIsChildren()) {
                    builder.append(')');
                }

                if (isFirst) {
                    isFirst = false;
                }
            }
            builder.append(')');
        }
        // we can't correctly detect query language => any language assumed
        List<String> languages = languageService.getAvailableLanguages();
        Map<String, Object> queries = new HashMap<>();
        for (String language : languages) {
            queries.put(language, createQuery(builder.toString(), getAnalyzerByLanguageISOName(language)));
        }
        return queries;
    }

    @Override
    public Map<String, Object> createQueries(String queryText) {
        List<String> languages = languageService.getAvailableLanguages();
        Map<String, Object> queries = new HashMap<>();
        for (String language : languages) {
            queries.put(language, createQuery(queryText, getAnalyzerByLanguageISOName(language)));
        }
        return queries;
    }

    @Override
    public Object createQuery(List<SearchTermEntry> query, Integer slop) {
        Map<String, Object> queries = createQueries(query, slop);
        for (Map.Entry<String, Object> queryEntry : queries.entrySet()) {
            return queryEntry.getValue();
        }
        return null;
    }


    @Override
    public void removeIndexForDocument(Integer documentId) {
        // todo: m.b. optimize: use previously found languages only
        for (String lang : languageService.getAvailableLanguages()) {
            IndexReader reader = null;
            try {
                Directory directory = getDirectoryByLanguageISOName(lang);
                reader = DirectoryReader.open(directory);
                IndexSearcher searcher = new IndexSearcher(reader);
                Query query = NumericRangeQuery.newIntRange(fieldNameDocumentId, documentId, documentId, true, true);
                TopDocs search = searcher.search(query, 10);
                if (search.totalHits > 0) {
                    IndexWriter writer = getIndexWriterByLanguageISOName(lang);
                    try {
                        writer.deleteDocuments(query);
                    } finally {
                        try {
                            writer.commit();
                            writer.close();
                        } catch (Exception e) {
                            log.warn("", e);
                        }
                    }
                }
            } catch (IOException e) {
                log.error("", e);
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        log.warn("", e);
                    }
                }
            }
        }
    }

    @Override
    @Deprecated
    public double matches(Object content, Object query, Analyzer analyzer) {
        return 0;
    }

    @Override
    public Map<Integer, Float> matches(Object query, Analyzer analyzer) {
        return matches(query, directory);
    }

    @Override
    public Map<Integer, Float> matches(Object query, Directory directory) {
        Map<Integer, Float> res = new HashMap<>();
        IndexReader reader = null;
        try {
            reader = DirectoryReader.open(directory);
            IndexSearcher searcher = new IndexSearcher(reader);
            TopDocs search = searcher.search((Query) query, SettingService.getSetting(Settings.HITS_COUNT, Integer.class, 300));    // todo: result count
            if (search.totalHits > 0) {
                for (int i = 0; i < search.scoreDocs.length; i++) {
                    try {
                        ScoreDoc scoreDoc = search.scoreDocs[i];
                        Document doc = searcher.doc(scoreDoc.doc);
                        IndexableField documentId = doc.getField(fieldNameDocumentId);
                        res.put(documentId.numericValue().intValue(), scoreDoc.score);
                    } catch (Exception e) {
                        log.error("i=" + i, e);
                    }
                }
            }
        } catch (IndexNotFoundException e) {
            log.error("", e);
        } catch (IOException e) {
            log.error("", e);
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    log.warn("", e);
                }
            }
        }
        return res;
    }

    @Override
    public Map<Integer, Float> matches(Map<String, Object> queries) {
        return convertToTopMatched(matchesAll(queries));
    }

    @Override
    public Map<Integer, SortedMap<String, Float>> matchesAll(Map<String, Object> queries) {
        Map<Integer, SortedMap<String, Float>> res = new HashMap<>();
        Map<Integer, Map<String, Float>> res0 = new HashMap<>();
        for (Map.Entry<String, Object> queryEntry : queries.entrySet()) {
            Directory directory = getDirectoryByLanguageISOName(queryEntry.getKey());
            Map<Integer, Float> matches = matches(queryEntry.getValue(), directory);
            for (Map.Entry<Integer, Float> matchEntry : matches.entrySet()) {
                if (!res0.containsKey(matchEntry.getKey())) {
                    res0.put(matchEntry.getKey(), new HashMap<String, Float>());
                }
                res0.get(matchEntry.getKey()).put(queryEntry.getKey(), matchEntry.getValue());
            }
        }
        for (final Map.Entry<Integer, Map<String, Float>> resEntry : res0.entrySet()) {
            res.put(resEntry.getKey(), new TreeMap<String, Float>(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return resEntry.getValue().get(o1) > resEntry.getValue().get(o2) ? 1 : -1;
                }
            }));
            res.get(resEntry.getKey()).putAll(resEntry.getValue());
        }
        return res;
    }

    @Override
    public Map<Integer, Float> matches(Object query) {
        return matches(query, defaultAnalyzer);
    }

    @Override
    public Object createConjuctionQuery(String[] terms) {
        return null;
    }

    @Override
    public Object createConjuctionQuery(Map<String, List<String>> termWithSynonyms) {
        StringBuilder queryString = new StringBuilder();
        for (Map.Entry<String, List<String>> termWithSynonymsEntry : termWithSynonyms.entrySet()) {
            if (termWithSynonymsEntry.getValue() == null || termWithSynonymsEntry.getValue().isEmpty()) {
                queryString.append(termWithSynonymsEntry.getKey());
            } else {
                queryString.append('(');
                queryString.append(termWithSynonymsEntry.getKey());
                for (String synonym : termWithSynonymsEntry.getValue()) {
                    queryString.append(" OR " + synonym);
                }
                queryString.append(')');
            }
        }
        try {
            return getQueryParser(defaultAnalyzer).parse(queryString.toString());
        } catch (ParseException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    private final String fieldNameContent = "f";
    private final String fieldNameDocumentId = "documentId";
    public static final String fieldNameSourceId = "sourceId";

    final Map<Analyzer, QueryParser> analyzerQueryParserMap = new HashMap<>();

    protected QueryParser getQueryParser(Analyzer analyzer) {
        if (!analyzerQueryParserMap.containsKey(analyzer)) {
            analyzerQueryParserMap.put(analyzer, new QueryParser(fieldNameContent, analyzer));
        }
        return analyzerQueryParserMap.get(analyzer);
    }

    @Override
    public Object createQuery(String query, Analyzer analyzer) {
        try {
            QueryParser queryParser = getQueryParser(analyzer);
            return queryParser.parse(query);
        } catch (ParseException e) {
            log.error("q=" + query, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * todo: what does it mean - default? clarify or remove further
     */
    final Analyzer defaultAnalyzer = new StandardAnalyzer();

    @Override
    public Object createQuery(String query) {
        try {
            QueryParser queryParser = getQueryParser(defaultAnalyzer);
            return queryParser.parse(query);
        } catch (ParseException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    DocumentService documentService;

    public static final String FMT_START = "<font color='magenta'><b><i>";
    public static final String FMT_END = "</i></b></font>";
    /**
     * this entries used as highlight markers only, cause we have to remove tags before actual highlight
     */
    public static final String FMT_MARKER_START = "--q39487teghzasset455f46vaf--";
    public static final String FMT_MARKER_END = "--45treg9wgafhs9g8gvsfvh--";
    final Encoder encoder = new DefaultEncoder();
    final Fragmenter fullFragmenter = new NullFragmenter();

    final Formatter formatter = new SimpleHTMLFormatter(FMT_START, FMT_END);
    final Formatter formatterMarker = new SimpleHTMLFormatter(FMT_MARKER_START, FMT_MARKER_END);

    @Override
    public <T> T highlight(cz.atria.dataAggregator.core.entity.Document document, String queryString, Class<T> asClass) {
        Analyzer analyzer = documentService.getAnalyzer(document);
        Object query1 = createQuery(queryString, analyzer);
        return highlight(document, query1, asClass);
    }

    @Override
    public <T> T highlight(cz.atria.dataAggregator.core.entity.Document document, Object query, Class<T> asClass) {
        T content = documentService.getContent(document, asClass);
        return highlight(document, query, asClass, languageService.detect(content).get(0).getName());
    }

    @Override
    public <T> T highlight(cz.atria.dataAggregator.core.entity.Document document, Object query, Class<T> asClass, String lang) {
        T content = documentService.getContent(document, asClass);
        return highlight(content, query);
    }

    protected <T> T highlight(T content, Object query, Fragmenter fragmenter, String lang) {
        return highlight(content, formatter, query, fragmenter, lang);
    }

    protected <T> T highlight(T content, Formatter formatter, Object query, Fragmenter fragmenter, String lang) {
        T res = content;
        Analyzer analyzer = getAnalyzerByLanguageISOName(lang);
        Highlighter highlighter = new Highlighter(formatter, encoder, new QueryScorer((Query) query));
        highlighter.setTextFragmenter(fragmenter);
        TokenStream tokenStream = null;
        boolean isHtml = isHtml(content);
        try {
            if (content instanceof Reader) {
                tokenStream = analyzer.tokenStream(fieldNameContent, isHtml ? new HTMLStripCharFilter((Reader) content): (Reader) content);
            } else if (content instanceof String) {
                tokenStream = analyzer.tokenStream(fieldNameContent, isHtml ? new HTMLStripCharFilter(new StringReader((String) content)): new StringReader((String) content));
            }
            highlighter.setMaxDocCharsToAnalyze(Integer.MAX_VALUE);
            res = (T) highlighter.getBestFragment(tokenStream, (String) content);   // todo: resolve type conversions
        } catch (Exception e) {
            log.error("", e);
        }
        return res;
    }

    protected boolean isHtml(Object content) {
        if (content == null) {
            return false;
        }
        if (content instanceof String) {
            if (StringUtils.isEmpty((String) content)) {
                return false;
            }
            String contentStr = (String) content;
            return DetectHtml.isHtml(contentStr);
        } else {
            log.warn("{}:not supported yet:{}", Common.getMethodName(), content.getClass());
            return false;
        }
    }

    final SimpleFragmenter simpleFragmenter = new SimpleFragmenter();

    @Override
    public <T> T highlightFragment(cz.atria.dataAggregator.core.entity.Document document, Object query, Class<T> asClass, String lang) {
        T content = documentService.getContent(document, asClass);
        return highlightFragment(content, query, asClass, lang);
    }

    @Override
    public <T> T highlightFragment(T content, Object query, Class<T> asClass, String lang) {
        boolean isContainsTags = false;
        Formatter formatterActual = formatter;
        if (content != null) {
            if (content instanceof String) {
                isContainsTags = ((String) content).contains("<") || ((String) content).contains(">");
            } else {
                log.warn("todo: add support type:" + content.getClass());   // todo
            }
        }
        if (isContainsTags) {
            formatterActual = formatterMarker;
        }
        T res = highlight(content, formatterActual, query, simpleFragmenter, lang);
        if (formatterActual == formatterMarker && res != null) {
            if (res instanceof String) {
                res = (T) ((String) res).replaceAll("<", "").replaceAll(">", "").replaceAll(FMT_MARKER_START, FMT_START).replaceAll(FMT_MARKER_END, FMT_END);
            } else {
                log.warn("todo: add support type:" + res.getClass());   // todo
            }
        }
        return res;
    }

    public <T> T highlight(T content, Object query) {
        return highlight(content, query, fullFragmenter, languageService.detect(content).get(0).getName());
    }

    @Override
    public <T> T highlight(URI uri, Object query) {
        String content = documentService.getContent(uri, String.class);
        return (T) highlight(content, query);
    }

    @Override
    public <T> T highlight(URI uri, Object query, String lang) {
        String content = documentService.getContent(uri, String.class);
        return (T) highlight(content, query, fullFragmenter, lang);
    }

    @Override
    public <T> T highlight(URI uri, Object query, String lang, Charset charset) {
        String content = documentService.getContent(uri, String.class, charset);
        return (T) highlight(content, query, fullFragmenter, lang);
    }

    @Autowired
    EntityService entityService;

    @Override
    public <T> T highlight(Integer documentId, Object query) {
        return (T) highlight(entityService.get(cz.atria.dataAggregator.core.entity.Document.class, documentId), query);
    }

    @Override
    public <T> void set(T t) {
        if (t instanceof EntityService) {
            this.entityService = (EntityService) t;
        } else if (t instanceof LanguageService) {
            this.languageService = (LanguageService) t;
        }
    }

    @Override
    public <T> T get(Class<T> c) {
        if (EntityService.class.isAssignableFrom(c)) {
            return (T) entityService;
        } else if (LanguageService.class.isAssignableFrom(c)) {
            return (T) languageService;
        }
        return null;
    }
}
