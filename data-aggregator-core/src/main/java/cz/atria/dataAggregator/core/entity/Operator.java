package cz.atria.dataAggregator.core.entity;

/**
 * @author rnuriev
 * @since 14.08.2015.
 */
public enum Operator {
    AND, OR, NOT;

    public String nameLucene() {
        return name();
    }
}
