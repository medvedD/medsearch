package cz.atria.dataAggregator.core.rss;

import java.net.URI;
import java.nio.charset.Charset;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.charset.CharsetService;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.service.DocumentService;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
public abstract class RssServiceBase implements RssService {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected abstract void process0(Source source, URI uri, FeedCallback feedCallback);

    @Override
    public void process(Source source, FeedCallback feedCallback) {
        process(source, feedCallback, false);
    }

    @Override
    public void process(Source source, FeedCallback feedCallback, boolean isBlock) {
        if (isBlock) {
            process0(source, feedCallback);
        } else {
            Thread thread = new Thread(new RssReaderThread(source, feedCallback));
            thread.start();
            log.debug("started");
        }
    }

    @Autowired
    protected DocumentService documentService;

    @Autowired
    CharsetService charsetService;

    protected void processBase(Source source, String link) {
        Document document = documentService.getDocument(link);
        URI cacheUri = null;
        if(document == null || document.getCacheUri() == null) {
            URI uri = URI.create(link);
            Charset charset = charsetService.detect(uri);
            if (charset != null) {
                String content = documentService.getContentExtracted(uri, String.class,
                        document != null && document.getMediaType() != null ? document.getMediaType() : MediaType.TEXT_HTML,
                        charset);
                if (StringUtils.isNotEmpty(content)) {
                    cacheUri = documentService.cacheDocument(content, charset);
                }
            } else {
                String content = documentService.getContentExtracted(uri, String.class,
                        document != null && document.getMediaType() != null ? document.getMediaType() : MediaType.TEXT_HTML);
                if (StringUtils.isNotEmpty(content)) {
                    cacheUri = documentService.cacheDocument(content);
                }
            }
        }
        if (document == null) {
            documentService.createDocument(source, URI.create(link), cacheUri);
        } else  {
            documentService.updateDocument(document, source, cacheUri);
        }
    }

    protected void process0(Source source, FeedCallback feedCallback) {
        try {
            URI uri = Common.normilize(source.getResourceUri());
            log.debug("rss thread started for:" + uri);
            process0(source, uri, feedCallback);
            log.debug("rss thread complete for:" + uri);
        } catch (Exception e) {
            log.error("source=" + ToStringBuilder.reflectionToString(source), e);
        }
    }

    private class RssReaderThread implements Runnable {
        Source source;
        FeedCallback feedCallback;

        public RssReaderThread(Source source, FeedCallback feedCallback) {
            this.source = source;
            this.feedCallback = feedCallback;
        }

        @Override
        public void run() {
            process0(source, feedCallback);
        }
    }
}
