package cz.atria.dataAggregator.core.charset;

import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;

import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cz.atria.dataAggregator.core.Common;

/**
 * <a href = "https://code.google.com/p/juniversalchardet/">juniversalchardet</a> specific impl.
 *
 * @author rnuriev
 * @since 30.09.2015.
 */
@Service
public class CharsetServiceUniversalchardetImpl implements CharsetService {
    final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * use source specific encoding info, like http\html headers, etc
     */
    @Override
    public Charset detect(URI uri) {
        try {
            return detect(Common.get(uri));
        } catch (Exception e) {
            log.warn("uri=" + uri, e);
            return null;
        }
    }

    @Override
    public <T> Charset detect(T content) {
        if (content == null) {
            log.warn("content undefined");
            return null;
        }
        Charset res = null;
        UniversalDetector detector = new UniversalDetector(null);
        byte[] buf = new byte[4096];
        InputStream is = null;
        try {
            if (content instanceof InputStream) {
                is = (InputStream) content;
            } else if (content instanceof String) {
                log.warn("{}:string content detected. Default charset assumed:{}", Common.getMethodName(), Charset.defaultCharset());
                return Charset.defaultCharset();
            } else {
                log.warn("{}:not supported yet:{}", Common.getMethodName(), content.getClass().getCanonicalName());
            }
            int nread;
            while ((nread = is.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }
            detector.dataEnd();
            String encoding = detector.getDetectedCharset();
            if (encoding != null) {
                res = Charset.forName(encoding);
            } else {
                log.warn("can't detect encoding");
            }
            detector.reset();
        } catch (Exception e) {
            log.warn("", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    log.warn("", e);
                }
            }
        }
        return res;
    }
}
