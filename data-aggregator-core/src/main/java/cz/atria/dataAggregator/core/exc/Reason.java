package cz.atria.dataAggregator.core.exc;

/**
 * @author rnuriev
 * @since 03.09.2015.
 */
public enum Reason {
    UNIQUE_CONSTRAINT_VIOLATION("Value duplication (unique constraint violation)"),
    UPLOAD_FILE_ERROR("Upload file error"),
    QUERY_IS_EMPTY("Search conditions aren't set"),
    AT_LEAST_ONE_PHRASE("At least one phrase have to be set."),
    NULL_CONSTRAINT_VIOLATION("Value is empty"),
    CONSTRAINT_VIOLATION("Wrong value"),
    UNSUPPORTED_MEDIA_TYPE("This file type isn't supported.")
    ;
    String message;

    Reason() {
    }

    Reason(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
