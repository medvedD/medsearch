package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.MediaType;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
@Entity
@Table
@Inheritance (strategy = InheritanceType.JOINED)
public class Source implements Serializable {
    @Id
    @GeneratedValue
    Integer id;

    @NotBlank
    String name;

    @Convert (converter = URIAttributeConverter.class)
    URI uri;

    @Convert (converter = MediaTypeAttributeConverter.class)
    MediaType mediaType;

    public Source() {
    }

    public Source(String name, URI uri, MediaType mediaType, SourceType type) {
        this.name = name;
        this.uri = uri;
        this.mediaType = mediaType;
        this.type = type;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    @ManyToMany (targetEntity = Document.class, mappedBy = "sources")
    List<Document> documents;

    SourceType type;

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SourceType getType() {
//        SourceType res = null;
//        if(getUri() != null) {
//            String scheme = getUri().getScheme();
//            if (scheme.equals("http") || scheme.equals("https")) {
//                MediaType mediaType = getMediaType();
//                if (mediaType != null) {
//                    if (mediaType.equals(MediaType.TEXT_HTML)) {
//                        res = SourceType.URL;
//                    } else if (mediaType.equals(cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_RSS_XML) || mediaType.equals(MediaType.APPLICATION_ATOM_XML)) {
//                        res = SourceType.RSS;
//                    }
//                }
//            } else if (scheme.equals("file")) {
//                res = SourceType.FILE;
//            }
//        }
        return type;
    }

    public void setType(SourceType type) {
        this.type = type;
    }

    /**this is media type of actual resouce which is used for document retrieval*/
    @Transient
    public MediaType getResourceMediaType() {
        return mediaType;
    }

    @Transient
    public URI getResourceUri() {
        return uri;
    }

    protected boolean isEqual(Document d1, Document d2) {
        if (d1.getId() != null && d2.getId() != null) {
            return d1.getId().equals(d2.getId());
        } else if (d1.getUri() != null && d2.getUri() != null) {
            return d1.getUri().equals(d2.getUri());
        }
        return false;
    }

    public void addDocument(Document document) {
        if (documents == null) {
            documents = new ArrayList<>();
        } else {
            for (Document d : documents) {
                if (isEqual(d, document)) {
                    return;
                }
            }
        }
        documents.add(document);
        document.addSource(this);
    }

    public void removeDocument(Document document) {
        if (documents != null) {
            Document docFound = null;
            for (Document d : documents) {
                if (isEqual(d, document)) {
                    docFound = d;
                    break;
                }
            }
            if (docFound != null) {
                documents.remove(docFound);
                docFound.removeSource(this);
            }
        }
    }
}
