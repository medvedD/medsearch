package cz.atria.dataAggregator.core.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author rnuriev
 * @since 07.08.2015.
 */
@Entity
@Table
public class Privilege {
    @Id
    Integer id;

    String name;

    @ManyToOne
    Privilege parent;

    public Privilege() {
    }

    public Privilege(Integer id, String name, Privilege parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }

    public Privilege getParent() {
        return parent;
    }

    public void setParent(Privilege parent) {
        this.parent = parent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
