package cz.atria.dataAggregator.core.gooalert;

/**
 * @author rnuriev
 * @since 16.11.2015.
 */
public interface PropertyAccessor {
    void setGooAccount(String gooAccount);
    void setPassword(String password);
}
