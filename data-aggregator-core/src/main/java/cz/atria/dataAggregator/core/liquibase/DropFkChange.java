package cz.atria.dataAggregator.core.liquibase;

import java.sql.ResultSet;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
public class DropFkChange implements CustomTaskChange {
    String pkTableName;
    String fkTableName;
    String pkColumnName;
    String fkColumnName;

    public String getPkTableName() {
        return pkTableName;
    }

    public void setPkTableName(String pkTableName) {
        this.pkTableName = pkTableName;
    }

    public String getFkTableName() {
        return fkTableName;
    }

    public void setFkTableName(String fkTableName) {
        this.fkTableName = fkTableName;
    }

    public String getPkColumnName() {
        return pkColumnName;
    }

    public void setPkColumnName(String pkColumnName) {
        this.pkColumnName = pkColumnName;
    }

    public String getFkColumnName() {
        return fkColumnName;
    }

    public void setFkColumnName(String fkColumnName) {
        this.fkColumnName = fkColumnName;
    }

    @Override
    public void execute(Database database) throws CustomChangeException {
        try {
            JdbcConnection connectionLb = (JdbcConnection) database.getConnection();
            ResultSet crossReference = connectionLb.getMetaData().getCrossReference(null, null, pkTableName.toUpperCase(), null, null, fkTableName.toUpperCase());
            boolean isFound = crossReference.next();
            if (!isFound) {
                crossReference = connectionLb.getMetaData().getCrossReference(null, null, pkTableName.toLowerCase(), null, null, fkTableName.toLowerCase());
                isFound = crossReference.next();
            }
            if (isFound) {
                if (crossReference.getString("PKCOLUMN_NAME").equalsIgnoreCase(pkColumnName) && crossReference.getString("FKCOLUMN_NAME").equalsIgnoreCase(fkColumnName)) {
                    connectionLb.prepareStatement(String.format("alter table %1$s drop constraint %2$s", fkTableName, crossReference.getString("FK_NAME"))).execute();
                }
            }
            crossReference.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {

    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {

    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }
}
