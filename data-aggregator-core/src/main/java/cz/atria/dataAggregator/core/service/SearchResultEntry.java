package cz.atria.dataAggregator.core.service;

import cz.atria.dataAggregator.core.entity.Document;

/**Определяет результат поиска по кокнретному документу
 * todo: хотелось бы тут также хранить результаты поиска (score, position)
 * @author rnuriev
 * @since 24.07.2015.
 */
public class SearchResultEntry {
    Document document;
    double totalScore;
    /**actual query used to get this result.*/
    Object query;
    String lang;
    public SearchResultEntry(Document document, double totalScore, Object query, String lang) {
        this.document = document;
        this.totalScore = totalScore;
        this.query = query;
        this.lang = lang;
    }

    public SearchResultEntry(Document document, double totalScore) {
        this(document, totalScore, null, null);
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public SearchResultEntry(Document document) {
        this.document = document;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public String getDocumentUri() {
        if(document != null) {
            return document.getUri().toString();
        }
        return null;
    }

    public Object getQuery() {
        return query;
    }

    public void setQuery(Object query) {
        this.query = query;
    }

    public String getSourceName() {
        if(document != null) {
            return document.getSource().getName();
        }
        return null;
    }
}
