package cz.atria.dataAggregator.core.lng;

import java.util.List;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
public interface LanguageService {
    <T> List<Language> detect(T content);
    List<String> getAvailableLanguages();
}
