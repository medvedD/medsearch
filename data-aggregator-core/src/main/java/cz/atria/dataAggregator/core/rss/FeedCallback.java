package cz.atria.dataAggregator.core.rss;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
public interface FeedCallback {
    void process(String link);
}
