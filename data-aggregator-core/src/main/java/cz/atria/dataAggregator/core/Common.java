package cz.atria.dataAggregator.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.http.HttpHeaders;
import org.apache.http.client.utils.DateUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;

import cz.atria.dataAggregator.core.entity.URIAttributeConverter;

/**
 * @author rnuriev
 * @since 08.10.2015.
 */
public class Common {
    final static Logger log = LoggerFactory.getLogger(Common.class);
    /**todo: this is teporary, due to URL encoding\decoding issues*/
    public static URI normilize(URI uri) {
        if (uri == null) {
            return null;
        }
        String uriAsString = uri.toString();
        URI actualUri = null;
        if (uriAsString.contains("%3A%2F")) {
            try {
                actualUri = URI.create(URLDecoder.decode(uriAsString, URIAttributeConverter.defaultCharset));
            } catch (UnsupportedEncodingException e) {
                log.error("", e);
            }
        } else {
            actualUri = uri;
        }
        return actualUri;
    }

    public static InputStream get(URI uri) throws IOException {
        return get(uri, InputStream.class);
    }

    /**decode %20 codes to space chars*/
    public static String decodeSpace(String str) {
        if (StringUtils.isEmpty(str) || !str.contains("%20")) {
            return str;
        } else {
            return str.replaceAll("%20", " ");
        }
    }

    /**encode spaces to %20 codes*/
    public static String encodeSpace(String str) {
        if (StringUtils.isEmpty(str) || !str.contains(" ")) {
            return str;
        } else {
            return str.replaceAll(" ", "%20");
        }
    }

    public static final String DEFAULT_AGENT = "Mozilla/5.0";

    protected static <T> T getByHtmlUnit(URI uri, Class<T> asClass) {
        T res = null;
        final WebClient webClient = new WebClient(BrowserVersion.CHROME);
        try {
            final WebClientOptions options = webClient.getOptions();
            options.setCssEnabled(false);
            options.setAppletEnabled(false);
            final Page page = webClient.getPage(uri.toURL());
            if (page instanceof HtmlPage) {
                webClient.waitForBackgroundJavaScriptStartingBefore(SettingService.getSetting(Settings.WAIT_FOR_BACKGROUND_JAVASCRIPT_STARTING_BEFORE, Long.class, Settings.WAIT_FOR_BACKGROUND_JAVASCRIPT_STARTING_BEFORE_DEFAULT));
            }
            final String textContent = page instanceof HtmlPage ? ((HtmlPage)page).getBody().getTextContent() : page.getWebResponse().getContentAsString();
            if (StringUtils.isEmpty(textContent)) {
                log.warn("HtmlUnit extract textContent is empty for:{}", uri);
                return null;
            }
            final String pageEncoding = page instanceof HtmlPage ? ((HtmlPage)page).getPageEncoding() : page.getWebResponse().getContentCharset();
            if (String.class.isAssignableFrom(asClass)) {
                res = (T) textContent;
            } else if (InputStream.class.isAssignableFrom(asClass)) {
                if (StringUtils.isNotEmpty(pageEncoding)) {
                    res = (T) IOUtils.toInputStream(textContent, pageEncoding);
                } else {
                    res = (T) IOUtils.toInputStream(textContent);
                }
            } else if (AbstractContent.class.isAssignableFrom(asClass)) {
                String entityTag = null;
                String lastModifiedString = null;
                final List<NameValuePair> responseHeaders = page.getWebResponse().getResponseHeaders();
                for (NameValuePair header : responseHeaders) {
                    if (header.getName().equals(HttpHeaders.ETAG)) {
                        entityTag = header.getValue();
                    } else if (header.getName().equals(HttpHeaders.LAST_MODIFIED)) {
                        lastModifiedString = header.getValue();
                    }
                }
                AbstractContent contentObject = (AbstractContent) asClass.newInstance();
                contentObject.setEntityTag(entityTag);
                if (StringUtils.isNotEmpty(lastModifiedString)) {
                    contentObject.setLastModified(DateUtils.parseDate(lastModifiedString));
                }
                if (InputStream.class.isAssignableFrom(contentObject.getType())) {
                    if (StringUtils.isNotEmpty(pageEncoding)) {
                        contentObject.setContent(IOUtils.toInputStream(textContent, pageEncoding));
                    } else {
                        contentObject.setContent(IOUtils.toInputStream(textContent));
                    }
                } else if (String.class.isAssignableFrom(contentObject.getType())) {
                    contentObject.setContent(textContent);
                } else {
                    log.warn("{}:not supported content type:{}", Common.getMethodName(), contentObject.getType());
                }

            }
        } catch (Exception e) {
            log.error("", e);
        } finally {
            try {
                webClient.close();
            } catch (Exception e) {
                log.warn("", e);
            }
        }
        return res;
    }

    public static <T> T get(URI uri, Class<T> asClass) throws IOException {
        uri = cz.atria.dataAggregator.core.Common.normilize(uri);
        if (uri.getScheme().equals("file")) {
            File file = new File(uri);
            // todo: due to unresolved issue with some files with spaces in name: they cant view while highlighting
            if (!file.exists()) {
                log.warn("file not found by uri:{}", uri);
                if (uri.toString().contains(" ")) {
                    URI uri2 = URI.create(encodeSpace(uri.toString()));
                    file = new File(uri2);
                    if (file.exists()) {
                        uri = uri2;
                    } else {
                        log.warn("file not found also by:{}", uri2);
                    }
                }
            }
        }
        URLConnection urlConnection = uri.toURL().openConnection();
        urlConnection.addRequestProperty("User-Agent", DEFAULT_AGENT);
        String lastModifiedString = null;
        String entityTag = null;
        if (urlConnection instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP || responseCode == HttpURLConnection.HTTP_SEE_OTHER) {
                String location = urlConnection.getHeaderField("Location");
                uri = URI.create(location);
                urlConnection = uri.toURL().openConnection();
                urlConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
                lastModifiedString = urlConnection.getHeaderField(HttpHeaders.LAST_MODIFIED);
                entityTag = urlConnection.getHeaderField(HttpHeaders.ETAG);
            } else {
                lastModifiedString = urlConnection.getHeaderField(HttpHeaders.LAST_MODIFIED);
                entityTag = urlConnection.getHeaderField(HttpHeaders.ETAG);
            }
        }
        if (InputStream.class.isAssignableFrom(asClass)) {
            return (T) urlConnection.getInputStream();
        } else if (AbstractContent.class.isAssignableFrom(asClass)) {
            try {
                AbstractContent contentObject = (AbstractContent) asClass.newInstance();
                contentObject.setEntityTag(entityTag);
                if (StringUtils.isNotEmpty(lastModifiedString)) {
                    contentObject.setLastModified(DateUtils.parseDate(lastModifiedString));
                }
                if (InputStream.class.isAssignableFrom(contentObject.getType())) {
                    contentObject.setContent(urlConnection.getInputStream());
                } else {
                    log.warn("not supported content type(1):{}", contentObject.getType());
                }
                return (T) contentObject;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            log.warn("todo:not supported yet (2):{}", asClass);
            return null;
        }
    }

    public static String reflectionToString(Object o, String contentIfFail) {
        if (o == null) {
            return null;
        }
        try {
            return ToStringBuilder.reflectionToString(o);
        } catch (Exception e) {
            log.error("", e);
            return contentIfFail;
        }
    }

    public static String reflectionToString(Object o) {
        if (o == null) {
            return null;
        } else {
            return reflectionToString(o, o.getClass().getCanonicalName() + ":" + o.toString());
        }
    }

    /**extract media type from HTTP Content-Type header*/
    public static MediaType extractMediaType(String contentTypeHeader) {
        if (StringUtils.isEmpty(contentTypeHeader)) {
            return null;
        } else {
            String mediaTypeString = contentTypeHeader.contains(";") ? contentTypeHeader.substring(0, contentTypeHeader.indexOf(';')) : contentTypeHeader;
            return cz.atria.dataAggregator.core.entity.MediaType.parseMediaType(mediaTypeString);
        }
    }

    /**extract charset from HTTP Content-Type header*/
    public static Charset extractCharset(String contentTypeHeader) {
        if (StringUtils.isEmpty(contentTypeHeader)) {
            return null;
        } else {
            String charsetParamEntry = "charset=";
            String charsetString = contentTypeHeader.contains(charsetParamEntry) ?
                    contentTypeHeader.substring(contentTypeHeader.indexOf(charsetParamEntry) + charsetParamEntry.length())
                    : null;
            if (StringUtils.isNotEmpty(charsetString)) {
                return Charset.forName(charsetString);
            } else {
                return null;
            }
        }
    }

    public static String getMethodName() {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        if (stackTraceElements != null && stackTraceElements.length > 2) {
            return stackTraceElements[2].getMethodName();
        } else {
            return null;
        }
    }

    static boolean check(String uriStringCorrected) {
        URI uri = URI.create(uriStringCorrected.replaceAll(" ", "%20"));
        boolean isAppropriate = true;
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) uri.toURL().openConnection();
            urlConnection.addRequestProperty("User-Agent", DEFAULT_AGENT);
            int responseCode = urlConnection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                log.debug("uri={} response.code={}", uriStringCorrected, responseCode);
                isAppropriate = false;
            }
        } catch (IOException e) {
            log.warn("seems uri is not correct:{}", uriStringCorrected);
            isAppropriate = false;
        }
        return isAppropriate;
    }

    public static String guessUriSchemeIfAbsent(final String uriStr) {
        URI uri = URI.create(uriStr.replaceAll(" ", "%20"));
        String uriStringCorrected = uriStr;
        if (StringUtils.isEmpty(uri.getScheme())) {
            uriStringCorrected = "http://" + uriStr;
            if (!check(uriStringCorrected)) {
                uriStringCorrected = "https://" + uriStr;
                if (!check(uriStringCorrected)) {
                    log.error("can't detect scheme for:{}", uriStr);
                    uriStringCorrected = uriStr;
                }
            }
        }
        return uriStringCorrected;
    }

    public static String getEntityName(Class entityClass) {
        if (entityClass == null) {
            return null;
        }
        String name = entityClass.getSimpleName();
        if (entityClass.getAnnotation(Entity.class) != null && StringUtils.isNotEmpty(((Entity) entityClass.getAnnotation(Entity.class)).name())) {
            name = ((Entity) entityClass.getAnnotation(Entity.class)).name();
        }
        return name;
    }

    public static void clone(InputStream in, InputStream[] out) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            IOUtils.copy(in, bos);
            for (int i = 0; i < out.length; i++) {
                out[i] = new ByteArrayInputStream(bos.toByteArray());
            }
        } catch (IOException e) {
            log.error("", e);
        }
    }

    public static <T> T extractBody(T content, Charset charset) {
        if (content == null) {
            return null;
        }
        T res = content;
        Document doc = null;
        if (content instanceof String) {
            doc = Jsoup.parse((String) content);
        } else if (content instanceof InputStream) {
            try {
                doc = Jsoup.parse((InputStream) content, charset != null ? charset.name() : Charset.defaultCharset().name(), "");
            } catch (Exception e) {
                log.error("", e);
            }
        } else {
            logNotSupported(Common.getMethodName(), log, content.getClass());
        }
        if (doc == null) {
            log.error("can't extract body");
        } else {
            final String text = doc.body().text();
            if (content instanceof String) {
                res = (T) text;
            } else if (content instanceof InputStream) {
                if (charset != null) {
                    res = (T) IOUtils.toInputStream(text, charset);
                } else {
                    res = (T) IOUtils.toInputStream(text);
                }
            } else {
                logNotSupported(Common.getMethodName(), log, content.getClass());
            }
        }
        return content;
    }

    public static void logNotSupported(String methName, Logger log1, Class c) {
        log1.warn("{}:not supported yet:{}", methName, c.getCanonicalName());
    }

    public static String getTabName (Class c) {
        final Annotation annotation = c.getAnnotation(Table.class);
        if (annotation != null) {
            Table table = (Table) annotation;
            if (StringUtils.isEmpty(table.name())) {
                return c.getSimpleName();
            } else {
                return table.name();
            }
        } else {
            return c.getSimpleName();
        }
    }
}
