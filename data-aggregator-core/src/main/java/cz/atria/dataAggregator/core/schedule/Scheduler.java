package cz.atria.dataAggregator.core.schedule;

import java.io.IOException;
import java.util.Collection;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.spider.Indicator;

/**
 * @author rnuriev
 * @since 01.08.2015.
 */
public interface Scheduler {

    void indexDocuments();

    void cacheDocuments();

    @Transactional (propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    void readRss();

    void clearIndex();

    void removeOrphanUploadedFiles();

    void setDocumentsTitle();

    void detectMediaType();

    /**update documents (cache, index), if changes detected*/
    void updateDocuments();

    /**<a href = "http://intra.dicomresearch.com/issues/1036">#1036</a><p></p>
     * update sources (crawler assumed)*/
    Collection<Indicator> updateSources();
    Indicator updateSource(Integer sourceId);
    void updateDocument(Integer documentId) throws IOException;
    String stat();
    void removeOrphanedCaches();
}
