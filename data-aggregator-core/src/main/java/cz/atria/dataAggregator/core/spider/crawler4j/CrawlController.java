package cz.atria.dataAggregator.core.spider.crawler4j;

import java.net.URI;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cz.atria.dataAggregator.core.SettingService;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * @author rnuriev
 * @since 29.07.2015.
 */
@Service
public class CrawlController {
    public static final String MAX_PAGES_TO_FETCH = "cz.atria.dataAggregator.core.spider.maxPagesToFetch";
    public static final String MAX_DEPTH_OF_CRAWLING = "cz.atria.dataAggregator.core.spider.maxDepthOfCrawling";
    public static final String NUMBER_OF_CRAWLERS = "cz.atria.dataAggregator.core.spider.numberOfCrawlers";
    final Logger log = LoggerFactory.getLogger(getClass());
    public edu.uci.ics.crawler4j.crawler.CrawlController process(String folder, URI baseUri, BasicCrawler.PageCallback pageCallback) {
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(folder);
        config.setPolitenessDelay(3000);
        config.setMaxDepthOfCrawling(SettingService.getSetting(MAX_DEPTH_OF_CRAWLING, Integer.class, 5));
        config.setMaxPagesToFetch(SettingService.getSetting(MAX_PAGES_TO_FETCH, Integer.class, 100));
        config.setIncludeBinaryContentInCrawling(false);
        config.setResumableCrawling(false);
        
// if there is problem with Exception in thread "main" java.lang.NoSuchFieldError: INSTANCE at org.apache.http.conn.ssl.SSLConnectionSocketFactory.<clinit>(SSLConnectionSocketFactory.java:144)
// this 3 lines of code show which library is used instead httpcore-4.4.3
//        ClassLoader classLoader = CrawlController.class.getClassLoader();
//        URL resource = classLoader.getResource("org/apache/http/message/BasicLineFormatter.class");
//        System.out.println(resource);
        
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        edu.uci.ics.crawler4j.crawler.CrawlController controller = null;
        try {
            controller = new edu.uci.ics.crawler4j.crawler.CrawlController(config, pageFetcher, robotstxtServer);
            String baseUriAsString = baseUri.toString();
            String baseUriWoPath = null;
            if (StringUtils.isNotEmpty(baseUri.getPath()) && !baseUri.getPath().equals("/")) {
                baseUriWoPath = baseUriAsString.substring(0, baseUriAsString.indexOf(baseUri.getPath()));
            } else {
                baseUriWoPath = baseUriAsString;
            }
            controller.setCustomData(new CustomData(baseUriWoPath, pageCallback));
            controller.addSeed(baseUri.toString());
            controller.startNonBlocking(BasicCrawler.class, SettingService.getSetting(NUMBER_OF_CRAWLERS, Integer.class, 3));
        } catch (Exception e) {
            log.error("", e);
        }
        return controller;
    }
}
