package cz.atria.dataAggregator.core.lng;

/**
 * @author rnuriev
 * @since 25.09.2015.
 */
public class Language {
    String name;
    double prob;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getProb() {
        return prob;
    }

    public void setProb(double prob) {
        this.prob = prob;
    }

    public Language() {
    }

    public Language(String name, double prob) {
        this.name = name;
        this.prob = prob;
    }

    @Override
    public String toString() {
        return this.name == null?"":this.name + ":" + this.prob;
    }
}
