package cz.atria.dataAggregator.core.lng;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
@Service
public class LanguageServiceCybozuImpl implements LanguageService {
    final Logger log = LoggerFactory.getLogger(getClass());
    final static List<String> availableLanguages = new ArrayList<>();
    final static List<String> defaultAvailableLanguages = Arrays.asList("en", "de", "ru");
    public LanguageServiceCybozuImpl() {
        try {
            if (DetectorFactory.getLangList() == null || DetectorFactory.getLangList().isEmpty()) {
                final String profilesDirPath = "lng/profiles";

                File profilesDir = null;
                URL profilesDirUrl = this.getClass().getClassLoader().getResource(profilesDirPath);
                if (profilesDirUrl != null) {
                    profilesDir = new File(profilesDirUrl.toURI());
                } else {
                    profilesDir = new File(profilesDirPath);
                }
                DetectorFactory.loadProfile(profilesDir);
                if (profilesDir.exists() && profilesDir.isDirectory() && profilesDir.listFiles().length > 0) {
                    for (String name : profilesDir.list()) {
                        availableLanguages.add(name);
                    }
                } else {
                    availableLanguages.addAll(defaultAvailableLanguages);
                    log.error("language profiles directory doesn't exists or is not directory or is empty:{}. Default used:{}.", profilesDirPath, defaultAvailableLanguages);
                }
            }
            DetectorFactory.setSeed(0);
        } catch (Exception e) {
            log.warn("", e);
        }
    }

    @Override
    public <T> List<Language> detect(T content) {
        try {
            Detector detector = DetectorFactory.create();
            if (content instanceof String) {
                detector.append((String) content);
            } else if (content instanceof Reader) {
                detector.append((Reader) content);
            }
            List<com.cybozu.labs.langdetect.Language> probabilities = detector.getProbabilities();
            List<Language> res = new ArrayList<>();
            for (com.cybozu.labs.langdetect.Language language : probabilities) {
                res.add(new Language(language.lang, language.prob));
            }
            return res;
        } catch (LangDetectException e) {
            log.error("", e);
            return Collections.emptyList();
        } catch (IOException e) {
            log.warn("", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> getAvailableLanguages() {
        return availableLanguages;
    }
}
