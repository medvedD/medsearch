package cz.atria.dataAggregator.core.db;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.entity.Document;

/**
 * @author rnuriev
 * @since 05.12.2015.
 */
@Component
public class DocumentJdbcInsert extends SimpleJdbcInsert {
    final SimpleJdbcInsert simpleJdbcInsertDocumentSource;
    @Autowired
    public DocumentJdbcInsert(DataSource dataSource) {
        super(dataSource);
        withTableName(Common.getTabName(Document.class));
        usingGeneratedKeyColumns("id");
        simpleJdbcInsertDocumentSource = new SimpleJdbcInsert(dataSource).withTableName("document_source");
    }

    @Transactional (propagation = Propagation.REQUIRES_NEW)
    public void execute(Document doc, Integer sourceId) {
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(doc);
        final Number id = executeAndReturnKey(parameters);
        doc.setId((Integer) id);
        final Map<String, Object> args = new HashMap<>();
        args.put("document_id", id);
        args.put("source_id", sourceId);
        simpleJdbcInsertDocumentSource.execute(args);
    }

}
