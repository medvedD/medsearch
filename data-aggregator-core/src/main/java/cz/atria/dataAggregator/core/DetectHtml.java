package cz.atria.dataAggregator.core;

import java.io.InputStream;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * /**
 * Detect HTML markup in a string
 * This will detect tags or entities
 *
 * @author dbennett455@gmail.com - David H. Bennett
 */
public class DetectHtml {
    final static Logger log = LoggerFactory.getLogger(DetectHtml.class);

    // adapted from post by Phil Haack and modified to match better
    public final static String tagStart =
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)\\>";
    public final static String tagEnd =
            "\\</\\w+\\>";
    public final static String tagSelfClosing =
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)/\\>";
    public final static String htmlEntity =
            "&[a-zA-Z][a-zA-Z0-9]+;";
    public final static Pattern htmlPattern = Pattern.compile(
            "(" + tagStart + ".*" + tagEnd + ")|(" + tagSelfClosing + ")|(" + htmlEntity + ")",
            Pattern.DOTALL
    );

    /**
     * Will return true if s contains HTML markup tags or entities.
     *
     * @param s String to test
     * @return true if string contains HTML
     */
    public static <T> boolean isHtml(T s) {
        boolean ret = false;
        if (s != null) {
            if (s instanceof String) {
                ret = htmlPattern.matcher((String) s).find();
            } else if (s instanceof InputStream) {
                try {
                    ret = htmlPattern.matcher(IOUtils.toString((InputStream) s)).find();
                } catch (Exception e) {
                    log.error("", e);
                }
            } else {
                log.warn("{}:not supported yet:{}", Common.getMethodName(), s.getClass().getCanonicalName());
            }
        }
        return ret;
    }

}