package cz.atria.dataAggregator.core;

import java.lang.reflect.ParameterizedType;
import java.util.Date;

/**Document content itself and HTTP related data, like headers, obtained by single request.
 * Used for exclude duplicate requests.
 * @author rnuriev
 * @since 09.11.2015.
 */
public abstract class AbstractContent<T> implements Content<T> {
    T content;
    Date lastModified;
    String entityTag;
    // todo: add contentType

    @Override
    public T getContent() {
        return content;
    }

    @Override
    public void setContent(T content) {
        this.content = content;
    }

    @Override
    public Date getLastModified() {
        return lastModified;
    }

    @Override
    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String getEntityTag() {
        return entityTag;
    }

    @Override
    public void setEntityTag(String entityTag) {
        this.entityTag = entityTag;
    }

    @Override
    public Class<T> getType() {
        return (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
}
