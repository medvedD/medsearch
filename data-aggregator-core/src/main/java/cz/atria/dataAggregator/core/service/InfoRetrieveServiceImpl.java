package cz.atria.dataAggregator.core.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.NumericRangeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.Synonym;
import cz.atria.dataAggregator.core.entity.Term;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
@Service
public class InfoRetrieveServiceImpl implements InfoRetrieveService {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    EntityService entityService;
    @Autowired
    DocumentService documentService;
    @Autowired
    NLPService nlpService;

    @Override
    public SortedSet<SearchResultEntry> retrieve(Collection<Source> sources, Collection<Term> terms) {
        Object query = createQuery(terms);
        return retrieve(query);
    }

    /**
     * it is reverse comparator
     */
    final Comparator<SearchResultEntry> searchResultEntryComparator = new Comparator<SearchResultEntry>() {
        @Override
        public int compare(SearchResultEntry o1, SearchResultEntry o2) {
            int res = 0;
            if (o1 == null && o2 != null) {
                res = 1;
            } else if (o1 != null && o2 == null) {
                res = -1;
            } else if (o1 != null && o2 != null) {
                if (o1.getTotalScore() == o2.getTotalScore()) {
                    res = -1;    // entries are equal, but we need inclusion of both of them
                } else if (o1.getTotalScore() > o2.getTotalScore()) {
                    res = -1;
                } else {
                    res = 1;
                }
            }
            return res;
        }
    };

    @Override
    public SortedSet<SearchResultEntry> retrieve(Collection<Source> sources, Object query) {
        Map<String, Object> queries = new HashMap<>();
        queries.put(Locale.ENGLISH.getLanguage(), query);
        return retrieve(sources, queries);
    }

    @Override
    public SortedSet<SearchResultEntry> retrieve(Collection<Source> sources, Map<String, Object> queries) {
        List<SearchResultEntry> searchResultEntries = new ArrayList<>();
        Map<String, Object> queriesWithFilter = queries;
        if (sources != null && !sources.isEmpty()) {
            BooleanQuery sourcesQuery = new BooleanQuery();
            for (Source source : sources) {
                sourcesQuery.add(NumericRangeQuery.newIntRange(NLPServiceLuceneImpl.fieldNameSourceId, source.getId(), source.getId(), true, true), BooleanClause.Occur.SHOULD);
            }
            queriesWithFilter = new HashMap<>();
            for (Map.Entry<String, Object> queryEntry : queries.entrySet()) {
                BooleanQuery queryWithFilter = new BooleanQuery.Builder()
                        .add(sourcesQuery, BooleanClause.Occur.FILTER)
                        .add((org.apache.lucene.search.Query) queryEntry.getValue(), BooleanClause.Occur.MUST)
                        .build();
                queriesWithFilter.put(queryEntry.getKey(), queryWithFilter);
            }
        }

        Map<Integer, SortedMap<String, Float>> matchesAll = nlpService.matchesAll(queriesWithFilter);
        Collection ids = matchesAll.keySet();
        List<Document> documents;
        if (!ids.isEmpty()) {
            documents = entityService.get(Document.class, ids);
        } else {
            documents = new ArrayList<>();
        }
        Map<Integer, Document> documentMap = new HashMap<>();
        for (Document document : documents) {
            documentMap.put(document.getId(), document);
        }
        for (Map.Entry<Integer, SortedMap<String, Float>> matchesEntry : matchesAll.entrySet()) {
            Document document = documentMap.get(matchesEntry.getKey());
            if (document != null) {
                Float score = null;
                Object query = null;
                String lang = null;
                for (Map.Entry<String, Float> scoreEntry : matchesEntry.getValue().entrySet()) {
                    score = scoreEntry.getValue();
                    query = queries.get(scoreEntry.getKey());
                    lang = scoreEntry.getKey();
                    break;  // assumed, that the best score is first, and it's used only.
                }
                searchResultEntries.add(new SearchResultEntry(document, score, query, lang));
            }
        }

        TreeSet<SearchResultEntry> searchResultEntriesSorted = new TreeSet<>(searchResultEntryComparator);
        searchResultEntriesSorted.addAll(searchResultEntries);
        return searchResultEntriesSorted;
    }

    @Override
    public SortedSet<SearchResultEntry> retrieve(Collection<Term> terms) {
        Collection<Source> sources = entityService.get(Source.class);
        return retrieve(sources, terms);
    }

    @Override
    public SortedSet<SearchResultEntry> retrieve(Object query) {
        return retrieve(entityService.get(Source.class), query);
    }

    @Override
    public SortedSet<SearchResultEntry> retrieve(Map<String, Object> queries) {
        return retrieve(null, queries);
    }

    @Override
    public String highlight(Document document, String query) {
        return nlpService.highlight(document, query, String.class);
    }

    @Override
    public String highlight(Document document, Object query) {
        return nlpService.highlight(document, query, String.class);
    }

    @Override
    public String highlight(Integer documentId, Object query) {
        return highlight(entityService.get(Document.class, documentId), query);
    }

    @Override
    public String highlight(URI uri, Object query) {
        return nlpService.highlight(uri, query);
    }

    @Override
    public Object createQuery(String queryAsString) {
        return nlpService.createQuery(queryAsString);
    }

    protected void addSynonyms(Map<String, List<String>> termWithSynonyms, Term term) {
        if (!termWithSynonyms.containsKey(term.getName())) {
            termWithSynonyms.put(term.getName(), new ArrayList<String>());
        }
        List<Synonym> synonyms = entityService.getSynonyms(term);
        for (Synonym synonym : synonyms) {
            if (!termWithSynonyms.get(term.getName()).contains(synonym.getName())) {
                termWithSynonyms.get(term.getName()).add(synonym.getName());
            }
        }
        List<Term> children = entityService.getChildren(term);
        if (!children.isEmpty()) {
            for (Term termChild : children) {
                addSynonyms(termWithSynonyms, termChild);
            }
        }
    }

    @Override
    public Object createQuery(Collection<Term> terms) {
        Map<String, List<String>> termWithSynonyms = new HashMap<>();
        for (Term term : terms) {
            addSynonyms(termWithSynonyms, term);
        }
        return nlpService.createConjuctionQuery(termWithSynonyms);
    }

    public void setEntityService(EntityService entityService) {
        this.entityService = entityService;
    }

    public void setDocumentService(DocumentService documentService) {
        this.documentService = documentService;
    }

    public void setNlpService(NLPService nlpService) {
        this.nlpService = nlpService;
    }
}
