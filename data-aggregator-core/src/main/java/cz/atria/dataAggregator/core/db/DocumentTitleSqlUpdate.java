package cz.atria.dataAggregator.core.db;

import java.sql.Types;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author rnuriev
 * @since 04.12.2015.
 */
@Component
public class DocumentTitleSqlUpdate extends SqlUpdate {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    public DocumentTitleSqlUpdate(DataSource ds) {
        super(ds, "update document set title = ? where id = ?");
        declareParameter(new SqlParameter("title", Types.VARCHAR));
        declareParameter(new SqlParameter("id", Types.INTEGER));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void execute(String title, Integer id) {
        final int updateRes = update(title, id);
        if (updateRes == 0) {
            log.warn("document not updated:id={}", id);
        }
    }
}
