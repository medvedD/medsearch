package cz.atria.dataAggregator.core.convert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

/**
 * @author rnuriev
 * @since 14.09.2015.
 */
@Service
public class ConvertDocxServiceImpl implements ConvertService {
    @Autowired
    @Qualifier (ConvertXHTMLServiceImpl.NAME)
    ConvertService convertXhtmlService;

    @Override
    public <T> InputStream convert(T content) {
        try {
            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
            XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);
            wordMLPackage.getMainDocumentPart().getContent().addAll(XHTMLImporter.convert(convertXhtmlService.convert(content), null));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            wordMLPackage.save(bos);
            return new ByteArrayInputStream(bos.toByteArray());
        } catch (Docx4JException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public MediaType getSupportedMediaType() {
        return cz.atria.dataAggregator.core.entity.MediaType.OOXML_DOCUMENT;
    }
}
