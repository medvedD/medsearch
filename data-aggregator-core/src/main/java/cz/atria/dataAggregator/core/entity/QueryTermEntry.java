package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author rnuriev
 * @since 01.09.2015.
 */
@Entity
@Table(name = "query_term_entry")
public class QueryTermEntry implements Serializable {
    @Id
    @GeneratedValue
    Integer id;

    @ManyToOne
    @JoinColumn(nullable = true)
    TermElement termElement;

    @Column(name = "is_synonym")
    Boolean isSynonym;

    @Column(name = "is_children")
    Boolean isChildren;

    @ManyToOne
    @JoinColumn (nullable = false, name = "query_id", foreignKey = @ForeignKey (name = "query_term_entry_query_fk", foreignKeyDefinition = "foreign key (query_id) references query(id) on delete cascade"))
    Query query;

    Integer pos;

    @Enumerated (EnumType.STRING)
    Operator operator;

    String searchPhrase;

    public QueryTermEntry() {
    }

    public QueryTermEntry(TermElement termElement, Boolean isSynonym, Boolean isChildren, Query query, Integer pos, Operator operator) {
        this(termElement, isSynonym, isChildren, null, query, pos, operator);
    }

    public QueryTermEntry(TermElement termElement, Boolean isSynonym, Boolean isChildren, String searchPhrase, Query query, Integer pos, Operator operator) {
        this.termElement = termElement;
        this.isSynonym = isSynonym;
        this.isChildren = isChildren;
        this.query = query;
        this.pos = pos;
        this.operator = operator;
        this.searchPhrase = searchPhrase;
    }

    public QueryTermEntry(String searchPhrase, Query query, Integer pos, Operator operator) {
        this(null, null, null, searchPhrase, query, pos, operator);
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TermElement getTermElement() {
        return termElement;
    }

    public void setTermElement(TermElement termElement) {
        this.termElement = termElement;
    }

    public Boolean getIsSynonym() {
        return isSynonym;
    }

    public void setIsSynonym(Boolean isSynonym) {
        this.isSynonym = isSynonym;
    }

    public Boolean getIsChildren() {
        return isChildren;
    }

    public void setIsChildren(Boolean isChildren) {
        this.isChildren = isChildren;
    }
}
