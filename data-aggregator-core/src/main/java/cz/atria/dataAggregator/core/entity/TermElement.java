package cz.atria.dataAggregator.core.entity;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**this is replacement for vocabulary\term model, according to new requirements (see: <a href = "http://intra.dicomresearch.com/issues/313">#313</a>)
 * @author rnuriev
 * @since 12.08.2015.
 */
@Entity
@Table (name = "term_element")
public class TermElement implements Serializable {
    @Id
    @GeneratedValue
    Integer id;

    @ManyToOne (cascade = CascadeType.REMOVE)
    TermElement parent;

    @Column (nullable = false)
    String code;

    @Column (nullable = false)
    String name;

    @Column (length = 255)
    String description;

    @OneToMany (targetEntity = TermElementSynonym.class, mappedBy = "termElement", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(value= FetchMode.JOIN)
    List<TermElementSynonym> termElementSynonyms;

    public TermElement() {
    }

    public List<TermElementSynonym> getTermElementSynonyms() {
        return termElementSynonyms;
    }

    public void setTermElementSynonyms(List<TermElementSynonym> termElementSynonyms) {
        this.termElementSynonyms = termElementSynonyms;
    }

    public TermElement(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public TermElement(TermElement parent, String code, String name) {
        this.parent = parent;
        this.code = code;
        this.name = name;
    }

    public TermElement(TermElement parent, String code, String name, String description) {
        this.parent = parent;
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TermElement getParent() {
        return parent;
    }

    public void setParent(TermElement parent) {
        this.parent = parent;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Transient
    final Iterator iterator = new Iterator() {
        int i = 0;

        @Override
        public boolean hasNext() {
            boolean res = i < termElementSynonyms.size();
            if (!res) {
                i = 0;
            }
            return res;
        }

        @Override
        public Object next() {
            return termElementSynonyms.get(i++).getName();
        }

        @Override
        public void remove() {

        }
    };

    @Transient
    public String getSynonymNames() {
        if (getTermElementSynonyms() == null || getTermElementSynonyms().isEmpty()) {
            return null;
        } else {
            return StringUtils.join(iterator, ",");
        }
    }
}
