package cz.atria.dataAggregator.core.charset;

import java.net.URI;
import java.nio.charset.Charset;

/**
 * @author rnuriev
 * @since 30.09.2015.
 */
public interface CharsetService {
    Charset detect(URI uri);
    <T> Charset detect(T content);
}
