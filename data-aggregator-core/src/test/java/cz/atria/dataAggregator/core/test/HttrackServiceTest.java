package cz.atria.dataAggregator.core.test;

import org.junit.Assert;
import org.junit.Test;

import cz.atria.dataAggregator.core.service.HttrackService;

/**
 * @author rnuriev
 * @since 27.11.2015.
 */
public class HttrackServiceTest {
    HttrackService httrackService = new HttrackService();

    /**<a href="http://intra.dicomresearch.com/issues/1512">#1512</a>
     * */
    @Test
    public void testPathDetectionWithRedirection() throws Exception {
        String savePath = this.getClass().getClassLoader().getResource("samples/bf/1512/").toURI().toString();
        savePath = savePath.replaceAll("\\\\", "/");
        String path = httrackService.getPath(savePath);
        Assert.assertEquals("copy/productforums.google.com//d/topic/google-plus-diskussionsforum/DD6n7v1fqUQ/productforums.google.com/forum/#!topic/google-plus-diskussionsforum/DD6n7v1fqUQ.html", path);
    }
}
