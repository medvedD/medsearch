package cz.atria.dataAggregator.core.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.common.datagenerator.jpa.ValueProvider;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
public class FixtureImpl implements Fixture {
    @Autowired
    ValueProvider vpq;
    @Autowired
    cz.atria.common.datagenerator.ValueProvider vp;
    @Autowired
    @Qualifier("emTest")
    EntityManager em;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public <T> T get(Class<T> asClass) {
        T o = vpq.get(asClass);
        persist(o);
        return o;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public <T> void persist(T o) {
        em.getTransaction().begin();
        em.persist(o);
        em.getTransaction().commit();
    }

    @Override
    public Document createDocument() {
        return createDocument(vp.get(String.class));
    }

    @Override
    public Source createSource() {
        return createSource((File) null);
    }

    @Override
    public Source createSource(URI uri) {
        Source source = vpq.get(Source.class);
        source.setDocuments(null);
        source.setUri(uri);
        source.setMediaType(null);
        source.setType(null);
        persist(source);
        return source;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Source createSource(File uploadedDir) {
        try {
            Path dagg = createFile(uploadedDir);
            return createSource(dagg.toUri());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected Path createFile(File parentdir) throws IOException {
        return createFile(vp.get(String.class), parentdir);
    }

    protected Path createFile() throws IOException {
        return createFile(vp.get(String.class));
    }

    protected Path createFile(String content) throws IOException {
        return createFile(content, null);
    }

    protected Path createFile(String content, File parentDir) throws IOException {
        Path dagg = null;
        if (parentDir != null) {
            dagg = Files.createTempFile(parentDir.toPath(), "dagg", null);
        } else {
            dagg = Files.createTempFile("dagg", null);
        }
        FileOutputStream fos = new FileOutputStream(dagg.toFile());
        fos.write(content.getBytes());
        fos.flush();
        fos.close();
        return dagg;
    }

    @Override
    public Document createDocument(String content) {
        Source source = vpq.find(Source.class);
        if (source == null) {
            source = createSource();
        }
        return createDocument(source, content);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Document createDocument(URI uri) {
        Source source = vpq.find(Source.class);
        if (source == null) {
            source = createSource();
        }
        return createDocument(source, null, uri);
    }

    public Document createDocument(Source source, String content) {
        return createDocument(source, content, null);
    }

    protected Document createDocument(Source source, String content, URI uri) {
        try {
            Path dagg = null;
            if (content != null) {
                dagg = createFile(content);
            }
            Document document = vpq.get(Document.class);
            if (source != null) {
                boolean isAdded = false;
                if (document.getSources() != null) {
                    for (Source source1 : document.getSources()) {
                        if (isAdded = source.getId().equals(source1.getId())) {
                            break;
                        }
                    }
                } else {
                    document.setSources(new ArrayList<Source>());
                }
                if (!isAdded) {
                    document.getSources().add(source);
                }
            }
            document.setMediaType(MediaType.TEXT_HTML);
            document.setCacheUri(dagg != null ? dagg.toUri() : null);
            document.setIsIndexed(false);
            document.setUri(dagg != null ? dagg.toUri() : uri);
            persist(document);
            return document;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
