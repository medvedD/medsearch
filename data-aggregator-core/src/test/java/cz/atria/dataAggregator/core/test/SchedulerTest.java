package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.SerializationUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.utils.DateUtils;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.io.Files;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import cz.atria.common.container.web.WebServerBase;
import cz.atria.common.datagenerator.ValueProvider;
import cz.atria.dataAggregator.core.DependencyAccessor;
import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.core.Settings;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.lng.LanguageService;
import cz.atria.dataAggregator.core.schedule.Scheduler;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.InfoRetrieveService;
import cz.atria.dataAggregator.core.service.MediaTypeService;
import cz.atria.dataAggregator.core.service.NLPService;
import cz.atria.dataAggregator.core.service.NLPServiceLuceneImpl;
import cz.atria.dataAggregator.core.service.SearchResultEntry;
import cz.atria.dataAggregator.core.spider.Indicator;
import cz.atria.dataAggregator.core.spider.crawler4j.CrawlController;

/**
 * @author rnuriev
 * @since 13.10.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-test-common-context.xml"
})
@Transactional
public class SchedulerTest {
    @Autowired
    Fixture fixture;
    @Autowired
    Scheduler scheduler;
    @Autowired
    EntityService entityService;

    @Test
    public void testIndexDocuments() throws Exception {
        Document document = fixture.createDocument();
        Assert.assertTrue(document.getIsIndexed() == null || !document.getIsIndexed());
        scheduler.indexDocuments();
        document = entityService.get(Document.class, document.getId());
        Assert.assertTrue(document.getIsIndexed());
    }

    @Test
    public void testCacheDocuments() throws Exception {
        Document document = fixture.createDocument();
        document.setCacheUri(null);
        entityService.save(document);
        scheduler.cacheDocuments();
        document = entityService.get(Document.class, document.getId());
        Assert.assertNotNull(document.getCacheUri());
    }

    @Test
    public void testRemoveOrphanUploadedFiles() throws Exception {
        String prop = System.getProperty(Settings.UPLOAD_DIR_PROP);
        try {
            File uploadedDir = Files.createTempDir();
            System.setProperty(Settings.UPLOAD_DIR_PROP, uploadedDir.getPath());
            final Source source1 = fixture.createSource(uploadedDir);
            final Source source2 = fixture.createSource(uploadedDir);
            entityService.remove(source2);
            scheduler.removeOrphanUploadedFiles();
            Assert.assertTrue(source1.getUri().toString(), new File(source1.getUri()).exists());
            Assert.assertFalse(source2.getUri().toString(), new File(source2.getUri()).exists());
        } finally {
            if (prop != null) {
                System.setProperty(Settings.UPLOAD_DIR_PROP, prop);
            } else {
                System.clearProperty(Settings.UPLOAD_DIR_PROP);
            }
        }
    }

    @Autowired
    ValueProvider vp;

    @Test
    public void testSetUndefinedTitles() throws Exception {
        final String title = vp.get(String.class);
        final int port = WebServerBase.getFreePort();
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        final String contextPath = "test";
        server.createContext("/" + contextPath, new HttpHandler() {
            @Override
            public void handle(HttpExchange httpExchange) throws IOException {
                String response = String.format("<html><title>%1$s</title><body></body></html", title);
                httpExchange.sendResponseHeaders(200, response.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(response.getBytes());
                os.close();
            }
        });
        server.setExecutor(null);
        server.start();

        final Document documentWithOkUri = createDocumentForTitleCheck(String.format("http://127.0.0.1:%1$d/%2$s", port, contextPath));
        final Document documentWithWrongUri = createDocumentForTitleCheck("http://" + vp.get(String.class));

        scheduler.setDocumentsTitle();
        assertEquals(title, entityService.getA(Document.class, documentWithOkUri.getId()).getTitle());
        Assert.assertNull(entityService.getA(Document.class, documentWithWrongUri.getId()).getTitle());
    }

    @Test
    public void testDetectMediaType() throws Exception {
        final Document document = fixture.createDocument();
        document.setMediaType(null);
        entityService.save(document);
        final MediaTypeService mediaTypeService = ((DependencyAccessor) scheduler).getMediaTypeService();
        try {
            MediaTypeService mockMediaTypeService = Mockito.mock(MediaTypeService.class);
            final MediaType mediaType = MediaType.TEXT_PLAIN;
            when(mockMediaTypeService.detectMediaType(any(URI.class), any(Class.class))).thenReturn(mediaType);
            ((DependencyAccessor) scheduler).setMediaTypeService(mockMediaTypeService);
            scheduler.detectMediaType();
            assertEquals(mediaType, entityService.get(Document.class, document.getId()).getMediaType());
        } finally {
            ((DependencyAccessor) scheduler).setMediaTypeService(mediaTypeService);
        }
    }

    @Autowired
    InfoRetrieveService infoRetrieveService;
    @Autowired
    NLPService nlpService;
    @PersistenceContext
    EntityManager em;
    /**
     * with *Modified headers, and document changed
     */
    @Test
    public void testUpdateDocumentWithModifiedHeadersSupportAndDocumentChanged() throws Exception {
        final int port = WebServerBase.getFreePort();
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        final String contextPath = "test";
        final String firstVersionWord = "first";
        final String secondVersionWord = "second";
        final String context = "/" + contextPath;
        final boolean isFirst[] = new boolean[1];
        isFirst[0] = true;
        final Date lasModified2 = new Date();
        final Date lasModified1 = vp.getDateBefore(lasModified2);
        server.createContext(context, new HttpHandler() {

            protected void send(HttpExchange httpExchange, String content, Date lastModified) throws IOException {
                Headers responseHeaders = httpExchange.getResponseHeaders();
                responseHeaders.put(HttpHeaders.ETAG, Arrays.asList(content.hashCode() + ""));
                responseHeaders.put(HttpHeaders.LAST_MODIFIED, Arrays.asList(DateUtils.formatDate(lastModified)));
                httpExchange.sendResponseHeaders(200, content.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(content.getBytes());
                os.close();
            }

            @Override
            public void handle(HttpExchange httpExchange) throws IOException {
                final String contentTemplate = "<html><body>%1$s content</body></html>";
                final String content1 = String.format(contentTemplate, firstVersionWord);
                final String content2 = String.format(contentTemplate, secondVersionWord);
                if (httpExchange.getRequestHeaders().containsKey(HttpHeaders.IF_MODIFIED_SINCE) || httpExchange.getRequestHeaders().containsKey(HttpHeaders.IF_NONE_MATCH)) {
                    if (isFirst[0]) {
                        httpExchange.sendResponseHeaders(HttpStatus.SC_NOT_MODIFIED, 0);
                    } else {
                        send(httpExchange, content2, lasModified2);
                    }
                } else {
                    if (isFirst[0]) {
                        send(httpExchange, content1, lasModified1);
                    } else {
                        send(httpExchange, content2, lasModified2);
                    }
                }
            }
        });
        server.setExecutor(null);
        server.start();
        URI uri = URI.create(String.format("http://127.0.0.1:%1$d/%2$s", port, contextPath));
        Document document = fixture.createDocument(uri);
        scheduler.cacheDocuments();
        nlpService.indexThis(document.getId());
        document = entityService.get(Document.class, document.getId());
        final Document document1 = (Document) SerializationUtils.clone(document);
        final List<SearchTermEntry> searchTermEntries1 = new ArrayList<>();
        searchTermEntries1.add(new SearchTermEntry(firstVersionWord));
        final Map<String, Object> queries1 = nlpService.createQueries(searchTermEntries1, null);
        final SortedSet<SearchResultEntry> searchResult1 = infoRetrieveService.retrieve(Arrays.asList(document.getSource()), queries1);
        assertEquals(1, searchResult1.size());
        // 2.
        isFirst[0] = false;
        scheduler.updateDocuments();
        final Source source = document.getSource();
//        em.refresh(document);
        document = entityService.getA(Document.class, document.getId());
        final Document document2 = (Document) SerializationUtils.clone(document);
        assertNotEquals(document1.getCacheUri(), document2.getCacheUri());
        assertNotEquals(document1.getEntityTag(), document2.getEntityTag());
        assertNotEquals(document1.getLastModified(), document2.getLastModified());

        final SortedSet<SearchResultEntry> searchResult2a = infoRetrieveService.retrieve(Arrays.asList(source), queries1);
        assertEquals(0, searchResult2a.size());

        final List<SearchTermEntry> searchTermEntries2 = new ArrayList<>();
        searchTermEntries2.add(new SearchTermEntry(secondVersionWord));
        final Map<String, Object> queries2 = nlpService.createQueries(searchTermEntries2, null);
        final SortedSet<SearchResultEntry> searchResult2b = infoRetrieveService.retrieve(Arrays.asList(source), queries2);
        assertEquals(1, searchResult2b.size());

    }

    protected void updateSourcesWithWait() throws InterruptedException {
        Collection<Indicator> indicators = scheduler.updateSources();
        while (true) {
            boolean isFinished = true;
            for (Indicator indicator : indicators) {
                if (!indicator.isFinished()) {
                    isFinished = false;
                    break;
                }
            }
            if (isFinished) {
                break;
            } else {
                Thread.sleep(1000);
            }
        }
    }

    @Test
    public void testUpdateSources() throws Exception {
        SettingService.setSetting(CrawlController.NUMBER_OF_CRAWLERS, 1);
        final int port = WebServerBase.getFreePort();
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        final String contextPath = "test";
        final String firstDoc = vp.get(String.class);
        final String secondDoc = vp.get(String.class);
        final boolean isFirst[] = new boolean[] {true};
        server.createContext("/" + contextPath, new HttpHandler() {
            @Override
            public void handle(HttpExchange httpExchange) throws IOException {
                String path = httpExchange.getRequestURI().getPath();
                String content = null;
                if (path.contains(firstDoc)) {
                    content = "This is content of first document";
                } else if (path.contains(secondDoc)) {
                    content = "This is content of second document";
                } else {
                    final String linkTemplate = String.format("<a href='http://127.0.0.1:%2$d/%3$s/%1$s'>%1$s</a>", "%1$s", port, contextPath);
                    final String link1 = String.format(linkTemplate, firstDoc);
                    final String link2 = String.format(linkTemplate, secondDoc);
                    content = String.format("<html><body>%1$s<p>%2$s</body></html>", link1, isFirst[0] ? "" : link2);
                }
                httpExchange.sendResponseHeaders(200, content.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(content.getBytes());
                os.close();
            }
        });
        server.setExecutor(null);
        server.start();
        URI uri = URI.create(String.format("http://127.0.0.1:%1$d/%2$s", port, contextPath));
        Source source = fixture.createSource(uri);
        // execute
        updateSourcesWithWait();
        List<Document> documents = entityService.getDocuments(source);
        assertEquals(2, documents.size());
        // 2.
        isFirst[0] = false;
        updateSourcesWithWait();
        documents = entityService.getDocuments(source);
        assertEquals(3, documents.size());
    }

    @Autowired
    LanguageService languageService;
    @Before
    public void before() {
        if (nlpService instanceof NLPServiceLuceneImpl) {
            for (String name : languageService.getAvailableLanguages()) {
                ((NLPServiceLuceneImpl)nlpService).addDirectory(name, new RAMDirectory());  // fresh index for each test
            }
        }
    }

    protected Document createDocumentForTitleCheck(String uri) {
        final Document document = fixture.createDocument();
        document.setTitle(null);
        document.setUri(URI.create(uri));
        document.setCacheUri(null);
        entityService.save(document);
        return document;
    }
}
