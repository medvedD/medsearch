package cz.atria.dataAggregator.core.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author rnuriev
 * @since 07.10.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-test-common-context.xml"
})
@Transactional
public class GoogleAlertServiceTest {
//    @Autowired
//    ValueProvider vp;
//    @Autowired
//    GoogleAlertService<Alert> googleAlertService;
//
//    @Autowired
//    Fixture fixture;
//
//    @Autowired
//    cz.atria.common.datagenerator.jpa.ValueProvider vpq;
//
//    @PostConstruct
//    public void init() {
//        ((PropertyAccessor) googleAlertService).setGooAccount("dr.dataAggregator.test@gmail.com");
//        ((PropertyAccessor) googleAlertService).setPassword("qw12er34");
//    }
//
//    @Test
//    public void testLanguage() throws Exception {
//        String query = vp.get(String.class);
//        Alert alert = googleAlertService.createAlert(query, "", "RU", DeliveryTo.FEED);
//        alert = googleAlertService.getAlert(alert.getId());
//        assertEquals("", alert.getLanguage());
//    }
//
//    @Autowired
//    SourceService sourceService;
//
//    @Test
//    public void testSynchronizeCreateNew() throws Exception {
//        String query = vp.get(String.class);
//        Alert alert = googleAlertService.createAlert(query, "", "RU", DeliveryTo.FEED);
//        googleAlertService.synchronizeAlertList();
//        boolean isFound = false;
//        for (SourceDetail source : sourceService.getSourceDetailsByType(SourceType.GOOGLE_ALERT)) {
//            if (isFound = source.getExternalId().equals(alert.getId())) {
//                break;
//            }
//        }
//        assertTrue(isFound);
//    }
//
//    /**<a href = "http://intra.dicomresearch.com/issues/792">#792</a>*/
//    @Test
//    public void testAlertDeletionSync() throws Exception {
//        String query = getRandomWord();
//        final Alert alert = googleAlertService.createAlert(query, "", "EN", DeliveryTo.FEED);
//        googleAlertService.synchronizeAlertList();
//        googleAlertService.deleteAlert(alert.getId());
//        googleAlertService.synchronizeAlertList();
//        boolean isFound = false;
//        for (SourceDetail source : sourceService.getSourceDetailsByType(SourceType.GOOGLE_ALERT)) {
//            if (isFound = source.getExternalId().equals(alert.getId())) {
//                break;
//            }
//        }
//        assertFalse(isFound);
//    }
//
//    protected String getRandomWord() throws Exception {
//        HttpClient httpClient = HttpClients.createDefault();
//        HttpGet httpGet = new HttpGet("http://randomword.setgetgo.com/get.php");
//        HttpResponse httpResponse = httpClient.execute(httpGet);
//        return IOUtils.toString(httpResponse.getEntity().getContent());
//    }
}
