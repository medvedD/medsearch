package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import cz.atria.dataAggregator.core.lng.Language;
import cz.atria.dataAggregator.core.lng.LanguageService;
import cz.atria.dataAggregator.core.lng.LanguageServiceCybozuImpl;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
public class LanguageServiceTest {
    LanguageService languageService = new LanguageServiceCybozuImpl();


    protected void testSingleLang(String text, String lng) {
        List<Language> detect = languageService.detect(text);
        assertEquals(1, detect.size());
        assertEquals(lng, detect.get(0).getName());
        assertTrue(detect.get(0).getProb() > 0.9);
    }

    final String textEn = "This is sample text for language detection test.";
    final String textRu = "Это тестовое сообщение для проверки идентификации языка.";
    final String textDe = "Dies ist eine Testnachricht, um die Identität der Sprache zu überprüfen.";

    @Test
    public void testEn() throws Exception {
        testSingleLang(textEn, "en");
    }


    @Test
    public void testRu() throws Exception {
        testSingleLang(textRu, "ru");
    }

    @Test
    public void testDe() throws Exception {
        testSingleLang(textDe, "de");
    }

    @Test
    public void testMixEnDe() throws Exception {
        List<Language> detect = languageService.detect(textEn + " " + textDe + " " + textEn);
        assertEquals(2, detect.size());
    }
}
