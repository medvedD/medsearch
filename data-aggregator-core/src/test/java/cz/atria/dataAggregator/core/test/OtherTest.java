package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Test;

import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.entity.URIAttributeConverter;

/**
 * @author rnuriev
 * @since 07.10.2015.
 */
public class OtherTest {

    @Test
    public void testURIAttributeConverterWithSpace() throws Exception {
        Path fileWithSpaceInName = Files.createTempFile("a b", null);
        URIAttributeConverter uriAttributeConverter = new URIAttributeConverter();
        URI uri = uriAttributeConverter.convertToEntityAttribute("file:/" + fileWithSpaceInName.toFile().getAbsolutePath().replaceAll("\\\\", "/"));
        assertNotNull(uri);
    }

    /**related to: <a href="http://intra.dicomresearch.com/issues/807">#807</a><p></p>
     * Scheme isn't guessed for translate.google.ru due to 403 response for both (http and https) schemes
     * */
    @Test
    public void testGuessUriWith403Response() throws Exception {
        String s = Common.guessUriSchemeIfAbsent("translate.google.ru");
        URI uri = URI.create(s);
        assertTrue(uri.getScheme().equals("http") || uri.getScheme().equals("https"));
    }
}
