package cz.atria.dataAggregator.core.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.atria.dataAggregator.core.liquibase.DecodeURL;
import liquibase.database.jvm.JdbcConnection;

/**
 * @author rnuriev
 * @since 14.10.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-empty-property-context.xml",
        "classpath:cz/atria/dataAggregator/core/data-aggregator-db-context.xml"
})
public class LiquibaseTest {
    @Autowired
    DataSource dataSource;

    @Test
    public void testDecodeURL() throws Exception {
        final String tableName = "tab12808";
        final String columnName = "col";
        Connection connection = dataSource.getConnection();
        connection.setAutoCommit(true);
        try {
            connection.createStatement().execute(String.format("drop table %1$s", tableName));
        } catch (SQLException e) {
        }
        connection.createStatement().execute(String.format("create table %1$s(%2$s varchar(255))", tableName, columnName));
        final String uriDecoded = "file:///ok/ok/";
        final String uriEncoded = "file%3A%2F%2F%2Fsrv%2Ftomcat%2Fuploaded%2FHealth%2520care1-%D1%8F%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F.pdf";
        final String uriEncodedAfterDecodingExpected = "file:///srv/tomcat/uploaded/Health care1-я копия.pdf";
        final PreparedStatement ps = connection.prepareStatement(String.format("insert into %1$s values(?)", tableName));
        ps.setString(1, uriDecoded); ps.executeUpdate();
        ps.setString(1, uriEncoded);  ps.executeUpdate();
        DecodeURL decodeURL = new DecodeURL(tableName, columnName);
        decodeURL.execute(new JdbcConnection(connection));
        // check
        final PreparedStatement psCheck = connection.prepareStatement(String.format("select 1 from %1$s where %2$s = ?", tableName, columnName));
        psCheck.setString(1, uriDecoded);
        Assert.assertTrue(psCheck.executeQuery().next());
        psCheck.setString(1, uriEncodedAfterDecodingExpected);
        Assert.assertTrue(psCheck.executeQuery().next());
    }
}
