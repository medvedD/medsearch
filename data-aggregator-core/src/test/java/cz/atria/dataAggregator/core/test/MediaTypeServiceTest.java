package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import cz.atria.common.container.web.WebServerBase;
import cz.atria.dataAggregator.core.service.MediaTypeService;
import cz.atria.dataAggregator.core.service.MediaTypeServiceImpl;

/**
 * @author rnuriev
 * @since 08.09.2015.
 */
public class MediaTypeServiceTest {
    MediaTypeService mediaTypeService = new MediaTypeServiceImpl();

    protected void testDetect(URI uri, MediaType expectedMediaType) throws URISyntaxException {
        MediaType mediaType = mediaTypeService.detectMediaType(uri, MediaType.class);
        assertEquals(expectedMediaType.getType(), mediaType.getType());
        assertEquals(expectedMediaType.getSubtype(), mediaType.getSubtype());
    }

    protected void testDetect(String uri, MediaType expectedMediaType) throws URISyntaxException {
        testDetect(new URI(uri), expectedMediaType);
    }

    @Test
    public void testDetectHtml() throws Exception {
        testDetect("http://google.ru", MediaType.TEXT_HTML);
    }

    @Test
    public void testDetectRss() throws Exception {
        testDetect("https://news.yandex.ru/hardware.rss", cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_RSS_XML);
    }

    @Test
    public void testDetectPdf() throws Exception {
        testDetect("http://martinfowler.com/ieeeSoftware/whenType.pdf", cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_PDF);
    }

    @Test
    public void testDetectDoc() throws Exception {
        testDetect("http://fredrik.hubbe.net/plugger/test.doc", cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_MSWORD);
    }

    @Test
    public void testDetectPdfLocal() throws Exception {
        testDetect(getClass().getClassLoader().getResource("samples/sample.pdf").toURI(), cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_PDF);
    }

    /**<a href = "http://intra.dicomresearch.com/issues/978">#978</a>*/
    @Test
    public void testDetectTxt_BF_978() throws Exception {
        testDetect(getClass().getClassLoader().getResource("samples/bf/978-test.txt").toURI(), cz.atria.dataAggregator.core.entity.MediaType.TEXT_PLAIN);
    }

    /**related to: <a href="http://intra.dicomresearch.com/issues/1023">#1023</a>*/
    @Test
    public void testDetectRtf() throws Exception {
        testDetect(getClass().getClassLoader().getResource("samples/bf/rtf-detection-test-bf-1023.doc").toURI(), cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_RTF);
    }

    /**<a href = "http://intra.dicomresearch.com/issues/1043">#1043</a>
     * */
    @Test
    public void testDetectRssPresentedAsXml() throws Exception {
        final int port = WebServerBase.getFreePort();
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        final String contextPath = "test";
        server.createContext("/" + contextPath, new HttpHandler() {
            @Override
            public void handle(HttpExchange httpExchange) throws IOException {
                Headers responseHeaders = httpExchange.getResponseHeaders();
                responseHeaders.put(HttpHeaders.CONTENT_TYPE, Arrays.asList(String.format("%1$s; charset=UTF-8", MediaType.TEXT_XML_VALUE)));
                String content = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("samples/bf/rss-as-xml-bf-1043.xml"));
                httpExchange.sendResponseHeaders(200, content.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(content.getBytes());
                os.close();
            }
        });
        server.setExecutor(null);
        server.start();
        testDetect(String.format("http://127.0.0.1:%1$d/%2$s", port, contextPath), cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_RSS_XML);
        server.stop(1);
    }
}
