package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.atria.common.datagenerator.ValueProvider;
import cz.atria.dataAggregator.core.entity.Privilege;
import cz.atria.dataAggregator.core.entity.UserPrivilege;
import cz.atria.dataAggregator.core.service.EntityService;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-test-common-context.xml"
})
public class EntityServiceTest {
    @Autowired
    EntityService entityService;
    @Autowired
    Fixture fixture;
    @Autowired
    ValueProvider vp;

    @Test
    public void testUserPrivilegeDeletionCascade() throws Exception {
        List<Object> excluded = new ArrayList<>(); excluded.add("setParent");
        Privilege privilege = vp.get(Privilege.class, excluded);
        fixture.persist(privilege);
        UserPrivilege userPrivilege = fixture.get(UserPrivilege.class);
        entityService.remove(userPrivilege.getUser());
        assertNull(entityService.get(UserPrivilege.class, userPrivilege.getId()));
    }
}
