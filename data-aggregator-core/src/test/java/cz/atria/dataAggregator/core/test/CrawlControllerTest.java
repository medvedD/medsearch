package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import cz.atria.dataAggregator.core.spider.crawler4j.BasicCrawler;
import cz.atria.dataAggregator.core.spider.crawler4j.CrawlController;
import cz.atria.dataAggregator.core.spider.crawler4j.CustomData;
import edu.uci.ics.crawler4j.parser.HtmlParseData;

/**
 * @author rnuriev
 * @since 26.11.2015.
 */
public class CrawlControllerTest {
    CrawlController crawlController = new CrawlController();

    protected void checkBaseUri(String uriStr) throws IOException {
        Path dir = Files.createTempDirectory("dagg");
        final URI uri = URI.create(uriStr);
        edu.uci.ics.crawler4j.crawler.CrawlController process = crawlController.process(dir.toFile().getAbsolutePath(), uri, new BasicCrawler.PageCallback() {
            @Override
            public void process(String url, HtmlParseData htmlParseData) {

            }
        });
        CustomData customData = (CustomData) process.getCustomData();
        String baseUrl = customData.getBaseUrl();
        final URI resUri = URI.create(baseUrl);
        assertEquals(uri.getScheme(), resUri.getScheme());
        assertEquals(uri.getHost(), resUri.getHost());
        assertEquals(uri.getPort(), resUri.getPort());
        assertTrue(StringUtils.isEmpty(resUri.getPath()) || resUri.getPath().equals("/"));
    }

    /**<a href="http://intra.dicomresearch.com/issues/1493">#1493</a>
     * incorrect baseUri detection. E.g. "https://www.novartis.com/" resolved to "https",
     * which result to .BasicCrawler#shouldVisit() returns true for pages outside "www.novartis.com" domain.
     * */
    @Test
    public void testBaseUriProcessing() throws Exception {
        checkBaseUri("https://www.novartis.com/");
        checkBaseUri("https://www.novartis.com");
    }
}
