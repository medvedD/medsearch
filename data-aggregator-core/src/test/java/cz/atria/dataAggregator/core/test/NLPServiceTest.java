package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.common.datagenerator.ValueProvider;
import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Operator;
import cz.atria.dataAggregator.core.lng.LanguageService;
import cz.atria.dataAggregator.core.service.NLPService;
import cz.atria.dataAggregator.core.service.NLPServiceLuceneImpl;

/**
 * @author rnuriev
 * @since 29.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations = {
        "classpath:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-test-common-context.xml"
})
@Transactional
public class NLPServiceTest {
    @Autowired
    NLPService nlpService;
    @Autowired
    Fixture fixture;
    @Autowired
    LanguageService languageService;

    protected Map<Integer, Float> checkLang1Query(String searchText) {
        List<SearchTermEntry> searchTermEntries = new ArrayList<>();
        searchTermEntries.add(new SearchTermEntry(searchText));
        Map<String, Object> queries = nlpService.createQueries(searchTermEntries, 0);
        Map<Integer, Float> matches = nlpService.matches(queries);
        assertEquals(1, matches.size());
        return matches;
    }

    @Before
    public void before() {
        if (nlpService instanceof NLPServiceLuceneImpl) {
            for (String name : languageService.getAvailableLanguages()) {
                ((NLPServiceLuceneImpl)nlpService).addDirectory(name, new RAMDirectory());  // fresh index for each test
            }
        }
    }

    protected Document prepare(String content) {
        Document document = fixture.createDocument(content);
        nlpService.indexThis(document.getId(), content);
        return document;
    }

    protected Map<Integer, Float> testLang1Query(String content, String searchText) {
        Document document = prepare(content);
        Map<Integer, Float> matches = checkLang1Query(searchText);
        assertTrue(matches.containsKey(document.getId()));
        return matches;
    }

    /**single language document and single language query*/
    @Test
    public void testLang1DocLang1Query() throws Exception {
        final String searchText = "amaze";
        final String content = "This is very amazing part of some text.";
        testLang1Query(content, searchText);
    }

    final String searchTextEn = "amaze";
    final String searchTextRu = "солнечный";
    final String contentEnRu = "This is very amazing part of some text. We could consume such things. Погода сегодня солнечная в отличии от некоторых.";
    /**multiple language document and single language query*/
    @Test
    public void testLang2DocLang1Query() throws Exception {
        prepare(contentEnRu);
        checkLang1Query(searchTextEn);
        checkLang1Query(searchTextRu);
    }

    /**document with only single word in another language
     * */
    @Test
    public void testLang1WithAnotherLangWordDocLang1Query() throws Exception {
        final String contentRuWithEngWord = "Раз два три четыре пять вышел зайчик погулять. Это слово test. Окончание предложения.";
        prepare(contentRuWithEngWord);
        checkLang1Query("test");
    }

    /**content have tag symbols*/
    @Test
    public void testHighlightFragmentWithTags() throws Exception {
        final String queryString = "text";
        String content = String.format("This is < sample %1$s with < > tags inline", queryString);
        Object query = nlpService.createQuery(queryString);
        String highlightedContent = nlpService.highlightFragment(content, query, String.class, Locale.ENGLISH.getLanguage());
        assertTrue("highlightedContent=" + highlightedContent, highlightedContent.contains((NLPServiceLuceneImpl.FMT_START)));
        assertTrue(highlightedContent.contains((NLPServiceLuceneImpl.FMT_END)));
    }

    /**content without tag symbols*/
    @Test
    public void testHighlightFragmentWithoutTags() throws Exception {
        final String queryString = "text";
        String content = String.format("This is sample %1$s without tags inline", queryString);
        Object query = nlpService.createQuery(queryString);
        String highlightedContent = nlpService.highlightFragment(content, query, String.class, Locale.ENGLISH.getLanguage());
        assertTrue("highlightedContent=" + highlightedContent, highlightedContent.contains((NLPServiceLuceneImpl.FMT_START)));
        assertTrue(highlightedContent.contains((NLPServiceLuceneImpl.FMT_END)));
    }

    @Autowired
    ValueProvider vp;

    /**<a href = "http://intra.dicomresearch.com/issues/912">#912</a>*/
    @Test
    public void testQueryWithOrAndSlop() throws Exception {
        testSlopWithNonAnd(Operator.OR);
    }

    protected void testSlopWithNonAnd(Operator opr) {
        final String queryString1 = "Ocean";
        final String queryString2 = "android";
        final int slop = 2;
        String content = String.format("Sample text %1$s %2$s %3$s", queryString1, StringUtils.repeat("inside", " ", slop + 1), !opr.equals(Operator.NOT) ? queryString2 : "");
        final Integer documentId = vp.getInt();
        final Integer sourceId = vp.getInt();
        nlpService.indexThis(sourceId, documentId, content);

        final List<SearchTermEntry> terms1 = new ArrayList<>();
        terms1.add(new SearchTermEntry(queryString1));
        terms1.add(new SearchTermEntry(opr, queryString2, null, null, null));


        Map<String, Object> queries = nlpService.createQueries(terms1, slop);
        Map<Integer, SortedMap<String, Float>> matchesAll = nlpService.matchesAll(queries);
        assertTrue(matchesAll.containsKey(documentId));
    }

    /**<a href = "http://intra.dicomresearch.com/issues/1078">#1078</a>: "при операторе отличном от AND поле дистанция должно игнорироваться."*/
    @Test
    public void testQueryWithNotAndSlop() throws Exception {
        testSlopWithNonAnd(Operator.NOT);
    }

    protected void checkMatches(List<SearchTermEntry> terms1, List<Integer> foundDocIds) {
        checkMatches(terms1, foundDocIds, null);
    }
    
    protected void checkMatches(List<SearchTermEntry> terms1, List<Integer> foundDocIds, Integer slop) {
        Map<String, Object> queries = nlpService.createQueries(terms1, slop);
        Map<Integer, Float> matches = nlpService.matches(queries);
        final Object query = queries.values().iterator().next();
        assertEquals("query=" + query, foundDocIds.size(), matches.size());
        for (Integer docId : foundDocIds) {
            assertTrue("query=" + query, matches.containsKey(docId));
        }
    }

    /**<a href = "http://intra.dicomresearch.com/issues/1037">#1037</a><p></p>
     * example: (a AND b) OR c
     * */
    @Test
    public void testQueryWith3OperandsPrecedence1() throws Exception {
        final String queryString1 = "Ocean";
        final String queryString2 = "android";
        final String queryString3 = "sample";
        final String content1 = String.format("This is first text %1$s %2$s %3$s. Found expected.", queryString1, queryString2, queryString3);
        final String content2 = String.format("This is second text %1$s %2$s. Found expected.", queryString1, queryString2);
        final String content3 = String.format("This is third text %1$s. Found expected.", queryString3);
        final String content4 = String.format("This is forth text %1$s. Not found expected.", queryString1);

        final Integer documentId1 = vp.getInt(); final Integer sourceId1 = vp.getInt(); nlpService.indexThis(sourceId1, documentId1, content1);
        final Integer documentId2 = vp.getInt(); final Integer sourceId2 = vp.getInt(); nlpService.indexThis(sourceId2, documentId2, content2);
        final Integer documentId3 = vp.getInt(); final Integer sourceId3 = vp.getInt(); nlpService.indexThis(sourceId3, documentId3, content3);
        final Integer documentId4 = vp.getInt(); final Integer sourceId4 = vp.getInt(); nlpService.indexThis(sourceId4, documentId4, content4);

        final List<SearchTermEntry> terms1 = new ArrayList<>();
        terms1.add(new SearchTermEntry(queryString1));
        terms1.add(new SearchTermEntry(Operator.AND, queryString2));
        terms1.add(new SearchTermEntry(Operator.OR, queryString3));
        checkMatches(terms1, Arrays.asList(documentId1, documentId2, documentId3));
    }

    /**<a href = "http://intra.dicomresearch.com/issues/1037">#1037</a><p></p>
     * example: (a OR b) AND c
     * */
    @Test
    public void testQueryWith3OperandsPrecedence2() throws Exception {
        final String queryString1 = "Ocean";
        final String queryString2 = "android";
        final String queryString3 = "sample";
        final String content1 = String.format("This is first text %1$s %2$s %3$s. Found expected.", queryString1, queryString2, queryString3);
        final String content2 = String.format("This is second text %1$s %2$s. Not found expected.", queryString1, queryString2);
        final String content3 = String.format("This is third text %1$s. Not found expected.", queryString3);
        final String content4 = String.format("This is forth text %1$s. Not found expected.", queryString1);
        final String content5 = String.format("This is fifth (5) text %1$s %2$s. Found expected.", queryString1, queryString3);
        final String content6 = String.format("This is sixth (6) text %1$s %2$s. Found expected.", queryString2, queryString3);
        int d = 0, s = 0;
        final Integer documentId1 = d++; final Integer sourceId1 = s++; nlpService.indexThis(sourceId1, documentId1, content1);
        final Integer documentId2 = d++; final Integer sourceId2 = s++; nlpService.indexThis(sourceId2, documentId2, content2);
        final Integer documentId3 = d++; final Integer sourceId3 = s++; nlpService.indexThis(sourceId3, documentId3, content3);
        final Integer documentId4 = d++; final Integer sourceId4 = s++; nlpService.indexThis(sourceId4, documentId4, content4);
        final Integer documentId5 = d++; final Integer sourceId5 = s++; nlpService.indexThis(sourceId5, documentId5, content5);
        final Integer documentId6 = d++; final Integer sourceId6 = s++; nlpService.indexThis(sourceId6, documentId6, content6);

        final List<SearchTermEntry> terms1 = new ArrayList<>();
        terms1.add(new SearchTermEntry(queryString1));
        terms1.add(new SearchTermEntry(Operator.OR, queryString2));
        terms1.add(new SearchTermEntry(Operator.AND, queryString3));
        checkMatches(terms1, Arrays.asList(documentId1, documentId5, documentId6));
    }

    /**<a href = "http://intra.dicomresearch.com/issues/1037">#1037</a><p></p>
     * example: (a OR b) NOT c
     * */
    @Test
    public void testQueryWith3OperandsPrecedence3() throws Exception {
        final String queryString1 = "Ocean";
        final String queryString2 = "android";
        final String queryString3 = "sample";
        final String content1 = String.format("This is first text %1$s %2$s %3$s. Not found expected.", queryString1, queryString2, queryString3);
        final String content2 = String.format("This is second text %1$s %2$s. Found expected.", queryString1, queryString2);
        final String content3 = String.format("This is third text %1$s. Not found expected.", queryString3);
        final String content4 = String.format("This is forth text %1$s. Found expected.", queryString1);
        final String content5 = String.format("This is fifth (5) text %1$s %2$s. Not found expected.", queryString1, queryString3);
        final String content6 = String.format("This is sixth (6) text %1$s %2$s. Not found expected.", queryString2, queryString3);
        int d = 0, s = 0;
        final Integer documentId1 = d++; final Integer sourceId1 = s++; nlpService.indexThis(sourceId1, documentId1, content1);
        final Integer documentId2 = d++; final Integer sourceId2 = s++; nlpService.indexThis(sourceId2, documentId2, content2);
        final Integer documentId3 = d++; final Integer sourceId3 = s++; nlpService.indexThis(sourceId3, documentId3, content3);
        final Integer documentId4 = d++; final Integer sourceId4 = s++; nlpService.indexThis(sourceId4, documentId4, content4);
        final Integer documentId5 = d++; final Integer sourceId5 = s++; nlpService.indexThis(sourceId5, documentId5, content5);
        final Integer documentId6 = d++; final Integer sourceId6 = s++; nlpService.indexThis(sourceId6, documentId6, content6);

        final List<SearchTermEntry> terms1 = new ArrayList<>();
        terms1.add(new SearchTermEntry(queryString1));
        terms1.add(new SearchTermEntry(Operator.OR, queryString2));
        terms1.add(new SearchTermEntry(Operator.NOT, queryString3));
        checkMatches(terms1, Arrays.asList(documentId2, documentId4));
    }


    /**related to: <a href = "http://intra.dicomresearch.com/issues/1066">#1066</a>*/
    @Test
    public void testQueryByExactPhraseWithSlop() throws Exception {
        final String queryString1 = "Ocean";
        final String queryString2 = "android";
        final String queryString3 = "sample";
        final String queryString4 = "wonder";
        final int slop = 3;
        final String template = "This is %1$s %2$s %3$s %4$s %5$s";
        final String contentFound = String.format(template, queryString1, queryString2, StringUtils.repeat("word", " ", slop - 1), queryString3, queryString4);
        final String contentNotFound = String.format(template, queryString1, queryString2, StringUtils.repeat("word", " ", slop + 1), queryString3, queryString4);

        final Integer documentId1 = vp.getInt(); final Integer sourceId1 = vp.getInt(); nlpService.indexThis(sourceId1, documentId1, contentFound);
        final Integer documentId2 = vp.getInt(); final Integer sourceId2 = vp.getInt(); nlpService.indexThis(sourceId2, documentId2, contentNotFound);

        final List<SearchTermEntry> terms1 = new ArrayList<>();
        terms1.add(new SearchTermEntry("\"" + queryString1 + " " + queryString2 + "\""));
        terms1.add(new SearchTermEntry(Operator.AND, "\"" + queryString3 + " " + queryString4 + "\""));

        checkMatches(terms1, Arrays.asList(documentId1), slop);
    }

    @Test
    public void testQueryByNotExactPhrase() throws Exception {
        final String queryString1 = "Ocean";
        final String queryString2 = "android";
        final String queryString3 = "sample";
        final String queryString4 = "wonder";
        final String content1 = String.format("This is %1$s %2$s %3$s. Found expected.", queryString1, StringUtils.repeat("word", " ", 3), queryString3);
        final String content2 = String.format("This is %1$s %2$s %3$s. Found expected.", queryString2, StringUtils.repeat("word", " ", 3), queryString3);
        final String content3 = String.format("This is %1$s %2$s %3$s. Not found expected.", queryString1, StringUtils.repeat("word", " ", 3), queryString2);
        final String content4 = String.format("This is %1$s %2$s %3$s. Not found expected.", queryString3, StringUtils.repeat("word", " ", 3), queryString4);
        final String content5 = String.format("This is %1$s %2$s %3$s %4$s %5$s. Found expected.", queryString4, StringUtils.repeat("word", " ", 3), queryString3, queryString2, queryString1);

        int d = 0, s = 0;
        final Integer documentId1 = d++; final Integer sourceId1 = s++; nlpService.indexThis(sourceId1, documentId1, content1);
        final Integer documentId2 = d++; final Integer sourceId2 = s++; nlpService.indexThis(sourceId2, documentId2, content2);
        final Integer documentId3 = d++; final Integer sourceId3 = s++; nlpService.indexThis(sourceId3, documentId3, content3);
        final Integer documentId4 = d++; final Integer sourceId4 = s++; nlpService.indexThis(sourceId4, documentId4, content4);
        final Integer documentId5 = d++; final Integer sourceId5 = s++; nlpService.indexThis(sourceId5, documentId5, content5);

        final List<SearchTermEntry> terms1 = new ArrayList<>();
        terms1.add(new SearchTermEntry(queryString1 + " " + queryString2));
        terms1.add(new SearchTermEntry(Operator.AND, queryString3 + " " + queryString4));

        checkMatches(terms1, Arrays.asList(documentId1, documentId2, documentId5));
    }


    /**<a href = "http://intra.dicomresearch.com/issues/1045">#1045</a>
     * */
    @Test
    public void testSearchWithStopWords() throws Exception  {
        final String queryString1 = "Ocean";
        final String queryString2 = "android";
        final String englishStopWord = "and";
        String content = String.format("Это текст явно на русском %1$s окончание текста.", englishStopWord);
        final Document document = fixture.createDocument(content);
        final Integer documentId = document.getId();
        nlpService.indexThis(documentId, content);
        final List<SearchTermEntry> terms1 = new ArrayList<>();
        terms1.add(new SearchTermEntry(String.format("\"%1$s %2$s %3$s\"", queryString1, englishStopWord, queryString2)));
        Map<String, Object> queries = nlpService.createQueries(terms1, null);
        Map<Integer, SortedMap<String, Float>> matchesAll = nlpService.matchesAll(queries);
        assertFalse(matchesAll.containsKey(documentId));
    }

    /**<a href="http://intra.dicomresearch.com/issues/1499">#1499</a>
     * */
    @Test
    public void testHighlightByHtmlKeyword() throws Exception {
        final String queryStr = "class";
        final String contentTemplate = "<html><body %2$s='foo'>%1$s</body></html>";
        final String content = String.format(contentTemplate, queryStr, queryStr);
        final String highlightedExpected = String.format(contentTemplate, NLPServiceLuceneImpl.FMT_START + queryStr + NLPServiceLuceneImpl.FMT_END, queryStr);
        Object query = nlpService.createQuery(queryStr);
        final String highlighted = nlpService.highlight(content, query);
        assertEquals(highlightedExpected, highlighted);
    }
}
