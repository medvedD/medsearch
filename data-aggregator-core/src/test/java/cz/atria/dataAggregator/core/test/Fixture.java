package cz.atria.dataAggregator.core.test;

import java.io.File;
import java.net.URI;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
public interface Fixture {
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    <T> T get(Class<T> asClass);
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    <T> void persist(T o);
    Document createDocument();
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    Source createSource();
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    Source createSource(URI uri);
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    Source createSource(File uploadedDir);
    Document createDocument(String content);
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    Document createDocument(URI uri);
    Document createDocument(Source source, String content);
}
