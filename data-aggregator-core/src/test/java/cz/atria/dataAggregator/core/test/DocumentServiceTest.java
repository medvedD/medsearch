package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.lucene.store.RAMDirectory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.io.Files;

import cz.atria.common.datagenerator.jpa.ValueProvider;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.DocumentTitleTruncateConverter;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.service.DocumentService;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.NLPService;
import cz.atria.dataAggregator.core.service.NLPServiceLuceneImpl;
import cz.atria.dataAggregator.core.service.SourceService;

/**
 * @author rnuriev
 * @since 28.09.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-test-common-context.xml"
})
@Transactional
public class DocumentServiceTest {
    @Autowired
    DocumentService documentService;
    @Autowired
    NLPService nlpService;
    @Autowired
    Fixture fixture;
    @Autowired
    ValueProvider vpq;
    @Autowired
    cz.atria.common.datagenerator.ValueProvider vp;
    @Autowired
    EntityService entityService;

    @PostConstruct
    public void init() {
        if (nlpService instanceof NLPServiceLuceneImpl) {
            ((NLPServiceLuceneImpl)nlpService).setDirecoryClass(RAMDirectory.class);
        }
    }

    /**
     * check if index entry removed after document deletion
     */
    @Test
    public void testIndexDeletionAfterDocumentRemove() throws Exception {
        Document document = fixture.createDocument();
        String content = documentService.getContent(document.getUri(), String.class);
        nlpService.indexThis(document.getId(), content);
        Map<String, Object> queries = nlpService.createQueries(content);
        Map<Integer, Float> matches = nlpService.matches(queries);
        assertEquals(1, matches.size());
        assertNotNull(matches.get(document.getId()));
        documentService.removeDocumentBySource(document.getSource().getId());
        // check
        queries = nlpService.createQueries(content);
        matches = nlpService.matches(queries);
        assertEquals(0, matches.size());
    }

    /**<a href = "http://intra.dicomresearch.com/issues/814">#814</a>*/
    @Test
    public void testDocumentTitleLength() throws Exception {
        String value = vp.getUnique("", 290);
        Document document = fixture.createDocument();
        document.setTitle(value);
        entityService.save(document);
    }

    @Test
    public void testDocumentTitleAutoTruncate() throws Exception {
        int length = new DocumentTitleTruncateConverter() {
            @Override
            public int getLength() {
                return super.getLength();
            }
        }.getLength();
        String value = vp.getUnique("", length + 1);
        Document document = fixture.createDocument();
        document.setTitle(value);
        entityService.save(document);
        // successful insertion expected
    }

    /**<a href = "http://intra.dicomresearch.com/issues/837">#837</a>
     * */
    @Test
    public void testDocumentCopyUriLength() throws Exception {
        String value = vp.getUnique("", 500);
        Document document = fixture.createDocument();
        document.setCopyUri(URI.create("file:/" + value));
        entityService.save(document);
    }

    @Autowired
    SourceService sourceService;

    protected void writeTo(File file, String content) throws IOException {
        final FileOutputStream fos = new FileOutputStream(file);
        fos.write(content.getBytes());
        fos.flush();
        fos.close();
    }

    @Test
    public void testAddAllFromDirectory() throws Exception {
        final File dir = Files.createTempDir();
        File file1 = File.createTempFile("dagg", null, dir);
        writeTo(file1, "test");
        documentService.addAllFromDirectory(dir.getCanonicalPath());
        List<Source> sources = sourceService.getSourcesByUriPrefixAndSuffix("", file1.getName());
        assertFalse(sources.isEmpty());
        List<Document> documents = entityService.getDocuments(sources.get(0));
        assertFalse(documents.isEmpty());
    }

    /**<a href = "http://intra.dicomresearch.com/issues/1047">#1047</a>
     * */
    @Test
    public void testDocumentWithMultipleSources() throws Exception {
        final File docFile = File.createTempFile("dagg", null);
        final Source src1 = createSource(docFile);
        final Source src2 = createSource(docFile);

        final List<Document> docs1 = entityService.getDocuments(src1);
        final List<Document> docs2 = entityService.getDocuments(src2);
        assertEquals(1, docs1.size());
        assertEquals(1, docs2.size());
        assertEquals(docs1.get(0).getId(), docs2.get(0).getId());
    }

    protected Source createSource(File docFile) throws IOException {
        final File srcFile = File.createTempFile("dagg", null);
        final Source src = fixture.createSource(srcFile.toURI());
        documentService.createDocumentA(src, docFile.toURI(), null);
        return src;
    }

    /**related to: <a href = "http://intra.dicomresearch.com/issues/1047">#1047</a>
     * */
    @Test
    public void testRemoveDocumentsForSourceWithMultipleRelations() throws Exception {
        final File docFile = File.createTempFile("dagg", null);
        final Source src1 = createSource(docFile);
        final Source src2 = createSource(docFile);
        documentService.removeDocumentBySourceA(src1);
        final List<Document> docs1 = entityService.getDocuments(src1);
        final List<Document> docs2 = entityService.getDocuments(src2);
        assertEquals(0, docs1.size());
        assertEquals(1, docs2.size());
    }
}
