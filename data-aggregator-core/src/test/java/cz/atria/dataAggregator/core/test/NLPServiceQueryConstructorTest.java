package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cz.atria.common.datagenerator.ValueProvider;
import cz.atria.common.datagenerator.ValueProviderRandom;
import cz.atria.dataAggregator.core.DependencyAccessorA;
import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.entity.Operator;
import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.entity.TermElementSynonym;
import cz.atria.dataAggregator.core.lng.LanguageServiceCybozuImpl;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.NLPService;
import cz.atria.dataAggregator.core.service.NLPServiceLuceneImpl;

/**
 * @author rnuriev
 * @since 02.11.2015.
 */
public class NLPServiceQueryConstructorTest {
    NLPService nlpService = new NLPServiceLuceneImpl();
    ValueProvider vp = new ValueProviderRandom();

    @Test
    public void test2SimpleAndOperandsUndefSlop() throws Exception {
        List<SearchTermEntry> termList = new ArrayList<>();
        termList.add(new SearchTermEntry(vp.get(String.class)));
        termList.add(new SearchTermEntry(Operator.AND, vp.get(String.class)));
        Map<String, Object> queries = nlpService.createQueries(termList, null);
        Query query = (Query) queries.values().iterator().next();
        assertTrue(query instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query;
        Assert.assertEquals(termList.size(), booleanQuery.clauses().size());
        for (BooleanClause booleanClause : booleanQuery.getClauses()) {
            Assert.assertEquals(BooleanClause.Occur.MUST, booleanClause.getOccur());
            assertTrue(booleanClause.getQuery() instanceof TermQuery);
        }
    }

    @Test
    public void test2SimpleAndOperandsDefSlop() throws Exception {
        List<SearchTermEntry> termList = new ArrayList<>();
        termList.add(new SearchTermEntry(vp.get(String.class)));
        termList.add(new SearchTermEntry(Operator.AND, vp.get(String.class)));
        final int slop = 3;
        Map<String, Object> queries = nlpService.createQueries(termList, slop);
        Query query = (Query) queries.values().iterator().next();
        assertTrue(query instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query;
        SpanNearQuery spanNearQuery = null;
        for (BooleanClause booleanClause : booleanQuery.getClauses()) {
            if (booleanClause.getQuery() instanceof SpanNearQuery) {
                spanNearQuery = (SpanNearQuery) booleanClause.getQuery();
                break;
            }
        }
        assertNotNull(spanNearQuery);
        assertEquals(slop, spanNearQuery.getSlop());
        assertEquals(termList.size(), spanNearQuery.getClauses().length);
    }

    @Before
    public void before() {
        ((DependencyAccessorA) nlpService).set(mock(EntityService.class));
        ((DependencyAccessorA) nlpService).set(new LanguageServiceCybozuImpl());
    }


    @Test
    public void test1OperandWithSynonym() throws Exception {
        final TermElementSynonym termElementSynonym = vp.get(TermElementSynonym.class);
        EntityService entityService = ((DependencyAccessorA) nlpService).get(EntityService.class);
        when(entityService.getTermElementSynonyms(termElementSynonym.getTermElement())).thenReturn(Arrays.asList(termElementSynonym));
        List<SearchTermEntry> termList = new ArrayList<>();
        termList.add(new SearchTermEntry(termElementSynonym.getTermElement(), true, false));
        Map<String, Object> queries = nlpService.createQueries(termList, null);
        Query query = (Query) queries.values().iterator().next();
        assertTrue(query.getClass().getCanonicalName(), query instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query;
        assertEquals(2, booleanQuery.clauses().size());
        for (BooleanClause booleanClause : booleanQuery.getClauses()) {
            assertEquals(BooleanClause.Occur.SHOULD, booleanClause.getOccur());
        }
    }

    @Test
    public void test1OperandWithChildren() throws Exception {
        final TermElement termElementChild = vp.get(TermElement.class);
        final TermElement termElementParent = vp.get(TermElement.class);
        termElementChild.setParent(termElementParent);
        termElementParent.setParent(null);
        EntityService entityService = ((DependencyAccessorA) nlpService).get(EntityService.class);
        when(entityService.getChildren(termElementParent)).thenReturn(Arrays.asList(termElementChild));
        List<SearchTermEntry> termList = new ArrayList<>();
        termList.add(new SearchTermEntry(termElementParent, false, true));
        Map<String, Object> queries = nlpService.createQueries(termList, null);
        Query query = (Query) queries.values().iterator().next();
        assertTrue(query.getClass().getCanonicalName(), query instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query;
        assertEquals(2, booleanQuery.clauses().size());
        for (BooleanClause booleanClause : booleanQuery.getClauses()) {
            assertEquals(BooleanClause.Occur.SHOULD, booleanClause.getOccur());
        }
    }

    @Test
    public void test1OperandWithSynonymAndChildren() throws Exception {
        EntityService entityService = ((DependencyAccessorA) nlpService).get(EntityService.class);

        final TermElement termElementChild = vp.get(TermElement.class);
        final TermElement termElementParent = vp.get(TermElement.class);
        termElementChild.setParent(termElementParent);
        termElementParent.setParent(null);
        when(entityService.getChildren(termElementParent)).thenReturn(Arrays.asList(termElementChild));

        final TermElementSynonym termElementSynonym = vp.get(TermElementSynonym.class);
        termElementSynonym.setTermElement(termElementParent);
        when(entityService.getTermElementSynonyms(termElementSynonym.getTermElement())).thenReturn(Arrays.asList(termElementSynonym));

        List<SearchTermEntry> termList = new ArrayList<>();
        termList.add(new SearchTermEntry(termElementParent, true, true));
        Map<String, Object> queries = nlpService.createQueries(termList, null);
        Query query = (Query) queries.values().iterator().next();
        assertTrue(query.getClass().getCanonicalName(), query instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query;
        assertEquals(3, booleanQuery.clauses().size());
        for (BooleanClause booleanClause : booleanQuery.getClauses()) {
            assertEquals(BooleanClause.Occur.SHOULD, booleanClause.getOccur());
        }
    }

    /**<a href="http://intra.dicomresearch.com/issues/1037">#1037</a>: operator precedence*/
    @Test
    public void test3OperandSimple() throws Exception {
        List<SearchTermEntry> termList = new ArrayList<>();
        termList.add(new SearchTermEntry(vp.get(String.class)));
        termList.add(new SearchTermEntry(Operator.AND, vp.get(String.class)));
        termList.add(new SearchTermEntry(Operator.OR, vp.get(String.class)));
        Map<String, Object> queries = nlpService.createQueries(termList, null);
        Query query = (Query) queries.values().iterator().next();
        assertTrue(query.getClass().getCanonicalName(), query instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query;
        assertEquals(2, booleanQuery.clauses().size());
        final BooleanClause booleanClause1 = booleanQuery.getClauses()[0];
        assertTrue(booleanClause1.getQuery() instanceof BooleanQuery);
        final BooleanClause booleanClause2 = booleanQuery.getClauses()[1];
        assertTrue(booleanClause2.getQuery() instanceof TermQuery);
    }
}
