package cz.atria.dataAggregator.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import cz.atria.common.datagenerator.ValueProvider;
import cz.atria.common.datagenerator.ValueProviderRandom;
import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.DependencyAccessorA;
import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.lng.Language;
import cz.atria.dataAggregator.core.lng.LanguageService;
import cz.atria.dataAggregator.core.service.DocumentService;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.InfoRetrieveService;
import cz.atria.dataAggregator.core.service.NLPService;
import cz.atria.dataAggregator.core.service.NLPServiceLuceneImpl;
import cz.atria.dataAggregator.core.service.SearchResultEntry;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-test-common-context.xml"
})
@Transactional
public class InfoRetrieveServiceTest {
    @Autowired
    InfoRetrieveService infoRetrieveService;
    ValueProvider vp = new ValueProviderRandom(10);
    @Autowired
    Fixture fixture;
    @Autowired
    NLPService nlpService;
    @Autowired
    LanguageService languageService;

    @Before
    public void before() {
        if (nlpService instanceof NLPServiceLuceneImpl) {
            for (String name : languageService.getAvailableLanguages()) {
                ((NLPServiceLuceneImpl)nlpService).addDirectory(name, new RAMDirectory());  // fresh index for each test
            }
        }
    }

    Document prepare(String str) {
        final Source src = fixture.createSource();
        final String content = vp.get(String.class) + " " + str + " " + vp.get(String.class);
        final Document doc = fixture.createDocument(src, content);
        nlpService.indexThis(doc.getId(), content);
        return doc;
    }

    @Test
    public void testSourceFilterQueryWithUndefSources() throws Exception {
        final String str = vp.get(String.class);
        final int cnt = 2;
        for (int i = 0; i < cnt; i++) {
            prepare(str);
        }
        final Map<String, Object> queries = nlpService.createQueries(str);
        SortedSet<SearchResultEntry> retrieve = infoRetrieveService.retrieve(queries);
        assertEquals(cnt, retrieve.size());
    }

    @Test
    public void testSourceFilterQueryWithDefSource() throws Exception {
        final String str = vp.get(String.class);
        final int expectedCount = 1;
        final int cnt = expectedCount + 1;
        Document doc = null;
        for (int i = 0; i < cnt; i++) {
            Document doc1 = prepare(str);
            if (doc == null) {
                doc = doc1;
            }
        }
        final Map<String, Object> queries = nlpService.createQueries(str);
        SortedSet<SearchResultEntry> retrieve = infoRetrieveService.retrieve(Arrays.asList(doc.getSource()), queries);
        assertEquals(expectedCount, retrieve.size());
        assertEquals(doc.getId(), retrieve.first().getDocument().getId());
    }

    @Test
    public void testSourceFilterQueryWithAllSources() throws Exception {
        final String str = vp.get(String.class);
        final int expectedCount = 2;
        final int cnt = expectedCount;
        Collection<Source> sources = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Document doc1 = prepare(str);
            sources.add(doc1.getSource());
        }
        final Map<String, Object> queries = nlpService.createQueries(str);
        SortedSet<SearchResultEntry> retrieve = infoRetrieveService.retrieve(sources, queries);
        assertEquals(expectedCount, retrieve.size());
    }

    @Test
    public void testSourceFilterQueryWithDefSourceAndMultipleDocs() throws Exception {
        final String str = vp.get(String.class);
        Document doc11 = prepare(str);
        prepare(str);
        String content = str + str;
        Document doc12 = fixture.createDocument(doc11.getSource(), content);
        nlpService.indexThis(doc12.getId(), content);
        final Map<String, Object> queries = nlpService.createQueries(str);
        SortedSet<SearchResultEntry> retrieve = infoRetrieveService.retrieve(Arrays.asList(doc11.getSource()), queries);
        assertEquals(1, retrieve.size());
        assertEquals(doc11.getId(), retrieve.first().getDocument().getId());
    }

    /**related to: <a href = "http://intra.dicomresearch.com/issues/1047">#1047</a>*/
    @Test
    public void testUpdateIndexWithNewSourceId() throws Exception {
        LanguageService languageService = ((DependencyAccessorA) nlpService).get(LanguageService.class);
        try {
            LanguageService languageServiceMock = mock(LanguageService.class);
            final String lang = Locale.ENGLISH.getLanguage();
            when(languageServiceMock.detect(any())).thenReturn(Arrays.asList(new Language(lang, 1.)));
            when(languageServiceMock.getAvailableLanguages()).thenReturn(Arrays.asList(lang));
            ((DependencyAccessorA) nlpService).set(languageServiceMock);
            final String content = "test";
            final Source source1 = fixture.createSource();
            Document doc = fixture.createDocument(source1, content);
            nlpService.indexThis(source1.getId(), doc.getId(), content);
            Source source2 = fixture.createSource();
            doc.addSource(source2);
            nlpService.updateIndex(source2.getId(), doc.getId(), content);
            List<SearchTermEntry> searchTermEntries = new ArrayList<>();
            searchTermEntries.add(new SearchTermEntry(content));
            Map<String, Object> queries = nlpService.createQueries(searchTermEntries, null);
            SortedSet<SearchResultEntry> resultEntries = infoRetrieveService.retrieve(Arrays.asList(source1), queries);
            assertEquals(1, resultEntries.size());
            resultEntries = infoRetrieveService.retrieve(Arrays.asList(source2), queries);
            assertEquals(1, resultEntries.size());
        } finally {
            ((DependencyAccessorA) nlpService).set(languageService);
        }
    }

    @Autowired
    DocumentService documentService;

    @Autowired
    EntityService entityService;

    /**<a href="http://intra.dicomresearch.com/issues/1515">#1515</a>
     * */
    @Test
    public void testBF_1515() throws Exception {
        final Source source = fixture.createSource(getClass().getClassLoader().getResource("samples/bf/1515/нужный.doc").toURI());
        documentService.createDocumentBySourceA(source);
        final Document document = entityService.getDocuments(source) .get(0);
        Collection<Source> sources = Arrays.asList(source);
        Map<String, Object> queries = nlpService.createQueries(Arrays.asList(new SearchTermEntry("Аааааааааарррр")), null);
        SortedSet<SearchResultEntry> searchResultEntries = infoRetrieveService.retrieve(sources, queries);
        assertEquals(Common.reflectionToString(document, "isIndexed=" + document.getIsIndexed()), 1, searchResultEntries.size());
    }


    protected Analyzer getAnalyzer(String lang) {
        Analyzer analyzer = lang.equals("ru") ? new RussianAnalyzer() : new EnglishAnalyzer();
        return analyzer;
    }

    /**<a href="http://intra.dicomresearch.com/issues/1512">#1512</a>
     * resolved after lucene 5.4 release.
     * */
    @Test
    @Ignore
    public void testHighlightWithStopwordInTheMiddle() throws Exception {
        final String lang = "en";
        final String searchContent = "word for test is clever the close";
        final String searchPhrase = "\"" + searchContent + "\"";
        final String content = String.format("This is document with content: %1$s. End of document.", searchContent);
        final Document document =  fixture.createDocument(content);
        final Object query = new QueryParser("f", getAnalyzer(lang)).parse(searchPhrase);
        String highlighted = infoRetrieveService.highlight(document, query);
        assertNotNull(highlighted);
    }

    /**<a href = "http://intra.dicomresearch.com/issues/1515">#1515</a>
     * */
    @Test
    public void testHighlightHtmlWithoutKeywordInText() throws Exception {
        final String queryStrAsKeyword = "class";
        final String content = String.format("This is content without keyword in text, but in tag: <span %1$s=val></span>. End of document.", queryStrAsKeyword);
        final Document doc = fixture.createDocument(content);
        nlpService.indexThis(doc.getId());
        final Map<String, Object> queries = nlpService.createQueries(Arrays.asList(new SearchTermEntry("class")), null);
        final String highlighted = infoRetrieveService.highlight(doc, queries.get("en"));
        assertNull(highlighted);
    }
}
