import static org.junit.Assert.fail;

import java.net.URI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.test.Fixture;

/** tests with db.
 * @author rnuriev
 * @since 27.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
        "classpath:cz/atria/dataAggregator/core/test/data-aggregator-test-common-context.xml"
})
public class Other2Test {
    @Autowired
    EntityService entityService;
    @Autowired
    Fixture fixture;

    /**<a href="http://intra.dicomresearch.com/issues/1504">#1504</a>*/
    @Test
    public void testSourceNameUnique() throws Exception {
        final Source src1 = fixture.createSource();
        final Source src2 = new Source(src1.getName(), URI.create("http://foo"), null, null);
        try {
            entityService.persist(src2);
            fail();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
