package cz.atria.dataAggregator.ui.proto;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author rnuriev
 * @since 31.07.2015.
 */
public class SpringHelper {
    final static ApplicationContext context = new ClassPathXmlApplicationContext("cz/atria/dataAggregator/core/data-aggregator-core-context.xml", "classpath:cz/atria/dataAggregator/core/data-aggregator-em-context.xml");
    public static <T> T getBean(Class<T> clazz) {
        return context.getBean(clazz);
    }

}
