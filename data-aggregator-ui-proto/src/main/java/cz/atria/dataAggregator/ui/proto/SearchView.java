package cz.atria.dataAggregator.ui.proto;

import java.util.SortedSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.Synonym;
import cz.atria.dataAggregator.core.entity.Term;
import cz.atria.dataAggregator.core.entity.Vocabulary;
import cz.atria.dataAggregator.core.service.DocumentService;
import cz.atria.dataAggregator.core.service.InfoRetrieveService;
import cz.atria.dataAggregator.core.service.SearchResultEntry;
import cz.atria.dataAggregator.core.spider.SpiderService;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
public class SearchView extends VerticalLayout {
    final ApplicationContext context = new ClassPathXmlApplicationContext("cz/atria/dataAggregator/core/data-aggregator-core-context.xml");
    final EntityManager em = context.getBean(EntityManagerFactory.class).createEntityManager();
    final InfoRetrieveService infoRetrieveService = context.getBean(InfoRetrieveService.class);
    final DocumentService documentService = context.getBean(DocumentService.class);
    final SpiderService spiderService = context.getBean(SpiderService.class);
    final JPAContainer<Source> sourceContainer = JPAContainerFactory.make(Source.class, em);
    final Table sourceTable = new Table("Sources", sourceContainer);

    {
        sourceTable.setSizeFull();
        sourceTable.setVisibleColumns("name", "uri");
        sourceTable.setSelectable(true);
    }

    final Button addSource = new Button("New", new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            final BeanItem<Source> item = new BeanItem<>(new Source());
            SourceEditor sourceEditor = new SourceEditor(item);
            sourceEditor.addListener(new SourceEditor.EditorSavedListener() {
                @Override
                public void editorSaved(SourceEditor.EditorSavedEvent event) {
                    Object o = sourceContainer.addEntity(item.getBean());
                    documentService.createDocumentBySource((Integer) o);
                }
            });
            UI.getCurrent().addWindow(sourceEditor);
        }
    });
    final Button deleteSource = new Button("Delete", new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            sourceContainer.removeItem(sourceTable.getValue());
        }
    });
    final Button spiderButton = new Button("Scan", new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            spiderService.visit(em.find(Source.class, sourceTable.getValue()));
        }
    });
    final HorizontalLayout sourceToolBar = new HorizontalLayout(addSource, deleteSource, spiderButton);
    final VerticalLayout sourcePanel = new VerticalLayout(sourceTable, sourceToolBar);

    final JPAContainer<Vocabulary> vocabularyContainer = JPAContainerFactory.make(Vocabulary.class, em);
    final Table vocabularyTable = new Table("Vocabularies", vocabularyContainer);

    {
        vocabularyTable.setSelectable(true);
        vocabularyTable.setImmediate(true);
        vocabularyTable.setVisibleColumns("name");
        vocabularyTable.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Object itemId = event.getProperty().getValue();
                EntityItem<Vocabulary> item = vocabularyContainer.getItem(itemId);
                Vocabulary vocabulary = item.getEntity();
                termContainer.removeAllContainerFilters();
                Container.Filter filter = new Compare.Equal("vocabulary", vocabulary);
                termContainer.addContainerFilter(filter);
            }
        });
    }

    final JPAContainer<Term> termContainer = JPAContainerFactory.make(Term.class, em);
    final TextField searchFiled = new TextField();

    {
        searchFiled.setSizeFull();
    }

    final Table termTable = new Table("Term", termContainer);

    {
        termTable.setSelectable(true);
        termTable.setVisibleColumns("name", "parentName");
        termTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    Term term = (Term) ((JPAContainerItem) event.getItem()).getEntity();
                    searchFiled.setValue(searchFiled.getValue() + " " + term.getName());
                    selectedTermContainer.addBean(term);
                }
            }
        });
        termTable.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Object itemId = event.getProperty().getValue();
                if (itemId != null) {
                    EntityItem<Term> item = termContainer.getItem(itemId);
                    Term term = item.getEntity();
                    synonymContainer.removeAllContainerFilters();
                    Container.Filter filter = new Compare.Equal("term", term);
                    synonymContainer.addContainerFilter(filter);
                }
            }
        });
    }

    final JPAContainer<Synonym> synonymContainer = JPAContainerFactory.make(Synonym.class, em);
    final Table synonymTable = new Table("Synonyms", synonymContainer);

    {
        synonymTable.setSizeFull();
        synonymTable.setVisibleColumns("name");
    }

    final VerticalLayout termPanel = new VerticalLayout(termTable, synonymTable);

    {
        termPanel.setSizeFull();
    }

    /**
     * todo: this is temporary.
     * it's not thread safe
     *
     * @deprecated
     */
    @Deprecated
    Object currentQuery = null;

    final Button searchButton = new Button("Search", new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            Object query = infoRetrieveService.createQuery(searchFiled.getValue());
            SortedSet<SearchResultEntry> searchResultEntries = infoRetrieveService.retrieve(query);
            searchResultEntryContainer.removeAllItems();
            searchResultEntryContainer.addAll(searchResultEntries);
            currentQuery = query;
        }
    });
    final BeanItemContainer<Term> selectedTermContainer = new BeanItemContainer<Term>(Term.class);
    final Table selectedTermTable = new Table("Terms selected", selectedTermContainer);

    {
        selectedTermTable.setSizeFull();
        selectedTermTable.setSelectable(true);
        selectedTermTable.setVisibleColumns("name");
        selectedTermTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    selectedTermContainer.removeItem(((BeanItem) event.getItem()).getBean());
                }
            }
        });
    }

    final Button searchByListButton = new Button("Search", new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
            Object query = infoRetrieveService.createQuery(selectedTermContainer.getItemIds());
            SortedSet<SearchResultEntry> searchResultEntries = infoRetrieveService.retrieve(query);
            searchResultEntryContainer.removeAllItems();
            searchResultEntryContainer.addAll(searchResultEntries);
            currentQuery = query;
        }
    });
    final VerticalLayout searchBoxPanel = new VerticalLayout(selectedTermTable, searchByListButton, searchFiled, searchButton);

    {
        searchBoxPanel.setWidth(100f, Unit.PERCENTAGE);
    }

    final HorizontalLayout searchPanel = new HorizontalLayout(sourcePanel, vocabularyTable, termPanel, searchBoxPanel);

    {
        vocabularyTable.setSizeFull();
        termTable.setSizeFull();
        searchPanel.setSizeFull();
        searchPanel.setExpandRatio(sourcePanel, 0.3f);
        searchPanel.setExpandRatio(vocabularyTable, 0.15f);
        searchPanel.setExpandRatio(termPanel, 0.3f);
        searchPanel.setExpandRatio(searchBoxPanel, 0.25f);
    }

    //    final BeanItemContainer<SearchResultEntry> searchResultEntryContainer = new BeanItemContainer<SearchResultEntry>(SearchResultEntry.class);
    final BeanContainer<Integer, SearchResultEntry> searchResultEntryContainer = new BeanContainer<>(SearchResultEntry.class);

    {
        searchResultEntryContainer.setBeanIdProperty("document");
    }

    final Table documentFoundTable = new Table("Documents found", searchResultEntryContainer);

    {
        documentFoundTable.setSizeFull();
        documentFoundTable.setSelectable(true);
        documentFoundTable.setVisibleColumns("sourceName", "documentUri", "totalScore");
        documentFoundTable.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (event.getProperty().getValue() != null) {
                    documentTextArea.setValue(infoRetrieveService.highlight((Document) event.getProperty().getValue(), currentQuery));
                }
            }
        });
    }

    final RichTextArea documentTextArea = new RichTextArea("Document content");

    {
        documentTextArea.setSizeFull();
    }

    final HorizontalLayout searchResultPanel = new HorizontalLayout(documentFoundTable, documentTextArea);

    {
        searchResultPanel.setSizeFull();
        searchResultPanel.setExpandRatio(documentFoundTable, 0.3f);
        searchResultPanel.setExpandRatio(documentTextArea, 0.7f);
    }

    public SearchView() {
        setSizeFull();
        addComponent(searchPanel);
        addComponent(searchResultPanel);
    }
}
