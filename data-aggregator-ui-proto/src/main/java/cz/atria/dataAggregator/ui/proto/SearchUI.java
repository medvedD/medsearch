package cz.atria.dataAggregator.ui.proto;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

/**
 * @author rnuriev
 * @since 24.07.2015.
 */
public class SearchUI extends UI {
    @Override
    protected void init(VaadinRequest request) {
        setContent(new SearchView());
    }
}
