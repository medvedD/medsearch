package cz.atria.dataAggregator.ui.proto;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class SourceEditor extends Window implements Button.ClickListener,
        FormFieldFactory {

    private final Item item;
    private Form editorForm;
    private Button saveButton;
    private Button cancelButton;

    public SourceEditor(Item item) {
        this.item = item;
        editorForm = new Form();
        editorForm.setFormFieldFactory(this);
        editorForm.setBuffered(true);
        editorForm.setImmediate(true);
        editorForm.setItemDataSource(item, Arrays.asList("name", "uri"));

        saveButton = new Button("Save", this);
        cancelButton = new Button("Cancel", this);

        editorForm.getFooter().addComponent(saveButton);
        editorForm.getFooter().addComponent(cancelButton);
        setSizeUndefined();
        setContent(editorForm);
    }

    @Override
    public void buttonClick(ClickEvent event) {
        if (event.getButton() == saveButton) {
            editorForm.commit();
            fireEvent(new EditorSavedEvent(this, item));
        } else if (event.getButton() == cancelButton) {
            editorForm.discard();
        }
        close();
    }

    public void addListener(EditorSavedListener listener) {
        try {
            Method method = EditorSavedListener.class.getDeclaredMethod(
                    "editorSaved", new Class[] { EditorSavedEvent.class });
            addListener(EditorSavedEvent.class, listener, method);
        } catch (final NoSuchMethodException e) {
            // This should never happen
            throw new RuntimeException(
                    "Internal error, editor saved method not found");
        }
    }

    public void removeListener(EditorSavedListener listener) {
        removeListener(EditorSavedEvent.class, listener);
    }

    @Override
    public Field<?> createField(Item item, Object propertyId, Component uiContext) {
        Field field = DefaultFieldFactory.get().createField(item, propertyId,
                uiContext);
        if(propertyId.toString().equalsIgnoreCase("uri")) {
            ((AbstractField)field).setConverter(new URIConverter());
        }
        return field;
    }

    public static class EditorSavedEvent extends Component.Event {

        private Item savedItem;

        public EditorSavedEvent(Component source, Item savedItem) {
            super(source);
            this.savedItem = savedItem;
        }

        public Item getSavedItem() {
            return savedItem;
        }
    }

    public interface EditorSavedListener extends Serializable {
        public void editorSaved(EditorSavedEvent event);
    }

    private class URIConverter implements Converter<String, URI> {
        @Override
        public URI convertToModel(String value, Class<? extends URI> targetType, Locale locale) throws ConversionException {
            if(value == null || value.isEmpty()) {
                return null;
            } else {
                return URI.create(value);
            }
        }

        @Override
        public String convertToPresentation(URI value, Class<? extends String> targetType, Locale locale) throws ConversionException {
            if(value == null) {
                return null;
            } else {
                return value.toString();
            }
        }

        @Override
        public Class<URI> getModelType() {
            return URI.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }
    }
}
