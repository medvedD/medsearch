package cz.atria.dataAggregator.ui.proto;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

/**
 * @author rnuriev
 * @since 14.08.2015.
 */
public class CommonProtoUI extends UI {
    @Override
    protected void init(VaadinRequest request) {
        setContent(null);
    }
}
