package cz.atria.dataAggregator.ui;

import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

/**
 * @author rnuriev
 * @since 03.08.2015.
 */
@Deprecated
public class SourceForm extends FormLayout {
    @PropertyId("name")
    TextField nameField = new TextField("Name");
    @PropertyId("uri")
    TextField uriField = new TextField("Link");

    public SourceForm() {
        setMargin(new MarginInfo(true, true, true, true));
        setSpacing(true);
        nameField.setWidth(100, Unit.PERCENTAGE);
        uriField.setWidth(100, Unit.PERCENTAGE);
        addComponent(nameField);
        addComponent(uriField);
        uriField.setConverter(new URIConverter());
    }
}
