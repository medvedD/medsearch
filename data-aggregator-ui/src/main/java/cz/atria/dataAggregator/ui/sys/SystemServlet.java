package cz.atria.dataAggregator.ui.sys;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.ui.SpringHelper;

/**
 * @author rnuriev
 * @since 22.09.2015.
 */
public class SystemServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        if (StringUtils.isNotEmpty(req.getParameter("spring"))) {
            String cmd = req.getParameter("cmd");
            if (cmd == null || cmd.equalsIgnoreCase("list")) {
                for (Map.Entry<String, Class> beanEntry : SpringHelper.getBeans().entrySet()) {
                    writer.write(beanEntry.getKey() + ":" + beanEntry.getValue().getCanonicalName() + "\n");
                }
            } else if (cmd.equalsIgnoreCase("exe")) {
                String beanName = req.getParameter("bean");
                String methodName = req.getParameter("method");
                Object bean = SpringHelper.getBean(beanName);
                Method method = null;
                for (Method method1 : bean.getClass().getMethods()) {
                    if (method1.getName().equals(methodName)) {
                        method = method1;
                        break;
                    }
                }
                if (method != null) {
                    Object[] paramsAsObject = null;
                    String[] params = req.getParameterValues("param");
                    if (params != null) {
                        paramsAsObject = new Object[params.length];
                        int i = 0;
                        for (String param : params) {
                            Class<?> parameterType = method.getParameterTypes()[i];
                            Object paramAsObject = param;
                            if (!String.class.isAssignableFrom(parameterType)) {
                                try {
                                    Constructor<?> constructor = parameterType.getConstructor(String.class);
                                    paramAsObject = constructor.newInstance(param);
                                } catch (Exception e) {
                                    log("", e);
                                }
                            }
                            paramsAsObject[i] = paramAsObject;
                            i++;
                        }
                    }
                    try {
                        Object o = null;
                        if (paramsAsObject != null) {
                            o = method.invoke(bean, paramsAsObject);
                        } else {
                            o = method.invoke(bean);
                        }
                        if (o != null) {
                            writer.write(o.toString());
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    writer.write("Method not found");
                }
            }
        } else if (req.getParameterMap().containsKey("setting")) {
            SettingService.setSetting(req.getParameter("name"), req.getParameter("value"));
        } else {
            for (Map.Entry prop : System.getProperties().entrySet()) {
                writer.write(prop.toString() + "\n");
            }
        }
        writer.flush();
        writer.close();

    }
}
