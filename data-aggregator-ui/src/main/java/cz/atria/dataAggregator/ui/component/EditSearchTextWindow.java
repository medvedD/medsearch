package cz.atria.dataAggregator.ui.component;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.validator.AbstractStringValidator;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;

import cz.atria.dataAggregator.ui.CommonDesignWindow;
import cz.atria.dataAggregator.ui.SelectedTermEntry;

/**
 * @author rnuriev
 * @since 10.09.2015.
 */
@DesignRoot
public class EditSearchTextWindow extends CommonDesignWindow {
    final Logger log = LoggerFactory.getLogger(getClass());
    TextField textField;
    Button saveQueryButton;
    Button cancelButton;
    @Deprecated
    public EditSearchTextWindow(String caption) {

    }

    BeanFieldGroup<SelectedTermEntry> fieldGroup = new BeanFieldGroup<>(SelectedTermEntry.class);

    public BeanFieldGroup<SelectedTermEntry> getFieldGroup() {
        return fieldGroup;
    }
    final QueryParser queryParser = new QueryParser("f", new StandardAnalyzer());
    public EditSearchTextWindow(String caption, SelectedTermEntry selectedTermEntry) {
        super();
        setCaption(caption);
        textField.setNullRepresentation("");
        fieldGroup.bind(textField, "searchPhrase");
        fieldGroup.setItemDataSource(selectedTermEntry);
        textField.addValidator(new NullValidator("Empty\\blank string not allowed", false) {
            @Override
            public void validate(Object value) throws InvalidValueException {
                super.validate(value);
                if (StringUtils.isEmpty(value.toString().trim())) {
                    throw new Validator.InvalidValueException(getErrorMessage());
                }
            }
        });
        textField.addValidator(new AbstractStringValidator("Incorrect value.") {
            @Override
            protected boolean isValidValue(String value) {
                try {
                    queryParser.parse(value);
                    return true;
                } catch (ParseException e) {
                    return false;
                }
            }
        });
        textField.setValidationVisible(false);
        saveQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    try {
                        textField.validate();
                    } catch (Validator.InvalidValueException e) {
                        textField.setValidationVisible(true);
                        return;
                    }
                    fieldGroup.commit();
                } catch (FieldGroup.CommitException e) {
                    log.warn("", e);
                    throw new RuntimeException(e);
                }
                close();
            }
        });
        cancelButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                fieldGroup.discard();
                close();
            }
        });
    }
}
