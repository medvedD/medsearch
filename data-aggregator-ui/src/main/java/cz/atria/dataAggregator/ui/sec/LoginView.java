package cz.atria.dataAggregator.ui.sec;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.themes.BaseTheme;

import cz.atria.dataAggregator.ui.MainUI;
import cz.atria.dataAggregator.ui.MainView;
import cz.atria.dataAggregator.ui.RestorePasswordWindow;

/**
 * @author rnuriev
 * @since 27.08.2015.
 */
@DesignRoot
public class LoginView extends HorizontalLayout implements View {
    public static final String VIEW_NAME = "";
    TextField loginField;
    PasswordField passwordField;
    Button passwordRecoverButton;
    Button loginButton;
    AuthenticationManager authenticationManager = new AuthenticationManagerImpl();

    public LoginView() {
    	addStyleName("main-background");
        Design.read(getClass().getClassLoader().getResourceAsStream("design/loginView.html"), this);
        
        passwordRecoverButton.setStyleName(BaseTheme.BUTTON_LINK);
        passwordRecoverButton.addStyleName("color-white"); 
        
        loginField.addValidator(new StringLengthValidator("Username is empty", 1, 50, false));
        loginField.addStyleName("text-white");
        
        passwordField.addValidator(new StringLengthValidator("Password is empty", 1, 50, false));
        loginButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                boolean isValid = true;
                try {
                    loginField.validate();
                } catch (Validator.InvalidValueException e) {
                    loginField.setValidationVisible(true);
                    isValid = false;
                }
                try {
                    passwordField.validate();
                } catch (Validator.InvalidValueException e) {
                    passwordField.setValidationVisible(true);
                    isValid = false;
                }
                if (!isValid) {
                    return;
                }
                UsernamePasswordAuthenticationToken request = new UsernamePasswordAuthenticationToken(loginField.getValue(), passwordField.getValue());
                try {
                    Authentication result = authenticationManager.authenticate(request);
                    SecurityContext.setAuthentication(result);
                    MainUI current = (MainUI) UI.getCurrent();
                    Navigator navigator = current.getNavigator();
                    navigator.navigateTo(MainView.VIEW_NAME);
                } catch (AuthenticationException e) {
                    Notification.show("Wrong username or password", Notification.Type.ERROR_MESSAGE);
                }
            }
        });
        passwordRecoverButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                RestorePasswordWindow restorePasswordWindow = new RestorePasswordWindow();
                UI.getCurrent().addWindow(restorePasswordWindow);
            }
        });
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        loginField.setValidationVisible(false); 
        passwordField.setValidationVisible(false);
    }
}
