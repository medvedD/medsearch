package cz.atria.dataAggregator.ui.component;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.pagingcomponent.PagingComponent;
import org.vaadin.pagingcomponent.listener.impl.SimplePagingComponentListener;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedHttpSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.core.Settings;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.exc.DataAggregatorException;
import cz.atria.dataAggregator.core.exc.Reason;
import cz.atria.dataAggregator.core.service.InfoRetrieveService;
import cz.atria.dataAggregator.core.service.NLPService;
import cz.atria.dataAggregator.core.service.SearchResultEntry;
import cz.atria.dataAggregator.ui.MainView;
import cz.atria.dataAggregator.ui.SelectedTermEntry;
import cz.atria.dataAggregator.ui.SessionDataHolder;
import cz.atria.dataAggregator.ui.SpringHelper;

public class SearchComponent {
    
    final static Logger log = LoggerFactory.getLogger(SearchComponent.class);
    
    public static final String PROP_SEARCH_TIMEOUT = "dataAggregator.search.timeout";
    public static final String AUX = "aux";

    public static void doSearch(
            Collection<SelectedTermEntry> selectedTermEntries, 
            List<String> searchResultDisplayEntryList,
            final BeanItemContainer<Source> selectedSourcesContainer,
            PositiveIntTextField slopField,
            Panel searchResultPanel,
            Panel paringComponentPanel) {
        
        if (selectedTermEntries.isEmpty()) {
            throw new DataAggregatorException(Reason.QUERY_IS_EMPTY);
        }
        List<SearchTermEntry> searchTermEntries = new ArrayList<SearchTermEntry>(selectedTermEntries);
        searchResultDisplayEntryList.clear();
        NLPService nlpService = SpringHelper.getBean(NLPService.class);
        final Map<String, Object> queries = nlpService.createQueries(searchTermEntries, StringUtils.isNotEmpty(slopField.getValue()) ? Integer.valueOf(slopField.getValue()) : null);

        SessionDataHolder.setQueries(Page.getCurrent().getWebBrowser().getAddress(), queries);
        SortedSet<SearchResultEntry> searchResultEntries = null;
        final InfoRetrieveService infoRetrieveService = SpringHelper.getBean(InfoRetrieveService.class);
        Runnable target = new Runnable() {
            SortedSet<SearchResultEntry> searchResultEntries = null;

            @Override
            public void run() {
                if (selectedSourcesContainer.getItemIds().isEmpty()) {
                    searchResultEntries = infoRetrieveService.retrieve(queries);
                } else {
                    searchResultEntries = infoRetrieveService.retrieve(selectedSourcesContainer.getItemIds(), queries);
                }
            }

            public SortedSet<SearchResultEntry> getSearchResultEntries() {
                return searchResultEntries;
            }
        };
        Thread retrieveThread = new Thread(target);
        retrieveThread.start();
        try {
            retrieveThread.join(SettingService.getSetting(PROP_SEARCH_TIMEOUT, Long.class, 3 * 60 * 1000L));
            if (retrieveThread.getState().equals(Thread.State.TERMINATED)) {
                try {
                    searchResultEntries = (SortedSet<SearchResultEntry>) target.getClass().getMethod("getSearchResultEntries").invoke(target); // todo: simplify
                } catch (Exception e) {
                    log.error("", e);
                }
            } else {
                Notification.show("Search timeout.", "Perhaps you specified many synonyms or children elements in the search conditions. Try to simplify search conditions.", Notification.Type.WARNING_MESSAGE);
                return;
            }
        } catch (InterruptedException e) {
            log.warn("", e);
        }

        try {
            InputStream is;
            if (searchResultEntries != null && !searchResultEntries.isEmpty()) {
                List<CustomLayout> searchResultDisplayEntries = new ArrayList<CustomLayout>();
                URI locationUri = UI.getCurrent().getPage().getLocation();
                String location = locationUri.toString();
                String path = locationUri.getPath();
                String baseUrl = location.substring(0, location.indexOf(path)) + ((WrappedHttpSession) VaadinSession.getCurrent().getSession()).getHttpSession().getServletContext().getContextPath() + "/";
                for (SearchResultEntry searchResultEntry : searchResultEntries) {
                    String highlightFragment = nlpService.highlightFragment(searchResultEntry.getDocument(), searchResultEntry.getQuery(), String.class, searchResultEntry.getLang());
                    String relevancyInfo = "";
                    if (VaadinSession.getCurrent().getAttribute(AUX) != null) {
                        relevancyInfo = "&nbsp;(" + String.format("%.3f", searchResultEntry.getTotalScore()) + ")";
                    }
                    String docQueryPath = String.format("doc?documentId=%1$d&lang=%2$s", searchResultEntry.getDocument().getId(), searchResultEntry.getLang());
                    String title = searchResultEntry.getDocument().getTitle();
                    if (StringUtils.isEmpty(title)) {
                        String s = searchResultEntry.getDocumentUri().replaceAll("%20", "");
                        if (s.lastIndexOf("/") > 0) {
                            title = s.substring(s.lastIndexOf("/"));
                        } else {
                            title = s;
                        }
                    }
                    final String title2 = title;  // explicitly have original text due to encoding issues in export.
                    final String highlightFragment2 = highlightFragment;
                    String characterEncoding = VaadinService.getCurrentRequest().getCharacterEncoding();
                    if (characterEncoding != null && title != null) {
                        try {
                            if (title != null) {
                                title = new String(title.getBytes(characterEncoding), Charset.defaultCharset());
                            }
                            if (highlightFragment != null) {
                                highlightFragment = new String(highlightFragment.getBytes(characterEncoding), Charset.defaultCharset());
                            }
                        } catch (UnsupportedEncodingException e) {
                            log.warn("", e);
                        }
                    }

                    String documentUriString = searchResultEntry.getDocumentUri();
                    String actualDocumentUriString = documentUriString;
                    try {
                        URI documentUri = URI.create(documentUriString);
                        if (documentUri.getScheme().equals("file")) {
                            VaadinServletRequest req = (VaadinServletRequest) VaadinService.getCurrent().getCurrentRequest();
                            String docPath = documentUri.getPath();
                            actualDocumentUriString = String.format("%1$s://%2$s:%3$d/%4$s",
                                    req.getScheme(),
                                    req.getServerName(),
                                    req.getServerPort(),
                                    docPath.substring(docPath.indexOf(Settings.DEFAULT_UPLOAD_DIR))
                            );
                        }
                    } catch (Exception e) {
                        log.warn("", e);
                    }
                    if (documentUriString.contains("%20")) {
                        documentUriString = documentUriString.replaceAll("%20", " ");
                    }
                    String searchResultDisplayEntry = String.format(MainView.resultEntryTemplate,
                            documentUriString,
                            title,
                            highlightFragment,
                            baseUrl + docQueryPath,
                            relevancyInfo,
                            actualDocumentUriString);
                    String searchResultDisplayEntry2 = String.format(MainView.resultEntryTemplate,
                            documentUriString,
                            title2,
                            highlightFragment2,
                            baseUrl + docQueryPath,
                            relevancyInfo,
                            actualDocumentUriString);
                    searchResultDisplayEntryList.add(searchResultDisplayEntry2);
                    
                    CustomLayout customLayout = new CustomLayout(IOUtils.toInputStream(searchResultDisplayEntry));
                    customLayout.setWidth(20, Unit.PERCENTAGE);
                    searchResultDisplayEntries.add(customLayout);
                }
                CssLayout searchResultDisplayEntryPanel = new CssLayout();
                final PagingComponent<CustomLayout> pagingComponent = PagingComponent.paginate(searchResultDisplayEntries).addListener(new SimplePagingComponentListener<CustomLayout>(searchResultDisplayEntryPanel) {
                    @Override
                    protected Component displayItem(int i, CustomLayout i1) {
                        return i1;
                    }
                }).numberOfItemsPerPage(10).build();
                searchResultPanel.setContent(searchResultDisplayEntryPanel);
                searchResultPanel.setScrollTop(0);
                paringComponentPanel.setContent(pagingComponent);
            } else {
                is = IOUtils.toInputStream("No Results were found. Try to change search conditions.");
                searchResultPanel.setContent(new CustomLayout(is));
                paringComponentPanel.setContent(null);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
