package cz.atria.dataAggregator.ui;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author rnuriev
 * @since 31.07.2015.
 */
public class SpringHelper {
	final static ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { 
			"classpath*:cz/atria/dataAggregator/core/data-aggregator-core-context.xml",
			"classpath*:cz/atria/dataAggregator/core/data-aggregator-em-context.xml"
	});

	public static <T> T getBean(Class<T> clazz) {
		return context.getBean(clazz);
	}

	public static <T> Collection<T> getBeans(Class<T> clazz) {
		return context.getBeansOfType(clazz).values();
	}

	public static Map<String, Class> getBeans() {
		Map<String, Class> res = new HashMap<>();
		for (String beanName : context.getBeanDefinitionNames()) {
			res.put(beanName, context.getBean(beanName).getClass());
		}
		return res;
	}

	public static Object getBean(String name) {
		return context.getBean(name);
	}
}
