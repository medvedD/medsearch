package cz.atria.dataAggregator.ui.sec;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.security.core.Authentication;

import cz.atria.dataAggregator.ui.SpringHelper;

/**
 * @author rnuriev
 * @since 27.08.2015.
 */
public class SecurityServiceImpl implements SecurityService {
    @Override
    public boolean isGranted(String priv) {
        boolean res = false;
        Authentication authentication = SecurityContext.getAuthentication();
        if (authentication != null) {
            EntityManager em = SpringHelper.getBean(EntityManager.class);
            try {
                res = em.createQuery("select isSet from UserPrivilege where user.login = :login and privilege.name = :privilegeName", Boolean.class)
                        .setParameter("login", authentication.getName())
                        .setParameter("privilegeName", priv)
                        .getSingleResult();
            } catch (NoResultException e) {
                res = false;
            }
        }
        return res;
    }

    @Override
    public boolean isGranted(String parentPriv, String priv) {
        boolean res = false;
        Authentication authentication = SecurityContext.getAuthentication();
        if (authentication != null) {
            EntityManager em = SpringHelper.getBean(EntityManager.class);
            try {
                res = em.createQuery("select coalesce(isSet, false) from UserPrivilege where user.login = :login and privilege.name = :privilegeName and privilege.parent.name = :parentPrivilegeName", Boolean.class)
                        .setParameter("login", authentication.getName())
                        .setParameter("privilegeName", priv)
                        .setParameter("parentPrivilegeName", parentPriv)
                        .getSingleResult();
            } catch (NoResultException e) {
                res = false;
            }
        }
        return res;
    }

    @Override
    public boolean isAnyOfGranted(String priv) {
        boolean res = false;
        Authentication authentication = SecurityContext.getAuthentication();
        if (authentication != null) {
            EntityManager em = SpringHelper.getBean(EntityManager.class);
            res = em.createQuery("select count(1) from UserPrivilege where privilege.parent.name = :parentPrivilegeName and isSet is not null and isSet = true and user.login = :login", Long.class)
                    .setParameter("parentPrivilegeName", priv)
                    .setParameter("login", authentication.getName())
                    .getSingleResult() > 0;
        }
        return res;
    }
}
