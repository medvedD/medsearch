package cz.atria.dataAggregator.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;

import cz.atria.dataAggregator.core.entity.Query;
import cz.atria.dataAggregator.ui.sec.SecurityService;
import cz.atria.dataAggregator.ui.sec.SecurityServiceImpl;

/**
 * @author rnuriev
 * @since 01.09.2015.
 */
@DesignRoot
public class SaveQueryWindow extends Window {

    public interface SaveQueryCallback {
        void save(String name, boolean isUser);
    }

    Button saveQueryButton;
    TextField queryNameField;
    ComboBox queryTypeField;
    public static final String PUBLIC = "Team queries";
    public static final String USER = "My queries";
    final SecurityService securityService = new SecurityServiceImpl();

    public SaveQueryWindow(final SaveQueryCallback saveQueryCallback, Query query) {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/" + getClass().getSimpleName() + ".html"), this);
        center();
        setModal(true);
        queryTypeField.addItems(PUBLIC, USER);
        if (query != null) {
            queryNameField.setValue(query.getName());
            if (query.getUser() == null) {
                queryTypeField.setValue(PUBLIC);
            } else {
                queryTypeField.setValue(USER);
            }
        } else {
            queryTypeField.setValue(USER);
        }

        queryNameField.setValidationVisible(false);
        queryTypeField.setValidationVisible(false);
        queryNameField.addValidator(new StringLengthValidator("Wrong value", 1, 50, false));
        queryTypeField.addValidator(new NullValidator("Type undefined", false));
        saveQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                queryNameField.setValidationVisible(false);
                queryTypeField.setValidationVisible(false);
                try {
                    queryNameField.validate();
                } catch (Validator.InvalidValueException e) {
                    queryNameField.setValidationVisible(true);
                    return;
                }
                try {
                    queryTypeField.validate();
                } catch (Validator.InvalidValueException e) {
                    queryTypeField.setValidationVisible(true);
                    return;
                }
                saveQueryCallback.save(queryNameField.getValue(), queryTypeField.getValue().equals(USER));
                close();
            }
        });

        queryTypeField.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (queryTypeField.getValue() != null) {
                    if (queryTypeField.getValue().equals(PUBLIC) && !securityService.isGranted(MainView.SEARCH_QUERIES_PRIV, MainView.CREATE_PRIV)) {
                        saveQueryButton.setEnabled(false);
                    } else {
                        saveQueryButton.setEnabled(true);
                    }
                }
            }
        });
    }
}
