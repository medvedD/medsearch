package cz.atria.dataAggregator.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.entity.TermElementSynonym;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.ui.component.CheckBoxTermElement;
import cz.atria.dataAggregator.ui.sec.SecurityService;
import cz.atria.dataAggregator.ui.sec.SecurityServiceImpl;

/**
 * @author rnuriev
 * @since 12.08.2015.
 */
@DesignRoot
public class TermElementView extends VerticalLayout {
    BeanItemContainer<TermElement> termElementContainer;
    TreeTable termElementTable;
    TermElement currentExpandedElement = null;
    TextField searchTextField;
    Button addTermElement;
    Button downSearchButton;
    Button upSearchButton;
    TermElement currentSelected;
    List<TermElement> search;
    HorizontalLayout searchTermElementPanel;
//    HorizontalLayout spaceLayout;
    HorizontalLayout toolBoxPanel;
    public static final String synonymNamesFieldName = "synonymNames";
    public static final String descriptionFieldName = "description";
    public static final String selectFieldName = "select";
    public static final String nameFieldName = "name";
    public static final String viewFieldName = "view";

    protected void initSearchNavigateButtons() {
        downSearchButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (search != null && !search.isEmpty()) {
                    if (currentSelected == null) {
                        termElementTable.select(search.get(0));
                        currentSelected = search.get(0);
                    } else {
                        boolean isFound = false;
                        TermElement selectionCandidate = null;
                        for (TermElement termElement : search) {
                            if (isFound) {
                                selectionCandidate = termElement;
                                break;
                            } else {
                                if (termElement == currentSelected) {
                                    isFound = true;
                                }
                            }
                        }
                        if (selectionCandidate == null) {
                            selectionCandidate = search.get(0);
                        }
                        termElementTable.select(selectionCandidate);
                        currentSelected = selectionCandidate;
                    }
                }
            }
        });

        upSearchButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (search != null && !search.isEmpty()) {
                    if (currentSelected == null) {
                        termElementTable.select(search.get(search.size() - 1));
                        currentSelected = search.get(search.size() - 1);
                    } else {
                        boolean isFound = false;
                        TermElement selectionCandidate = null;
                        int i = 0;
                        for (TermElement termElement : search) {
                            if (isFound) {
                                if (i >= 2) {
                                    selectionCandidate = search.get(i - 2);
                                } else {
                                    selectionCandidate = search.get(search.size() - 1);
                                }
                                break;
                            } else {
                                if (termElement == currentSelected) {
                                    isFound = true;
                                    if (i == search.size() - 1) {
                                        selectionCandidate = search.get(search.size() - 2);
                                    }
                                }
                            }
                            i++;
                        }
                        if (selectionCandidate == null) {
                            selectionCandidate = search.get(search.size() - 1);
                        }
                        termElementTable.select(selectionCandidate);
                        currentSelected = selectionCandidate;
                    }
                }
            }
        });
    }

    protected void addItemIfNotExists(TermElement termElement) {
        if (termElement.getParent() != null) {
            if (termElementTable.getParent(termElement) == null) {
                addItemIfNotExists(termElement.getParent());
            }
        } else {
            termElementTable.setParent(termElement, null);
        }
        if (termElementTable.getItem(termElement) == null) {
            termElementTable.addItem(termElement);
        }
        if (termElement.getParent() != null) {
            if (termElementTable.getParent(termElement) == null) {
                termElementTable.setParent(termElement, termElement.getParent());
            }
        }
    }

    protected void initTermElementContainer() {
        final EntityManager em = SpringHelper.getBean(EntityManager.class);
        searchTextField.setImmediate(true);
        searchTextField.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                System.out.println(event);
            }
        });
        searchTextField.addShortcutListener(new ShortcutListener("Shortcut Name", ShortcutAction.KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                termElementTable.removeAllItems();
                currentSelected = null;
                if (StringUtils.isEmpty(searchTextField.getValue())) {
                    refreshResetToRoots();
                } else {
                    search = em.createQuery("from TermElement te where lower(te.name) like lower('%'||:search||'%') " +
                            " or exists (select 1 from TermElementSynonym tes where tes.termElement = te and lower(tes.name) like lower('%'||:search||'%'))" +
                            " or lower(te.description) like lower('%'||:search||'%')",
                            TermElement.class)
                            .setParameter("search", searchTextField.getValue())
                            .getResultList();
                    if (!search.isEmpty()) {
                        for (TermElement o : search) {
                            addItemIfNotExists(o);
                        }
                        Collection items = termElementContainer.getItemIds();
                        for (Object o : items) {
                            termElementTable.setCollapsed(o, false);
                            setChildrenAllowed((TermElement) o);
                        }
                    }
                }
            }
        });
    }

    protected void refreshResetToRoots() {
        termElementContainer.removeAllItems();
        termElementTable.removeAllItems();
        EntityManager em = SpringHelper.getBean(EntityManager.class);
        Collection<TermElement> rootTermElements = em.createQuery("from TermElement where parent is null", TermElement.class).getResultList();
        termElementContainer.addAll(rootTermElements);
        for (TermElement termElement : rootTermElements) {
            setChildrenAllowed(termElement);
        }
    }

    protected void setChildrenAllowed(TermElement termElement) {
        EntityManager em = SpringHelper.getBean(EntityManager.class);
        Long result = em.createQuery("select count(te) from TermElement te where te.parent = :parent", Long.class).setParameter("parent", termElement).getSingleResult();
        if (result == 0) {
            termElementTable.setChildrenAllowed(termElement, false);
        }
    }

    protected void refrechAfterCreateOrEdit(TermElement termElement) {
        refreshResetToRoots();
    }

    SecurityService securityService = new SecurityServiceImpl();
    /**todo: made thread-safe, if need*/
    protected Object selectedCheckBox = null;
    protected TermElement selectedTermElement = null;


    public Object getSelectedCheckBox() {
        return selectedCheckBox;
    }

    public void setSelectedCheckBox(Object selectedCheckBox) {
        this.selectedCheckBox = selectedCheckBox;
    }

    public TermElementView() {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/termElementForm.html"), this);
        initSearchNavigateButtons();
        downSearchButton.setIcon(FontAwesome.CHEVRON_DOWN);
        upSearchButton.setIcon(FontAwesome.CHEVRON_UP);
        final EntityService entityService = SpringHelper.getBean(EntityService.class);
        addTermElement.setEnabled(securityService.isGranted(MainView.VOCABULARY_PRIV, MainView.CREATE_PRIV));
        addTermElement.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                TermElementWindow termElementWindow = new TermElementWindow(new TermElementWindow.SaveCallback() {
                    @Override
                    public void save(String code, String name, Integer parentId, String description, List<TermElementSynonym> synonyms) {
                        TermElement parent = null;
                        if (parentId != null) {
                            parent = entityService.get(TermElement.class, parentId);
                        }
                        TermElement termElement = new TermElement(parent, code, name, description);
                        entityService.persist(termElement);
                        if (parent != null) {
                            entityService.save(parent);
                        }
                        if (synonyms != null) {
                            for (TermElementSynonym termElementSynonym : synonyms) {
                                if (termElementSynonym.getTermElement() == null) {
                                    termElementSynonym.setTermElement(termElement);
                                    entityService.persist(termElementSynonym);
                                }
                            }
                        }
                        refrechAfterCreateOrEdit(termElement);
                        // todo: addItemIfNotExists(termElement);
                        // termElementTable.refreshRowCache();

                    }
                });
                getUI().addWindow(termElementWindow);
            }
        });
        initTermElementContainer();
        final EntityManager em = SpringHelper.getBean(EntityManager.class);
        termElementContainer = new BeanItemContainer<TermElement>(TermElement.class);
        refreshResetToRoots();
        termElementTable.setSelectable(true);

        termElementTable.addCollapseListener(new Tree.CollapseListener() {
            @Override
            public void nodeCollapse(Tree.CollapseEvent event) {
                Collection<?> children = termElementTable.getChildren(event.getItemId());
                if (children != null) {
                    // todo: delete if need
//                    List childrenCopy = new ArrayList();
//                    childrenCopy.addAll(children);
//                    for (Object child : childrenCopy) {
//                        termElementTable.removeItem(child);
//                    }
                }
            }
        });

        termElementTable.addExpandListener(new Tree.ExpandListener() {
            @Override
            public void nodeExpand(Tree.ExpandEvent event) {
                if (StringUtils.isEmpty(searchTextField.getValue())) {
                    currentExpandedElement = (TermElement) event.getItemId();
                    List<TermElement> termElements = em.createQuery("from TermElement where parent = :parent", TermElement.class).setParameter("parent", event.getItemId()).getResultList();
                    if (termElements.isEmpty()) {
                        termElementTable.setChildrenAllowed(event.getItemId(), false);
                    } else {
                        for (TermElement termElement : termElements) {
                            Item item = termElementTable.addItem(termElement);
                            termElementTable.setParent(termElement, event.getItemId());
                            setChildrenAllowed(termElement);
                        }
                    }
                }
            }
        });

        termElementTable.setContainerDataSource(termElementContainer);
        termElementTable.addGeneratedColumn(selectFieldName, new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                CheckBox checkBox = null;
                TermElement termElement = (TermElement) itemId;
                if (itemId instanceof TermElement) {
                    checkBox = new CheckBoxTermElement(termElement);
                } else {
                    checkBox = new CheckBox();
                }
                if (getCheckBoxValueChangeListener() != null) {
                    checkBox.addValueChangeListener(getCheckBoxValueChangeListener());
                }
                if (selectedCheckBox != null && ((CheckBoxTermElement)selectedCheckBox).getTermElement().getId().equals(termElement.getId()) ) {
                    checkBox.setValue(true);
                } else if (selectedTermElement != null && selectedTermElement.getId().equals(termElement.getId())) {
                    checkBox.setValue(true);
                }
                return checkBox;
            }
        });
        termElementTable.addGeneratedColumn(viewFieldName, new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                Label label = new Label(FontAwesome.EYE.getHtml(), ContentMode.HTML);
                label.setCaptionAsHtml(true);
                return label;
            }
        });
        setVisibleColumns(nameFieldName, synonymNamesFieldName, descriptionFieldName);
        termElementTable.setColumnHeader("synonymNames", "Synonyms");
        final String columnNameEdit = "edit";
        final boolean isEditable = securityService.isGranted(MainView.VOCABULARY_PRIV, MainView.EDIT_PRIV);
        termElementTable.addGeneratedColumn(columnNameEdit, new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                String icon = isEditable ? FontAwesome.EDIT.getHtml() : MainView.DISABLED_EDIT;
                Label label = new Label(icon, ContentMode.HTML);
                label.setCaptionAsHtml(true);
                return label;
            }
        });
        final String columnNameDelete = "delete";
        final boolean isDeletable = securityService.isGranted(MainView.VOCABULARY_PRIV, MainView.REMOVE_PRIV);
        termElementTable.addGeneratedColumn(columnNameDelete, new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                String icon = isDeletable ? FontAwesome.TRASH_O.getHtml() : MainView.DISABLED_TRASH;
                Label label = new Label(icon, ContentMode.HTML);
                label.setCaptionAsHtml(true);
                return label;
            }
        });
        termElementTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(final ItemClickEvent event) {
                if (event.getPropertyId().equals(columnNameDelete)) {
                    if (isDeletable) {
                        ConfirmDialog.show(getUI(), "Please Confirm:", "Delete selected record?",
                                "Yes", "Not", new ConfirmDialog.Listener() {
                                    public void onClose(ConfirmDialog dialog) {
                                        if (dialog.isConfirmed()) {
                                            entityService.remove(event.getItemId());
                                            termElementTable.getContainerDataSource().removeItem(event.getItemId());
                                        }
                                    }
                                });
                    }
                } else if (event.getPropertyId().equals(columnNameEdit)) {
                    if (isEditable) {
                        final TermElement termElement = (TermElement) event.getItemId();
                        final TermElement parentBefore = termElement.getParent();
                        TermElementWindow termElementWindow = new TermElementWindow(termElement, new TermElementWindow.SaveCallback() {
                            @Override
                            public void save(String code, String name, Integer parentId, String description, List<TermElementSynonym> synonyms) {
                                TermElement parent = null;
                                if (parentId != null) {
                                    parent = entityService.get(TermElement.class, parentId);
                                }
                                termElement.setParent(parent);
                                termElement.setName(name);
                                termElement.setCode(code);
                                termElement.setDescription(description);
                                entityService.save(termElement);
                                if (termElement.getTermElementSynonyms() != null && !termElement.getTermElementSynonyms().isEmpty()) {
                                    for (TermElementSynonym termElementSynonym : termElement.getTermElementSynonyms()) {
                                        entityService.remove(termElementSynonym);
                                    }
                                    termElement.setTermElementSynonyms(new ArrayList<TermElementSynonym>());
                                }
                                if (synonyms != null) {
                                    for (TermElementSynonym termElementSynonym : synonyms) {
                                        termElementSynonym.setId(null);
                                        termElementSynonym.setTermElement(termElement);
                                        if (termElement.getTermElementSynonyms() == null) {
                                            termElement.setTermElementSynonyms(new ArrayList<TermElementSynonym>());
                                        }
                                        termElement.getTermElementSynonyms().add(termElementSynonym);
                                        if (termElementSynonym.getId() == null) {
                                            entityService.persist(termElementSynonym);
                                        } else {
                                            entityService.save(termElementSynonym);
                                        }
                                    }
                                }
                                if ((parentBefore == null && termElement.getParent() == null) ||
                                        (parentBefore != null && termElement.getParent() != null && parentBefore.getId().equals(termElement.getParent().getId()))) {
                                    termElementTable.refreshRowCache();
                                } else {
                                    refrechAfterCreateOrEdit(termElement);
                                }
                            }
                        });
                        getUI().addWindow(termElementWindow);
                    }
                }
            }
        });

        termElementTable.setItemDescriptionGenerator(new AbstractSelect.ItemDescriptionGenerator() {
            @Override
            public String generateDescription(Component source, Object itemId, Object propertyId) {
                if (propertyId != null && itemId != null) {
                    TermElement termElement = (TermElement) itemId;
                    if (propertyId.equals(synonymNamesFieldName)) {
                        return termElement.getSynonymNames();   // todo: m.b. list with wrapping ?
                    } else if (propertyId.equals(descriptionFieldName)) {
                        return termElement.getDescription();
                    }
                }
                return null;
            }
        });

        termElementTable.setColumnHeader(columnNameEdit, "");
        termElementTable.setColumnHeader(columnNameDelete, "");
        termElementTable.setColumnHeader("name", StringUtils.capitalize("name"));
        termElementTable.setColumnHeader(descriptionFieldName, StringUtils.capitalize(descriptionFieldName));
        termElementTable.setColumnExpandRatio("name", 0.3f);
        termElementTable.setColumnExpandRatio(synonymNamesFieldName, 0.3f);
        termElementTable.setColumnExpandRatio(descriptionFieldName, 0.38f);
        termElementTable.setColumnExpandRatio(columnNameDelete, 0.01f);
        termElementTable.setColumnExpandRatio(columnNameEdit, 0.01f);
    }

    Property.ValueChangeListener checkBoxValueChangeListener;

    public Property.ValueChangeListener getCheckBoxValueChangeListener() {
        return checkBoxValueChangeListener;
    }

    public void setCheckBoxValueChangeListener(Property.ValueChangeListener checkBoxValueChangeListener) {
        this.checkBoxValueChangeListener = checkBoxValueChangeListener;
    }

    public void setVisibleColumns(Object ... names) {
        termElementTable.setVisibleColumns(names);
    }
}
