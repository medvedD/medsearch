package cz.atria.dataAggregator.ui;

import java.io.IOException;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;

/**<a href = "http://stackoverflow.com/questions/15815848/lazily-detemine-filename-when-using-filedownloader">see: http://stackoverflow.com/</a>
 * This specializes {@link FileDownloader} in a way, such that both the file name and content can be determined
 * on-demand, i.e. when the user has clicked the component.
 */
public class OnDemandFileDownloader extends FileDownloader {
    public interface OnDemandStreamResource extends StreamResource.StreamSource {
        String getFilename ();
    }

    private final OnDemandStreamResource onDemandStreamResource;

    public OnDemandFileDownloader (OnDemandStreamResource onDemandStreamResource) {
        super(new StreamResource(onDemandStreamResource, ""));
        this.onDemandStreamResource = onDemandStreamResource;
    }

    @Override
    public boolean handleConnectorRequest (VaadinRequest request, VaadinResponse response, String path)
            throws IOException {
        getResource().setFilename(onDemandStreamResource.getFilename());
        return super.handleConnectorRequest(request, response, path);
    }

    private StreamResource getResource () {
        return (StreamResource) this.getResource("dl");
    }
}
