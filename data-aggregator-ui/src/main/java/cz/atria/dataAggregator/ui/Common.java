package cz.atria.dataAggregator.ui;

import java.util.Iterator;
import java.util.List;

import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;

import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.entity.TermElementSynonym;
import cz.atria.dataAggregator.core.service.EntityService;

/**
 * @author rnuriev
 * @since 08.10.2015.
 */
public class Common {
    public static String constructFullTermList(TermElement termElement, boolean isSynonym, boolean isChildren) {
        StringBuilder sb = new StringBuilder();
        addWithWrap(sb, termElement.getName());
        if (isSynonym) {
            EntityService entityService = SpringHelper.getBean(EntityService.class);
            boolean isFirst = true;
            List<TermElementSynonym> termElementSynonyms = entityService.getTermElementSynonyms(termElement);
            if (!termElementSynonyms.isEmpty()) {
                sb.append(", ");
            }
            for (TermElementSynonym termElementSynonym : termElementSynonyms) {
                if (!isFirst) {
                    sb.append(", ");
                }
                addWithWrap(sb, termElementSynonym.getName());
                if (isFirst) {
                    isFirst = false;
                }
            }
        }
        if (isChildren) {
            EntityService entityService = SpringHelper.getBean(EntityService.class);
            boolean isFirst = true;
            List<TermElement> children = entityService.getChildren(termElement);
            if (!children.isEmpty()) {
                sb.append(", ");
            }
            for (TermElement termElementChild : children) {
                if (!isFirst) {
                    sb.append(", ");
                }
                addWithWrap(sb, termElementChild.getName());
                if (isFirst) {
                    isFirst = false;
                }
            }
        }
        return sb.toString();
    }

    public static void addWithWrap(StringBuilder sb, String val) {
        sb.append("\"");
        sb.append(val);
        sb.append("\"");
    }

    /**
     * return component (from children or itself) of given type
     */
    public static <T> T getComponentOfType(Component component, Class<T> type) {
        if (component == null) {
            return null;
        }
        if (type.isAssignableFrom(component.getClass())) {
            return (T) component;
        } else {
            if (component instanceof HasComponents) {
                Iterator<Component> iterator = ((HasComponents) component).iterator();
                while (iterator.hasNext()) {
                    Component child = iterator.next();
                    T res = getComponentOfType(child, type);
                    if (res != null) {
                        return res;
                    }
                }
            }
        }
        return null;
    }
}
