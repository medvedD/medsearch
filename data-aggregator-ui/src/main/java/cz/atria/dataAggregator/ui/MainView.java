package cz.atria.dataAggregator.ui;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.JavaScriptFunction;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.TextRenderer;
import com.vaadin.ui.themes.BaseTheme;

import cz.atria.dataAggregator.core.entity.Privilege;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.SourceType;
import cz.atria.dataAggregator.core.entity.URIAttributeConverter;
import cz.atria.dataAggregator.core.entity.User;
import cz.atria.dataAggregator.core.entity.UserPrivilege;
import cz.atria.dataAggregator.core.exc.Reason;
import cz.atria.dataAggregator.core.schedule.Scheduler;
import cz.atria.dataAggregator.core.service.DocumentService;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.MediaTypeService;
import cz.atria.dataAggregator.ui.sec.LoginView;
import cz.atria.dataAggregator.ui.sec.SecurityContext;
import cz.atria.dataAggregator.ui.sec.SecurityService;
import cz.atria.dataAggregator.ui.sec.SecurityServiceImpl;
import elemental.json.JsonArray;
import elemental.json.JsonValue;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
@DesignRoot
public class MainView extends VerticalLayout implements View, Refreshable {
    Logger log = LoggerFactory.getLogger(getClass());
    
    public static final String VIEW_NAME = "MainView";
    
    BeanItemContainer<Source> selectedSourcesContainer = new BeanItemContainer<Source>(Source.class);
    public static String resultEntryTemplate;
    public static String resultRootTemplate;
    // Data Sources
    JPAContainer<Source> sourceContainer;
    Grid sourceTable;
    Button addNewSourceButton;
    Button logoutButton;
    Label metaInfLabel;
    
    SearchPanelView searchPanelView;

    static {
        try {
            resultEntryTemplate = IOUtils.toString(MainView.class.getClassLoader().getResourceAsStream("design/resultEntry.template"));
            resultRootTemplate = IOUtils.toString(MainView.class.getClassLoader().getResourceAsStream("design/resultRoot.temaplte"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // users
    Button addUserButton;
    Grid userTable;
    TreeTable privilegeTable;
    /**
     * @deprecated we use directly to table insertion
     */
    @Deprecated
    BeanItemContainer<UserPrivilege> userPrivilegeContainer = new BeanItemContainer<UserPrivilege>(UserPrivilege.class);
    //    JPAContainer<User> userContainer;
    BeanItemContainer<User> userContainer;
    Random rnd = new Random();

    TabSheet mainTabSheet;

    protected Privilege addPrivilegeIfNotExists(Integer id, String name, Privilege parent) {
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        Privilege privilege = entityService.get(Privilege.class, id);
        if (privilege == null) {
            privilege = new Privilege(id, name, parent);
            entityService.save(privilege);
        }
        return privilege;
    }

    final static String CREATE_PRIV = "Create";
    final static String EDIT_PRIV = "Edit";
    final static String VIEW_PRIV = "View";
    final static String REMOVE_PRIV = "Remove";

    protected Privilege initPrivilegeGroup(Integer parentId, String name) {
        Privilege parent = addPrivilegeIfNotExists(parentId, name, null);
        addPrivilegeIfNotExists(++parentId, CREATE_PRIV, parent);
        addPrivilegeIfNotExists(++parentId, EDIT_PRIV, parent);
        addPrivilegeIfNotExists(++parentId, VIEW_PRIV, parent);
        return addPrivilegeIfNotExists(++parentId, REMOVE_PRIV, parent);
    }

    protected void initPrivilegesForUser(User user) {
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        for (Privilege privilege : entityService.get(Privilege.class)) {
            UserPrivilege userPrivilege = entityService.getUserPrivilege(user, privilege);
            if (userPrivilege == null) {
                userPrivilege = new UserPrivilege(user, privilege);
                entityService.persist(userPrivilege);
            }
        }
    }

    final static String VOCABULARY_PRIV = "Vocabulary";
    final static String DATA_SOURCES_PRIV = "Data sources";
    final static String SEARCH_QUERIES_PRIV = "Team search queries";
    final static String USERS_PRIV = "Users";

    protected void initPrivilegeData() {
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        EntityManager em = SpringHelper.getBean(EntityManager.class);
        Privilege lastPrivilege = initPrivilegeGroup(1, VOCABULARY_PRIV);
        lastPrivilege = initPrivilegeGroup(lastPrivilege.getId() + 1, DATA_SOURCES_PRIV);
        lastPrivilege = initPrivilegeGroup(lastPrivilege.getId() + 1, SEARCH_QUERIES_PRIV);
        lastPrivilege = initPrivilegeGroup(lastPrivilege.getId() + 1, USERS_PRIV);
        for (User user : entityService.get(User.class)) {
            initPrivilegesForUser(user);
        }
    }

    /**
     * todo: temporary used. It's only indication of user selection
     */
    User selectedUser = null;

    /**
     * this char used for equality of representation of password mask char in view and edit modes
     */
    static String BULLET_CHAR;

    static {
        try {
            BULLET_CHAR = new String(new byte[]{(byte) 0xE2, (byte) 0x80, (byte) 0xA2}, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    protected void initUserTab() {
//        userContainer = JPAContainerFactory.make(User.class, SpringHelper.getBean(EntityManager.class));

        final EntityService entityService = SpringHelper.getBean(EntityService.class);
        final List<User> users = entityService.get(User.class);
        userContainer = new BeanItemContainer<User>(User.class, users);
        final String columnNameDelete = "delete";
        final boolean isEditable = securityService.isGranted(USERS_PRIV, EDIT_PRIV);
        addUserButton.setEnabled(securityService.isGranted(USERS_PRIV, CREATE_PRIV));
        addUserButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                userTable.setEditorEnabled(true);
                User bean = new User();
                // todo: this explicitly initialization due to NullRepresentation doesn't work
                bean.setEmail("");
                bean.setLogin("");
                bean.setPassword("");

                userTable.getContainerDataSource().addItem(bean);
                for (Field field : userTable.getEditorFieldGroup().getFields()) {
                    if (field instanceof TextField) {
                        ((TextField) field).setNullRepresentation("");
                    }
                }
            }
        });


        GeneratedPropertyContainer userContainerWithDelete = new GeneratedPropertyContainer(userContainer);
        final boolean isDeletable = securityService.isGranted(USERS_PRIV, REMOVE_PRIV);
        userContainerWithDelete.addGeneratedProperty(columnNameDelete, new PropertyValueGenerator<String>() {
            @Override
            public String getValue(Item item, Object itemId, Object propertyId) {
                if (isDeletable) {
                    return FontAwesome.TRASH_O.getHtml();
                } else {
                    return DISABLED_TRASH;
                }
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }
        });
        userTable.setContainerDataSource(userContainerWithDelete);
        userTable.setColumns("login", "email", "password", columnNameDelete);
        userTable.getColumn("password").setRenderer(new TextRenderer() {
            @Override
            public JsonValue encode(String value) {
                if (StringUtils.isEmpty(value)) {
                    return null;
                } else {
                    return encode(StringUtils.repeat(BULLET_CHAR, 5 + rnd.nextInt(3)), String.class);
                }
            }
        });

        HtmlRenderer renderer = new HtmlRenderer();
        userTable.getColumn(columnNameDelete).setRenderer(renderer);
        userTable.getColumn(columnNameDelete).setHeaderCaption("");
        userTable.getColumn(columnNameDelete).setExpandRatio(0);
        userTable.getColumn(columnNameDelete).setEditable(false);
        userTable.getColumn("login").setExpandRatio(1);
        userTable.getColumn("email").setExpandRatio(2);
        userTable.setImmediate(true);
        userTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(final ItemClickEvent event) {
                final User user = (User) event.getItemId();
                if (user.getId() != null) {
                    userTable.setEditorEnabled(isEditable);
                }
                if (event.isDoubleClick() && !event.getPropertyId().equals(columnNameDelete)) {
                    //
                } else {
                    if (event.getPropertyId().equals(columnNameDelete)) {
                        if (user.getId() == null) {
                            userTable.getContainerDataSource().removeItem(user);
                        } else {
                            if (isDeletable) {
                                ConfirmDialog.show(getUI(), "Please Confirm:", "Delete selected record?",
                                        "Yes", "Not", new ConfirmDialog.Listener() {
                                            public void onClose(ConfirmDialog dialog) {
                                                if (dialog.isConfirmed()) {
                                                    entityService.remove(user);
                                                    userTable.getContainerDataSource().removeItem(user);
                                                }
                                            }
                                        });
                            }
                        }
                    } else {
                        if (selectedUser != null && user != null && selectedUser.getId() != null && user.getId() != null && selectedUser.getId().equals(user.getId())) {
                            return;
                        } else {
                            selectedUser = user;
                            refreshPrivileges(user);
                        }
                    }
                }
                selectedUser = user;
            }
        });
        final ErrorHandler defaultErrorHandler = userTable.getErrorHandler();
        userTable.setErrorHandler(new ErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                // todo: temporary. Due to exception when removing empty row: "IllegalArgumentException: Given item id (....) does not exist in the container"
                if (event.getThrowable() != null && event.getThrowable().getCause() != null && event.getThrowable().getCause().getCause() != null && event.getThrowable().getCause().getCause() instanceof IllegalArgumentException) {
                    log.error("", event.getThrowable());
                } else if (defaultErrorHandler != null) {
                    defaultErrorHandler.error(event);
                }
            }
        });

        // privileges
        initPrivilegeData();
//        privilegeTable.setContainerDataSource(userPrivilegeContainer);
        privilegeTable.addContainerProperty("privilegeName", String.class, null);
        privilegeTable.addContainerProperty("isSet", Boolean.class, null);
        privilegeTable.setVisibleColumns("privilegeName", "isSet");
        privilegeTable.addGeneratedColumn("isSet", new Table.ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                if (selectedUser == null) {
                    return null;
                }
                UserPrivilege userPrivilege = null;
                if (itemId instanceof UserPrivilege) {
                    userPrivilege = (UserPrivilege) itemId;
                } else if (itemId instanceof Integer) {
                    userPrivilege = entityService.get(UserPrivilege.class, itemId);
                }
                if (userPrivilege.getPrivilege().getParent() != null) {
                    boolean isSet = userPrivilege.getIsSet() == null ? false : userPrivilege.getIsSet();
                    CheckBox checkBox = new CheckBox(null, isSet);
                    checkBox.addValueChangeListener(new Property.ValueChangeListener() {
                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            CheckBox checkBoxChecked = (CheckBox) ((Field.ValueChangeEvent) event).getComponent();
                            Boolean val = (Boolean) event.getProperty().getValue();
                            UserPrivilege userPrivilegeChecked = checkBoxUserPrivilegeMap.get(checkBoxChecked);
                            if (userPrivilegeChecked != null && !userPrivilegeChecked.getIsSet().equals(val)) {
                                userPrivilegeChecked.setIsSet(val);
                                entityService.save(userPrivilegeChecked);
                            }
                        }
                    });
                    checkBoxUserPrivilegeMap.put(checkBox, userPrivilege);
                    checkBox.setEnabled(securityService.isGranted(USERS_PRIV, EDIT_PRIV));
                    return checkBox;
                } else {
                    return null;
                }
            }
        });

        privilegeTable.setColumnExpandRatio("privilegeName", 0.95f);
        privilegeTable.setColumnExpandRatio("isSet", 0.05f);
        privilegeTable.setColumnHeader("privilegeName", "");
        privilegeTable.setColumnHeader("isSet", "");
        userTable.setFrozenColumnCount(4);  // todo: temporary due to automatic horizontal scrolling, which is not allowed
        refreshPrivileges(null);

        userTable.setEditorFieldGroup(new FieldGroup() {
            @Override
            public <T extends Field> T buildAndBind(String caption, Object propertyId, Class<T> fieldType) throws BindException {
                T res = null;
                if (propertyId.equals("password")) {
                    PasswordField passwordField = new PasswordField();
                    passwordField.setCaption(caption);
                    T field = (T) passwordField;
                    bind(field, propertyId);
                    return field;
                } else {
                    res = super.buildAndBind(caption, propertyId, fieldType);
                }
                return res;
            }

            @Override
            public void commit() throws CommitException {
                try {
                    super.commit();
                } catch (CommitException e) {
                    ConstraintViolationException constraintViolationException = null;
                    try {
                        constraintViolationException = (ConstraintViolationException) e.getCause().getCause().getCause();
                    } catch (Exception e1) {
                        if (e.getCause() instanceof ConstraintViolationException) {
                            constraintViolationException = (ConstraintViolationException) e.getCause();
                        }
                    }
                    if (constraintViolationException != null) {
                        // throw new DataAggregatorException(, constraintViolationException, Reason.CONSTRAINT_VIOLATION);
                        ConstraintViolation<?> constraintViolation = constraintViolationException.getConstraintViolations().iterator().next();
                        Notification.show(Reason.CONSTRAINT_VIOLATION.getMessage(), constraintViolation.getMessage() + ": " + constraintViolation.getPropertyPath().toString(), Notification.Type.ERROR_MESSAGE);
                    } else {
                        throw e;
                    }
                }
            }
        });
        userTable.setEditorFieldFactory(Grid.EditorFieldFactory.get());
        userTable.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
            @Override
            public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                System.out.println(commitEvent);
            }

            @Override
            public void postCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                User user = null;
                Item itemDataSource = commitEvent.getFieldBinder().getItemDataSource();
                if (itemDataSource instanceof BeanItem) {
                    user = (((BeanItem<User>) itemDataSource).getBean());
                } else {
                    // todo: тут получается GeneratedPropertyItem. Пока непонятно как получить непосредственно bean.
                    Object id = itemDataSource.getItemProperty("id").getValue();
                    for (User user1 : userContainer.getItemIds()) {
                        if ((id == null && user1.getId() == null) || (id != null && user1.getId() != null && user1.getId().equals(id))) {
                            user = user1;
                            break;
                        }
                    }
                }
                if (user.getId() == null) {
                    try {
                        entityService.persist(user);
                    } catch (Exception e) {
                        user.setId(null);   // jpa may set id, regardless of exception
                        throw e;
                    }
                    initPrivilegesForUser(user);
                    refreshPrivileges(user);
                    userTable.select(user);
                } else {
                    entityService.save(user);
                }
            }
        });
    }

    protected HashMap<CheckBox, UserPrivilege> checkBoxUserPrivilegeMap = new HashMap<>();

    protected void refreshPrivileges(User user) {
        // todo: this is simplified use of tree table (w\o explicitly Container). Try to use HierarchicalContainer.
        checkBoxUserPrivilegeMap.clear();
        privilegeTable.removeAllItems();
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        List list = null;
        if (user != null) {
            list = entityService.getUserPrivilegesOrdered(user);
        } else {
            list = entityService.getPrivilegesOrdered();
        }
        Object parentItem = null;
        for (Object o : list) {
            UserPrivilege userPrivilege = o instanceof UserPrivilege ? (UserPrivilege) o : null;
            Privilege privilege = o instanceof Privilege ? (Privilege) o : userPrivilege.getPrivilege();
            Object item = privilegeTable.addItem(new Object[]{privilege.getName()}, userPrivilege != null ? userPrivilege.getId() : null);
            if (privilege.getParent() == null) {
                parentItem = item;
                privilegeTable.setCollapsed(item, false);
            } else {
                privilegeTable.setParent(item, parentItem);
                privilegeTable.setChildrenAllowed(item, false);
            }
        }
    }

    public static Object getQuery() {
        return SessionDataHolder.getQuery(Page.getCurrent().getWebBrowser().getAddress());
    }

    public static void setQuery(Object o) {
        SessionDataHolder.setQuery(Page.getCurrent().getWebBrowser().getAddress(), o);
    }

    public static final String DISABLED_PRE = "<font color = 'lightgrey'>";
    public static final String DISABLED_POST = "</font>";
    public static final String DISABLED_EDIT = DISABLED_PRE + FontAwesome.EDIT.getHtml() + DISABLED_POST;
    public static final String DISABLED_TRASH = DISABLED_PRE + FontAwesome.TRASH_O.getHtml() + DISABLED_POST;
    public static final double defaultControlWidth = 45.0;


    protected void initSourceTab() {
        sourceContainer = JPAContainerFactory.make(Source.class, SpringHelper.getBean(EntityManager.class));
        GeneratedPropertyContainer containerWithEdit = new GeneratedPropertyContainer(sourceContainer);
        final String columnNameEdit = "Edit";
        final boolean isEditable = securityService.isGranted(DATA_SOURCES_PRIV, EDIT_PRIV);
        containerWithEdit.addGeneratedProperty(columnNameEdit, new PropertyValueGenerator<String>() {
            @Override
            public String getValue(Item item, Object itemId, Object propertyId) {
                if (isEditable) {
                    return FontAwesome.EDIT.getHtml();
                } else {
                    return DISABLED_EDIT;
                }
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }
        });
        GeneratedPropertyContainer containerWithEditAndDelete = new GeneratedPropertyContainer(containerWithEdit);
        final String columnNameDelete = "Delete";
        final boolean isDeletable = securityService.isGranted(DATA_SOURCES_PRIV, REMOVE_PRIV);
        containerWithEditAndDelete.addGeneratedProperty(columnNameDelete, new PropertyValueGenerator<String>() {
            @Override
            public String getValue(Item item, Object itemId, Object propertyId) {
                if (item != null && item.getItemProperty("type") != null && item.getItemProperty("type").getValue() != null && item.getItemProperty("type").getValue().equals(SourceType.GOOGLE_ALERT)) {
                    return DISABLED_TRASH;
                } else {
                    if (isDeletable) {
                        return FontAwesome.TRASH_O.getHtml();
                    } else {
                        return DISABLED_TRASH;
                    }
                }
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }
        });

        sourceTable.setContainerDataSource(containerWithEditAndDelete);
        containerWithEditAndDelete.addGeneratedProperty("uri", new PropertyValueGenerator<String>() {
            @Override
            public String getValue(Item item, Object itemId, Object propertyId) {
                Object uri = item.getItemProperty("uri").getValue();
                String uriDecoded = null;
                try {
                    uriDecoded = uri != null && uri instanceof URI ? URLDecoder.decode(uri.toString(), URIAttributeConverter.defaultCharset) : null;
                } catch (UnsupportedEncodingException e) {
                    log.warn("", e);
                    uriDecoded = uri.toString();
                }
                if (uri != null) {
                    if (item != null && item.getItemProperty("type") != null && item.getItemProperty("type").getValue() != null && item.getItemProperty("type").getValue().equals(SourceType.GOOGLE_ALERT)) {
                        uriDecoded = "<a href='" + uriDecoded + "'>" + uriDecoded + "</a>";
                    }
                }
                return uriDecoded != null ? uriDecoded : null;
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }
        });


        sourceTable.setColumns("name", "uri", columnNameEdit, columnNameDelete);
        sourceTable.getColumn("name").setHeaderCaption("Name");
        sourceTable.getColumn("uri").setHeaderCaption("Link");
        sourceTable.getColumn("uri").setRenderer(new HtmlRenderer());
        sourceTable.getColumn(columnNameEdit).setHeaderCaption("");
        sourceTable.getColumn(columnNameDelete).setHeaderCaption("");

        // sizes
        sourceTable.getColumn(columnNameEdit).setWidth(defaultControlWidth);
        sourceTable.getColumn(columnNameDelete).setWidth(defaultControlWidth);

        HtmlRenderer rendererEdit = new HtmlRenderer("");
        sourceTable.getColumn(columnNameEdit).setRenderer(rendererEdit);
        HtmlRenderer rendererDelete = new HtmlRenderer("");
        sourceTable.getColumn(columnNameDelete).setRenderer(rendererDelete);
        sourceTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(final ItemClickEvent event) {
                final EntityService entityService = SpringHelper.getBean(EntityService.class);
                final Source source = entityService.get(Source.class, event.getItemId());
                final URI uriCurrent = source.getUri();
                if (event.getPropertyId().equals(columnNameEdit)) {
                    if (isEditable) {
                    	EditSourceWindow editSourceWindow = new EditSourceWindow(source);
                        editSourceWindow.getFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
                            @Override
                            public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                            }

                            @Override
                            public void postCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                                Object o = sourceContainer.addEntity(source);
                                Thread editSourceThread = new Thread(new EditSourceThread(uriCurrent, source, o));
                                editSourceThread.start();
                            }
                        });
                    }
                } else if (event.getPropertyId().equals(columnNameDelete)) {
                    if (isDeletable) {
                        ConfirmDialog.show(getUI(), "Please Confirm:", "Delete selected record?",
                                "Yes", "Not", new ConfirmDialog.Listener() {
                                    public void onClose(ConfirmDialog dialog) {
                                        if (dialog.isConfirmed()) {
                                            sourceTable.getContainerDataSource().removeItem(event.getItemId());
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Scheduler scheduler = SpringHelper.getBean(Scheduler.class);
                                                    scheduler.removeOrphanUploadedFiles();
                                                }
                                            }).start();
                                        }
                                    }
                                });
                    }
                }
            }
        });
        addNewSourceButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final Source source = new Source();
                EditSourceWindow editSourceWindow = new EditSourceWindow(source);
                editSourceWindow.getFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
                    @Override
                    public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                    }

                    @Override
                    public void postCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                        Object o = sourceContainer.addEntity(source);
                        EntityService entityService = SpringHelper.getBean(EntityService.class);
                        Source sourceSaved = entityService.get(Source.class, o);
                        Thread saveSourceThread = new Thread(new SaveSourceThread(sourceSaved, o));
                        saveSourceThread.start();
                    }
                });
            }
        });
        sourceTable.setFrozenColumnCount(sourceTable.getColumns().size());
    }

    @Override
    public void refresh() {
        int tabIndex = mainTabSheet.getTabIndex();
        if (tabIndex == TAB_IDX_SOURCE) {
            sourceContainer.refresh();
        }
        // todo: complete
    }

    class EditSourceThread implements Runnable {
        URI uriCurrent;
        Source sourceEdited;
        Object entityId;

        public EditSourceThread(URI uriCurrent, Source sourceEdited, Object o) {
            this.uriCurrent = uriCurrent;
            this.sourceEdited = sourceEdited;
            this.entityId = o;
        }

        @Override
        public void run() {
            // todo: is mediaType change have to be considered?
            if (!uriCurrent.toASCIIString().equals(sourceEdited.getUri().toASCIIString())) {
                DocumentService documentService = SpringHelper.getBean(DocumentService.class);
                documentService.removeDocumentBySource(sourceEdited);
                documentService.createDocumentBySource((Integer) entityId);
            }
        }
    }

    class SaveSourceThread implements Runnable {
        Source source;
        Object entityId;

        public SaveSourceThread(Source source, Object entityId) {
            this.source = source;
            this.entityId = entityId;
        }

        @Override
        public void run() {
            if (source.getMediaType() == null) {
                MediaTypeService mediaTypeService = SpringHelper.getBean(MediaTypeService.class);

                // <todo:this is temporary, due to URI conversion issue>
                URI actualUri = cz.atria.dataAggregator.core.Common.normilize(source.getUri());
                // </todo>
                source.setMediaType(mediaTypeService.detectMediaType(actualUri, org.springframework.http.MediaType.class));
                EntityService entityService = SpringHelper.getBean(EntityService.class);
                entityService.save(source);
            }
            DocumentService documentService = SpringHelper.getBean(DocumentService.class);
            documentService.createDocumentBySource(source);
        }
    }

    protected void initVocabularyTab() {
        // todo:
    }

    SecurityService securityService = new SecurityServiceImpl();
    public static final int TAB_IDX_SOURCE = 0;

    protected void setAccess() {
    	if (SecurityContext.getAuthentication() == null) {
            mainTabSheet.getTab(TAB_IDX_SOURCE).setVisible(false);
            mainTabSheet.getTab(1).setVisible(false);
            mainTabSheet.getTab(3).setVisible(false);
            addNewSourceButton.setEnabled(securityService.isGranted(DATA_SOURCES_PRIV, CREATE_PRIV));
    	} else {
            mainTabSheet.getTab(TAB_IDX_SOURCE).setVisible(securityService.isAnyOfGranted(DATA_SOURCES_PRIV));
            mainTabSheet.getTab(1).setVisible(securityService.isAnyOfGranted(VOCABULARY_PRIV));
            mainTabSheet.getTab(3).setVisible(securityService.isAnyOfGranted(USERS_PRIV));
            addNewSourceButton.setEnabled(securityService.isGranted(DATA_SOURCES_PRIV, CREATE_PRIV));
    	}
    }




    public MainView() {
    	addStyleName("main-background");
        Design.read(getClass().getClassLoader().getResourceAsStream("design/main.html"), this);
        sourceTable.setId("sourceTableSourceTabId");

//        distanceLabel.setWidthUndefined();
        initSourceTab();
        initVocabularyTab();
        initUserTab();
        logoutButton.setStyleName(BaseTheme.BUTTON_LINK);
        logoutButton.addStyleName("background-white");
        logoutButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
            	if (SecurityContext.getAuthentication() != null) { 
            		SecurityContext.clear();
            		UI.getCurrent().close();
            		VaadinSession.getCurrent().close();
            		Page.getCurrent().setLocation("main"); // todo: sync with UI path
            	} else {
            		UI.getCurrent().getNavigator().navigateTo(LoginView.VIEW_NAME);
            	}
            }
        });
        setAccess();
//        try {
//            GitRepositoryState gitRepositoryState = SpringHelper.getMetaInfBean(GitRepositoryState.class);
//            metaInfLabel.setValue("id:" + gitRepositoryState.getCommitIdAbbrev() + " id.dt:" + gitRepositoryState.getCommitTime() + " build.dt:" + gitRepositoryState.getBuildTime());
//        } catch (Exception e) {
//            log.warn("metaInf obtain error", e);
//        }
        Page.getCurrent().addBrowserWindowResizeListener(new Page.BrowserWindowResizeListener() {
            @Override
            public void browserWindowResized(Page.BrowserWindowResizeEvent event) {
                adjustToWindow();
            }
        });
        JavaScript.getCurrent().addFunction("GetComponentWidth",
                new JavaScriptFunction() {
                    @Override
                    public void call(JsonArray jsonArray) {
                        String id = jsonArray.getString(0);
                        final double width = jsonArray.getNumber(1);
                        final double verticalScrollerWidth = jsonArray.getNumber(2);
                        if (id.equals(sourceTable.getId())) {
                            double nameAndUriWidth = width - 2 * defaultControlWidth - verticalScrollerWidth;
                            sourceTable.getColumn("name").setWidth(nameAndUriWidth * nameFieldPart);
                            sourceTable.getColumn("uri").setWidth(nameAndUriWidth * uriFieldPart);
                        } else if (id.equals(searchPanelView.selectedSourcesTable.getId())) {
                            double nameAndUriWidth = width - defaultControlWidth;
                            searchPanelView.selectedSourcesTable.getColumn(SearchPanelView.sourceNameColumnName).setWidth(nameAndUriWidth * nameFieldPart);
                            searchPanelView.selectedSourcesTable.getColumn(SearchPanelView.sourceUriColumnName).setWidth(nameAndUriWidth * uriFieldPart);
                        }
                    }
                });

//        adjustToWindow(); // this not actually work on init time => set width explicitly
        double browserWindowWidth = Page.getCurrent().getBrowserWindowWidth();
        sourceTable.getColumn("uri").setWidth(browserWindowWidth * 0.5);
        searchPanelView.selectedSourcesTable.getColumn("uri").setWidth(browserWindowWidth * 0.5);
    }

    static double uriFieldPart = 0.6;
    static double nameFieldPart = 0.4;

    protected void executeGetComponentWidthFor(String id) {
        JavaScript.getCurrent().execute("GetComponentWidth('" +
                id +
                "', document.getElementById('" + id + "').clientWidth, document.getElementsByClassName('v-grid-scroller-vertical')[0].childNodes[0].clientWidth);");
    }

    protected void executeGetComponentWidth() {
        executeGetComponentWidthFor(sourceTable.getId());
        executeGetComponentWidthFor(searchPanelView.selectedSourcesTable.getId());
    }

    protected void adjustToWindow() {
        executeGetComponentWidth();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
//        if (SecurityContext.getAuthentication() == null) {
//            UI.getCurrent().getNavigator().navigateTo(LoginView.VIEW_NAME);
//        } else {
    	if (SecurityContext.getAuthentication() == null) { 
    		Navigator navigator = UI.getCurrent().getNavigator();
        	navigator.navigateTo(BasicSearchView.VIEW_NAME);
    	} else {
    		logoutButton.setCaption(SecurityContext.getAuthentication().getName() + " (Logout)");
    	}
        setAccess();
        mainTabSheet.setSelectedTab(2);
    }
}
