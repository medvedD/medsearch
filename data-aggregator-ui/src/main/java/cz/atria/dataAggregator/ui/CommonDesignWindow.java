package cz.atria.dataAggregator.ui;

import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;

/**
 * @author rnuriev
 * @since 08.09.2015.
 */
public abstract class CommonDesignWindow extends Window {
    public CommonDesignWindow() {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/" + getClass().getSimpleName() + ".html"), this);
        center();
        setModal(true);
        UI.getCurrent().addWindow(this);
    }
}
