package cz.atria.dataAggregator.ui;

import java.util.ArrayList;
import java.util.Collection;

import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Component;

/**
 * @author rnuriev
 * @since 05.10.2015.
 */
public class VaadinUtil {
    public static Collection<Component> getChildren(AbstractOrderedLayout layout) {
        Collection<Component> res = new ArrayList<>();
        for (int i = 0; i < layout.getComponentCount(); i++) {
            Component component = layout.getComponent(i);
            if (component instanceof AbstractOrderedLayout) {
                res.addAll(getChildren((AbstractOrderedLayout) component));
            } else {
                res.add(component);
            }
        }
        return res;
    }

    /**explicitly set readOnly property for all children, cause seems there is no possibility to set it implicitly
     * (that is by parent only.) M.b. this is: <a href="https://dev.vaadin.com/ticket/9144">vaadin#9144</a>
     * */
    public static void setReadOnly(AbstractOrderedLayout layout, boolean readOnly) {
        Collection<Component> components = getChildren(layout);
        for (Component component : components) {
            component.setReadOnly(readOnly);
        }
    }
}
