package cz.atria.dataAggregator.ui;

import java.util.Collection;

import javax.persistence.EntityManagerFactory;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;

import cz.atria.dataAggregator.core.entity.Source;

/**
 * @author rnuriev
 * @since 31.07.2015.
 */
@DesignRoot
public class SourcesWindow extends Window {
    JPAContainer<Source> sourceContainer;
    Grid sourcesTable;
    Button selectSourcesButton;

    public interface SourceSelectedCallback {
        void process(Source source);
    }

    SourceSelectedCallback sourceSelectedCallback;

    public SourcesWindow(final SourceSelectedCallback sourceSelectedCallback) {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/sources-window.html"), this);
        this.sourceSelectedCallback = sourceSelectedCallback;
        sourceContainer = JPAContainerFactory.make(Source.class, SpringHelper.getBean(EntityManagerFactory.class).createEntityManager());
        sourcesTable.setContainerDataSource(sourceContainer);
        sourcesTable.setColumns("name", "uri");
        sourcesTable.getColumn("uri").setHeaderCaption("Link");
        sourcesTable.getColumn("uri").setConverter(new StringURIWithSpaceConverter());
        sourcesTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    Source source = (Source) ((JPAContainerItem) event.getItem()).getEntity();
                    sourceSelectedCallback.process(source);
                }
            }
        });
        sourcesTable.getColumn("name").setExpandRatio(1);
        sourcesTable.getColumn("uri").setExpandRatio(2);
        sourcesTable.setFrozenColumnCount(sourcesTable.getColumns().size());
        setModal(true);
        selectSourcesButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Collection<Object> selectedRows = sourcesTable.getSelectedRows();
                for (Object o : selectedRows) {
                    Source source = ((JPAContainerItem<Source>)sourcesTable.getContainerDataSource().getItem(o)).getEntity();
                    sourceSelectedCallback.process(source);
                    close();
                }
            }
        });
    }
}
