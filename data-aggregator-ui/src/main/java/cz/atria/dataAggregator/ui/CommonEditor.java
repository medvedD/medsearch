package cz.atria.dataAggregator.ui;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;

import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.Window;

/**
 * @author rnuriev
 * @since 03.08.2015.
 */
public abstract class CommonEditor extends Window implements Button.ClickListener, FormFieldFactory {
    protected final Item item;
    protected Form editorForm;
    protected Button saveButton;
    protected Button cancelButton;

    public CommonEditor(Item item, String[] fields) {
        this.item = item;
        editorForm = new Form();
        editorForm.setFormFieldFactory(this);
        editorForm.setBuffered(true);
        editorForm.setImmediate(true);
        editorForm.setItemDataSource(item, Arrays.asList(fields));

        saveButton = new Button("Save", this);
        cancelButton = new Button("Cancel", this);

        editorForm.getFooter().addComponent(saveButton);
        editorForm.getFooter().addComponent(cancelButton);
        if (editorForm.getFooter() instanceof AbstractOrderedLayout) {
            ((AbstractOrderedLayout)editorForm.getFooter()).setSpacing(true);
        }
        ((AbstractOrderedLayout)editorForm.getFooter()).setComponentAlignment(cancelButton, Alignment.BOTTOM_RIGHT);
        ((AbstractOrderedLayout)editorForm.getFooter()).setComponentAlignment(saveButton, Alignment.BOTTOM_RIGHT);

        ((AbstractOrderedLayout)editorForm.getFooter()).setMargin(new MarginInfo(true, true, true, true));
//        ((AbstractOrderedLayout)editorForm.getFooter()).setSpacing(true);
//        setHeight(100, Unit.PERCENTAGE);
//        setWidthUndefined();
        setWidth(30, Unit.PERCENTAGE);
        ((AbstractOrderedLayout)editorForm.getLayout()).setMargin(new MarginInfo(true, true, true, true));
        ((AbstractOrderedLayout) editorForm.getLayout()).setSpacing(true);
        editorForm.getLayout().setSizeFull();
        setContent(editorForm);
        center();
        setModal(true);
    }
    @Override
    public void buttonClick(Button.ClickEvent event) {
        if (event.getButton() == saveButton) {
            try {
                validate();
            } catch (Validator.InvalidValueException e) {
                setValidationVisible(true);
                return;
            }
            editorForm.commit();
            fireEvent(new EditorSavedEvent(this, item));
        } else if (event.getButton() == cancelButton) {
            editorForm.discard();
        }
        close();
    }

    protected abstract void setValidationVisible(boolean b);

    protected abstract void validate() throws Validator.InvalidValueException;

    public void addListener(EditorSavedListener listener) {
        try {
            Method method = EditorSavedListener.class.getDeclaredMethod(
                    "editorSaved", new Class[] { EditorSavedEvent.class });
            addListener(EditorSavedEvent.class, listener, method);
        } catch (final NoSuchMethodException e) {
            // This should never happen
            throw new RuntimeException(
                    "Internal error, editor saved method not found");
        }
    }

    public void removeListener(EditorSavedListener listener) {
        removeListener(EditorSavedEvent.class, listener);
    }
    public static class EditorSavedEvent extends Component.Event {

        private Item savedItem;

        public EditorSavedEvent(Component source, Item savedItem) {
            super(source);
            this.savedItem = savedItem;
        }

        public Item getSavedItem() {
            return savedItem;
        }
    }

    public interface EditorSavedListener extends Serializable {
        public void editorSaved(EditorSavedEvent event);
    }
}
