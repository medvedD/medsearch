package cz.atria.dataAggregator.ui.sec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import cz.atria.dataAggregator.core.entity.User;
import cz.atria.dataAggregator.ui.SpringHelper;

/**
 * @author rnuriev
 * @since 27.08.2015.
 */
public class UserDetailsServiceImpl implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        EntityManager em = SpringHelper.getBean(EntityManager.class);
        try {
            User user = em.createQuery("from User where login = :login", User.class).setParameter("login", username).getSingleResult();
            List<? extends GrantedAuthority> authorities = new ArrayList<>();
            // todo: add authorities
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);
            return userDetails;
        } catch (NoResultException e) {
            throw new UsernameNotFoundException("username=" + username, e);
        }
    }
}
