package cz.atria.dataAggregator.ui.component;

import com.vaadin.server.Page;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/**
 * @author rnuriev
 * @since 09.09.2015.
 */
public class ProgressBarWindow extends Window {
    ProgressBar progressBar;

    public ProgressBarWindow() {
        progressBar = new ProgressBar();
        setContent(progressBar);
        UI.getCurrent().addWindow(this);
        int browserWindowWidth = Page.getCurrent().getBrowserWindowWidth();
        int browserWindowHeight = Page.getCurrent().getBrowserWindowHeight();
        setPositionX((int) (browserWindowWidth - getWidth()));
        setPositionY((int) (browserWindowHeight - getHeight()));
        setVisible(false);
    }

    public void show() {
        setVisible(true);
    }

    public void hide() {
        setVisible(false);
    }

    public void reset() {
        progressBar.setValue(0f);
    }
    public void setValue(Float value) {
        progressBar.setValue(value);
    }
}
