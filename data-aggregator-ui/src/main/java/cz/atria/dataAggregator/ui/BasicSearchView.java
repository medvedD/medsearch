package cz.atria.dataAggregator.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.ui.component.PositiveIntTextField;
import cz.atria.dataAggregator.ui.component.SearchComponent;
import cz.atria.dataAggregator.ui.pages.PageAboutView;
import cz.atria.dataAggregator.ui.pages.PageContactsView;
import cz.atria.dataAggregator.ui.pages.PageFaqView;
import cz.atria.dataAggregator.ui.pages.PageTermsView;
import cz.atria.dataAggregator.ui.sec.LoginView;

public class BasicSearchView extends Panel implements View {
    public static final String VIEW_NAME = "BasicSearch";
    
    public static String resultEntryTemplate;
    private TextField searchF;
    
    Panel searchResultPanel;
    Panel paringComponentPanel;
    
    static {
        try {
            resultEntryTemplate = IOUtils.toString(MainView.class.getClassLoader().getResourceAsStream("design/resultEntry.template"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public BasicSearchView() {
		super();
		init();
	}
    
    private void init() {
    	addStyleName("main-background");
    	setSizeFull();
    	
    	VerticalLayout layout = new VerticalLayout();
    	setContent(layout);

        VerticalLayout boxLayout = createDialogBox();       
        layout.addComponent(boxLayout);
        layout.setComponentAlignment(boxLayout, Alignment.MIDDLE_CENTER);
        layout.setExpandRatio(boxLayout, 1f);
        
        HorizontalLayout menuHL = createMenu();
        layout.addComponent(menuHL);
        layout.setComponentAlignment(menuHL, Alignment.BOTTOM_CENTER);
        layout.setExpandRatio(menuHL, 0f);
    }

    private HorizontalLayout createMenu() {
        HorizontalLayout menuHL = new HorizontalLayout();
        menuHL.setWidth(100, Unit.PERCENTAGE);
        menuHL.addStyleName("menu");
        menuHL.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        menuHL.setSpacing(true);
       
        Label leftL = new Label();
        menuHL.addComponent(leftL);
        menuHL.setExpandRatio(leftL, 1f);
        
        Image homeI = new Image();
        homeI.setSource(new ThemeResource("img/home.png"));
        homeI.addStyleName("home");
        menuHL.addComponent(homeI);
        menuHL.setExpandRatio(homeI, 0f);
        
//        Label nameL = new Label("BioVariance MED-SEARCH");
//        nameL.addStyleName("name");
//        nameL.setSizeUndefined();
//        menuHL.addComponent(nameL);
//        menuHL.setExpandRatio(nameL, 0f);
//        
//        Button aboutB = new Button("About");
//        aboutB.addStyleName("link");
//        aboutB.setSizeUndefined();
//        aboutB.addClickListener(new ClickListener() {
//            @Override
//            public void buttonClick(ClickEvent event) {
//                doAbout();
//            }
//        });
//        menuHL.addComponent(aboutB);
//        menuHL.setExpandRatio(aboutB, 0f);
//        
//        Label sepL = new Label("|");
//        sepL.addStyleName("separator");
//        sepL.setSizeUndefined();
//        menuHL.addComponent(sepL);
//        menuHL.setExpandRatio(sepL, 0f);
//        
//        Button faqB = new Button("Faq");
//        faqB.addClickListener(new ClickListener() {
//            @Override
//            public void buttonClick(ClickEvent event) {
//                doFaq();
//            }
//        });
//        faqB.addStyleName("link");
//        menuHL.addComponent(faqB);
//        menuHL.setExpandRatio(faqB, 0f);
//        
//        sepL = new Label("|");
//        sepL.addStyleName("separator");
//        sepL.setSizeUndefined();
//        menuHL.addComponent(sepL);
//        menuHL.setExpandRatio(sepL, 0f);
//        
//        Button termsB = new Button("Terms of use");
//        termsB.addClickListener(new ClickListener() {
//            @Override
//            public void buttonClick(ClickEvent event) {
//                doTerms();
//            }
//        });
//        termsB.addStyleName("link");
//        menuHL.addComponent(termsB);
//        menuHL.setExpandRatio(termsB, 0f);
//        
//        sepL = new Label("|");
//        sepL.addStyleName("separator");
//        sepL.setSizeUndefined();
//        menuHL.addComponent(sepL);
//        menuHL.setExpandRatio(sepL, 0f);
//        
//        Button contactsB = new Button("Contacts");
//        contactsB.addClickListener(new ClickListener() {
//            @Override
//            public void buttonClick(ClickEvent event) {
//                doContacts();
//            }
//        });
//        contactsB.addStyleName("link");
//        menuHL.addComponent(contactsB);
//        menuHL.setExpandRatio(contactsB, 0f);
        
        Button signin = new Button("Login");
        signin.addStyleName("link");
        signin.addStyleName("signin");
        signin.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                doSignIn();
            }
        });
        menuHL.addComponent(signin);
        menuHL.setExpandRatio(signin, 0f);
        
//        Image languagesI = new Image();
//        languagesI.setSource(new ThemeResource("img/language.png"));
//        languagesI.addStyleName("languages");
//        menuHL.addComponent(languagesI);
//        menuHL.setExpandRatio(languagesI, 0f);
        
        Label right = new Label();
        menuHL.addComponent(right);
        menuHL.setExpandRatio(right, 1f);
        
        return menuHL;
    }

    private VerticalLayout createDialogBox() {
        VerticalLayout boxLayout = new VerticalLayout();
        boxLayout.setWidth(70, Unit.PERCENTAGE);
//        boxLayout.setHeight(80, Unit.PERCENTAGE);
        boxLayout.addStyleName("search-box");
        boxLayout.setDefaultComponentAlignment(Alignment.TOP_CENTER);
        
//        Image imageI = new Image();
//        imageI.setSource(new ThemeResource("img/logo.png"));
//        imageI.addStyleName("image");
//		boxLayout.addComponent(imageI);
//		boxLayout.setExpandRatio(imageI, 0f);
//        
//        Label textL = new Label("fast searching in medical records and sources");
//        textL.addStyleName("text-align-center");
//        textL.addStyleName("text");
//        boxLayout.addComponent(textL);
//		boxLayout.setExpandRatio(imageI, 0f);
		
		HorizontalLayout searchHL = createSearchHL();
        boxLayout.addComponent(searchHL);
        boxLayout.setExpandRatio(searchHL, 0f);
        
        Label heightL = new Label();
        heightL.setHeight(40, Unit.PIXELS);
        boxLayout.addComponent(heightL);
        boxLayout.setExpandRatio(heightL, 0f);
        
        searchResultPanel = new Panel();
        boxLayout.addComponent(searchResultPanel);
        boxLayout.setExpandRatio(searchResultPanel, 1f);
        
        paringComponentPanel = new Panel();
        boxLayout.addComponent(paringComponentPanel);
        boxLayout.setExpandRatio(paringComponentPanel, 0f);
        
        heightL = new Label();
        heightL.setHeight(50, Unit.PIXELS);
        boxLayout.addComponent(heightL);
        boxLayout.setExpandRatio(heightL, 0f);
        
        Label copyrightL = new Label("&#0169; 2016" /*BioVariance GmbH"*/);
        copyrightL.addStyleName("text-align-center");
        copyrightL.addStyleName("copyright");
        copyrightL.setContentMode(ContentMode.HTML);
        boxLayout.addComponent(copyrightL);
		boxLayout.setExpandRatio(copyrightL, 0f);
        return boxLayout;
    }

    private HorizontalLayout createSearchHL() {
        HorizontalLayout searchHL = new HorizontalLayout();
		searchHL.addStyleName("search-row");
		searchHL.setWidth(100, Unit.PERCENTAGE);
        
        searchF = new TextField();
        searchF.addStyleName("search-field");
        searchF.setWidth(100, Unit.PERCENTAGE);
        searchF.setInputPrompt("Type Keyword");
        searchHL.addComponent(searchF);
        searchHL.setExpandRatio(searchF, 1f);
        
        Button searchB = new Button(FontAwesome.SEARCH); 
        searchB.addStyleName("search-button");
        searchB.setClickShortcut(KeyCode.ENTER);
        searchB.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                doSearch();
            }
        });
        searchHL.addComponent(searchB); 
        searchHL.setExpandRatio(searchB, 0f);
        return searchHL;
    }

    @Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	
    private void doSignIn() {
        Navigator navigator = UI.getCurrent().getNavigator();
        navigator.navigateTo(LoginView.VIEW_NAME);
    }
    
    private void doSearch() {
        List<SelectedTermEntry> queryTermEntries = new ArrayList<SelectedTermEntry>();
        SelectedTermEntry queryTermEntry = new SelectedTermEntry();
        queryTermEntry.setSearchPhrase(searchF.getValue());
        queryTermEntry.setIsFirst(true);
        queryTermEntries.add(queryTermEntry);
        
        List<String> searchResultDisplayEntryList = new ArrayList<>();
        BeanItemContainer<Source> selectedSourcesContainer = new BeanItemContainer<Source>(Source.class);       
        PositiveIntTextField slopField = new PositiveIntTextField();
        
        SearchComponent.doSearch(
                queryTermEntries, 
                searchResultDisplayEntryList, 
                selectedSourcesContainer,
                slopField, 
                searchResultPanel, 
                paringComponentPanel);
        
        searchResultPanel.setScrollTop(0);
        this.setScrollTop(0);
    }
    
    private void doAbout() {
        Navigator navigator = UI.getCurrent().getNavigator();
        navigator.navigateTo(PageAboutView.VIEW_NAME);
    }

    private void doContacts() {
        Navigator navigator = UI.getCurrent().getNavigator();
        navigator.navigateTo(PageContactsView.VIEW_NAME);
    }

    private void doTerms() {
        Navigator navigator = UI.getCurrent().getNavigator();
        navigator.navigateTo(PageTermsView.VIEW_NAME);
    }

    private void doFaq() {
        Navigator navigator = UI.getCurrent().getNavigator();
        navigator.navigateTo(PageFaqView.VIEW_NAME);
    }

}
