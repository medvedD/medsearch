package cz.atria.dataAggregator.ui;

import java.util.List;

import javax.persistence.Column;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.renderers.HtmlRenderer;

import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.entity.TermElementSynonym;

/**
 * @author rnuriev
 * @since 13.08.2015.
 */
@DesignRoot
public class TermElementWindow extends Window {
    final Logger log = LoggerFactory.getLogger(getClass());
    TextField termElementCodeField;
    TextField termElementParentField;
    TextField termElementNameField;
    TextArea termElementDescriptionText;
    Button addSynonymToTermElementButton;
    Button saveTermElementButton;
    Button cancelTermElementButton;
    TermElement parentTermElement;
    BeanItemContainer<TermElementSynonym> synonymContainer;
    Grid synonymTable;
    Button selectParentButton;


    public interface SaveCallback {
        void save(String code, String name, Integer parentId, String description, List<TermElementSynonym> synonyms);
    }

    TermElement termElement;

    final String edit = "edit";
    final String delete = "delete";
    public TermElementWindow(TermElement termElement, final SaveCallback saveCallback) {
        this(saveCallback);
        this.termElement = termElement;
        termElementCodeField.setValue(termElement.getCode());
        termElementNameField.setValue(termElement.getName());
        termElementDescriptionText.setValue(termElement.getDescription());
        parentTermElement = termElement.getParent();
        if (termElement.getParent() != null) {
            termElementParentField.setValue(termElement.getParent().getName());
        }
        synonymContainer.addAll(termElement.getTermElementSynonyms());
    }

    public TermElementWindow(final SaveCallback saveCallback) {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/termElementWindow.html"), this);
        center();
        setModal(true);
        try {
            termElementDescriptionText.setMaxLength(TermElement.class.getDeclaredField("description").getAnnotation(Column.class).length());
        } catch (NoSuchFieldException e) {
            log.warn("", e);
        }
        synonymContainer = new BeanItemContainer<TermElementSynonym>(TermElementSynonym.class);
        synonymTable.setContainerDataSource(synonymContainer);
        saveTermElementButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (saveCallback != null) {
                    saveCallback.save(termElementCodeField.getValue(), termElementNameField.getValue(), parentTermElement != null ? parentTermElement.getId() : null, termElementDescriptionText.getValue(), synonymContainer.getItemIds());
                }
                close();
            }
        });
        cancelTermElementButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                close();
            }
        });

        selectParentButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                TermElementSelectWindow termElementSelectWindow = new TermElementSelectWindow(new TermElementSelectWindow.ItemSelectedCallback() {
                    @Override
                    public void selected(TermElement termElement) {
                        parentTermElement = termElement;
                        termElementParentField.setValue(termElement.getName());
                    }
                }, new TermElementSelectWindow.ItemSelectedCallback2() {
                    @Override
                    public void selected(SelectedTermEntry selectedTermEntry) {
                        parentTermElement = selectedTermEntry.getTermElement();
                        termElementParentField.setValue(selectedTermEntry.getTermElement().getName());
                    }
                });
                termElementSelectWindow.childrenCheckBox.setVisible(false);
                termElementSelectWindow.synonymCheckBox.setVisible(false);
                termElementSelectWindow.resultTermListTextArea.setVisible(false);
                termElementSelectWindow.termElementView.setVisibleColumns(TermElementView.nameFieldName, TermElementView.synonymNamesFieldName, TermElementView.descriptionFieldName);
                getUI().addWindow(termElementSelectWindow);
            }
        });
        synonymTable.setEditorEnabled(true);
        final String name = "name";
        GeneratedPropertyContainer generatedPropertyContainer = new GeneratedPropertyContainer(synonymTable.getContainerDataSource());
        generatedPropertyContainer.addGeneratedProperty(edit, new PropertyValueGenerator<String>() {
            @Override
            public String getValue(Item item, Object itemId, Object propertyId) {
                return FontAwesome.EDIT.getHtml();
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }
        });
        GeneratedPropertyContainer generatedPropertyContainer1 = new GeneratedPropertyContainer(generatedPropertyContainer);
        generatedPropertyContainer1.addGeneratedProperty(delete, new PropertyValueGenerator<String>() {
            @Override
            public String getValue(Item item, Object itemId, Object propertyId) {
                return FontAwesome.TRASH_O.getHtml();
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }
        });
        synonymTable.setContainerDataSource(generatedPropertyContainer1);
        synonymTable.setColumns(name, edit, delete);
        synonymTable.getColumn(edit).setHeaderCaption(" ");
        synonymTable.getColumn(delete).setHeaderCaption(" ");

        synonymTable.getColumn(name).setExpandRatio(2);
        synonymTable.getColumn(edit).setWidth(MainView.defaultControlWidth);
        synonymTable.getColumn(delete).setWidth(MainView.defaultControlWidth);
//        synonymTable.getColumn(edit).setExpandRatio(0);
//        synonymTable.getColumn(delete).setExpandRatio(0);

        synonymTable.getColumn(edit).setRenderer(new HtmlRenderer());
        synonymTable.getColumn(edit).setEditable(false);

        synonymTable.getColumn(delete).setRenderer(new HtmlRenderer());
        synonymTable.getColumn(delete).setEditable(false);
        synonymTable.setFrozenColumnCount(3);
        addSynonymToTermElementButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                TermElementSynonym termElementSynonym = new TermElementSynonym();
                termElementSynonym.setName("");
                synonymContainer.addBean(termElementSynonym);
                for (Field field : synonymTable.getEditorFieldGroup().getFields()) {
                    if (field instanceof TextField) {
                        ((TextField) field).setNullRepresentation("");
                    }
                }
            }
        });

        synonymTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.getPropertyId().equals(edit)) {
                    synonymTable.editItem(event.getItemId());
                } else if (event.getPropertyId().equals(delete)) {
                    synonymContainer.removeItem(event.getItemId());
                }
            }
        });
    }

    HorizontalLayout saveCancelPanel;
    AbstractOrderedLayout mainPanel;

    public void viewOnly() {
        saveTermElementButton.setEnabled(false);
        saveCancelPanel.setVisible(false);
        selectParentButton.setVisible(false);
        addSynonymToTermElementButton.setVisible(false);
        VaadinUtil.setReadOnly(mainPanel, true);
        synonymTable.setEditorEnabled(false);
        synonymTable.getColumn(edit).setHidden(true);
        synonymTable.getColumn(delete).setHidden(true);
    }
}
