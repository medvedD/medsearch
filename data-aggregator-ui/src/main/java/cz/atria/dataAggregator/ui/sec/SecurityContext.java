package cz.atria.dataAggregator.ui.sec;

import org.springframework.security.core.Authentication;

import com.vaadin.server.VaadinSession;

/**
 * @author rnuriev
 * @since 20.08.2015.
 */
public class SecurityContext {

    public static void set(String name, Object o) {
        VaadinSession.getCurrent().setAttribute(name, o);
    }

    public static Object get(String name) {
        return VaadinSession.getCurrent().getAttribute(name);
    }

    public static <T> void set(Class<T> clazz, T o) {
        VaadinSession.getCurrent().setAttribute(clazz, o);
    }

    public static <T> T get(Class<T> clazz) {
        return VaadinSession.getCurrent().getAttribute(clazz);
    }

    public static void setAuthentication(Authentication authentication) {
        VaadinSession.getCurrent().setAttribute(Authentication.class, authentication);
    }

    public static Authentication getAuthentication() {
        Object attribute = VaadinSession.getCurrent().getAttribute(Authentication.class);
        return attribute == null ? null : (Authentication) attribute;
    }

    public static void clear() {
        VaadinSession.getCurrent().setAttribute(Authentication.class, null);
    }
}
