package cz.atria.dataAggregator.ui;

import com.vaadin.ui.UI;

/**
 * @author rnuriev
 * @since 01.09.2015.
 */
public class ConfirmDialog {
    String caption = "Please Confirm:";
    String message = "Delete selected record?";

    public interface ConfirmCallback {
        void onConfirm();
    }

    final ConfirmCallback confirmCallback;

    public ConfirmDialog(String message, ConfirmCallback confirmCallback) {
        this.message = message;
        this.confirmCallback = confirmCallback;
    }

    public ConfirmDialog(ConfirmCallback confirmCallback) {
        this.confirmCallback = confirmCallback;
    }

    public void show() {
        org.vaadin.dialogs.ConfirmDialog.show(UI.getCurrent(), caption, message,
                "Yes", "No", new org.vaadin.dialogs.ConfirmDialog.Listener() {
                    public void onClose(org.vaadin.dialogs.ConfirmDialog dialog) {
                        if (dialog.isConfirmed()) {
                            confirmCallback.onConfirm();
                        }
                    }
                });
    }
}
