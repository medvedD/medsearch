package cz.atria.dataAggregator.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;

import cz.atria.dataAggregator.core.entity.Query;

/**
 * @author rnuriev
 * @since 01.09.2015.
 */
@DesignRoot
public class EditQueryWindow extends Window {
    public interface SaveCallback {
        void save(String name);
    }

    TextField queryNameField;
    Button saveQueryButton;

    public EditQueryWindow(final SaveCallback saveCallback, Query query) {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/" + getClass().getSimpleName() + ".html"), this);
        center();
        setModal(true);
        if (query != null) {
            queryNameField.setValue(query.getName());
        }
        saveQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                saveCallback.save(queryNameField.getValue());
                close();
            }
        });
    }
}
