package cz.atria.dataAggregator.ui.component;

import org.apache.commons.lang.StringUtils;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

/**explicitly introduce checkBox component with left-side caption.
 * todo: try to use more convenient solution, like css direction.
 * @author rnuriev
 * @since 10.09.2015.
 */
public class CheckBoxLeftCaption extends HorizontalLayout {
    CheckBox checkBox = new CheckBox();
    Label label = new Label();

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public CheckBoxLeftCaption() {
        super();
        setSpacing(true);
        addComponent(label);
        addComponent(checkBox);
        setComponentAlignment(label, Alignment.MIDDLE_LEFT);
        setComponentAlignment(checkBox, Alignment.MIDDLE_RIGHT);
    }

    public CheckBoxLeftCaption(String caption) {
        this();
        label.setCaption(caption);
    }

    @Override
    public void setCaption(String caption) {
        label.setCaption(caption);
        if (StringUtils.isNotEmpty(caption) && caption.contains("<")) {
            label.setCaptionAsHtml(true);
        }
    }

    @Override
    public String getCaption() {
        return label.getCaption();
    }

    @Override
    public void setCaptionAsHtml(boolean captionAsHtml) {
        label.setCaptionAsHtml(captionAsHtml);
    }

    @Override
    public boolean isCaptionAsHtml() {
        return label.isCaptionAsHtml();
    }
}
