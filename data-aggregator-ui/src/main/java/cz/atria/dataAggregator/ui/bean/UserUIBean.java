package cz.atria.dataAggregator.ui.bean;

import cz.atria.dataAggregator.core.entity.User;

/** this is just wrapper for actual entity, for simplify UI processing
 * @author rnuriev
 * @since 07.08.2015.
 * @deprecated
 */
@Deprecated
public class UserUIBean extends cz.atria.dataAggregator.core.entity.User {
    cz.atria.dataAggregator.core.entity.User user;

    public UserUIBean() {
    }

    public UserUIBean(cz.atria.dataAggregator.core.entity.User user) {
        this.user = user;
    }

    @Override
    public Integer getId() {
        return user.getId();
    }

    @Override
    public void setId(Integer id) {
        user.setId(id);
    }

    @Override
    public String getLogin() {
        return user.getLogin();
    }

    @Override
    public void setLogin(String login) {
        user.setLogin(login);
    }

    @Override
    public String getEmail() {
        return user.getEmail();
    }

    @Override
    public void setEmail(String email) {
        user.setEmail(email);
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public void setPassword(String password) {
        user.setPassword(password);
    }

    public User getUser() {
        return user;
    }
}
