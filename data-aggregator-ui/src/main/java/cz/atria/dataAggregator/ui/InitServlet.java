package cz.atria.dataAggregator.ui;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import cz.atria.dataAggregator.core.service.EntityService;

/**
 * @author rnuriev
 * @since 17.08.2015.
 */
public class InitServlet extends HttpServlet {

    protected void init0() {
        // just for warm-up
        EntityService bean = SpringHelper.getBean(EntityService.class);
        System.out.println(bean);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        init0();
    }

    @Override
    public void init() throws ServletException {
        super.init();
        init0();
    }
}
