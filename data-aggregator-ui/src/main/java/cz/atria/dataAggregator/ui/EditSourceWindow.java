package cz.atria.dataAggregator.ui;

import org.hibernate.exception.ConstraintViolationException;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.SourceType;
import cz.atria.dataAggregator.core.exc.DataAggregatorException;
import cz.atria.dataAggregator.core.exc.Reason;
import cz.atria.dataAggregator.ui.component.UriFieldWithFileOperations;

/**
 * @author rnuriev
 * @since 08.09.2015.
 */
@DesignRoot
public class EditSourceWindow extends CommonDesignWindow {
	
    BeanFieldGroup<Source> fieldGroup = new BeanFieldGroup<>(Source.class);
    
    TextField sourceNameField;
    Button saveButton;
    Button cancelButton;
    UriFieldWithFileOperations uriFieldWithFileOperations;
    
    Label notificationLabel;
    
    public EditSourceWindow(Source source) {
        super();
        sourceNameField.setNullRepresentation("");
        fieldGroup.bind(sourceNameField, "name");
        final TextField linkField = uriFieldWithFileOperations.getLinkField();
        fieldGroup.bind(linkField, "uri");
        fieldGroup.setItemDataSource(source);
        uriFieldWithFileOperations.setNameField(sourceNameField);
        if (source.getId() == null) {
            setCaption("New");
        } else {
            setCaption("Edit");
            if (source.getType() != null && source.getType().equals(SourceType.GOOGLE_ALERT)) {
                notificationLabel.setVisible(true);
                notificationLabel.setValue("Google alert/ Not available to edit");
                sourceNameField.setEnabled(false);
                uriFieldWithFileOperations.setEnabled(false);
                saveButton.setEnabled(false);
            }
        }
        sourceNameField.setValidationVisible(false);
        linkField.addValidator(new NullValidator("Empty value", false));
        linkField.setValidationVisible(false);
        saveButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    sourceNameField.validate();
                } catch (Validator.InvalidValueException e) {
                    sourceNameField.setValidationVisible(true);
                    return;
                }
                try {
                    linkField.validate();
                } catch (Validator.InvalidValueException e) {
                    linkField.setValidationVisible(true);
                    return;
                }

                linkField.setValue(cz.atria.dataAggregator.core.Common.guessUriSchemeIfAbsent(linkField.getValue()));

                try {
                    fieldGroup.commit();
                    close();
                } catch (FieldGroup.CommitException e) {
                    if (e.getCause() != null && e.getCause().getCause() != null && e.getCause().getCause() instanceof ConstraintViolationException) {
                        ConstraintViolationException constraintViolationException = (ConstraintViolationException) e.getCause().getCause();
                        if (constraintViolationException.getSQLState().equals("23505") && constraintViolationException.getSQLException() != null && constraintViolationException.getSQLException().getMessage().toLowerCase().contains("name")) {
                            throw new DataAggregatorException(Reason.UNIQUE_CONSTRAINT_VIOLATION, "name");
                        } else {
                            throw new DataAggregatorException(Reason.CONSTRAINT_VIOLATION);
                        }
                    } else {
                        throw new RuntimeException(e);
                    }
                }
            }
        });

        cancelButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                fieldGroup.discard();
                close();
            }
        });
    }

    public BeanFieldGroup<Source> getFieldGroup() {
        return fieldGroup;
    }
}
