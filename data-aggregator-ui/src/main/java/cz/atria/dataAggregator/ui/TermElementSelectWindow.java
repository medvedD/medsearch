package cz.atria.dataAggregator.ui;

import org.apache.commons.lang.StringUtils;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.declarative.DesignContext;

import cz.atria.dataAggregator.core.entity.Operator;
import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.ui.component.CheckBoxLeftCaption;
import cz.atria.dataAggregator.ui.component.CheckBoxTermElement;


/**
 * @author rnuriev
 * @since 13.08.2015.
 */
@DesignRoot
public class TermElementSelectWindow extends Window {
    CheckBoxLeftCaption synonymCheckBox;
    CheckBoxLeftCaption childrenCheckBox;
    TextArea resultTermListTextArea;
    Button selectButton;
    BeanFieldGroup<SelectedTermEntry> fieldGroup = new BeanFieldGroup<>(SelectedTermEntry.class);

    public interface ItemSelectedCallback {
        void selected(TermElement termElement);
    }

    public TermElementSelectWindow(SelectedTermEntry selectedTermEntry) {
        this(null, null, selectedTermEntry);
    }

    public interface ItemSelectedCallback2 {
        void selected(SelectedTermEntry selectedTermEntry);
    }

    public TermElementSelectWindow(final ItemSelectedCallback2 itemSelectedCallback) {
        this(null, itemSelectedCallback);
    }

    public TermElementSelectWindow(final ItemSelectedCallback itemSelectedCallback) {
        this(itemSelectedCallback, null);
    }

    public TermElementSelectWindow(final ItemSelectedCallback itemSelectedCallback, final ItemSelectedCallback2 itemSelectedCallback2) {
        this(itemSelectedCallback, itemSelectedCallback2, null);
    }

    TermElementView termElementView = null;
    public TermElementSelectWindow(final ItemSelectedCallback itemSelectedCallback, final ItemSelectedCallback2 itemSelectedCallback2, final SelectedTermEntry selectedTermEntry) {
        DesignContext designContext = Design.read(getClass().getClassLoader().getResourceAsStream("design/termElementSelect.html"), this);
        center();
        setModal(true);
        VerticalLayout layout = (VerticalLayout) ((TermElementSelectWindow) designContext.getRootComponent()).getContent();
        termElementView.addTermElement.setVisible(false);

        if (selectedTermEntry != null) {
            fieldGroup.setItemDataSource(selectedTermEntry);
            fieldGroup.bind(resultTermListTextArea, "searchPhrase");
            fieldGroup.bind(synonymCheckBox.getCheckBox(), "isSynonym");
            fieldGroup.bind(childrenCheckBox.getCheckBox(), "isChildren");
            termElementView.selectedTermElement = selectedTermEntry.getTermElement();
        }


        final TermElementView finalTermElementView = termElementView;
        termElementView.setCheckBoxValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                CheckBoxTermElement source = (CheckBoxTermElement) ((Field.ValueChangeEvent) event).getSource();
                if (event.getProperty().getValue().equals(Boolean.TRUE)) {
                    if (finalTermElementView.getSelectedCheckBox() != null) {
                        ((CheckBox) finalTermElementView.getSelectedCheckBox()).setValue(false);
                        synonymCheckBox.getCheckBox().setValue(false);
                        childrenCheckBox.getCheckBox().setValue(false);
                    }
                    finalTermElementView.setSelectedCheckBox(source);
                    if (selectedTermEntry != null) {
                        selectedTermEntry.setTermElement(source.getTermElement());
                    }
                }
                constructResultTermList(source);
            }
        });
//        termElementView.searchTermElementPanel.setVisible(false);

//        termElementView.spaceLayout.setVisible(true);
//        termElementView.setExpandRatio(termElementView.spaceLayout, 0.05f);
//        termElementView.setExpandRatio(termElementView.toolBoxPanel, 0.12f);
//        termElementView.setExpandRatio(termElementView.termElementTable, 0.88f);
        termElementView.setVisibleColumns(TermElementView.selectFieldName, TermElementView.nameFieldName, TermElementView.synonymNamesFieldName, TermElementView.viewFieldName);
        termElementView.termElementTable.setColumnHeader(TermElementView.selectFieldName, "");
        termElementView.termElementTable.setColumnHeader(TermElementView.viewFieldName, "");
        if (itemSelectedCallback != null) {
            termElementView.termElementTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
                @Override
                public void itemClick(ItemClickEvent event) {
                    if (event.isDoubleClick()) {
                        itemSelectedCallback.selected((TermElement) event.getItemId());
                        close();
                    }
                }
            });
        }
        termElementView.termElementTable.setHierarchyColumn(TermElementView.nameFieldName);
        if (StringUtils.isNotEmpty(resultTermListTextArea.getCaption()) && resultTermListTextArea.getCaption().contains("<")) {
            resultTermListTextArea.setCaptionAsHtml(true);
        }
        termElementView.termElementTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                Object propertyId = event.getPropertyId();
                if (propertyId.equals(TermElementView.viewFieldName)) {
                    TermElementWindow termElementWindow = new TermElementWindow((TermElement) event.getItemId(), null);
                    termElementWindow.viewOnly();
                    UI.getCurrent().addWindow(termElementWindow);
                }
            }
        });
        resultTermListTextArea.setEnabled(false);
        final TermElementView finalTermElementView2 = termElementView;
        synonymCheckBox.getCheckBox().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                constructResultTermList((CheckBoxTermElement) finalTermElementView2.getSelectedCheckBox());
            }
        });

        final TermElementView finalTermElementView1 = termElementView;
        childrenCheckBox.getCheckBox().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                constructResultTermList((CheckBoxTermElement) finalTermElementView1.getSelectedCheckBox());
            }
        });
        final TermElementView finalTermElementView3 = termElementView;
        selectButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (finalTermElementView3.getSelectedCheckBox() != null) {
                    if (itemSelectedCallback2 != null) {
                        CheckBoxTermElement checkBoxTermElement = (CheckBoxTermElement) finalTermElementView3.getSelectedCheckBox();
                        itemSelectedCallback2.selected(new SelectedTermEntry(Operator.AND, resultTermListTextArea.getValue(), checkBoxTermElement.getTermElement(), synonymCheckBox.getCheckBox().getValue(), childrenCheckBox.getCheckBox().getValue()));
                    }
                }
                if (selectedTermEntry != null) {
                    try {
                        fieldGroup.commit();
                    } catch (FieldGroup.CommitException e) {
                        throw new RuntimeException(e);
                    }
                }
                close();
            }
        });

        addCloseListener(new CloseListener() {
            @Override
            public void windowClose(CloseEvent e) {
                // todo: discard changes
            }
        });
    }

    protected void constructResultTermList(CheckBoxTermElement source) {
        if (source == null) {
            return;
        }
        String res = "";
        if (source.getValue()) {
            res = Common.constructFullTermList(source.getTermElement(), synonymCheckBox.getCheckBox().getValue(), childrenCheckBox.getCheckBox().getValue());
        }
        resultTermListTextArea.setValue(res);
    }

    public BeanFieldGroup<SelectedTermEntry> getFieldGroup() {
        return fieldGroup;
    }
}
