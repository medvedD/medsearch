package cz.atria.dataAggregator.ui;

/**
 * @author rnuriev
 * @since 12.10.2015.
 */
public interface Refreshable {
    void refresh();
}
