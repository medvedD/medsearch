package cz.atria.dataAggregator.ui;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.InfoRetrieveService;

/**
 * @author rnuriev
 * @since 01.08.2015.
 */
public class DocumentView extends VerticalLayout {
    Logger log = LoggerFactory.getLogger(getClass());
    String documentId;
    Label label = new Label();
    CustomLayout documentContent;
    public DocumentView(String documentId) {
        setSizeFull();
        this.documentId = documentId;
        InfoRetrieveService infoRetrieveService = SpringHelper.getBean(InfoRetrieveService.class);
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        Document document = entityService.get(Document.class, Integer.valueOf(documentId));
        String highlightedContent = null;
        if (document.getCacheUri() != null) {
            highlightedContent = infoRetrieveService.highlight(document.getCacheUri(), MainView.getQuery());
        }
        if(StringUtils.isEmpty(highlightedContent)) {
            highlightedContent = infoRetrieveService.highlight(document.getUri(), MainView.getQuery());
        }
        if(StringUtils.isEmpty(highlightedContent)) {
            log.warn("highlighted content is empty");
            try {
                highlightedContent = IOUtils.toString(cz.atria.dataAggregator.core.Common.get(document.getUri()));
            } catch (IOException e) {
                log.warn("", e);
            }
        }
        try {
            documentContent = new CustomLayout(IOUtils.toInputStream(highlightedContent));
            documentContent.setSizeFull();
        } catch (IOException e) {
            log.error("", e);
        }
        addComponent(documentContent);
    }

}
