package cz.atria.dataAggregator.ui;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.renderers.HtmlRenderer;

import cz.atria.dataAggregator.core.Format;
import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.convert.ConvertService;
import cz.atria.dataAggregator.core.entity.Query;
import cz.atria.dataAggregator.core.entity.QueryTermEntry;
import cz.atria.dataAggregator.core.entity.Source;
import cz.atria.dataAggregator.core.entity.User;
import cz.atria.dataAggregator.core.exc.DataAggregatorException;
import cz.atria.dataAggregator.core.exc.Reason;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.ui.component.EditSearchTextWindow;
import cz.atria.dataAggregator.ui.component.PositiveIntTextField;
import cz.atria.dataAggregator.ui.component.SearchComponent;
import cz.atria.dataAggregator.ui.sec.SecurityContext;
import cz.atria.dataAggregator.ui.sec.SecurityService;
import cz.atria.dataAggregator.ui.sec.SecurityServiceImpl;

/**
 * search panel tab. see <a href = "http://intra.dicomresearch.com/projects/data-aggregator/wiki/%D0%9F%D0%B0%D0%BD%D0%B5%D0%BB%D1%8C_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0">Панель поиска</a>
 *
 * @author rnuriev
 * @since 14.08.2015.
 */
@DesignRoot
public class SearchPanelView extends Panel {
    final Logger log = LoggerFactory.getLogger(getClass());
    public static final String PROP_SEARCH_TIMEOUT = "dataAggregator.search.timeout";
    public static final String AUX = "aux";
    Button selectTermButton;
    Button addTextButton;
    QueryTermGrid queryTermsPrimaryTable;
    Button searchButton;
    BeanItemContainer<Source> selectedSourcesContainer = new BeanItemContainer<Source>(Source.class);
    Grid selectedSourcesTable;
    Panel searchResultPanel;
    Button addSourceButton;
    Tree savedUserQueriesList;
    HorizontalLayout searchSettingsAndButtonPanel;
    HorizontalLayout currentQueryPanel;
    VerticalLayout searchConditionsPanel;
    VerticalLayout sourcesPanel;
    PositiveIntTextField slopField;
    Button saveQueryButton;
    Label currentQueryNameLabel;
    Button showHideQueryButton;
    AbstractLayout searchQueryPanel;
    VerticalLayout upperPanel;
    Panel paringComponentPanel;
    Button saveResultsButton;
    ComboBox saveResultFormatComboBox;
    Button clearSourceButton;
    Button clearTermsButton;

    final SecurityService securityService = new SecurityServiceImpl();

    /**
     * todo: make session scoped
     */
    List<String> searchResultDisplayEntryList = new ArrayList<>();
    public static final String sourceNameColumnName = "name";
    public static final String sourceUriColumnName = "uri";
    public static final String deletePropertyId = "delete";

    public SearchPanelView() {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/searchPanelView.html"), this);
        selectedSourcesTable.setId("selectedSourcesTableSearchTabId");
        GeneratedPropertyContainer containerWithDelete = new GeneratedPropertyContainer(selectedSourcesContainer);
        containerWithDelete.addGeneratedProperty(deletePropertyId, new PropertyValueGenerator<String>() {
            @Override
            public String getValue(Item item, Object itemId, Object propertyId) {
                return FontAwesome.TRASH_O.getHtml();
            }

            @Override
            public Class<String> getType() {
                return String.class;
            }
        });
        selectedSourcesTable.setContainerDataSource(containerWithDelete);
        selectedSourcesTable.getColumn(deletePropertyId).setRenderer(new HtmlRenderer());
        selectTermButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                TermElementSelectWindow termElementSelectWindow = new TermElementSelectWindow(new TermElementSelectWindow.ItemSelectedCallback2() {
                    @Override
                    public void selected(SelectedTermEntry searchTermEntry) {
                        queryTermsPrimaryTable.addTermElement(searchTermEntry);
                    }
                });
                getUI().addWindow(termElementSelectWindow);
            }
        });

        addTextButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final SelectedTermEntry selectedTermEntry = new SelectedTermEntry();
                EditSearchTextWindow editSearchTextWindow = new EditSearchTextWindow("Add", selectedTermEntry);
                editSearchTextWindow.getFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
                    @Override
                    public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {

                    }

                    @Override
                    public void postCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                        queryTermsPrimaryTable.addTermElement(selectedTermEntry);
                    }
                });
            }
        });

        searchButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                SearchComponent.doSearch(
                        queryTermsPrimaryTable.getBeans(),
                        searchResultDisplayEntryList, 
                        selectedSourcesContainer, 
                        slopField, 
                        searchResultPanel, 
                        paringComponentPanel);            }
        });

        addSourceButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                SourcesWindow sourcesWindow = new SourcesWindow(new SourcesWindow.SourceSelectedCallback() {
                    @Override
                    public void process(Source source) {
                        if (!selectedSourcesContainer.containsId(source.getId())) {
                            selectedSourcesContainer.addBean(source);
                        }
                    }
                });
                sourcesWindow.center();
                getUI().addWindow(sourcesWindow);
            }
        });

        queryTermsPrimaryTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.getPropertyId().equals(queryTermsPrimaryTable.deleteFieldName)) {
                    if (queryTermsPrimaryTable.getItemIds().size() == 1) {
                        // todo: this exception doesn't catched by global handler: throw new DataAggregatorException(Reason.AT_LEAST_ONE_PHRASE);
                        Notification.show(Reason.AT_LEAST_ONE_PHRASE.getMessage(), Notification.Type.ERROR_MESSAGE);
                        return;
                    }
                    SelectedTermEntry selectedTermEntryToDelete = queryTermsPrimaryTable.getBean(event.getItemId());
                    Object nextItemId = queryTermsPrimaryTable.nextItemId(event.getItemId());
                    SelectedTermEntry selectedTermEntryNext = nextItemId != null ? queryTermsPrimaryTable.getBean(nextItemId) : null;
                    if (selectedTermEntryNext != null && selectedTermEntryToDelete.isFirst()) {
                        selectedTermEntryNext.setIsFirst(true);
                        selectedTermEntryNext.setOperator(null);
                    }
                    queryTermsPrimaryTable.getContainerDataSource().removeItem(event.getItemId());
                    queryTermsPrimaryTable.refreshRowCache();
                } else if (event.getPropertyId().equals(QueryTermGrid.editFieldName)) {
                    SelectedTermEntry selectedTermEntryToEdit = queryTermsPrimaryTable.getBean(event.getItemId());
                    if (selectedTermEntryToEdit.getTermElement() != null) {
                        TermElementSelectWindow termElementSelectWindow = new TermElementSelectWindow(selectedTermEntryToEdit);
                        termElementSelectWindow.getFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
                            @Override
                            public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {

                            }

                            @Override
                            public void postCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                                queryTermsPrimaryTable.refreshRowCache();
                            }
                        });
                        getUI().addWindow(termElementSelectWindow);
                    } else {
                        EditSearchTextWindow editSearchTextWindow = new EditSearchTextWindow("Edit", selectedTermEntryToEdit);
                        editSearchTextWindow.getFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
                            @Override
                            public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {

                            }

                            @Override
                            public void postCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
                                queryTermsPrimaryTable.refreshRowCache();
                            }
                        });
                    }
                }
            }
        });

        final ErrorHandler gridErrorHandler = queryTermsPrimaryTable.getErrorHandler();
        queryTermsPrimaryTable.setErrorHandler(new ErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                Throwable throwable = null;
                if (event.getThrowable() != null && event.getThrowable().getCause() != null && event.getThrowable().getCause().getCause() != null) {
                    throwable = event.getThrowable().getCause().getCause();
                }
                if (throwable != null && throwable.getClass().equals(IllegalArgumentException.class) && throwable.getMessage().startsWith("Given item id")) {
                    // seems this is due to bug: https://vaadin.com/forum/#!/thread/9451689/9469134
                } else {
                    if (gridErrorHandler != null) {
                        gridErrorHandler.error(event);
                    }
                }
            }
        });
        savedUserQueriesList.setCaptionAsHtml(true);
        savedUserQueriesList.setItemCaptionMode(AbstractSelect.ItemCaptionMode.EXPLICIT);
        refreshQueriesList();
        searchSettingsAndButtonPanel.setMargin(new MarginInfo(true, false, false, false));
//        currentQueryPanel.setMargin(new MarginInfo(false, false, true, false));
        searchConditionsPanel.setMargin(new MarginInfo(true, false, false, false));
        sourcesPanel.setMargin(new MarginInfo(true, false, false, false));
        selectedSourcesTable.setColumns(sourceNameColumnName, sourceUriColumnName, deletePropertyId);
        selectedSourcesTable.getColumn(sourceUriColumnName).setHeaderCaption("Link");

        selectedSourcesTable.getColumn(deletePropertyId).setWidth(MainView.defaultControlWidth);
        selectedSourcesTable.getColumn(deletePropertyId).setHeaderCaption("");

        selectedSourcesTable.setFrozenColumnCount(selectedSourcesTable.getColumns().size());    // just for hide scrolling
        selectedSourcesTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(final ItemClickEvent event) {
                if (event.getPropertyId().equals(deletePropertyId)) {
                    ConfirmDialog confirmDialog = new ConfirmDialog(new ConfirmDialog.ConfirmCallback() {
                        @Override
                        public void onConfirm() {
                            selectedSourcesTable.getContainerDataSource().removeItem(event.getItemId());
                        }
                    });
                    confirmDialog.show();
                }
            }
        });
        final EntityService entityService = SpringHelper.getBean(EntityService.class);
        saveQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                SaveQueryWindow saveQueryWindow = new SaveQueryWindow(new SaveQueryWindow.SaveQueryCallback() {
                    @Override
                    public void save(String name, boolean isUser) {
                        Query currentQuery = SecurityContext.get(Query.class);
                        if (currentQuery != null && (currentQuery.getName().equalsIgnoreCase(name) && ((currentQuery.getUser() != null && isUser) || (currentQuery.getUser() == null && !isUser)))) {
                            saveQueryConditions(currentQuery);
                        } else {
                            Query query = new Query();
                            query.setName(name);
                            if (StringUtils.isNotEmpty(slopField.getValue())) {
                                query.setSlop(Integer.valueOf(slopField.getValue()));
                            }
                            if (isUser) {
                                User user = entityService.getUser(SecurityContext.getAuthentication().getName());
                                query.setUser(user);
                            } else {
                                // manually check constraint, cause it's complicated to implement H2 constraint with nullable columns
                                List<Query> queriesByName = entityService.getQueriesByName(name);
                                if (!queriesByName.isEmpty()) {
                                    throw new DataAggregatorException("Query name", null, Reason.UNIQUE_CONSTRAINT_VIOLATION);
                                }
                            }
                            // todo: use single transaction
                            try {
                                entityService.persist(query);
                            } catch (Exception e) {
                                // todo: unify exception handling
                                if (e.getCause() instanceof ConstraintViolationException) {
                                    ConstraintViolationException constraintViolationException = (ConstraintViolationException) e.getCause();
                                    final String expectedConstrainNameLower = Query.USER_NAME_UK_NAME.toLowerCase();
                                    final String currentConstraintName = constraintViolationException.getConstraintName();
                                    if ((currentConstraintName != null && currentConstraintName.toLowerCase().contains(expectedConstrainNameLower)) ||
                                            constraintViolationException.getSQLException().getMessage().toLowerCase().contains(expectedConstrainNameLower)
                                            ) {
                                        throw new DataAggregatorException("Query name", e, Reason.UNIQUE_CONSTRAINT_VIOLATION);
                                    } else {
                                        throw e;
                                    }
                                } else {
                                    throw e;
                                }
                            }
                            saveQueryConditions(query);
                            currentQuery = query;
                        }
                        refreshQueriesList();
                        selectQuery(currentQuery);
                    }
                }, SecurityContext.get(Query.class));
                UI.getCurrent().addWindow(saveQueryWindow);
            }
        });

        Query query = SecurityContext.get(Query.class);
        selectQuery(query);
        savedUserQueriesList.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.getButton() == MouseEventDetails.MouseButton.LEFT) {
                    if (savedUserQueriesList.getParent(event.getItemId()) != null) {
                        openQuery(event.getItemId());
                    } else {
                        if (savedUserQueriesList.isExpanded(event.getItemId())) {
                            savedUserQueriesList.collapseItem(event.getItemId());
                        } else {
                            savedUserQueriesList.expandItem(event.getItemId());
                        }
                    }
                }
            }
        });

        final Action openAction = new Action("Open");
        final Action renameAction = new Action("Rename");
        final Action deleteAction = new Action("Delete");
        final Action changeAction = new Action("Change");
        savedUserQueriesList.addActionHandler(new Action.Handler() {
            @Override
            public Action[] getActions(Object target, Object sender) {
                Action[] actionRes = null;
                if (target != null) {
                    Query queryTarget = (Query) target;
                    if (queryTarget.getUser() != null) {
                        actionRes = new Action[]{openAction, renameAction, deleteAction, changeAction};
                    } else {
                        List<Action> actions = new ArrayList<Action>();
                        actions.add(openAction);
                        if (securityService.isGranted(MainView.SEARCH_QUERIES_PRIV, MainView.EDIT_PRIV)) {
                            actions.add(renameAction);
                            actions.add(changeAction);
                        }
                        if (securityService.isGranted(MainView.SEARCH_QUERIES_PRIV, MainView.REMOVE_PRIV)) {
                            actions.add(deleteAction);
                        }
                        actionRes = new Action[actions.size()];
                        actions.toArray(actionRes);
                    }
                }
                return actionRes;
            }

            @Override
            public void handleAction(Action action, Object sender, final Object target) {
                Query query1 = null;
                if (target instanceof Query) {
                    query1 = (Query) target;
                }
                if (action == openAction || action == changeAction) {
                    openQuery(target);
                } else if (action == renameAction) {
                    final Query finalQuery = query1;
                    EditQueryWindow editQueryWindow = new EditQueryWindow(new EditQueryWindow.SaveCallback() {
                        @Override
                        public void save(String name) {
                            if (!finalQuery.getName().equals(name)) {
                                finalQuery.setName(name);
                                entityService.save(finalQuery);
                                refreshQueriesList();
                                selectQuery(finalQuery);
                            }
                        }
                    }, finalQuery);
                    UI.getCurrent().addWindow(editQueryWindow);
                } else if (action == deleteAction) {
                    final Query finalQuery1 = query1;
                    ConfirmDialog confirmDialog = new ConfirmDialog(new ConfirmDialog.ConfirmCallback() {
                        @Override
                        public void onConfirm() {
                            entityService.remove(finalQuery1);
                            refreshQueriesList();
                        }
                    });
                    confirmDialog.show();
                }
            }
        });

        showHideQueryButton.setCaptionAsHtml(true);
        setShowHideIcon();
        showHideQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                searchQueryPanel.setVisible(!searchQueryPanel.isVisible());
                setShowHideIcon();
            }
        });
        upperPanel.setStyleName("my-border-bottom-solid");


        saveResultFormatComboBox.addItems(Format.values());
        saveResultFormatComboBox.setNullSelectionAllowed(false);
        saveResultFormatComboBox.setValue(Format.PDF);
        final OnDemandFileDownloader fileDownloader = new OnDemandFileDownloader(new OnDemandFileDownloader.OnDemandStreamResource() {
            @Override
            public String getFilename() {
                Format format = (Format) saveResultFormatComboBox.getValue();
                if (format != null) {
                    String fileNameBase = "";
                    int i = 2;
                    for (Object itemId : queryTermsPrimaryTable.getContainerDataSource().getItemIds()) {
                        String searchPhrase = (String) queryTermsPrimaryTable.getContainerDataSource().getItem(itemId).getItemProperty("searchPhrase").getValue();
                        fileNameBase += searchPhrase + "-";
                        if (--i == 0) {
                            break;
                        }
                    }
                    return fileNameBase + System.currentTimeMillis() + "." + format.getExtension();
                } else {
                    return null;
                }
            }

            @Override
            public InputStream getStream() {
                Format format = (Format) saveResultFormatComboBox.getValue();
                if (searchResultDisplayEntryList != null && !searchResultDisplayEntryList.isEmpty() && format != null) {
                    ConvertService convertService = null;
                    for (ConvertService convertServiceCandidate : SpringHelper.getBeans(ConvertService.class)) {
                        if (convertServiceCandidate.getSupportedMediaType().equals(format.getMediaType())) {
                            convertService = convertServiceCandidate;
                            break;
                        }
                    }
                    if (convertService != null) {
                        String res = "";
                        for (String s : searchResultDisplayEntryList) {
                            res += s;
                        }
                        res = String.format("<html><head>  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=%1$s\"></head><body>%2$s</body></html>", Charset.defaultCharset().name(), res);
                        return convertService.convert(IOUtils.toInputStream(res));
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }
        });

        fileDownloader.extend(saveResultsButton);
        slopField.setNullRepresentation("");

        clearSourceButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedSourcesTable.getContainerDataSource().removeAllItems();
            }
        });

        clearTermsButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                queryTermsPrimaryTable.getContainerDataSource().removeAllItems();
                slopField.setValue("");
            }
        });

        selectedSourcesTable.getColumn(sourceUriColumnName).setConverter(new StringURIWithSpaceConverter());
    }

    String exportFileName;

    protected void setShowHideIcon() {
        if (searchQueryPanel.isVisible()) {
            showHideQueryButton.setCaption(FontAwesome.CARET_UP.getHtml());
        } else {
            showHideQueryButton.setCaption(FontAwesome.CARET_DOWN.getHtml());
        }
    }

    protected void saveQueryConditions(Query query) {
        saveQueryConditions(query, (Collection<SelectedTermEntry>) queryTermsPrimaryTable.getBeans(), selectedSourcesContainer.getItemIds(), getSlopValue());
    }

    protected void saveQueryConditions(Query query, Collection<SelectedTermEntry> searchTermEntries, List<Source> sources, Integer slop) {
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        query.setSlop(slop);
        entityService.save(query);
        List<QueryTermEntry> queryTermEntries = new ArrayList<>();
        int i = 0;
        for (Object itemId : searchTermEntries) {
            SelectedTermEntry selectedTermEntry = (SelectedTermEntry) itemId;
            QueryTermEntry queryTermEntry;
            if (selectedTermEntry.getTermElement() != null) {
                queryTermEntry = new QueryTermEntry(selectedTermEntry.getTermElement(), selectedTermEntry.getIsSynonym(), selectedTermEntry.getIsChildren(), query, i++, selectedTermEntry.getOperator());
            } else {
                queryTermEntry = new QueryTermEntry(selectedTermEntry.getSearchPhraseBase(), query, i++, selectedTermEntry.getOperator());
            }
            queryTermEntries.add(queryTermEntry);
        }
        entityService.saveQueryConditions(query, sources, queryTermEntries);
    }

    /**
     * is current query changed
     */
    protected boolean isChanged(Query query) {
        if (query == null) {
            return false;
        }
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        Query queryStored = entityService.get(Query.class, query.getId());
        Integer slopStored = queryStored != null ? queryStored.getSlop() : null;
        Integer currentSlop = StringUtils.isNotEmpty(slopField.getValue()) ? Integer.valueOf(slopField.getValue()) : null;
        if (!((slopStored != null && currentSlop != null && slopStored.equals(currentSlop)) || (slopStored == null && currentSlop == null))) {
            return true;
        }
        boolean isSourcesChanged = false;
        List<Source> sources = entityService.getSources(query);
        if (sources.size() != selectedSourcesContainer.size()) {
            isSourcesChanged = true;
        } else {
            for (Source source1 : sources) {
                boolean isExists = false;
                for (Source source2 : selectedSourcesContainer.getItemIds()) {
                    if (isExists = source1.getId().equals(source2.getId())) {
                        break;
                    }
                }
                if (!isExists) {
                    isSourcesChanged = true;
                    break;
                }
            }
        }
        if (!isSourcesChanged) {
            boolean isTermsChanged = false;
            List<QueryTermEntry> queryTermEntriesOrdered = entityService.getQueryTermEntriesOrdered(query);
            Collection<SelectedTermEntry> queryTermsSelected = queryTermsPrimaryTable.getBeans();
            if (queryTermEntriesOrdered.size() != queryTermsSelected.size()) {
                isTermsChanged = true;
            } else {
                for (QueryTermEntry queryTermEntry : queryTermEntriesOrdered) {
                    boolean isFound = false;
                    for (SelectedTermEntry selectedTermEntry : queryTermsSelected) {
                        SearchTermEntry searchTermEntry2 = selectedTermEntry.convert(queryTermEntry);
                        if (isFound = searchTermEntry2.equals(selectedTermEntry)) {
                            break;
                        }
                    }
                    if (!isFound) {
                        isTermsChanged = true;
                        break;
                    }
                }
            }
            return isTermsChanged;
        } else {
            return isSourcesChanged;
        }
    }

    protected Integer getSlopValue() {
        return StringUtils.isNotEmpty(slopField.getValue()) ? Integer.valueOf(slopField.getValue()) : null;
    }

    protected void openQuery(Object itemId) {
        Object parent = savedUserQueriesList.getParent(itemId);
        if (parent == null) {
            return; // it is root
        }
        Query query = itemId instanceof Query ? (Query) itemId : null;
        final Query currentQuery = SecurityContext.get(Query.class);
        boolean isChangeAllowed = false;
        if (currentQuery != null) {
            if (currentQuery.getUser() == null && securityService.isGranted(MainView.SEARCH_QUERIES_PRIV, MainView.EDIT_PRIV)) {
                isChangeAllowed = true;
            } else if (currentQuery.getUser() != null) {
                isChangeAllowed = true;
            }
        }
        if (isChangeAllowed && isChanged(currentQuery)) {
            if (currentQuery != null && !query.getId().equals(currentQuery.getId())) {
                final Collection<SelectedTermEntry> selectedTermEntries = new ArrayList<SelectedTermEntry>((Collection<SelectedTermEntry>) queryTermsPrimaryTable.getBeans());
                final List<Source> sources = new ArrayList<>(selectedSourcesContainer.getItemIds());
                final Integer slop = getSlopValue();
                ConfirmDialog confirmDialog = new ConfirmDialog("Save query changes?", new ConfirmDialog.ConfirmCallback() {
                    @Override
                    public void onConfirm() {
                        saveQueryConditions(currentQuery, selectedTermEntries, sources, slop);
                    }
                });
                confirmDialog.show();
            }
        }
        EntityService entityService = SpringHelper.getBean(EntityService.class);
        Query query1 = null;
        if (parent.equals(USER_QUERY)) {
            query1 = entityService.getQueryByName(entityService.getUser(SecurityContext.getAuthentication().getName()), query.getName());
        } else {
            query1 = entityService.getQueryByName(null, query.getName());
        }
        slopField.setValue(query1.getSlop() != null ? query1.getSlop().toString() : null);   // todo: temporary default set
        selectQuery(query1);
    }

    @Deprecated
    protected void saveQuery(Object itemId) {
        // todo
    }

    protected void selectQuery(Query query) {
        if (query != null) {
            EntityService entityService = SpringHelper.getBean(EntityService.class);
            currentQueryNameLabel.setCaptionAsHtml(true);
            currentQueryNameLabel.setCaption(String.format(NAME_TEMPLATE, query.getName()));

            selectedSourcesContainer.removeAllItems();
            selectedSourcesContainer.addAll(entityService.getSources(query));

            queryTermsPrimaryTable.getContainerDataSource().removeAllItems();
            boolean isFirst = true;
            for (QueryTermEntry queryTermEntry : entityService.getQueryTermEntriesOrdered(query)) {
                SelectedTermEntry selectedTermEntry = new SelectedTermEntry(
                        queryTermEntry.getOperator(),
                        StringUtils.isNotEmpty(queryTermEntry.getSearchPhrase()) && queryTermEntry.getTermElement() == null ?
                                queryTermEntry.getSearchPhrase() :
                                Common.constructFullTermList(queryTermEntry.getTermElement(), queryTermEntry.getIsSynonym(), queryTermEntry.getIsChildren()),
                        queryTermEntry.getTermElement(),
                        queryTermEntry.getIsSynonym(),
                        queryTermEntry.getIsChildren());
                selectedTermEntry.setIsFirst(isFirst);
                queryTermsPrimaryTable.addSelectedTermEntry(selectedTermEntry);
                if (isFirst) {
                    isFirst = false;
                }
            }
            if (!savedUserQueriesList.containsId(query)) {
                for (Object itemId : savedUserQueriesList.getItemIds()) {
                    if (itemId instanceof Query && ((Query) itemId).getId() != null) {
                        if (((Query) itemId).getId().equals(query.getId())) {
                            query = (Query) itemId; // cause actual item may be differ.
                            break;
                        }
                    }
                }
            }
            Object parent = savedUserQueriesList.getParent(query);
            if (parent != null) {
                savedUserQueriesList.expandItem(parent);
            }
            savedUserQueriesList.select(query);
            if (query.getUser() == null && !securityService.isGranted(MainView.SEARCH_QUERIES_PRIV, MainView.EDIT_PRIV)) {
                addSourceButton.setEnabled(false);
                selectTermButton.setEnabled(false);
                addTextButton.setEnabled(false);
                queryTermsPrimaryTable.setEnabled(false);
            } else {
                addSourceButton.setEnabled(true);
                selectTermButton.setEnabled(true);
                addTextButton.setEnabled(true);
                queryTermsPrimaryTable.setEnabled(true);
            }
            SecurityContext.set(Query.class, query);
        }
    }

    public static final String NAME_TEMPLATE = "<h3><font color=\"blue\">%1$s</font></h3>";

    public static final Query PUBLIC_QUERY = new Query("Team queries");
    public static final Query USER_QUERY = new Query("My queries");

    protected void refreshQueriesList() {
        savedUserQueriesList.removeAllItems();
        final EntityService entityService = SpringHelper.getBean(EntityService.class);
        if (securityService.isGranted(MainView.SEARCH_QUERIES_PRIV, MainView.VIEW_PRIV)) {
            savedUserQueriesList.addItem(PUBLIC_QUERY);
            final List<Query> publicQueries = entityService.getQueries(null);
            for (Query query : publicQueries) {
                savedUserQueriesList.addItem(query);
                savedUserQueriesList.setParent(query, PUBLIC_QUERY);
                savedUserQueriesList.setChildrenAllowed(query, false);
            }
        }
        savedUserQueriesList.addItem(USER_QUERY);
        if (SecurityContext.getAuthentication() != null) {
            final List<Query> userQueries = entityService.getQueries(SecurityContext.getAuthentication().getName());
            for (Query query : userQueries) {
                savedUserQueriesList.addItem(query);
                savedUserQueriesList.setParent(query, USER_QUERY);
                savedUserQueriesList.setChildrenAllowed(query, false);
            }
        }
        for (Object itemId : savedUserQueriesList.getItemIds()) {
            savedUserQueriesList.setItemCaption(itemId, ((Query) itemId).getName()); // todo: caption by property not work for now
        }
    }

}
