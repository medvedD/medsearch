package cz.atria.dataAggregator.ui;

import javax.servlet.annotation.WebServlet;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import cz.atria.dataAggregator.core.entity.TermElementSynonym;
import cz.atria.dataAggregator.core.exc.DataAggregatorException;
import cz.atria.dataAggregator.ui.pages.PageAboutView;
import cz.atria.dataAggregator.ui.pages.PageContactsView;
import cz.atria.dataAggregator.ui.pages.PageFaqView;
import cz.atria.dataAggregator.ui.pages.PageTermsView;
import cz.atria.dataAggregator.ui.sec.LoginView;
import cz.atria.dataAggregator.ui.sec.SecurityContext;

/**
 * @author rnuriev
 * @since 30.07.2015.
 */
@PreserveOnRefresh
@Theme("mytheme")
// @Push (transport = Transport.LONG_POLLING)
public class MainUI extends UI {
    final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        setContent(layout);
        getSession().setAttribute(SearchPanelView.AUX, request.getParameter(SearchPanelView.AUX));
        
        Navigator navigator = new Navigator(this, layout);
        navigator.addView(LoginView.VIEW_NAME, LoginView.class);
        navigator.addView(MainView.VIEW_NAME, MainView.class);
        navigator.addView(BasicSearchView.VIEW_NAME, BasicSearchView.class);
        
        navigator.addView(PageAboutView.VIEW_NAME, PageAboutView.class);
        navigator.addView(PageFaqView.VIEW_NAME, PageFaqView.class);
        navigator.addView(PageTermsView.VIEW_NAME, PageTermsView.class);
        navigator.addView(PageContactsView.VIEW_NAME, PageContactsView.class);
        
        setNavigator(navigator);
        navigator.navigateTo(MainView.VIEW_NAME);
        
        VaadinSession.getCurrent().setErrorHandler(new ErrorHandler() {
            protected <T extends Throwable> T findCause(Throwable e, Class<T> causeClass) {
                T res = null;
                Throwable e1 = e;
                int i = 0;
                do {
                    if (e1 != null) {
                        if (causeClass.isAssignableFrom(e1.getClass())) {
                            res = (T) e1;
                        } else {
                            e1 = e1.getCause();
                        }
                    }
                } while (res == null && i++ < 5);
                return res;
            }

            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                showError(event);
            }

			private void showError(com.vaadin.server.ErrorEvent event) {
				Throwable e = event.getThrowable();
                log.error("", e);
                String titleMessage;
                DataAggregatorException dataAggregatorException = findCause(e, DataAggregatorException.class);
                if (dataAggregatorException != null) {
                    Notification.show(dataAggregatorException.getReason().getMessage(),
                            (StringUtils.isNotEmpty(dataAggregatorException.getMessage()) ? dataAggregatorException.getMessage() : ""),
                            Notification.Type.ERROR_MESSAGE);
                } else {
                    ConstraintViolationException constraintViolationException = findCause(e, ConstraintViolationException.class);
                    if (constraintViolationException != null && constraintViolationException.getSQLState() != null && constraintViolationException.getSQLState().equals("23503")) {
                        Notification.show("The element can't be removed, because other objects of system refer to it",
                                Notification.Type.ERROR_MESSAGE);
                    } else {
                        IllegalArgumentException illegalArgumentException = findCause(e, IllegalArgumentException.class);
                        if (illegalArgumentException != null
                                && illegalArgumentException.getMessage().contains("Given item id (")
                                && illegalArgumentException.getMessage().contains(TermElementSynonym.class.getCanonicalName())
                                && illegalArgumentException.getMessage().contains(") does not exist in the container")
                                ) {
                            // todo:
                            log.warn("temporary ignore this exception (initial: http://intra.dicomresearch.com/issues/628), " +
                                    "cause there is no appropriate solution. May be this is the same issue: https://dev.vaadin.com/ticket/16195" +
                                    "and will be resolved with further vaadin release.");
                        } else {
                            IllegalStateException illegalStateException = findCause(e, IllegalStateException.class);
                            if (illegalStateException != null
                                    && illegalStateException.getMessage().contains("Item id")
                                    &&  illegalStateException.getMessage().contains("was not pinned")
                                    ) {
                                // todo:
                                log.warn("temporary ignore this exception (initial: http://intra.dicomresearch.com/issues/1632)");
                            } else {
                                showCommonErrorNotification(e);
                            }
                        }
                    }
                }
			}

            protected void showCommonErrorNotification(Throwable e) {
                String titleMessage = e.getLocalizedMessage();
                NotifyWindow notifyWindow = new NotifyWindow(null, new VerticalLayout(), titleMessage, ExceptionUtils.getStackTrace(e));
                UI.getCurrent().addWindow(notifyWindow);
            }
        });
    }

    @WebServlet(urlPatterns = {"/main/*", "/VAADIN/*"}, name = "MainUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MainUI.class, productionMode = false, widgetset = "cz.atria.dataAggregator.DataAggregatorWidgetset")
    public static class MainUIServlet extends VaadinServlet {
    }

    @Override
    protected void refresh(VaadinRequest request) {
    	Navigator navigator = UI.getCurrent().getNavigator();
    	if (SecurityContext.getAuthentication() == null) { 
    		navigator.navigateTo(BasicSearchView.VIEW_NAME);
    	} else {
    		navigator.navigateTo(MainView.VIEW_NAME);
    	}
    }
}
