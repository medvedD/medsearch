package cz.atria.dataAggregator.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.converter.WordToHtmlUtils;
import org.apache.poi.hwpf.usermodel.Picture;
import org.docx4j.Docx4J;
import org.docx4j.Docx4jProperties;
import org.docx4j.convert.out.HTMLSettings;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import cz.atria.dataAggregator.core.entity.Document;
import cz.atria.dataAggregator.core.entity.MediaType;
import cz.atria.dataAggregator.core.service.DocumentService;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.core.service.HtmlEditService;
import cz.atria.dataAggregator.core.service.HttrackService;
import cz.atria.dataAggregator.core.service.NLPService;

/**
 * it is alternate to DocumentView, used just for original document highlighting, without any additional info
 *
 * @author rnuriev
 * @since 04.08.2015.
 */
public class DocumentViewServlet extends HttpServlet {
    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String documentId = req.getParameter("documentId");
        if (StringUtils.isNotEmpty(documentId)) {
            NLPService nlpService = SpringHelper.getBean(NLPService.class);
            EntityService entityService = SpringHelper.getBean(EntityService.class);
            Document document = entityService.get(Document.class, Integer.valueOf(documentId));
            final String copyRootLocalPath = "copy";
            if (document.getCopyUri() == null) {
                if (document.getUri().getScheme().startsWith("http") && document.getMediaType().equals(MediaType.TEXT_HTML)) {
                    final String savePath = copyRootLocalPath + "/" + document.getUri().getHost() + "/" + document.getUri().getPath();
                    // try to copy content of page
                    final String processPath = "httrack";
                    ProcessBuilder ps = new ProcessBuilder(processPath, document.getUri().toString(), "-O", savePath, "--verbose", "--quiet", "--depth", "1");
                    ps.redirectErrorStream(true);
                    Process pr = ps.start();
                    try {
                        int res = pr.waitFor();
                        try {
                            pr.destroy();
                        } catch (Exception e) {
                            log.warn("", e);
                        }
                        if (res == 0) {
                            HttrackService httrackService = SpringHelper.getBean(HttrackService.class);
                            String pageFileLocalPath = httrackService.getPath(savePath);
                            if (StringUtils.isNotEmpty(pageFileLocalPath)) {
                                File copyFile = new File(pageFileLocalPath);
                                document.setCopyUri(copyFile.toURI());
                                entityService.save(document);
                            }
                        } else {
                            log.warn("can't copy page. httrack exit with code:" + res);
                        }
                    } catch (InterruptedException e) {
                        log.warn("", e);
                    }
                } else if (document.getMediaType().equals(MediaType.APPLICATION_PDF)) {
                    final String fileName = document.getUri().toString().substring(document.getUri().toString().lastIndexOf('/') + 1);
                    final String processPath = "pdftohtml";
                    final String savePath = copyRootLocalPath + "/" +
                            (StringUtils.isNotEmpty(document.getUri().getHost()) ?
                                    (document.getUri().getHost() + "/" + document.getUri().getPath()) :
                                    fileName
                            );
                    File saveDir = new File(savePath);
                    if (!saveDir.exists()) {
                        saveDir.mkdirs();
                    }
                    final String savedFileName = fileName + ".html";
                    String pageFileLocalPath = savePath + "/" + savedFileName;
                    String documentUri = document.getUri().toString();
                    String osName = System.getProperty("os.name");
                    String documentPath = null;
                    if (osName.toLowerCase().startsWith("windows")) {
                        documentPath = documentUri.substring("file:///".length());
                    } else {
                        documentPath = documentUri.substring("file://".length());
                    }
                    ProcessBuilder ps = new ProcessBuilder(processPath, "-noframes", documentPath, pageFileLocalPath);
//                    ps.directory(new File(savePath));   // explicitly executing in saved folder for correct path generation (e.g. for <img/> tag)
                    ps.redirectErrorStream(true);
                    Process pr = ps.start();
                    try {
                        int res = pr.waitFor();
                        try {
                            pr.destroy();
                        } catch (Exception e) {
                            log.warn("", e);
                        }
                        if (res == 0) {
                            // todo
                            if (StringUtils.isNotEmpty(pageFileLocalPath)) {
                                File copyFile = new File(pageFileLocalPath);
                                document.setCopyUri(copyFile.toURI());
                                entityService.save(document);
                                // correct img links - set to local (cause pdftohtml set to relative to directory specified)
                            }
                        } else {
                            log.warn("pdftohtml exit with code:" + res);
                        }
                    } catch (InterruptedException e) {
                        log.warn("", e);
                    }
                } else if (document.getMediaType().equals(MediaType.OOXML_DOCUMENT)) {
                    final String fileName = document.getUri().toString().substring(document.getUri().toString().lastIndexOf('/') + 1).replaceAll(" ", "_").replaceAll("%20", "_");
                    final String savePath = copyRootLocalPath + "/" +
                            (StringUtils.isNotEmpty(document.getUri().getHost()) ?
                                    (document.getUri().getHost() + "/" + document.getUri().getPath()) :
                                    fileName
                            );
                    File saveDir = new File(savePath);
                    if (!saveDir.exists()) {
                        saveDir.mkdirs();
                    }
                    final String savedFileName = fileName + ".html";
                    final String pageFileLocalPath = savePath + "/" + savedFileName;

                    DocumentService documentService = SpringHelper.getBean(DocumentService.class);
                    InputStream content = documentService.getContent(document.getUri(), InputStream.class);
                    try {
                        WordprocessingMLPackage wordMLPackage = Docx4J.load(content);
                        HTMLSettings htmlSettings = Docx4J.createHTMLSettings();
                        String imageDirPath = saveDir + "/" + fileName + "_files";
                        htmlSettings.setImageDirPath(imageDirPath);
                        Docx4jProperties.setProperty("docx4j.Convert.Out.HTML.OutputMethodXML", false);
                        htmlSettings.setImageTargetUri("../" + imageDirPath);
                        htmlSettings.setWmlPackage(wordMLPackage);
                        File copyFile = new File(pageFileLocalPath);
                        FileOutputStream fos = new FileOutputStream(copyFile);
                        Docx4J.toHTML(htmlSettings, fos, Docx4J.FLAG_NONE);
                        document.setCopyUri(copyFile.toURI());
                        entityService.save(document);
                    } catch (Exception e) {
                        log.error("", e);
                    }
                } else if (document.getMediaType().equals(MediaType.APPLICATION_MSWORD)) {
                    DocumentService documentService = SpringHelper.getBean(DocumentService.class);
                    InputStream content = documentService.getContent(document.getUri(), InputStream.class);
                    HWPFDocumentCore wordDocument = WordToHtmlUtils.loadDoc(content);
                    final String fileName = document.getUri().toString().substring(document.getUri().toString().lastIndexOf('/') + 1);
                    final String savePath = copyRootLocalPath + "/" +
                            (StringUtils.isNotEmpty(document.getUri().getHost()) ?
                                    (document.getUri().getHost() + "/" + document.getUri().getPath()) :
                                    fileName
                            );
                    File saveDir = new File(savePath);
                    if (!saveDir.exists()) {
                        saveDir.mkdirs();
                    }
                    final String savedFileName = fileName + ".html";
                    final String pageFileLocalPath = savePath + "/" + savedFileName;

                    try {
                        final org.w3c.dom.Document documentX = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
                        WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(
                                documentX) {
                            @Override
                            protected void processImageWithoutPicturesManager(Element currentBlock, boolean inlined, Picture picture) {
                                final String imgSavedPath = savePath + "/" + picture.suggestFullFileName();
                                try {
                                    FileOutputStream fos = new FileOutputStream(imgSavedPath);
                                    fos.write(picture.getContent());
                                    fos.flush();
                                    fos.close();
                                } catch (Exception e) {
                                    log.error("", e);
                                }
                                Element imgElement = documentX.createElement("img");
                                imgElement.setAttribute("src", "../" + imgSavedPath);
                                currentBlock.appendChild(imgElement);
                            }
                        };
                        wordToHtmlConverter.processDocument(wordDocument);
                        org.w3c.dom.Document htmlDocument = wordToHtmlConverter.getDocument();
                        DOMSource domSource = new DOMSource(htmlDocument);
                        File copyFile = new File(pageFileLocalPath);
                        Writer writer = new FileWriter(copyFile);
                        StreamResult streamResult = new StreamResult(writer);

                        TransformerFactory tf = TransformerFactory.newInstance();
                        Transformer serializer = tf.newTransformer();
                        serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                        serializer.setOutputProperty(OutputKeys.INDENT, "yes");
                        serializer.setOutputProperty(OutputKeys.METHOD, "html");
                        serializer.transform(domSource, streamResult);
                        writer.close();
                        document.setCopyUri(copyFile.toURI());
                        entityService.save(document);
                    } catch (Exception e) {
                        log.error("", e);
                    }
                }
            }

            boolean isSetBase = document.getMediaType().equals(MediaType.APPLICATION_PDF) && document.getCopyUri() != null; // need to set base url for pdf
            String lang = req.getParameter("lang");
            Object query = SessionDataHolder.getQueries(req.getRemoteAddr()).get(lang);
            Object highlightedContent = nlpService.highlight(document.getCopyUri() != null ? document.getCopyUri() : document.getUri(), query, lang, document.getCharset());
            if (highlightedContent == null || highlightedContent.toString().isEmpty() && document.getCacheUri() != null) {
                highlightedContent = nlpService.highlight(document.getCacheUri(), query, lang, document.getCharset());
            }
            if (highlightedContent != null) {
                if (!highlightedContent.toString().trim().contains("<body")) {
                    highlightedContent = "<html><body>" + highlightedContent + "</body></html>";
                }
                PrintWriter writer = null;
                String charset = document.getCharset() != null ? document.getCharset().name() : null;
                String contentTypeValue = null;
                if (charset == null) {
                    if (highlightedContent instanceof String) {
                        Elements httpEquivAttributes = Jsoup.parse((String) highlightedContent).head().getElementsByAttributeValue("http-equiv", "Content-Type");
                        for (org.jsoup.nodes.Element element : httpEquivAttributes) {
                            contentTypeValue = element.attr("content");
                        }
                    }
                }
                if (StringUtils.isEmpty(contentTypeValue)) {
                    contentTypeValue = org.springframework.http.MediaType.TEXT_HTML_VALUE + (charset != null ? "; charset=" + charset : "");
                }
                resp.addHeader("Content-Type", contentTypeValue);
                if (highlightedContent instanceof String && StringUtils.isNotEmpty(highlightedContent.toString())) {
                    writer = resp.getWriter();
                    if (isSetBase) {
                        HtmlEditService htmlEditService = SpringHelper.getBean(HtmlEditService.class);
                        // assumed, that web-server context configured appropriately (e.g. for tomcat: specify $TOMCAT_HOME/conf/Catalina/localhost/<copyRootLocalPath>.xml)
                        htmlEditService.insertIntoHead(highlightedContent,
                                String.format("<base href=\"%1$s://%2$s:%3$d/\" target=\"_blank\"/>",
                                        req.getScheme(),
                                        req.getServerName(),
                                        req.getServerPort()), writer);
                    } else {
                        writer.write((String) highlightedContent);
                        writer.flush();
                        writer.close();
                    }
                } else if (highlightedContent instanceof InputStream) {
                    log.warn("todo:implement InputStream");
                } else {
                    log.warn("todo:implement:" + highlightedContent.getClass().getCanonicalName());
                }
            }
        }
    }
}
