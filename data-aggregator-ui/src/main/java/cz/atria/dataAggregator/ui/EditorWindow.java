package cz.atria.dataAggregator.ui;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * @author rnuriev
 * @since 03.08.2015.
 */
@Deprecated
public class EditorWindow extends Window {
    Logger log = LoggerFactory.getLogger(getClass());
    FieldGroup binder;
    VerticalLayout main = new VerticalLayout();
    HorizontalLayout footerPanel = new HorizontalLayout();

    public interface SaveCallback {
        void save(PropertysetItem item);
    }

    public EditorWindow(FormLayout formLayout, final PropertysetItem item, final SaveCallback saveCallback) {
        binder = new FieldGroup(item);
        binder.bindMemberFields(formLayout);
//        main.setSizeFull();
        setContent(main);
        setWidth(30, Unit.PERCENTAGE);
        center();
        main.addComponent(formLayout);
        main.addComponent(footerPanel);
        Button saveButton = new Button("Save", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    binder.commit();
                    saveCallback.save(item);
                    close();
                } catch (FieldGroup.CommitException e) {
                    log.error("", e);
                }
            }
        });
        Button cancelButton = new Button("Cancel", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                binder.discard();
                close();
            }
        });
        footerPanel.addComponent(saveButton);
        footerPanel.addComponent(cancelButton);
        footerPanel.setMargin(true);
        footerPanel.setSpacing(true);
        formLayout.setSizeFull();
//        footerPanel.setSizeFull();
    }

    public static class EditorSavedEvent extends Component.Event {

        private Item savedItem;

        public EditorSavedEvent(Component source, Item savedItem) {
            super(source);
            this.savedItem = savedItem;
        }

        public Item getSavedItem() {
            return savedItem;
        }
    }

    public interface EditorSavedListener extends Serializable {
        public void editorSaved(EditorSavedEvent event);
    }
}
