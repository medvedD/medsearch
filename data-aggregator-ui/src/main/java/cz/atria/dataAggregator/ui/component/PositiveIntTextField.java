package cz.atria.dataAggregator.ui.component;

import org.apache.commons.lang.StringUtils;

import com.vaadin.event.FieldEvents;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.TextField;


/**
 * @author rnuriev
 * @since 01.12.2015.
 */
public class PositiveIntTextField extends TextField implements FieldEvents.TextChangeListener {
    String lastValue;

    public PositiveIntTextField() {
        setImmediate(true);
        setTextChangeEventMode(AbstractTextField.TextChangeEventMode.EAGER);
        addTextChangeListener(this);
    }

    @Override
    public void textChange(FieldEvents.TextChangeEvent event) {
        String text = event.getText();
        if (StringUtils.isEmpty(text)) {
            lastValue = text;
            return;
        }
        try {
            Integer integer = new Integer(text);
            if (integer < 0) {
                setValue(lastValue);
                return;
            }
            lastValue = text;
        } catch (NumberFormatException e) {
            setValue(lastValue);
        }
    }
}
