package cz.atria.dataAggregator.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.Container;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.JavaScriptFunction;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.TextField;

import cz.atria.dataAggregator.core.entity.Operator;
import cz.atria.dataAggregator.core.entity.TermElement;
import elemental.json.JsonArray;

/**
 * @author rnuriev
 * @since 14.08.2015.
 */
public class QueryTermGrid extends Table {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * container could any of type: Term or simple string
     */
    BeanContainer<Long, SelectedTermEntry> beanItemContainer = new BeanContainer<Long, SelectedTermEntry>(SelectedTermEntry.class);
    public static final String searchPhraseFieldNmae = "searchPhrase";
    public static final String deleteFieldName = "delete";
    public static final String editFieldName = "edit";
    public static final int OPERATOR_FIELD_WIDTH = 6;
    public static final String OPERATOR_COMBO_ID = "searchPanelOperatorComboId";
    boolean isOperatorComboIdSet = false;
    public QueryTermGrid() {
        setSortEnabled(false);
        beanItemContainer.setBeanIdProperty("id");
        setContainerDataSource(beanItemContainer);
// if it is grid
//        setEditorEnabled(true);
//        setColumns("operator", "searchPhrase", "isSynonym", "isChildren", "delete");
//        getColumn("delete").setRenderer(new HtmlRenderer());

// or as classic table
        setSelectable(true);
        setImmediate(true);
        setEditable(true);
        addGeneratedColumn(editFieldName, new ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                Label label = new Label(FontAwesome.EDIT.getHtml(), ContentMode.HTML);
                label.setCaptionAsHtml(true);
                return label;
            }
        });
        addGeneratedColumn(deleteFieldName, new ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                Label label = new Label(FontAwesome.TRASH_O.getHtml(), ContentMode.HTML);
                label.setCaptionAsHtml(true);
                return label;
            }
        });
        setVisibleColumns("operator", "searchPhrase", editFieldName, deleteFieldName);
        final TableFieldFactory tableFieldFactory = getTableFieldFactory();
        setTableFieldFactory(new TableFieldFactory() {
            @Override
            public Field<?> createField(Container container, Object itemId, Object propertyId, Component uiContext) {
                Field field = null;
                if (propertyId.equals("operator")) {
                    if (container.getItemIds().size() > 1) {
                        int itemIndex = indexOfId(itemId);
                        if (itemIndex > 0) {
                            field = new ComboBox(null, Arrays.asList(Operator.values()));
                            ((ComboBox) field).setNullSelectionAllowed(false);
                            field.setWidth(OPERATOR_FIELD_WIDTH, Unit.EM);
                            if (!isOperatorComboIdSet) {
                                field.setId(OPERATOR_COMBO_ID);
                                isOperatorComboIdSet = true;
                            }
                            if (getColumnWidth("operator") < 0) {
                                executeGetOperatorComboWidthFor(OPERATOR_COMBO_ID);
                            }
                        } else {
                            field = null;
                        }
                    }
                } else {
                    field = tableFieldFactory.createField(container, itemId, propertyId, uiContext);
                    if (field instanceof TextField) {
                        field.setWidth(100, Unit.PERCENTAGE);
                        field.setEnabled(false);
                    }
                    if (field instanceof CheckBox) {
                        field.setCaption("");
                    }
                }
                return field;
            }
        });
        setColumnHeader("operator", "Operator");
        setColumnHeader(searchPhraseFieldNmae, "Search phrase");
        setColumnHeader(editFieldName, "");
        setColumnHeader(deleteFieldName, "");

//        setColumnExpandRatio("operator", 0.14f);
        setColumnExpandRatio(searchPhraseFieldNmae, 1.f);
        setColumnWidth(editFieldName, (int) MainView.defaultControlWidth);
        setColumnWidth(deleteFieldName, (int) MainView.defaultControlWidth);
        JavaScript.getCurrent().addFunction("GetOperatorComboWidth",
                new JavaScriptFunction() {
                    @Override
                    public void call(JsonArray jsonArray) {
                        try {
                            final double width = jsonArray.getNumber(0);
                            setColumnWidth("operator", (int) width + 10);
                        } catch (Exception e) {
                            log.error("", e);
                        }
                    }
                });
    }

    protected void executeGetOperatorComboWidthFor(String id) {
        JavaScript.getCurrent().execute("GetOperatorComboWidth(document.getElementById('" + id + "').clientWidth);");
    }

    public SelectedTermEntry getBean(Object itemId) {
        Container container = getContainerDataSource();
        if (container instanceof BeanItemContainer) {
            return (SelectedTermEntry) itemId;
        } else if (container instanceof BeanContainer) {
            return (SelectedTermEntry) ((BeanContainer) container).getItem(itemId).getBean();
        } else {
            return null;
        }
    }

    public void addTermElement(TermElement termElement) {
        SelectedTermEntry selectedTermEntry = new SelectedTermEntry(termElement);
        addTermElement(selectedTermEntry);
    }

    public void addTermElement(SelectedTermEntry selectedTermEntry) {
        selectedTermEntry.setIsFirst(beanItemContainer.getItemIds().size() == 0);
        beanItemContainer.addItem(selectedTermEntry.getId(), selectedTermEntry);
        if (!selectedTermEntry.isFirst() && selectedTermEntry.getOperator() == null) {
            selectedTermEntry.setOperator(Operator.AND);
        }
    }


    public Collection<SelectedTermEntry> getBeans() {
        Container container = getContainerDataSource();
        Collection<SelectedTermEntry> res = null;
        if (container instanceof BeanItemContainer) {
            res = (Collection<SelectedTermEntry>) container.getItemIds();
        } else if (container instanceof BeanContainer) {
            Collection itemIds = container.getItemIds();
            res = new ArrayList<>();
            for (Object id : itemIds) {
                res.add((SelectedTermEntry) ((BeanContainer) container).getItem(id).getBean());
            }
        }
        return res;
    }

    public void addSelectedTermEntry(SelectedTermEntry selectedTermEntry) {
        beanItemContainer.addItem(selectedTermEntry.getId(), selectedTermEntry);
    }
}
