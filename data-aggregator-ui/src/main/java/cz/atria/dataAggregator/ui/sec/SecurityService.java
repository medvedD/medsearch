package cz.atria.dataAggregator.ui.sec;

/**
 * @author rnuriev
 * @since 27.08.2015.
 */
public interface SecurityService {
    boolean isGranted(String priv);
    boolean isGranted(String parentPriv, String priv);
    /**is any of children privileges granted*/
    boolean isAnyOfGranted(String priv);
}
