package cz.atria.dataAggregator.ui;

import java.net.URI;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.vaadin.data.util.converter.Converter;

/**
 * @author rnuriev
 * @since 01.12.2015.
 */
class StringURIWithSpaceConverter implements Converter<String, URI> {
    @Override
    public URI convertToModel(String value, Class<? extends URI> targetType, Locale locale) throws ConversionException {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return URI.create(value);
    }

    @Override
    public String convertToPresentation(URI value, Class<? extends String> targetType, Locale locale) throws ConversionException {
        if (value == null) {
            return null;
        }
        return value.toString().replaceAll("%20", " ");
    }

    @Override
    public Class<URI> getModelType() {
        return URI.class;
    }

    @Override
    public Class<String> getPresentationType() {
        return String.class;
    }
}
