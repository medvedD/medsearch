package cz.atria.dataAggregator.ui;

import java.util.HashMap;
import java.util.Map;

/**todo: resolve session variables holding by more appropriate means
 * @author rnuriev
 * @since 04.08.2015.
 * @deprecated
 */
@Deprecated
public class SessionDataHolder {
    /**
     * todo: use session scope variable to store queries
     */
    @Deprecated
    public static Map<String, Object> currentQuery = new HashMap<>();

    public static Map<String, Map<String, Object>> currentQueries = new HashMap<>();

    public static Object getQuery(String clientAddress) {
        return currentQuery.get(clientAddress);
    }

    public static void setQueries(String clientAddress, Map<String, Object> o) {
        currentQueries.put(clientAddress, o);
    }
    public static Map<String, Object> getQueries(String clientAddress) {
        return currentQueries.get(clientAddress);
    }

    public static void setQuery(String clientAddress, Object o) {
        currentQuery.put(clientAddress, o);
    }
}
