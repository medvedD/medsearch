package cz.atria.dataAggregator.ui;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.util.converter.Converter;

import cz.atria.dataAggregator.core.entity.URIAttributeConverter;

/**
 * @author rnuriev
 * @since 03.08.2015.
 */
public class URIConverter implements Converter<String, URI> {
    static final Logger log = LoggerFactory.getLogger(URIConverter.class);
    @Override
    public URI convertToModel(String value, Class<? extends URI> targetType, Locale locale) throws ConversionException {
        if (value == null || value.isEmpty()) {
            return null;
        } else {
            try {
                return URI.create(URLEncoder.encode(value, URIAttributeConverter.defaultCharset));
            } catch (UnsupportedEncodingException e) {
                log.warn("", e);
                return URI.create(value);
            }
        }
    }

    @Override
    public String convertToPresentation(URI value, Class<? extends String> targetType, Locale locale) throws ConversionException {
        if (value == null) {
            return null;
        } else {
            try {
                return URLDecoder.decode(value.toString(), URIAttributeConverter.defaultCharset);
            } catch (UnsupportedEncodingException e) {
                log.warn("", e);
                return value.toString();
            }
        }
    }

    @Override
    public Class<URI> getModelType() {
        return URI.class;
    }

    @Override
    public Class<String> getPresentationType() {
        return String.class;
    }
}
