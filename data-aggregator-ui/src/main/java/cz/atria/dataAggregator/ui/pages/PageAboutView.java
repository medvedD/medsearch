package cz.atria.dataAggregator.ui.pages;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class PageAboutView extends HorizontalLayout implements View {
    public static final String VIEW_NAME = "About";

    public PageAboutView() {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/pageAboutView.html"), this);
    }
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
