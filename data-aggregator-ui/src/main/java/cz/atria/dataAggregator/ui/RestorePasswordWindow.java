package cz.atria.dataAggregator.ui;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.AbstractStringValidator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;

import cz.atria.dataAggregator.core.service.SmtpService;

/**
 * @author rnuriev
 * @since 28.08.2015.
 */
@DesignRoot
public class RestorePasswordWindow extends Window {
    TextField emailField;
    Button sendRestorePasswordButton;

    public RestorePasswordWindow() {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/" + getClass().getSimpleName() + ".html"), this);
        center();
//        setModal(true);
        emailField.addValidator(new EmailValidator("Email is incorrect."));
        final EntityManager em = SpringHelper.getBean(EntityManager.class);
        emailField.addValidator(new AbstractStringValidator("Email is not recognized.") {
            @Override
            protected boolean isValidValue(String value) {
                try {
                    em.createQuery("select 1 from User where lower(email) = lower(:email)").setParameter("email", value).getSingleResult();
                    return true;
                } catch (NoResultException e) {
                    return false;
                }
            }
        });
        emailField.setValidationVisible(false);
        sendRestorePasswordButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    emailField.validate();
                } catch (Validator.InvalidValueException e) {
                    emailField.setValidationVisible(true);
                    return;
                }
                SmtpService smtpService = SpringHelper.getBean(SmtpService.class);
                String password = em.createQuery("select password from User where lower(email) = lower(:email)", String.class).setParameter("email", emailField.getValue()).getSingleResult();
                try {
                    smtpService.send(emailField.getValue(), "Your password: " + password);
                    close();
                    Notification.show("Password successfully sent.", Notification.Type.HUMANIZED_MESSAGE);
                } catch (Exception e) {
                    NotifyWindow notifyWindow = new NotifyWindow(null, new VerticalLayout(), "Failed to send recovery message:\n" + e.getMessage(), ExceptionUtils.getStackTrace(e));
                    UI.getCurrent().addWindow(notifyWindow);
                }
            }
        });
    }
}
