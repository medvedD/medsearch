package cz.atria.dataAggregator.ui.component;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.vaadin.data.Property;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

import cz.atria.dataAggregator.core.Common;
import cz.atria.dataAggregator.core.SettingService;
import cz.atria.dataAggregator.core.Settings;
import cz.atria.dataAggregator.core.exc.DataAggregatorException;
import cz.atria.dataAggregator.core.exc.Reason;
import cz.atria.dataAggregator.core.service.MediaTypeService;
import cz.atria.dataAggregator.ui.SpringHelper;
import cz.atria.dataAggregator.ui.URIConverter;
import pl.exsio.plupload.Plupload;
import pl.exsio.plupload.PluploadFile;

/**
 * @author rnuriev
 * @since 08.09.2015.
 */
public class UriFieldWithFileOperations extends HorizontalLayout {
    final Logger log = LoggerFactory.getLogger(getClass());
    TextField nameField;
	final TextField linkField = new TextField();
    final Plupload openFileButton = new Plupload(null, FontAwesome.FOLDER_OPEN);
    final Button deleteFileButton = new Button(FontAwesome.TRASH_O.getHtml());
    private static final Map<String, MediaType> SUPPORTED_MEDIA_TYPES = new HashMap<>();
    
    static {
        SUPPORTED_MEDIA_TYPES.put("pdf", cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_PDF);
        SUPPORTED_MEDIA_TYPES.put("doc", cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_MSWORD);
        SUPPORTED_MEDIA_TYPES.put("docx", cz.atria.dataAggregator.core.entity.MediaType.OOXML_DOCUMENT);
        SUPPORTED_MEDIA_TYPES.put("html", cz.atria.dataAggregator.core.entity.MediaType.TEXT_HTML);
        SUPPORTED_MEDIA_TYPES.put("htm", cz.atria.dataAggregator.core.entity.MediaType.TEXT_HTML);
        SUPPORTED_MEDIA_TYPES.put("txt", cz.atria.dataAggregator.core.entity.MediaType.TEXT_PLAIN);
        SUPPORTED_MEDIA_TYPES.put("rtf", cz.atria.dataAggregator.core.entity.MediaType.APPLICATION_RTF);
    }

    public UriFieldWithFileOperations() {
        super();
        addComponent(linkField);
        addComponent(openFileButton);
        addComponent(deleteFileButton);
        linkField.setNullRepresentation("");
        setSpacing(false);
        linkField.setWidth(100, Unit.PERCENTAGE);
        openFileButton.setWidth(100, Unit.PERCENTAGE);
        deleteFileButton.setWidth(100, Unit.PERCENTAGE);
        setExpandRatio(linkField, 0.8f);
        setExpandRatio(openFileButton, 0.1f);
        setExpandRatio(deleteFileButton, 0.1f);
        openFileButton.setCaptionAsHtml(true);
        deleteFileButton.setCaptionAsHtml(true);
        linkField.setConverter(new URIConverter());
        openFileButton.setMultiSelection(true);

        openFileButton.addFilesAddedListener(new Plupload.FilesAddedListener() {
            @Override
            public void onFilesAdded(PluploadFile[] files) {
                openFileButton.start();
            }
        });

        openFileButton.addFileUploadedListener(new Plupload.FileUploadedListener() {
            @Override
            public void onFileUploaded(PluploadFile pluploadFile) {
                MediaTypeService mediaTypeService = SpringHelper.getBean(MediaTypeService.class);
                org.springframework.http.MediaType mediaType = mediaTypeService.detectMediaType(pluploadFile.getUploadedFileAs(File.class).toPath().toUri(), MediaType.class);
                if (mediaType != null) {
                    boolean isFound = false;
                    for (MediaType mediaTypeSupported : SUPPORTED_MEDIA_TYPES.values()) {
                        if (isFound = mediaTypeSupported.getType().equals(mediaType.getType()) && mediaTypeSupported.getSubtype().equals(mediaType.getSubtype())) {
                            break;
                        }
                    }
                    if (!isFound) {
                        throw new DataAggregatorException(Reason.UNSUPPORTED_MEDIA_TYPE, "It is possible to use files of types: " + StringUtils.join(SUPPORTED_MEDIA_TYPES.keySet(), ',') + "\nActual:" + mediaType.getSubtype());
                    }
                }

                String uploadDirName = SettingService.getSetting(Settings.UPLOAD_DIR_PROP, String.class, Settings.DEFAULT_UPLOAD_DIR);
                File uploadDir = new File(uploadDirName);
                if (!uploadDir.exists()) {
                    uploadDir.mkdirs();
                }
                try {
                    File uploadedFile = new File(uploadDir, pluploadFile.getName());
                    Path uploadedPath = Files.move(pluploadFile.getUploadedFileAs(File.class).toPath(), uploadedFile.toPath());
                    linkField.setValue(URLDecoder.decode(uploadedPath.toUri().toString(), "UTF-8"));
                    nameField.setValue(pluploadFile.getName());
                } catch (IOException e) {
                    log.error("", e);
                    throw new DataAggregatorException(Reason.UPLOAD_FILE_ERROR, (e instanceof FileAlreadyExistsException ? "File already exists." : null));
                }
            }
        });
        linkField.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                URI uri = null;
                if (StringUtils.isNotEmpty(linkField.getValue())) {
                    try {
                        uri = new URI(linkField.getValue().replaceAll(" ", "%20"));
                    } catch (URISyntaxException e) {
                        log.warn("", e);
                    }
                }
                deleteFileButton.setEnabled(StringUtils.isNotEmpty(linkField.getValue()) && (uri != null && uri.getScheme() != null && uri.getScheme().equals("file")));
            }
        });
        deleteFileButton.setEnabled(StringUtils.isNotEmpty(linkField.getValue()));
        deleteFileButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    if (StringUtils.isNotEmpty(linkField.getValue())) {
                        File file = new File(URI.create(Common.encodeSpace(linkField.getValue())));
                        file.delete();
                    }
                    linkField.setValue(null);
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        });
    }

    public TextField getLinkField() {
        return linkField;
    }
    
    public void setNameField(TextField nameField) {
		this.nameField = nameField;
	}
}
