package cz.atria.dataAggregator.ui.pages;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.declarative.Design;

public class PageTermsView extends HorizontalLayout implements View {
    public static final String VIEW_NAME = "Terms";

    public PageTermsView() {
        Design.read(getClass().getClassLoader().getResourceAsStream("design/pageTermsView.html"), this);
    }
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
