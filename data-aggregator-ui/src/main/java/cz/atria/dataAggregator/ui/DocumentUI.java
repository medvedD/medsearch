package cz.atria.dataAggregator.ui;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

/**
 * @author rnuriev
 * @since 01.08.2015.
 */
public class DocumentUI extends UI {
    @Override
    protected void init(VaadinRequest request) {
        setContent(new DocumentView(request.getParameter("documentId")));
    }
}
