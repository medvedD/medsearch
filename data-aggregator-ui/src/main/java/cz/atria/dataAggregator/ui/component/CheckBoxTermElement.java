package cz.atria.dataAggregator.ui.component;

import com.vaadin.ui.CheckBox;

import cz.atria.dataAggregator.core.entity.TermElement;

/**
 * @author rnuriev
 * @since 10.09.2015.
 */
public class CheckBoxTermElement extends CheckBox {
    TermElement termElement;

    public CheckBoxTermElement(TermElement termElement) {
        this.termElement = termElement;
    }

    public TermElement getTermElement() {
        return termElement;
    }
}
