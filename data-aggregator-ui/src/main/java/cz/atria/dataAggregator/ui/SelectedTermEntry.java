package cz.atria.dataAggregator.ui;

import com.vaadin.server.FontAwesome;

import cz.atria.dataAggregator.core.SearchTermEntry;
import cz.atria.dataAggregator.core.entity.Operator;
import cz.atria.dataAggregator.core.entity.TermElement;

/**
 * @author rnuriev
 * @since 14.08.2015.
 * @deprecated todo: use QueryTermEntry directly
 *
 */
@Deprecated
public class SelectedTermEntry extends SearchTermEntry {
    static int i = 0;
    long id = System.currentTimeMillis() + (++i);
    boolean isFirst = false;

    public boolean isFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
        if (isFirst) {
            setOperator(null);
        }
    }

    public SelectedTermEntry() {
    }

    public SelectedTermEntry(Operator operator, String searchPhrase, TermElement termElement, Boolean isSynonym, Boolean isChildren) {
        super(operator, searchPhrase, termElement, isSynonym, isChildren);
    }

    public SelectedTermEntry(TermElement termElement) {
        super(termElement);
    }

    /**
     * this is icon only
     */
    public String getDelete() {
        return FontAwesome.TRASH_O.getHtml();
    }

    public void setDelete(String delete) {
    }

    public String getEdit() {
        return FontAwesome.EDIT.getHtml();
    }

    public void setEdit(String edit) {

    }

    /**this is fake identifier for item identification only*/
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
