package cz.atria.dataAggregator.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang.StringUtils;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.data.Property;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.declarative.Design;

import cz.atria.dataAggregator.core.entity.Term;
import cz.atria.dataAggregator.core.entity.Vocabulary;

/**
 * @author rnuriev
 * @since 31.07.2015.
 */
public class TermsWindow extends Window {
    Grid termsTable;
    JPAContainer<Term> termContainer;

    public interface TermSelectedCallback {
        void process(Term term);
    }

    TermSelectedCallback termSelectedCallback;

    public TermsWindow(final TermSelectedCallback termSelectedCallback) {
        this.termSelectedCallback = termSelectedCallback;
        Design.read(getClass().getClassLoader().getResourceAsStream("design/terms-window.html"), this);
        termContainer = JPAContainerFactory.make(Term.class, SpringHelper.getBean(EntityManagerFactory.class).createEntityManager());
        termsTable.setContainerDataSource(termContainer);
        String vocabularyColumnName = "vocabularyName";
        termsTable.setColumns(vocabularyColumnName, "name");
        termsTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    Term term = (Term) ((JPAContainerItem) event.getItem()).getEntity();
                    termSelectedCallback.process(term);
                }
            }
        });

        // Create a header row to hold column filters
        final Grid.HeaderRow filterRow = termsTable.appendHeaderRow();
        for (final Object pid : termsTable.getContainerDataSource().getContainerPropertyIds()) {
            if (termsTable.getColumn(pid) == null) {
                continue;
            }
            final Grid.HeaderCell cell = filterRow.getCell(pid);
            Field field;
            if (pid.toString().equals(vocabularyColumnName)) {
                field = new ComboBox();
                ComboBox cbx = (ComboBox) field;
                JPAContainer<Vocabulary> vocabularyContainer = JPAContainerFactory.make(Vocabulary.class, SpringHelper.getBean(EntityManager.class));
                cbx.setContainerDataSource(vocabularyContainer);
                cbx.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
                cbx.setItemCaptionPropertyId("name");
                cbx.addValueChangeListener(new Property.ValueChangeListener() {
                    @Override
                    public void valueChange(Property.ValueChangeEvent event) {
                        filter(filterRow, event);
                    }
                });
            } else {
                field = new TextField();
                TextField filterField = (TextField) field;
                filterField.addTextChangeListener(new FieldEvents.TextChangeListener() {
                    @Override
                    public void textChange(FieldEvents.TextChangeEvent event) {
                        filter(filterRow, event);
                    }
                });
            }
            field.setWidth(100, Unit.PERCENTAGE);
            field.setHeight(85, Unit.PERCENTAGE);
            cell.setComponent(field);
        }
    }

    protected void filter(Grid.HeaderRow filterRow, Object event) {
        termContainer.removeAllContainerFilters();
        for (final Object pid : termsTable.getContainerDataSource().getContainerPropertyIds()) {
            if (termsTable.getColumn(pid) == null) {
                continue;
            }
            final Grid.HeaderCell cell = filterRow.getCell(pid);
            if (cell.getComponent() != null && cell.getComponent() instanceof Field && event != null) {
                Object val = null;
                if (event instanceof FieldEvents.TextChangeEvent) {
                    val = ((FieldEvents.TextChangeEvent) event).getText();
                } else if (event instanceof Property.ValueChangeEvent) {
                    val = ((Property.ValueChangeEvent)event).getProperty().getValue();
                }
                if (val instanceof Integer) {
                    termContainer.addContainerFilter(new Compare.Equal("vocabulary.id", val));
                } else if (pid.toString().equals("name") && val instanceof String && StringUtils.isNotEmpty(val.toString())) {
                    termContainer.addContainerFilter(
                            new SimpleStringFilter(pid,
                                    val.toString(), true, false));
                }
            }
        }
    }
}
