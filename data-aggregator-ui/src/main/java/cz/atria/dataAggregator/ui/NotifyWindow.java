package cz.atria.dataAggregator.ui;

import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * @author rnuriev
 * @since 28.08.2015.
 */
public class NotifyWindow extends Window {
    boolean isPopupVisible = false;

    public NotifyWindow(String caption, Component content, String message, final String detail) {
        super(caption, content);
        int browserWindowWidth = Page.getCurrent().getBrowserWindowWidth();
        int browserWindowHeight = Page.getCurrent().getBrowserWindowHeight();
        setWidth(browserWindowWidth / 4, Unit.PIXELS);
        setPositionX((int) (browserWindowWidth - getWidth()));
        setResizable(false);
        VerticalLayout c = new VerticalLayout();
        c.addComponent(new Label("Error!"));
        c.addComponent(new Label("Please contact the administrator."));
        final TextArea detailContent = new TextArea(null, detail);
        detailContent.setWidth(browserWindowWidth / 3, Unit.PIXELS);
        detailContent.setHeight(browserWindowHeight / 2, Unit.PIXELS);
        Button selectAllButton = new Button("Select all", new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                detailContent.selectAll();
            }
        });
        VerticalLayout detailPanel = new VerticalLayout(detailContent, selectAllButton);
        detailPanel.setSpacing(true);
        detailPanel.setMargin(new MarginInfo(true, true, true, true));
        PopupView popupView = new PopupView("Detail", detailPanel);
        popupView.setCaptionAsHtml(true);
        popupView.setSizeUndefined();
        popupView.addPopupVisibilityListener(new PopupView.PopupVisibilityListener() {
            @Override
            public void popupVisibilityChange(PopupView.PopupVisibilityEvent event) {
                isPopupVisible = event.isPopupVisible();
            }
        });
        c.addComponent(popupView);
        ((ComponentContainer) content).addComponent(c);
        ((Layout.MarginHandler) content).setMargin(true);
        ((Layout.SpacingHandler) content).setSpacing(true);
        setImmediate(true);
        new Thread(new NotificationRemovalThread(this)).start();
    }

    public class NotificationRemovalThread implements Runnable {
        Window window;

        public NotificationRemovalThread(Window window) {
            this.window = window;
        }

        @Override
        public void run() {
            try {
                do {
                    Thread.sleep(10000);
                } while (isPopupVisible);
                window.close();
                UI.getCurrent().removeWindow(window);
                UI.getCurrent().push();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
