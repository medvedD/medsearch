package cz.atria.dataAggregator.loader;

/**
 * @author rnuriev
 * @since 13.08.2015.
 */
public interface VocabularyLoader {
    void load();
}
