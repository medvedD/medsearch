package cz.atria.dataAggregator.loader.icd.icd10;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cz.atria.dataAggregator.core.entity.TermElement;
import cz.atria.dataAggregator.core.service.EntityService;
import cz.atria.dataAggregator.loader.VocabularyLoader;

/**
 * @author rnuriev
 * @since 13.08.2015.
 */
public class VocabularyLoaderICD10Impl implements VocabularyLoader {
    final Logger log = LoggerFactory.getLogger(getClass());
    final CSVFormat CSV_FORMAT = CSVFormat.newFormat(';');
    public static final String VOCABULARY_CODE = "ICD10";
    public static final String VOCABULARY_NAME = "ICD-10";

    protected CSVParser getCsvRecords(String fileName) {
        try {
            return new CSVParser(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(fileName)), CSV_FORMAT);
        } catch (Exception e) {
            log.error("", e);
            return null;
        }
    }

    protected Integer convertToInt(String code) {
        final String firstLetter = code.substring(0, 1);
        final byte firstLetterAsInt = firstLetter.getBytes()[0];
        return Integer.valueOf("" + firstLetterAsInt + code.substring(1));
    }

    @Override
    public void load() {
        log.info("load:begin");
        try {
            ApplicationContext ctx = new ClassPathXmlApplicationContext("cz/atria/dataAggregator/core/data-aggregator-core-context.xml", "cz/atria/dataAggregator/core/data-aggregator-em-context.xml");
            EntityService entityService = ctx.getBean(EntityService.class);
            EntityManager em = ctx.getBean(EntityManager.class);
            TermElement termElementRoot = new TermElement(VOCABULARY_CODE, VOCABULARY_NAME);
            entityService.persist(termElementRoot);
            CSVParser chapters = getCsvRecords("chapters.txt");
            for (CSVRecord csvRecord : chapters) {
                TermElement termElementChapter = new TermElement(termElementRoot, csvRecord.get(0), csvRecord.get(1));
                entityService.persist(termElementChapter);
            }
            final CSVParser blocks = getCsvRecords("blocks.txt");
            final Map<Integer, Integer> blockInterval = new HashMap<>();
            final Map<Integer, TermElement> termElementBlocks = new HashMap<>();
            for (CSVRecord blockRecord : blocks) {
                TermElement parent = em.createQuery("from TermElement where parent = :root and code = :code", TermElement.class)
                        .setParameter("root", termElementRoot)
                        .setParameter("code", blockRecord.get(2))
                        .getSingleResult();
                TermElement termElementBlock = new TermElement(parent, blockRecord.get(0) + "-" + blockRecord.get(1), blockRecord.get(3));
                entityService.persist(termElementBlock);
                Integer begin = convertToInt(blockRecord.get(0));
                blockInterval.put(begin, convertToInt(blockRecord.get(1)));
                termElementBlocks.put(begin, termElementBlock);
            }
            final CSVParser codes = getCsvRecords("codes.txt");
            TermElement currentParent = null;
            for (CSVRecord codeRecord : codes) {
                String code = codeRecord.get(5);
                TermElement parent = null;
                if (code.endsWith("-")) {
                    Map.Entry<Integer, Integer> parentBlockEntry = null;
                    for (Map.Entry<Integer, Integer> blockEntry : blockInterval.entrySet()) {
                        int current = convertToInt(code.substring(0, code.lastIndexOf(".")));
                        if (current >= blockEntry.getKey() && current <= blockEntry.getValue()) {
                            parentBlockEntry = blockEntry;
                        }
                    }
                    parent = termElementBlocks.get(parentBlockEntry.getKey());
                }
                TermElement termElementCode = new TermElement(parent != null ? parent : currentParent, code, codeRecord.get(8));
                entityService.persist(termElementCode);
                if (code.endsWith("-")) {
                    currentParent = termElementCode;
                }
            }
        } finally {
            log.info("load.complete");
        }
    }

    public static void main(String[] args) {
        new VocabularyLoaderICD10Impl().load();
    }
}
